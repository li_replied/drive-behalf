package com.pearadmin.common.config.proprety;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Describe: 接 口 文 档 配 置 类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Data
@ConfigurationProperties("pear.swagger")
public class SwaggerProperty {

    /** 是 否 开 启 */
    private Boolean enable = true;

    /** 系 统 标 题 */
    private String title;

    /** 描 述 信 息 */
    private String describe;

    /** 分 组 名 称 */
    private String groupName;

    /** 扫 描 路 径 */
    private String scanPackage;

    /** 版 本 信 息 */
    private String version;

}