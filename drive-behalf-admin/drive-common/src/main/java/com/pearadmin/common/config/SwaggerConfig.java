package com.pearadmin.common.config;

import com.pearadmin.common.config.proprety.SwaggerProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * Describe: 接 口 文 档 配 置 文 件
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Slf4j
@Configuration
@EnableSwagger2
@EnableConfigurationProperties(SwaggerProperty.class)
public class SwaggerConfig {

    @Resource
    private SwaggerProperty documentAutoProperties;

    @Bean
    public Docket docker(){
        return new Docket(DocumentationType.SWAGGER_2)
                // 是否启用Swagger
                .enable(documentAutoProperties.getEnable())
                // 用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）
                .groupName(documentAutoProperties.getGroupName())
                .apiInfo(apiInfo())
                // 设置哪些接口暴露给Swagger展示
                .select()
                // 扫描所有有注解的api，用这种方式更灵活
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // 扫描指定包中的swagger注解
                .apis(RequestHandlerSelectors.basePackage(documentAutoProperties.getScanPackage()))
                // 扫描所有.apis(RequestHandlerSelectors.any())
                //.paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo(){
        return new ApiInfo(
                documentAutoProperties.getTitle(),
                documentAutoProperties.getDescribe(),
                documentAutoProperties.getVersion(),
                null,null,null,null,
                new ArrayList<>()
        );
    }
}