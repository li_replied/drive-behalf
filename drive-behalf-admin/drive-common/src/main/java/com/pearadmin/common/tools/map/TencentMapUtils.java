package com.pearadmin.common.tools.map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pearadmin.common.tools.http.HttpUtils;
import com.pearadmin.common.tools.text.StringUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author: admin
 * @Date: 2021/2/19 14:58
 * @Description: 腾讯地图位置地址解析工具类
 */
@Slf4j
public class TencentMapUtils {

    //地址解析请求URL
    private static final String geocoding_url = "https://apis.map.qq.com/ws/geocoder/v1/";

    //行政区划搜索请求URL
    private static final String district_search_url = "https://apis.map.qq.com/ws/district/v1/search";

    /**
     * 通过经纬度坐标获取地址（坐标转地址）
     *
     * @param location
     * @return
     */
    public static String getAddressByLocation(String location, String key) {
        try {
            String param = "location=" + location + "&key=" + key + "&get_poi=1";
            String rspStr = HttpUtils.sendGet(geocoding_url, param);
            if (StringUtils.isEmpty(rspStr)) {
                log.debug("获取地址异常 {}", location);
                return null;
            }
            JSONObject resp = JSONObject.parseObject(rspStr);
            log.info("经纬度坐标获取地址接口返回：" + resp.toJSONString());
            if ("0".equals(resp.getString("status")) && resp.getJSONObject("result") != null) {
                JSONObject result = resp.getJSONObject("result");
                String address = result.getString("address");
                if (address != null) {
                    return address;
                }
            }
        } catch (Exception e) {
            log.error("获取地址异常 {}", e);
        }
        return null;
    }

    /**
     * 获取经纬度坐标信息（地址转坐标）
     *
     * @param address
     * @return
     */
    public static String getLocationByAddress(String address, String key) {
        try {
            String param = "address=" + address + "&key=" + key;
            String rspStr = HttpUtils.sendGet(geocoding_url, param);
            if (StringUtils.isEmpty(rspStr)) {
                log.debug("获取经纬度坐标异常 {}", address);
                return null;
            }
            JSONObject resp = JSONObject.parseObject(rspStr);
            log.info("获取经纬度坐标信息接口返回：" + resp.toJSONString());
            if ("0".equals(resp.getString("status")) && resp.getJSONObject("result") != null) {
                JSONObject result = resp.getJSONObject("result");
                JSONObject location = result.getJSONObject("location");
                String lat = location.getString("lat");
                String lng = location.getString("lng");
                return new StringBuilder().append(lat).append(",").append(lng).toString();
            }
        } catch (Exception e) {
            log.error("获取经纬度坐标异常 {}", e);
        }
        return null;
    }

    /**
     * 获取行政区中心点
     *
     * @param keyword 行政区划关键字
     * @return
     */
    public static String getDistrictLocationByKeyword(String keyword, String key) {
        try {
            String param = "keyword=" + keyword + "&key=" + key;
            String rspStr = HttpUtils.sendGet(district_search_url, param);
            if (StringUtils.isEmpty(rspStr)) {
                log.debug("获取行政区划信息异常 {}", keyword);
                return null;
            }
            JSONObject resp = JSONObject.parseObject(rspStr);
            log.info("获取行政区划信息接口返回：" + resp.toJSONString());
            if ("0".equals(resp.getString("status")) && resp.getJSONArray("result").size() > 0) {
                JSONArray result = resp.getJSONArray("result");
                JSONObject object = result.getJSONArray(0).getJSONObject(0);
                JSONObject location = object.getJSONObject("location");
                return location.toString();
            }
        } catch (Exception e) {
            log.error("获取行政区划信息异常 {}", e);
        }
        return null;
    }
}
