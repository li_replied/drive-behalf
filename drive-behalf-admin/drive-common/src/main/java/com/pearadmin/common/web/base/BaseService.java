package com.pearadmin.common.web.base;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import java.util.List;

/**
 * 通用Service接口
 */
public interface BaseService<T> {

    T getById(Long key);

    List<T> list(T entity);

    PageInfo<T> listByPage(T entity, PageDomain pageDomain);

    int save(T entity);

    int update(T entity);

    int remove(Long key);

    int batchRemove(String[] ids);
}
