package com.pearadmin.common.constant;

/**
 * Describe: 接 口 静 态 常 量
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
public class ControllerConstant {

    /**
     * 系统业务基础路径
     * */
    public final static String API_SYSTEM_PREFIX = "/system/";

    /**
     * 代码生成基础路径
     * */
    public final static String API_GENERATOR_PREFIX = "/generator/";

    /**
     * 定时任务基础路径
     * */
    public final static String API_SCHEDULE_PREFIX = "/schedule/";

    /**
     * 代驾业务基础路径
     * */
    public final static String API_DRIVE_PREFIX = "/drive/";
}
