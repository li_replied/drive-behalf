package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.RechargeConfig;
import com.pearadmin.drive.service.IRechargeConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 充值金额配置Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "config")
public class RechargeConfigController extends BaseController
{
    private String prefix = "drive/config";

    @Autowired
    private IRechargeConfigService rechargeConfigService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/config/main','drive:config:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询充值金额配置列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/config/data','drive:config:data')")
    public ResultTable list(@ModelAttribute RechargeConfig rechargeConfig, PageDomain pageDomain)
    {
        PageInfo<RechargeConfig> pageInfo = rechargeConfigService.listByPage(rechargeConfig,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增充值金额配置
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/config/add','drive:config:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存充值金额配置
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/config/add','drive:config:add')")
    @Logging(title = "新增充值配置", describe = "新增", type = BusinessType.ADD)
    public Result save(@RequestBody RechargeConfig rechargeConfig)
    {
        return decide(rechargeConfigService.save(rechargeConfig));
    }

    /**
     * 修改充值金额配置
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/config/edit','drive:config:edit')")
    public ModelAndView edit(Long configId, ModelMap mmap)
    {
        RechargeConfig rechargeConfig = rechargeConfigService.getById(configId);
        mmap.put("rechargeConfig", rechargeConfig);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存充值金额配置
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/config/edit','drive:config:edit')")
    @Logging(title = "修改充值配置", describe = "修改", type = BusinessType.EDIT)
    public Result update(@RequestBody RechargeConfig rechargeConfig)
    {
        return decide(rechargeConfigService.update(rechargeConfig));
    }

    /**
     * 批量删除充值金额配置
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/config/remove','drive:config:remove')")
    @Logging(title = "删除充值配置", describe = "批量删除", type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(rechargeConfigService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除充值金额配置
     */
    @DeleteMapping("/remove/{configId}")
    @PreAuthorize("hasPermission('/drive/config/remove','drive:config:remove')")
    @Logging(title = "删除充值配置", describe = "删除", type = BusinessType.REMOVE)
    public Result remove(@PathVariable("configId") Long configId)
    {
        return decide(rechargeConfigService.remove(configId));
    }

    /**
     * 开启
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/drive/config/edit','drive:config:edit')")
    @Logging(title = "修改充值配置", describe = "修改状态", type = BusinessType.EDIT)
    public Result enable(@RequestBody RechargeConfig config) {
        config.setState(DriveConstants.STATE_YES);
        return decide(rechargeConfigService.update(config));
    }

    /**
     * 关闭
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/drive/config/edit','drive:config:edit')")
    @Logging(title = "修改充值配置", describe = "修改状态", type = BusinessType.EDIT)
    public Result disable(@RequestBody RechargeConfig config) {
        config.setState(DriveConstants.STATE_NO);
        return decide(rechargeConfigService.update(config));
    }
}
