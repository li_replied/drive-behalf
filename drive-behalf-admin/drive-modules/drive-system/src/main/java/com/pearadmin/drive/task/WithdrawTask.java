package com.pearadmin.drive.task;

import com.pearadmin.drive.domain.Driver;
import com.pearadmin.drive.service.IDriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 司机提现定时任务调度
 */
@Component("withdrawTask")
public class WithdrawTask {

    @Autowired
    private IDriverService driverService;

    /**
     * 司机提现记录生成调度任务
     */
    public void withdrawNoParams() {
        // 暂设为司机账户余额大于0可提现
        List<Driver> driverList = driverService.selectDriverMoneyList(new Driver());
        driverList.forEach(temp -> driverService.updateDriverMoney(temp.getUserId(), temp.getMoney()));
    }
}
