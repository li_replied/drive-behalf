package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 客户充值对象 driver_recharge
 */
@Data
public class Recharge extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long rechargeId;

    /** 充值金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal amount;

    /** 充值配置ID */
    private Long configId;

    /** 充值金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal rechargeAmount;

    /** 是否充值 */
    private Boolean income;

    /** 订单编号 */
    private String orderNumber;

    /** 乘客ID */
    private Long passengerId;

    /** 支付ID */
    private String payId;

    /** 支付状态 */
    private String payState;

    /** 成功时间 */
    private LocalDateTime successTime;

    /** 支付流水号ID */
    private String transactionId;

    // 客户名称
    private String passengerName;
}
