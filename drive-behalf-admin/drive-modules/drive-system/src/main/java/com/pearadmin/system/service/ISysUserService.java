package com.pearadmin.system.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.SysRole;
import com.pearadmin.system.domain.SysUser;
import com.pearadmin.system.domain.SysMenu;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Describe: 用户服务接口类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
public interface ISysUserService {

    /**
     * 根据条件查询用户列表数据
     * @param param
     * @return 返回用户列表数据
     */
    List<SysUser> list(SysUser param);

    /**
     * 根据条件查询用户列表数据 分页
     * @param pageDomain
     * @param param
     * @return 返回分页用户列表数据
     */
    PageInfo<SysUser> page(SysUser param, PageDomain pageDomain);

    /**
     * 根据 id 获取用户数据
     * @param id
     * @return SysUser
     */
    SysUser getById(String id);

    /**
     * 根据 id 删除用户数据
     * @param id
     * @return 操作结果
     */
    boolean remove(String id);

    /**
     * 批量删除用户数据
     * @param ids
     * @return 操作结果
     */
    boolean batchRemove(String[] ids);

    /**
     * 保存用户数据
     * @param sysUser
     * @return 操作结果
     */
    boolean save(SysUser sysUser);

    /**
     * 修改用户数据
     * @param sysUser
     * @return 操作结果
     */
    boolean update(SysUser sysUser);

    /**
     * 保存用户角色数据
     * @param userId
     * @param roleIds
     * @return 操作结果
     */
    boolean saveUserRole(String userId, List<String> roleIds);

    /**
     * 获取用户角色数据
     * @param userId
     * @return 操作结果
     */
    List<SysRole> getUserRole(String userId);

    /**
     * 获取用户菜单数据
     * @param username
     * @return 操作结果
     */
    List<SysMenu> getUserMenu(String username);

    /**
     * 递归获取菜单tree
     * @param sysMenus
     * @param parentId
     * @return 操作结果
     */
    List<SysMenu> toUserMenu(List<SysMenu> sysMenus, String parentId);

    /**
     * 用户头像上传
     * @param userId
     * @param file
     * @return 结果
     */
    String uploadProfile(String userId, MultipartFile file);

    /**
     * 校验登录账号是否唯一
     * @param sysUser 数据
     * @return 结果
     */
    String checkUserNameUnique(SysUser sysUser);

    /**
     * 校验手机号码是否唯一
     * @param sysUser 用户信息
     * @return 结果
     */
    String checkPhoneUnique(SysUser sysUser);

    /**
     * 校验email是否唯一
     * @param sysUser 用户信息
     * @return 结果
     */
    String checkEmailUnique(SysUser sysUser);

    /**
     * 校验用户是否允许操作
     * @param sysUser 用户信息
     */
    String checkUserAllowed(SysUser sysUser);
}

