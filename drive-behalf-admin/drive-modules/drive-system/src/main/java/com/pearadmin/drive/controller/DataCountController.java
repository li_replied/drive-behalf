package com.pearadmin.drive.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.enums.OrderStateEnum;
import com.pearadmin.common.tools.date.DateUtil;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.drive.service.IDataCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据统计Controller
 *
 * @author huangtao
 * @date 2020-12-09
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "count")
public class DataCountController extends BaseController {
    private String prefix = "drive/count";

    @Autowired
    private IDataCountService dataCountService;

    /**
     * 线性图表
     */
    @GetMapping("/line")
    @PreAuthorize("hasPermission('/drive/count/line','drive:count:line')")
    public ModelAndView line() {
        return jumpPage(prefix + "/line");
    }

    /**
     * 统计一周内用户量(乘客/司机)
     */
    @GetMapping("/line3")
    public Result line3(String beginTime, String endTime) {
        Map<String, Object> params = getParams(beginTime, endTime);
        JSONObject resultJson = new JSONObject();
        JSONArray dayArr = new JSONArray();
        JSONArray passengerArr = new JSONArray();
        JSONArray driverArr = new JSONArray();
        List<Map<String, Object>> countNums = dataCountService.countUsersNum(params);
        countNums.forEach(temp -> {
            dayArr.add(temp.get("days").toString());
            passengerArr.add((Long) temp.get("passengerCount"));
            driverArr.add((Long) temp.get("driverCount"));
        });
        resultJson.put("dayArr", dayArr);
        resultJson.put("passengerArr", passengerArr);
        resultJson.put("driverArr", driverArr);
        return Result.success(resultJson);
    }

    /**
     * 统计每日订单量
     */
    @GetMapping("/line4")
    public Result line4(String beginTime, String endTime) {
        Map<String, Object> params = getParams(beginTime, endTime);
        JSONObject resultJson = new JSONObject();
        JSONArray dayArr = new JSONArray();
        JSONArray orderArr = new JSONArray();
        JSONArray amountArr = new JSONArray();
        List<Map<String, Object>> countNums = dataCountService.countOrdersNum(params);
        countNums.forEach(temp -> {
            dayArr.add(temp.get("days").toString());
            orderArr.add((Long) temp.get("orderCount"));
            DecimalFormat df = new DecimalFormat("0.00");
            df.setRoundingMode(RoundingMode.HALF_UP);
            amountArr.add(df.format(Double.parseDouble(temp.get("payAmount").toString())));
        });
        resultJson.put("dayArr", dayArr);
        resultJson.put("orderArr", orderArr);
        resultJson.put("amountArr", amountArr);
        return Result.success(resultJson);
    }

    /**
     * 柱状图表
     */
    @GetMapping("/column")
    @PreAuthorize("hasPermission('/drive/count/column','drive:count:column')")
    public ModelAndView column() {
        return jumpPage(prefix + "/column");
    }

    /**
     * 统计每月订单量
     */
    @GetMapping("/column1")
    public Result column1(String year) {
        Map<String, Object> params = new HashMap<>();
        params.put("year",year);
        params.put("orderStatus", OrderStateEnum.COMPLETE.name());
        JSONObject resultJson = new JSONObject();
        //JSONArray monthArr = new JSONArray();
        JSONArray allOrderArr = new JSONArray();
        JSONArray orderArr = new JSONArray();
        List<Map<String, Object>> countNums = dataCountService.countOrdersNumByMonths(params);
        countNums.forEach(temp -> {
            //monthArr.add(monthToUpper(Integer.parseInt(temp.get("months").toString()))+"月");
            allOrderArr.add((Long) temp.get("allOrderCount"));
            orderArr.add((Long) temp.get("orderCount"));
        });
        //resultJson.put("monthArr", monthArr);
        resultJson.put("allOrderArr", allOrderArr);
        resultJson.put("orderArr", orderArr);
        return Result.success(resultJson);
    }

    /**
     * 统计时间段内订单收益
     */
    @GetMapping("/column2")
    public Result column2(String year,String beginTime, String endTime) {
        Map<String, Object> params = getParams(beginTime, endTime);
        params.put("year",year);
        params.put("orderStatus", OrderStateEnum.COMPLETE.name());
        JSONObject resultJson = new JSONObject();
        JSONArray dayArr = new JSONArray();
        JSONArray payAmountArr = new JSONArray();
        JSONArray platformProfitArr = new JSONArray();
        JSONArray driverProfitArr = new JSONArray();
        List<Map<String, Object>> countProfit = dataCountService.countOrdersProfit(params);
        countProfit.forEach(temp -> {
            dayArr.add(temp.get("days").toString());
            payAmountArr.add(temp.get("payAmount").toString());
            platformProfitArr.add(DateUtil.moneyToStr(Double.parseDouble(temp.get("platformProfit").toString())));
            driverProfitArr.add(DateUtil.moneyToStr(Double.parseDouble(temp.get("driverProfit").toString())));
        });
        resultJson.put("dayArr", dayArr);
        resultJson.put("payAmountArr", payAmountArr);
        resultJson.put("platformProfitArr", platformProfitArr);
        resultJson.put("driverProfitArr", driverProfitArr);
        return Result.success(resultJson);
    }

    /**
     * 统计一周内订单量
     */
    @GetMapping("/countOrdersByWeek")
    public Result countOrdersByWeek() {
        JSONObject resultJson = new JSONObject();
        JSONArray dayArr = new JSONArray();
        JSONArray orderArr = new JSONArray();
        List<Map<String, Object>> countNums = dataCountService.countOrdersByWeek();
        countNums.forEach(temp -> {
            dayArr.add(temp.get("days").toString());
            orderArr.add((Long) temp.get("orderCount"));
        });
        resultJson.put("dayArr", dayArr);
        resultJson.put("orderArr", orderArr);
        return Result.success(resultJson);
    }

    /**
     * 统计首页订单相关数据
     */
    @GetMapping("/countHomeOrders")
    public Result countHomeOrders() {
        Map<String, Object> data = dataCountService.countHomeOrders();
        return Result.success(data);
    }

    /**
     * 参数组装
     *
     * @param beginTime
     * @param endTime
     * @return
     */
    public Map<String, Object> getParams(String beginTime, String endTime) {
        Map<String, Object> params = new HashMap<>();
        if (StringUtils.isNotEmpty(beginTime)) {
            params.put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            String nowDate = DateUtil.dateTimeNow(DateUtil.YYYY_MM_DD);
            //返回正值是代表左侧日期大于参数日期
            if (endTime.compareTo(nowDate) > 0) {
                endTime = nowDate;
            }
            params.put("endTime", endTime);
        }
        return params;
    }

}
