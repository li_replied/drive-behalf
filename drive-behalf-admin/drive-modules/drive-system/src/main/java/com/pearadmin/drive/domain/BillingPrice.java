package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalTime;

/**
 * 代驾计费价格对象 driver_billing_price
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Data
public class BillingPrice extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 价格表ID */
    private Long billId;

    /** 区域编号 */
    private String regionId;

    /** 起步公里数(3km) */
    private BigDecimal startingKilometre;

    /** 起步价格(3km以内) */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal startingPrice;

    /** 开始时段 */
    private LocalTime startTime;

    /** 结束时段 */
    private LocalTime endTime;

    /** 里程费(超过起步里程后按X元每Y公里计算，不足Y公里的部分按Y公里计算) */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal mileageFee;

    /** 等待时间(X分钟) */
    private Long waitingTime;

    /** 等待费(每等待1分钟加收X元，不足1分钟的部分按1分钟计算) */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal waitingFee;

    /** 司机提成比例 */
    private BigDecimal commissionRatio;

    /** 平台固定抽成 */
    private BigDecimal fixDeduct;

    /** 平台客服电话 */
    private String customerPhone;

    // 区域名称
    private String regionName;

    // 时间段
    private String timeSlots;

    // 起步价格
    private String startPrices;
}
