package com.pearadmin.drive.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Distribution;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 代理商提成分润明细Service接口
 */
public interface IDistributionAgentService extends BaseService<Distribution>
{

    /**
     * 查询代理商提现明细
     * @param distribution 代理商收支明细
     * @param pageDomain
     * @return 代理商收支明细 分页集合
     */
    PageInfo<Distribution> selectExpendPage(Distribution distribution, PageDomain pageDomain);

    /**
     * 审核代理商提现
     *
     * @param withdrawId 数据ID
     * @return 结果
     */
    int audit(Long withdrawId);

    /**
     * 批量审核代理商提现
     *
     * @param ids 需要处理的数据ID
     * @return 结果
     */
    int auditByIds(String[] ids);

}
