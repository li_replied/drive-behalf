package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Version;

/**
 * APP版本Service接口
 * 
 * @author huangtao
 * @date 2020-12-31
 */
public interface IVersionService extends BaseService<Version>
{

    /**
     * 修改状态
     *
     * @param: version
     * @return: 操作结果
     * */
    boolean updateStatus(Version version);

    /**
     * 校验版本号是否唯一
     *
     * @param version
     * @return 结果
     */
    String checkVersionNumberUnique(Version version);
}
