package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.enums.OrderStateEnum;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.OrderState;
import com.pearadmin.drive.domain.TripOrder;
import com.pearadmin.drive.mapper.OrderStateMapper;
import com.pearadmin.drive.mapper.TripOrderMapper;
import com.pearadmin.drive.service.ITripOrderService;
import com.pearadmin.system.domain.SysDept;
import com.pearadmin.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代驾订单Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-02
 */
@Service
public class TripOrderServiceImpl implements ITripOrderService {
    @Autowired
    private TripOrderMapper tripOrderMapper;
    @Autowired
    private OrderStateMapper orderStateMapper;
    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询代驾订单
     *
     * @param tripOrderId 代驾订单ID
     * @return 代驾订单
     */
    @Override
    public TripOrder getById(Long tripOrderId) {
        return tripOrderMapper.selectTripOrderById(tripOrderId);
    }

    /**
     * 查询代驾订单列表
     *
     * @param tripOrder 代驾订单
     * @return 代驾订单
     */
    @Override
    public List<TripOrder> list(TripOrder tripOrder) {
        return tripOrderMapper.selectTripOrderList(tripOrder);
    }

    /**
     * 查询代驾订单
     *
     * @param tripOrder  代驾订单
     * @param pageDomain
     * @return 代驾订单 分页集合
     */
    @Override
    public PageInfo<TripOrder> listByPage(TripOrder tripOrder, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        if (CurrentUserUtil.isNotAdmin()) {
            SysDept dept = sysDeptMapper.getDeptAgentById(CurrentUserUtil.getDeptId());
            if (null != dept) {
                if (DriveConstants.STATE_YES.equals(dept.getAgentAlone())) {
                    Map<String, Object> params = tripOrder.getParams();
                    if (null == params) params = new HashMap<>();
                    params.put("agentAlone", dept.getAgentAlone());
                    params.put("deptId", dept.getDeptId());
                    tripOrder.setParams(params);
                } else if (DriveConstants.AGENT_LEVEL_DISTRICT.equals(dept.getAgentLevel())) {
                    tripOrder.setRegionId(dept.getRegionId());
                } else {
                    Map<String, Object> params = new HashMap<>();
                    if (null != tripOrder.getParams()) {
                        params = tripOrder.getParams();
                    }
                    params.put("agentLevel", dept.getAgentLevel());
                    params.put("deptId", dept.getDeptId());
                    tripOrder.setParams(params);
                }
            }
        }
        List<TripOrder> data = tripOrderMapper.selectTripOrderList(tripOrder);
        return new PageInfo<>(data);
    }

    /**
     * 新增代驾订单
     *
     * @param tripOrder 代驾订单
     * @return 结果
     */
    @Override
    public int save(TripOrder tripOrder) {
        return tripOrderMapper.insertTripOrder(tripOrder);
    }

    /**
     * 修改代驾订单
     *
     * @param tripOrder 代驾订单
     * @return 结果
     */
    @Override
    public int update(TripOrder tripOrder) {
        return tripOrderMapper.updateTripOrder(tripOrder);
    }

    /**
     * 删除代驾订单对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return tripOrderMapper.deleteTripOrderByIds(ids);
    }

    /**
     * 删除代驾订单信息
     *
     * @param tripOrderId 代驾订单ID
     * @return 结果
     */
    @Override
    public int remove(Long tripOrderId) {
        return tripOrderMapper.deleteTripOrderById(tripOrderId);
    }

    /**
     * 取消订单
     *
     * @param orderNumber 订单编号
     * @return 结果
     */
    @Override
    @Transactional
    public boolean cancel(String orderNumber) {
        TripOrder order = new TripOrder();
        order.setOrderNumber(orderNumber);
        order.setOrderStatus(OrderStateEnum.CANCEL.name());
        order.setCancelReason("管理后台取消订单");
        order.setModifiedUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
        order.setModifiedTime(LocalDateTime.now());
        int result = tripOrderMapper.cancel(order);
        if (result > 0) {
            //订单状态流转
            OrderState state = new OrderState();
            state.setOrderStatus(OrderStateEnum.CANCEL.name());
            state.setOrderNumber(orderNumber);
            state.setCreateTime(LocalDateTime.now());
            state.setCreatedUser(0L);
            state.setReason("管理后台取消订单");
            orderStateMapper.insertOrderState(state);
            return true;
        }
        return false;
    }

    /**
     * 批量取消订单
     *
     * @param orderNumbers 需要取消的订单编号
     * @return 结果
     */
    @Override
    @Transactional
    public int cancelByIds(String[] orderNumbers) {
        if (orderNumbers.length > 0) {
            Map<String, Object> params = new HashMap<>();
            params.put("orderNumbers", orderNumbers);
            TripOrder order = new TripOrder();
            order.setOrderStatus(OrderStateEnum.CANCEL.name());
            order.setCancelReason("管理后台取消订单");
            order.setModifiedUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
            order.setModifiedTime(LocalDateTime.now());
            order.setParams(params);
            int result = tripOrderMapper.cancelByIds(order);
            if (result > 0) {
                //订单状态流转
                OrderState state = new OrderState();
                state.setOrderStatus(OrderStateEnum.CANCEL.name());
                state.setCreateTime(LocalDateTime.now());
                state.setCreatedUser(0L);
                state.setReason("管理后台取消订单");
                state.setParams(params);
                orderStateMapper.batchInsertOrderState(state);
                return result;
            }
        }
        return 0;
    }

    /**
     * 查询代驾订单
     *
     * @param orderNo 代驾订单编号
     * @return 代驾订单
     */
    @Override
    public TripOrder selectTripOrderByOrderNo(String orderNo) {
        return tripOrderMapper.selectTripOrderByOrderNo(orderNo);
    }

}