package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.TripOrder;

/**
 * 代驾订单Service接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
public interface ITripOrderService extends BaseService<TripOrder>
{
    /**
     * 取消订单
     * @param: orderNumber 订单编号
     * @return: 操作结果
     * */
    boolean cancel(String orderNumber);

    /**
     * 批量取消订单
     *
     * @param orderNumbers 需要删除的数据编号
     * @return 结果
     */
    int cancelByIds(String[] orderNumbers);

    /**
     * 查询代驾订单
     *
     * @param orderNo 代驾订单编号
     * @return 代驾订单
     */
    TripOrder selectTripOrderByOrderNo(String orderNo);
}
