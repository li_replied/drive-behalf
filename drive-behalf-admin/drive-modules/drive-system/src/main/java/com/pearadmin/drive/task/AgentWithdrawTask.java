package com.pearadmin.drive.task;

import com.pearadmin.system.domain.SysDept;
import com.pearadmin.system.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 代理商提现定时任务调度
 */
@Component("agentWithdrawTask")
public class AgentWithdrawTask {

    @Autowired
    private ISysDeptService sysDeptService;

    /**
     * 代理商提现记录生成调度任务
     */
    public void withdrawNoParams() {
        // 暂设为代理商账户余额大于0可提现
        List<SysDept> agentList = sysDeptService.selectAgentMoneyList(new SysDept());
        agentList.forEach(temp -> sysDeptService.updateAgentMoney(temp.getDeptId(), temp.getMoney()));
    }
}
