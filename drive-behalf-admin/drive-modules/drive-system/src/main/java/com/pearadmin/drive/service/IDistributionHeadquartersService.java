package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Distribution;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 总部提成分润明细Service接口
 */
public interface IDistributionHeadquartersService  extends BaseService<Distribution>
{

}
