package com.pearadmin.system.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.system.domain.SysConfig;

/**
 * Describe: 系统配置服务接口
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
public interface ISysConfigService extends BaseService<SysConfig> {

    /**
     * 查询配置
     *
     * @param key
     * @return
     */
    SysConfig getById(String key);

    /**
     * 删除配置
     *
     * @param key
     * @return
     */
    int remove(String key);

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数信息
     * @return 结果
     */
    String checkConfigKeyUnique(SysConfig config);
}
