package com.pearadmin.drive.domain;

import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 订单状态流转对象 driver_order_state
 * 
 * @author huangtao
 * @date 2020-12-29
 */
@Data
public class OrderState extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long orderStateId;

    /** 订单编号 */
    private String orderNumber;

    /** 订单状态 */
    private String orderStatus;

    /** 备注/原因 */
    private String reason;

}
