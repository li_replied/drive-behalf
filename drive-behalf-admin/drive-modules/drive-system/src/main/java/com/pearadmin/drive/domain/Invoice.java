package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author huangtao
 * @Date 2021/01/09 15:24:07
 * @Description 发票对象 driver_invoice
 */
@Data
public class Invoice extends InvoiceTitle
{
    private static final long serialVersionUID = 1L;

    /** 发票ID */
    private Long invoiceId;

    /** 订单编号 */
    private String orderNo;

    /** 状态 */
    private String state;

    /** 用户ID */
    private Long userId;

    private Long id;

    /** 金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal amount;

    /** 卡号 */
    private String card;

    /** 姓名 */
    private String name;

    /** 支行 */
    private String subbranch;
}
