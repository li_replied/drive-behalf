package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.config.proprety.UploadProperty;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.tools.uuid.IdUtils;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Driver;
import com.pearadmin.drive.domain.Withdraw;
import com.pearadmin.drive.mapper.DriverMapper;
import com.pearadmin.drive.mapper.WithdrawMapper;
import com.pearadmin.drive.service.IDriverService;
import com.pearadmin.system.domain.SysFile;
import com.pearadmin.system.domain.SysUser;
import com.pearadmin.system.mapper.SysFileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 司机Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-07
 */
@Service
public class DriverServiceImpl implements IDriverService {
    @Autowired
    private DriverMapper driverMapper;

    @Autowired
    private SysFileMapper sysFileMapper;

    @Autowired
    private WithdrawMapper withdrawMapper;

    /**
     * 上 传 可 读 配 置
     */
    @Resource
    private UploadProperty uploadProperty;

    /**
     * 查询司机
     *
     * @param userId 司机ID
     * @return 司机
     */
    @Override
    public Driver getById(Long userId) {
        return driverMapper.selectDriverById(userId);
    }

    /**
     * 查询司机列表
     *
     * @param driver 司机
     * @return 司机
     */
    @Override
    public List<Driver> list(Driver driver) {
        return driverMapper.selectDriverList(driver);
    }

    @Override
    public List<Driver> getDriverListByAgentId(String agentId) {
        return driverMapper.selectDriverListByAgentId(agentId);
    }

    /**
     * 查询司机
     *
     * @param driver  司机
     * @param pageDomain
     * @return 司机 分页集合
     */
    @Override
    public PageInfo<Driver> listByPage(Driver driver, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        if (CurrentUserUtil.isNotAdmin()) {
            driver.setAgentId(CurrentUserUtil.getDeptId());
        }
        List<Driver> data = driverMapper.selectDriverList(driver);
        return new PageInfo<>(data);
    }

    /**
     * 新增司机
     *
     * @param driver 司机
     * @return 结果
     */
    @Override
    public int save(Driver driver) {
        driver.setCreateTime(LocalDateTime.now());
        driver.setCreatedUser(null != SecurityUtil.currentUser() ? Long.valueOf(((SysUser) SecurityUtil.currentUser().getPrincipal()).getUserId()) : 0);
        driver.setMoney(BigDecimal.ZERO);
        driver.setOnline(DriveConstants.STATE_NO);
        if (StringUtils.isNotEmpty(driver.getAvatar())) {
            SysFile file = sysFileMapper.selectById(driver.getAvatar());
            if (file != null) {
                driver.setAvatar(file.getFilePath().replace(uploadProperty.getUploadPath(), "/"));
            }
        }
        // 添加代理商ID
        if (null != CurrentUserUtil.getDeptId()) {
            driver.setAgentId(CurrentUserUtil.getDeptId());
        }
        return driverMapper.insertDriver(driver);
    }

    /**
     * 修改司机
     *
     * @param driver 司机
     * @return 结果
     */
    @Override
    @Transactional
    public int update(Driver driver) {
        driver.setModifiedTime(LocalDateTime.now());
        driver.setModifiedUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
        if (StringUtils.isNotEmpty(driver.getAvatar())) {
            SysFile newFile = sysFileMapper.selectById(driver.getAvatar());
            if (newFile != null) {
                Driver driverObj = driverMapper.selectDriverById(driver.getUserId());
                if (driverObj != null && StringUtils.isNotEmpty(driverObj.getAvatar())) {
                    SysFile oldFile = sysFileMapper.selectByPath(uploadProperty.getUploadPath() + driverObj.getAvatar());
                    if (oldFile != null) {
                        sysFileMapper.deleteById(oldFile.getId());
                        new java.io.File(oldFile.getFilePath()).delete();
                    }
                }
                driver.setAvatar(newFile.getFilePath().replace(uploadProperty.getUploadPath(), "/"));
            }
        }
        return driverMapper.updateDriver(driver);
    }

    /**
     * 删除司机对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return driverMapper.deleteDriverByIds(ids);
    }

    /**
     * 删除司机信息
     *
     * @param userId 司机ID
     * @return 结果
     */
    @Override
    public int remove(Long userId) {
        return driverMapper.deleteDriverById(userId);
    }

    /**
     * 查询司机余额列表
     *
     * @param driver 司机
     * @return 司机
     */
    @Override
    public List<Driver> selectDriverMoneyList(Driver driver) {
        return driverMapper.selectDriverMoneyList(driver);
    }

    /**
     * 司机余额提现
     *
     * @param userId 司机ID
     * @param amount 司机余额
     * @return 结果
     */
    @Override
    @Transactional
    public int updateDriverMoney(Long userId, BigDecimal amount) {
        // 司机账户余额清空
        Driver driver = new Driver();
        driver.setUserId(userId);
        driver.setModifiedTime(LocalDateTime.now());
        driver.setModifiedUser(0L);
        int result = driverMapper.updateDriverMoney(driver);
        if (result > 0) {
            // 添加提现记录
            BigDecimal serviceCharge = amount.multiply(DriveConstants.SERVER_RATE);// 提现手续费
            BigDecimal actualAmount = amount.subtract(serviceCharge);// 实际提现金额
            Withdraw withdraw = new Withdraw();
            withdraw.setAmount(amount);

            withdraw.setServiceRate(DriveConstants.SERVER_RATE);
            withdraw.setServiceCharge(serviceCharge);
            withdraw.setActualAmount(actualAmount);

            withdraw.setDriverId(userId);
            withdraw.setOrderNumber(IdUtils.fastSimpleUUID());
            withdraw.setStatus(DriveConstants.STATE_AUDIT);
            withdraw.setMethod(DriveConstants.TYPE_EXPEND);
            withdraw.setCreatedUser(0L);
            withdraw.setCreateTime(LocalDateTime.now());
            withdrawMapper.insertWithdraw(withdraw);
        }
        return result;
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param driver 司机信息
     * @return
     */
    @Override
    public String checkPhoneUnique(Driver driver) {
        Long userId = StringUtils.isNull(driver.getUserId()) ? -1L : driver.getUserId();
        Driver info = driverMapper.checkPhoneUnique(driver.getPhone());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.USER_PHONE_NOT_UNIQUE;
        }
        return UserConstants.USER_PHONE_UNIQUE;
    }

    /**
     * 修改状态
     *
     * @param driver
     * @return 操作结果
     */
    @Override
    public boolean updateStatus(Driver driver) {
        driver.setModifiedTime(LocalDateTime.now());
        Integer result = driverMapper.updateById(driver);
        if (result > 0) {
            return true;
        }
        return false;
    }
}
