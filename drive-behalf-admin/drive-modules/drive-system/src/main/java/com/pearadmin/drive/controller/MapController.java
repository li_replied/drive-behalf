package com.pearadmin.drive.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.tools.map.TencentMapUtils;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.drive.domain.Driver;
import com.pearadmin.drive.service.IDriverService;
import com.pearadmin.drive.vo.DriveAgent;
import com.pearadmin.drive.vo.DriverAddress;
import com.pearadmin.system.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author huangtao
 * @Date 2021/02/02 14:12:41
 * @Description 地图Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "map")
public class MapController extends BaseController {
    private String prefix = "drive/map";

    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private IDriverService driverService;

    /**
     * 腾讯地图服务密钥
     */
    @Value("${pear.tencent.key}")
    private String key;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/map/main','drive:map:main')")
    public ModelAndView main(ModelMap mmap) {
        String agentId = CurrentUserUtil.getDeptId();
        String regionId = deptService.getRegionIdById(agentId);
        if (StringUtils.isEmpty(regionId)) {
            regionId = "420625";//没有代理区域默认为谷城县行政区域代码
        }
        String result = TencentMapUtils.getDistrictLocationByKeyword(regionId, key);
        JSONObject location = JSONObject.parseObject(result);
        mmap.put("lat", location.getString("lat"));
        mmap.put("lng", location.getString("lng"));
        mmap.put("agentId", agentId);
        return jumpPage(prefix + "/main");
    }

    /**
     * 获取在线司机位置数据
     *
     * @param agentId 代理商ID
     * @return Result
     */
    @GetMapping("/positionData")
    public Result positionData(String agentId) {
        List<DriverAddress> driverAddresses = Lists.newArrayList();
        String regionId = deptService.getRegionIdById(agentId);
        boolean hasKey = redisTemplate.hasKey(regionId);
        if (hasKey) {
            DriveAgent driveAgent = (DriveAgent) redisTemplate.opsForValue().get(regionId);
            driverAddresses = driveAgent.getAddresses();
            Iterator<DriverAddress> iter = driverAddresses.iterator();
            while (iter.hasNext()) {
                DriverAddress item = iter.next();
                Long timestamp = System.currentTimeMillis();
                Long s = (timestamp - item.getTimestamp()) / 1000;
                if (s > 30) {//超过30秒未刷新位置的司机，不在地图上显示
                    iter.remove();
                } else {
                    item.setAddress(TencentMapUtils.getAddressByLocation(new StringBuilder().append(item.getLat()).append(",").append(item.getLng()).toString(), key));
                }
            }
        }
        return success(driverAddresses);
    }

    @GetMapping("/driver/location/list")
    public Result getDriverLocationList(String agentId) {
        List<Driver> drivers = driverService.getDriverListByAgentId(agentId);
        if (drivers != null && drivers.size() > 0) {
            List<DriverAddress> driverAddresses = drivers.stream().map(i -> {
                DriverAddress da = new DriverAddress();
                da.setDriverId(i.getUserId());
                da.setRealName(i.getName());
                da.setPhone(i.getPhone());
                da.setLat(String.valueOf(i.getLastLatitude()));
                da.setLng(String.valueOf(i.getLastLongitude()));
                da.setTimestamp(i.getLastOnlineTime());
                da.setAddress(i.getLastAddress());
                return da;
            }).collect(Collectors.toList());
            return success(driverAddresses);
        }
        return success("暂无司机");
    }
}
