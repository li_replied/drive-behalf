package com.pearadmin.drive.service.impl;

import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.drive.mapper.DataCountMapper;
import com.pearadmin.drive.service.IDataCountService;
import com.pearadmin.system.domain.SysDept;
import com.pearadmin.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据统计Service业务层处理
 * 
 * @author huangtao
 * @date 2020-12-23
 */
@Service
public class DataCountServiceImpl implements IDataCountService
{
    @Autowired
    private DataCountMapper dataCountMapper;
    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 统计近七天内乘客/司机注册量
     * @param params
     */
    @Override
    public List<Map<String, Object>> countUsersNum(Map<String,Object> params) {
        if (CurrentUserUtil.isNotAdmin()) {
            SysDept dept = sysDeptMapper.getDeptAgentById(CurrentUserUtil.getDeptId());
            if (null != dept) {
                params.put("deptId", dept.getDeptId());
            }
        }
        return dataCountMapper.countUsersNum(params);
    }

    /**
     * 统计时间段内订单量
     * @param params
     */
    @Override
    public List<Map<String, Object>> countOrdersNum(Map<String,Object> params) {
        return dataCountMapper.countOrdersNum(getParams(params));
    }

    /**
     * 统计每月的订单量
     * @param params
     */
    @Override
    public List<Map<String, Object>> countOrdersNumByMonths(Map<String, Object> params) {
        return dataCountMapper.countOrdersNumByMonths(getParams(params));
    }

    /**
     * 统计时间段内订单收益
     * @param params
     */
    @Override
    public List<Map<String, Object>> countOrdersProfit(Map<String, Object> params) {
        return dataCountMapper.countOrdersProfit(getParams(params));
    }

    /**
     * 首页-统计一周内订单量
     */
    @Override
    public List<Map<String, Object>> countOrdersByWeek() {
        return dataCountMapper.countOrdersByWeek(getParams(new HashMap<>()));
    }

    /**
     * 首页-统计订单相关数据
     */
    @Override
    public Map<String, Object> countHomeOrders() {
        return dataCountMapper.countHomeOrders(getParams(new HashMap<>()));
    }

    /**
     * 代理商数据过滤
     */
    public Map<String, Object> getParams(Map<String, Object> params) {
        if (CurrentUserUtil.isNotAdmin()) {
            SysDept dept = sysDeptMapper.getDeptAgentById(CurrentUserUtil.getDeptId());
            if (null != dept) {
                params.put("agentLevel", dept.getAgentLevel());
                params.put("deptId", dept.getDeptId());
                params.put("regionId", dept.getRegionId());
                params.put("agentAlone", dept.getAgentAlone());
            }
        }
        return params;
    }
}