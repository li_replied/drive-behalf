package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Version;
import com.pearadmin.drive.service.IVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * APP版本Controller
 *
 * @author huangtao
 * @date 2020-12-31
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "version")
public class VersionController extends BaseController {
    private String prefix = "drive/version";

    @Autowired
    private IVersionService versionService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/version/main','drive:version:main')")
    public ModelAndView main() {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询APP版本列表数据
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/version/data','drive:version:data')")
    public ResultTable list(@ModelAttribute Version version, PageDomain pageDomain) {
        PageInfo<Version> pageInfo = versionService.listByPage(version, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 新增APP版本
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/version/add','drive:version:add')")
    public ModelAndView add() {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存APP版本
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/version/add','drive:version:add')")
    @Logging(title = "新增版本", describe = "新增版本", type = BusinessType.ADD)
    public Result save(@RequestBody Version version) {
        if (StringUtils.isNotEmpty(version.getVersionNumber())
                && UserConstants.VERSION_NUMBER_NOT_UNIQUE.equals(versionService.checkVersionNumberUnique(version))) {
            return failure("新增版本'" + version.getVersionName() + "'失败，版本号已存在");
        }
        return decide(versionService.save(version));
    }

    /**
     * 修改APP版本
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/version/edit','drive:version:edit')")
    public ModelAndView edit(Long versionId, ModelMap mmap) {
        Version version = versionService.getById(versionId);
        mmap.put("version", version);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存APP版本
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/version/edit','drive:version:edit')")
    @Logging(title = "修改版本", describe = "修改版本", type = BusinessType.EDIT)
    public Result update(@RequestBody Version version) {
        if (StringUtils.isNotEmpty(version.getVersionNumber())
                && UserConstants.VERSION_NUMBER_NOT_UNIQUE.equals(versionService.checkVersionNumberUnique(version))) {
            return failure("修改版本'" + version.getVersionName() + "'失败，版本号已存在");
        }
        return decide(versionService.update(version));
    }

    /**
     * 批量删除
     */
    @DeleteMapping("/batchRemove")
    @PreAuthorize("hasPermission('/drive/version/remove','drive:version:remove')")
    @Logging(title = "批量删除版本", describe = "批量删除版本", type = BusinessType.REMOVE)
    public Result batchRemove(String ids) {
        return decide(versionService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{versionId}")
    @PreAuthorize("hasPermission('/drive/version/remove','drive:version:remove')")
    @Logging(title = "删除版本", describe = "删除版本", type = BusinessType.REMOVE)
    public Result remove(@PathVariable("versionId") Long versionId) {
        return decide(versionService.remove(versionId));
    }

    /**
     * 开启
     *
     * @param version
     * @return 执行结果
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/drive/coupon/edit','drive:coupon:edit')")
    @Logging(title = "修改版本", describe = "修改版本状态", type = BusinessType.EDIT)
    public Result enable(@RequestBody Version version) {
        version.setEnableState(DriveConstants.STATE_YES);
        return decide(versionService.updateStatus(version));
    }

    /**
     * 关闭
     *
     * @param version
     * @return 执行结果
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/drive/version/edit','drive:version:edit')")
    @Logging(title = "修改版本", describe = "修改版本状态", type = BusinessType.EDIT)
    public Result disable(@RequestBody Version version) {
        version.setEnableState(DriveConstants.STATE_NO);
        return decide(versionService.updateStatus(version));
    }

}
