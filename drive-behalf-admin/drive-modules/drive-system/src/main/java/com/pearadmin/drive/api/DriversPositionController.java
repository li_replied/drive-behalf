package com.pearadmin.drive.api;

import com.google.common.collect.Lists;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.drive.domain.Driver;
import com.pearadmin.drive.service.IDriverService;
import com.pearadmin.drive.vo.DriveAgent;
import com.pearadmin.drive.vo.DriverAddress;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: admin
 * @Date: 2021/2/6 10:54
 * @Description: 上传司机位置信息接口
 */
@Slf4j
@Api(tags = "实时上传司机位置信息接口")
@RestController
@RequestMapping("/drive")
@CrossOrigin(
        origins = "*",
        allowedHeaders = "*",
        allowCredentials = "true",
        methods = {RequestMethod.POST}
)
public class DriversPositionController extends BaseController {

    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    @Autowired
    private IDriverService driverService;

    /**
     * @param driverId 司机ID
     * @param lat      纬度
     * @param lng      经度
     * @param address  当前所在位置
     * @return Result
     */
    @ApiOperation("上传司机位置信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "driverId", value = "司机ID", dataType = "Long", required = true),
            @ApiImplicitParam(name = "lat", value = "定位纬度", dataType = "String", required = true),
            @ApiImplicitParam(name = "lng", value = "定位经度", dataType = "String", required = true),
            @ApiImplicitParam(name = "address", value = "当前位置", dataType = "String")
    })
    @PostMapping("/setPositionData")
    public Result setPositionData(Long driverId, String lat, String lng, String address) {
        boolean hasKey = false;
        Driver driver = driverService.getById(driverId);
        if (null != driver && StringUtils.isNotEmpty(driver.getAgentId())) {
            DriveAgent driveAgent = new DriveAgent();
            List<DriverAddress> addressList = Lists.newArrayList();
            String agentKey = driver.getAgentId();
            hasKey = redisTemplate.hasKey(agentKey);
            if (hasKey) {
                driveAgent = (DriveAgent) redisTemplate.opsForValue().get(agentKey);
                log.info("【driveAgent】= {}", driveAgent);
                addressList = driveAgent.getAddresses();
                if (addressList.stream().filter(w -> w.getDriverId() == driverId).findAny().isPresent()) {
                    addressList.forEach(w -> {
                        if (w.getDriverId().equals(driverId)) {
                            w.setAddress(address);
                            w.setLat(lat);
                            w.setLng(lng);
                            w.setTimestamp(System.currentTimeMillis());
                        }
                    });
                } else {
                    //没有这个司机的位置信息
                    addressList.add(new DriverAddress(driver.getUserId(), driver.getName(), driver.getPhone(), address, lat, lng, System.currentTimeMillis()));
                }
            } else {
                log.info("【driver.getUserId()】= {}", driver.getUserId());
                addressList.add(new DriverAddress(driver.getUserId(), driver.getName(), driver.getPhone(), address, lat, lng, System.currentTimeMillis()));
            }
            driveAgent.setAddresses(addressList);
            //存入redis
            redisTemplate.opsForValue().set(agentKey, driveAgent);

            //获取缓存内容
            hasKey = redisTemplate.hasKey(agentKey);
            log.debug("【result】= {}", hasKey);
        }
        return decide(hasKey);
    }
}