package com.pearadmin.drive.service;

import java.util.List;
import java.util.Map;

/**
 * 数据统计Service接口
 * 
 * @author huangtao
 * @date 2020-12-23
 */
public interface IDataCountService
{
    /**
     * 统计近七天内乘客/司机注册量
     * @param params
     */
    List<Map<String, Object>> countUsersNum(Map<String,Object> params);

    /**
     * 统计时间段内订单量
     * @param params
     */
    List<Map<String, Object>> countOrdersNum(Map<String,Object> params);

    /**
     * 统计每月的订单量
     * @param params
     */
    List<Map<String, Object>> countOrdersNumByMonths(Map<String,Object> params);

    /**
     * 统计时间段内订单收益
     * @param params
     */
    List<Map<String, Object>> countOrdersProfit(Map<String,Object> params);

    /**
     * 统计一周内订单量
     */
    List<Map<String, Object>> countOrdersByWeek();

    /**
     * 统计首页订单相关数据
     */
    Map<String, Object> countHomeOrders();
}
