package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Distribution;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 代理商提成分润明细Mapper接口
 */
@Mapper
public interface DistributionAgentMapper 
{
    /**
     * 查询代理商提成分润明细
     * 
     * @param distributionId 代理商提成分润明细ID
     * @return 代理商提成分润明细
     */
    Distribution selectDistributionAgentById(Long distributionId);

    /**
     * 查询代理商提成分润明细列表
     * 
     * @param distributionAgent 代理商提成分润明细
     * @return 代理商提成分润明细集合
     */
    List<Distribution> selectDistributionAgentList(Distribution distributionAgent);

    /**
     * 新增代理商提成分润明细
     * 
     * @param distributionAgent 代理商提成分润明细
     * @return 结果
     */
    int insertDistributionAgent(Distribution distributionAgent);

    /**
     * 修改代理商提成分润明细
     * 
     * @param distributionAgent 代理商提成分润明细
     * @return 结果
     */
    int updateDistributionAgent(Distribution distributionAgent);

    /**
     * 删除代理商提成分润明细
     * 
     * @param distributionId 代理商提成分润明细ID
     * @return 结果
     */
    int deleteDistributionAgentById(Long distributionId);

    /**
     * 批量删除代理商提成分润明细
     * 
     * @param distributionIds 需要删除的数据ID
     * @return 结果
     */
    int deleteDistributionAgentByIds(String[] distributionIds);

    /**
     * 查询代理商提现明细列表
     *
     * @param distribution 代理商收支明细
     * @return 代理商收支明细集合
     */
    List<Distribution> selectExpendList(Distribution distribution);

    /**
     * 批量审核代理商提现
     *
     * @param distribution
     * @return 结果
     */
    int auditByIds(Distribution distribution);

}
