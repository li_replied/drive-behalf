package com.pearadmin.drive.controller;

import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.DistributionRatio;
import com.pearadmin.drive.service.IDistributionRatioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/19 15:25:05
 * @Description 提成分润比例配置Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "ratio")
public class DistributionRatioController extends BaseController
{
    private String prefix = "drive/ratio";

    @Autowired
    private IDistributionRatioService distributionRatioService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/ratio/main','drive:ratio:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询提成分润比例配置列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/ratio/data','drive:ratio:data')")
    public ResultTable list(@ModelAttribute DistributionRatio distributionRatio)
    {
        List<DistributionRatio> list = distributionRatioService.list(distributionRatio);
        return dataTable(list);
    }

    /**
     * 修改提成分润比例配置
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/ratio/edit','drive:ratio:edit')")
    public ModelAndView edit(Long ratioId, ModelMap mmap)
    {
        DistributionRatio distributionRatio = distributionRatioService.getById(ratioId);
        mmap.put("distributionRatio", distributionRatio);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存提成分润比例配置
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/ratio/edit','drive:ratio:edit')")
    @Logging(title = "修改分成比例",describe = "修改分销提成比例",type = BusinessType.EDIT)
    public Result update(@RequestBody DistributionRatio distributionRatio)
    {
        return decide(distributionRatioService.update(distributionRatio));
    }

}
