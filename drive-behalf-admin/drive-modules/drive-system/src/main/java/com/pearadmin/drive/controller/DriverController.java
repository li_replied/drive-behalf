package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.config.proprety.UploadProperty;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.servlet.ServletUtil;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Driver;
import com.pearadmin.drive.service.IDriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.nio.file.FileSystemNotFoundException;

/**
 * 司机Controller
 * 
 * @author huangtao
 * @date 2020-12-07
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "driver")
public class DriverController extends BaseController
{
    private String prefix = "drive/driver";

    @Autowired
    private IDriverService driverService;
    @Resource
    private UploadProperty uploadProperty;


    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/driver/main','drive:driver:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询司机列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/driver/data','drive:driver:data')")
    public ResultTable list(@ModelAttribute Driver driver, PageDomain pageDomain)
    {
        PageInfo<Driver> pageInfo = driverService.listByPage(driver,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增司机
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/driver/add','drive:driver:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存司机
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/driver/add','drive:driver:add')")
    @Logging(title = "新增司机",describe = "新增司机",type = BusinessType.ADD)
    public Result save(@RequestBody Driver driver)
    {
        if (StringUtils.isNotEmpty(driver.getPhone())
                && UserConstants.USER_PHONE_NOT_UNIQUE.equals(driverService.checkPhoneUnique(driver)))
        {
            return failure("新增司机'" + driver.getName() +"，"+ driver.getPhone() + "'失败，手机号码已存在");
        }
        return decide(driverService.save(driver));
    }

    /**
     * 修改司机
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/driver/edit','drive:driver:edit')")
    public ModelAndView edit(Long userId, ModelMap mmap)
    {
        Driver driver = driverService.getById(userId);
        mmap.put("driver", driver);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存司机
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/driver/edit','drive:driver:edit')")
    @Logging(title = "修改司机",describe = "修改司机",type = BusinessType.EDIT)
    public Result update(@RequestBody Driver driver)
    {
        if (StringUtils.isNotEmpty(driver.getPhone())
                && UserConstants.USER_PHONE_NOT_UNIQUE.equals(driverService.checkPhoneUnique(driver)))
        {
            return failure("修改司机'" + driver.getName() +"，"+ driver.getPhone() + "'失败，手机号码已存在");
        }
        return decide(driverService.update(driver));
    }

    /**
     * 批量删除
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/driver/remove','drive:driver:remove')")
    @Logging(title = "批量删除司机",describe = "批量删除司机",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(driverService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{userId}")
    @PreAuthorize("hasPermission('/drive/driver/remove','drive:driver:remove')")
    @Logging(title = "删除司机",describe = "删除司机",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("userId") Long userId)
    {
        return decide(driverService.remove(userId));
    }

    /**
     * 文件获取接口
     * @param userId
     * @return 文件流
     */
    @GetMapping("download/{userId}")
    public void download(@PathVariable("userId") Long userId) {
        try {
            Driver driver = driverService.getById(userId);
            if (driver != null) {
                String filePath = uploadProperty.getUploadPath().substring(0, uploadProperty.getUploadPath().length() - 1) + driver.getAvatar();
                java.io.File files = new java.io.File(filePath);
                if (files.exists()) {
                    FileCopyUtils.copy(new FileInputStream(filePath), ServletUtil.getResponse().getOutputStream());
                }
            }
        } catch (Exception e) {
            throw new FileSystemNotFoundException();
        }
    }

    /**
     * 启用
     * @param driver
     * @return 执行结果
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/drive/driver/edit','drive:driver:edit')")
    @Logging(title = "修改司机",describe = "修改司机状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody Driver driver){
        driver.setUserEnable(DriveConstants.STATE_YES);
        return decide(driverService.updateStatus(driver));
    }

    /**
     * 禁用
     * @param driver
     * @return 执行结果
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/drive/driver/edit','drive:driver:edit')")
    @Logging(title = "修改司机",describe = "修改司机状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody Driver driver){
        driver.setUserEnable(DriveConstants.STATE_NO);
        return decide(driverService.updateStatus(driver));
    }

    /**
     * 已交押金
     * @param driver
     * @return 执行结果
     */
    @PutMapping("depositPaid")
    @PreAuthorize("hasPermission('/drive/driver/edit','drive:driver:edit')")
    @Logging(title = "修改司机",describe = "修改司机押金状态",type = BusinessType.EDIT)
    public Result depositPaid(@RequestBody Driver driver){
        driver.setDepositStatus(DriveConstants.STATE_YES);
        return decide(driverService.updateStatus(driver));
    }

    /**
     * 未交押金
     * @param driver
     * @return 执行结果
     */
    @PutMapping("depositNotPaid")
    @PreAuthorize("hasPermission('/drive/driver/edit','drive:driver:edit')")
    @Logging(title = "修改司机",describe = "修改司机押金状态",type = BusinessType.EDIT)
    public Result depositNotPaid(@RequestBody Driver driver){
        driver.setDepositStatus(DriveConstants.STATE_NO);
        return decide(driverService.updateStatus(driver));
    }

}
