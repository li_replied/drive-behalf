package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 客户积分收入提现 对象 driver_score
 */
@Data
public class Score extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal amount;

    /** 开户银行 */
    private String bank;

    /** 银行卡号 */
    private String card;

    /** NO-提现 YES-充值 */
    private String income;

    /** 姓名 */
    private String name;

    /** 手机号码 */
    private String phone;

    /** YES通过 NO-审批中 */
    private String state;

    /** 支行 */
    private String subbranch;

    /** 用户ID */
    private Long userId;

    // 提现用户名称
    private String userName;

}
