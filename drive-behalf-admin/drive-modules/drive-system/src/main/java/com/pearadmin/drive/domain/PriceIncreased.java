package com.pearadmin.drive.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 加倍对象 driver_price_increased
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Data
public class PriceIncreased extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 加倍ID */
    private Long priceIncreasedId;

    /** 价格表ID */
    private Long billingPrice;

    /** 加倍原因 */
    private String increaseReason;

    /** 加倍开始时间 */
    private LocalDate priceIncreasedBegin;

    /** 加倍结束时间 */
    private LocalDate priceIncreasedEnd;

    /** 加倍倍数 */
    private BigDecimal priceMultiple;

}
