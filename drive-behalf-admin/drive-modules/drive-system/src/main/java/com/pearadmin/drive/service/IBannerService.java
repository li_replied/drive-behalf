package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Banner;

/**
 * @Author huangtao
 * @Date 2021/01/09 10:55:41
 * @Description 广告Service接口
 */
public interface IBannerService extends BaseService<Banner>
{

}
