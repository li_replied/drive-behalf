package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.DistributionRatio;
import com.pearadmin.drive.mapper.DistributionRatioMapper;
import com.pearadmin.drive.service.IDistributionRatioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/19 15:25:05
 * @Description 提成分润比例配置Service业务层处理
 */
@Service
public class DistributionRatioServiceImpl implements IDistributionRatioService 
{
    @Autowired
    private DistributionRatioMapper distributionRatioMapper;

    /**
     * 查询提成分润比例配置
     * 
     * @param ratioId 提成分润比例配置ID
     * @return 提成分润比例配置
     */
    @Override
    public DistributionRatio getById(Long ratioId)
    {
        return distributionRatioMapper.selectDistributionRatioById(ratioId);
    }

    /**
     * 查询提成分润比例配置列表
     * 
     * @param distributionRatio 提成分润比例配置
     * @return 提成分润比例配置
     */
    @Override
    public List<DistributionRatio> list(DistributionRatio distributionRatio)
    {
        return distributionRatioMapper.selectDistributionRatioList(distributionRatio);
    }

    /**
     * 查询提成分润比例配置
     * @param distributionRatio 提成分润比例配置
     * @param pageDomain
     * @return 提成分润比例配置 分页集合
     */
    @Override
    public PageInfo<DistributionRatio> listByPage(DistributionRatio distributionRatio, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<DistributionRatio> data = distributionRatioMapper.selectDistributionRatioList(distributionRatio);
        return new PageInfo<>(data);
    }

    /**
     * 新增提成分润比例配置
     * 
     * @param distributionRatio 提成分润比例配置
     * @return 结果
     */
    @Override
    public int save(DistributionRatio distributionRatio)
    {
        return distributionRatioMapper.insertDistributionRatio(distributionRatio);
    }

    /**
     * 修改提成分润比例配置
     * 
     * @param distributionRatio 提成分润比例配置
     * @return 结果
     */
    @Override
    public int update(DistributionRatio distributionRatio)
    {
        return distributionRatioMapper.updateDistributionRatio(distributionRatio);
    }

    /**
     * 删除提成分润比例配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return distributionRatioMapper.deleteDistributionRatioByIds(ids);
    }

    /**
     * 删除提成分润比例配置信息
     * 
     * @param ratioId 提成分润比例配置ID
     * @return 结果
     */
    @Override
    public int remove(Long ratioId)
    {
        return distributionRatioMapper.deleteDistributionRatioById(ratioId);
    }
}
