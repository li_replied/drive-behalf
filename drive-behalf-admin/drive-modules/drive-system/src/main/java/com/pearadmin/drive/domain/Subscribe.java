package com.pearadmin.drive.domain;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;
import org.springframework.data.annotation.Transient;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Author huangtao
 * @Date 2021/03/19 15:59:17
 * @Description 【请填写功能名称】对象 driver_subscribe
 */
@Data
public class Subscribe extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;


    /** $column.columnComment */
    private LocalDateTime appointmentTime;

    /** $column.columnComment */
    private String startPoint;

    /** $column.columnComment */
    private String state;

    /** $column.columnComment */
    private Long driverId;

    /** $column.columnComment */
    private String phone;

    // 司机姓名
    @Transient
    private String driverName;

}
