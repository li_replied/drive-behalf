package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Recharge;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 客户充值Service接口
 */
public interface IRechargeService extends BaseService<Recharge>
{

}
