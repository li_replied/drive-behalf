package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.system.domain.SysFile;
import com.pearadmin.system.service.ISysFileService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Describe: 文 件 控 制 器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "file")
public class SysFileController extends BaseController {

    /**
     * 系 统 文 件
     */
    private String MODULE_PATH = "system/file/";

    @Resource
    private ISysFileService fileService;

    /**
     * 文件管理页面
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/system/file/main','sys:file:main')")
    public ModelAndView main(){
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 文件资源数据
     * Param: file, PageDomain
     * Return: 文件资源列表
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/system/file/data','sys:file:data')")
    public ResultTable data(@ModelAttribute SysFile file, PageDomain pageDomain){
        PageInfo<SysFile> pageInfo = fileService.selectFilePage(file,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 文件上传视图
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/system/file/add','sys:file:add')")
    public ModelAndView add(){
        return jumpPage(MODULE_PATH + "add");
    }

    /**
     * 文件上传接口
     * Param: file
     * Return: Result
     */
    @PostMapping("upload")
    @Logging(title = "上传文件",describe = "上传文件",type = BusinessType.ADD)
    public Result upload(@RequestParam("file") MultipartFile file){
        String result = fileService.upload(file);
        if(Strings.isNotBlank(result)){
            return success(0,"上传成功",result);
        }else{
            return failure("上传失败");
        }
    }

    /**
     * 文件获取接口
     * Param: id
     * Return: 文件流
     */
    @GetMapping("download/{id}")
    public void download(@PathVariable("id") String id){
        fileService.download(id);
    }

    /**
     * 文件删除接口
     * Param: id
     * Return: 文件流
     */
    @DeleteMapping("remove/{id}")
    @PreAuthorize("hasPermission('/system/file/remove','sys:file:remove')")
    @Logging(title = "删除文件",describe = "删除文件",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("id") String id){
       boolean result = fileService.remove(id);
       return decide(result,"删除成功","删除失败");
    }

    /**
     * 文件批量删除
     * Param: id
     * Return: 文件流
     */
    @Transactional
    @DeleteMapping("batchRemove")
    @PreAuthorize("hasPermission('/system/file/remove','sys:file:remove')")
    @Logging(title = "批量删除文件",describe = "批量删除文件",type = BusinessType.REMOVE)
    public Result batchRemove(String ids){
        boolean result = fileService.removeByIds(Convert.toStrArray(ids));
        return decide(result,"删除成功","删除失败");
    }

}
