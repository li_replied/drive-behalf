package com.pearadmin.system.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.system.domain.SysDictType;

/**
 * Describe: 数据字典类型服务类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
public interface ISysDictTypeService extends BaseService<SysDictType> {

    /**
     * 根据 Id 查询字典类型
     * @param id
     * @return SysDictType
     */
    SysDictType getById(String id);

    /**
     * 删除 SysDictType 数据
     * @param id
     * @return Boolean
     */
    Boolean remove(String id);

    /**
     * 校验字典编码是否唯一
     *
     * @param sysDictType 数据
     * @return 结果
     */
    String checkTypeCodeUnique(SysDictType sysDictType);
}
