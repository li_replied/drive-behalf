package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.TripOrder;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 代驾订单Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Mapper
public interface TripOrderMapper
{
    /**
     * 查询代驾订单
     * 
     * @param tripOrderId 代驾订单ID
     * @return 代驾订单
     */
    TripOrder selectTripOrderById(Long tripOrderId);

    /**
     * 查询代驾订单列表
     * 
     * @param tripOrder 代驾订单
     * @return 代驾订单集合
     */
    List<TripOrder> selectTripOrderList(TripOrder tripOrder);

    /**
     * 新增代驾订单
     * 
     * @param tripOrder 代驾订单
     * @return 结果
     */
    int insertTripOrder(TripOrder tripOrder);

    /**
     * 修改代驾订单
     * 
     * @param tripOrder 代驾订单
     * @return 结果
     */
    int updateTripOrder(TripOrder tripOrder);

    /**
     * 删除代驾订单
     * 
     * @param tripOrderId 代驾订单ID
     * @return 结果
     */
    int deleteTripOrderById(Long tripOrderId);

    /**
     * 批量删除代驾订单
     * 
     * @param tripOrderIds 需要删除的数据ID
     * @return 结果
     */
    int deleteTripOrderByIds(String[] tripOrderIds);

    /**
     * 取消订单
     *
     * @param tripOrder 代驾订单
     * @return 结果
     */
    int cancel(TripOrder tripOrder);

    /**
     * 批量取消订单
     *
     * @param tripOrder 代驾订单
     * @return 结果
     */
    int cancelByIds(TripOrder tripOrder);

    /**
     * 查询代驾订单
     *
     * @param orderNo 代驾订单编号
     * @return 代驾订单
     */
    TripOrder selectTripOrderByOrderNo(String orderNo);
}
