package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Invoice;
import com.pearadmin.drive.domain.InvoiceTitle;
import com.pearadmin.drive.mapper.InvoiceTitleMapper;
import com.pearadmin.drive.service.IInvoiceTitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/08 15:49:34
 * @Description 发票抬头Service业务层处理
 */
@Service
public class InvoiceTitleServiceImpl implements IInvoiceTitleService 
{
    @Autowired
    private InvoiceTitleMapper invoiceTitleMapper;

    /**
     * 查询发票抬头
     * 
     * @param titleId 发票抬头ID
     * @return 发票抬头
     */
    @Override
    public InvoiceTitle selectInvoiceTitleById(Long titleId)
    {
        return invoiceTitleMapper.selectInvoiceTitleById(titleId);
    }

    /**
     * 查询发票抬头
     * @param invoiceTitle 发票抬头
     * @param pageDomain
     * @return 发票抬头 分页集合
     */
    @Override
    public PageInfo<InvoiceTitle> selectInvoiceTitlePage(InvoiceTitle invoiceTitle, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<InvoiceTitle> data = invoiceTitleMapper.selectInvoiceTitleList(invoiceTitle);
        return new PageInfo<>(data);
    }

    /**
     * 查询发票
     *
     * @param invoiceId 发票ID
     * @return 发票
     */
    @Override
    public Invoice selectInvoiceById(Long invoiceId)
    {
        return invoiceTitleMapper.selectInvoiceById(invoiceId);
    }

    /**
     * 查询发票
     * @param invoice 发票
     * @param pageDomain
     * @return 发票 分页集合
     */
    @Override
    public PageInfo<Invoice> selectInvoicePage(Invoice invoice, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<Invoice> data = invoiceTitleMapper.selectInvoiceList(invoice);
        return new PageInfo<>(data);
    }

    /**
     * 发票审核
     * @param: invoiceId 发票ID
     * @return: 操作结果
     */
    @Override
    public boolean audit(Long invoiceId) {
        Invoice invoice = new Invoice();
        invoice.setInvoiceId(invoiceId);
        invoice.setState(DriveConstants.STATE_YES);
        invoice.setModifiedTime(LocalDateTime.now());
        int result = invoiceTitleMapper.audit(invoice);
        if (result > 0) {
            return true;
        }
        return false;
    }
}
