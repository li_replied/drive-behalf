package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Driver;

import java.math.BigDecimal;
import java.util.List;

/**
 * 司机Service接口
 *
 * @author huangtao
 * @date 2020-12-07
 */
public interface IDriverService extends BaseService<Driver> {

    List<Driver> getDriverListByAgentId(String agentId);

    /**
     * 查询司机余额列表
     *
     * @param driver 司机
     * @return 司机集合
     */
    List<Driver> selectDriverMoneyList(Driver driver);

    /**
     * 司机余额提现
     *
     * @param userId 司机ID
     * @param amount 司机余额
     * @return 结果
     */
    int updateDriverMoney(Long userId, BigDecimal amount);

    /**
     * 校验手机号码是否唯一
     *
     * @param driver 司机信息
     * @return 结果
     */
    String checkPhoneUnique(Driver driver);

    /**
     * 修改状态
     *
     * @param: driver
     * @return: 操作结果
     */
    boolean updateStatus(Driver driver);
}
