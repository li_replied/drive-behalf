package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Banner;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/09 10:55:41
 * @Description 广告Mapper接口
 */
@Mapper
public interface BannerMapper 
{
    /**
     * 查询广告
     * 
     * @param bannerId 广告ID
     * @return 广告
     */
    Banner selectBannerById(Long bannerId);

    /**
     * 查询广告列表
     * 
     * @param banner 广告
     * @return 广告集合
     */
    List<Banner> selectBannerList(Banner banner);

    /**
     * 新增广告
     * 
     * @param banner 广告
     * @return 结果
     */
    int insertBanner(Banner banner);

    /**
     * 修改广告
     * 
     * @param banner 广告
     * @return 结果
     */
    int updateBanner(Banner banner);

    /**
     * 删除广告
     * 
     * @param bannerId 广告ID
     * @return 结果
     */
    int deleteBannerById(Long bannerId);

    /**
     * 批量删除广告
     * 
     * @param bannerIds 需要删除的数据ID
     * @return 结果
     */
    int deleteBannerByIds(String[] bannerIds);

}
