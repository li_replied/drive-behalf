package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Withdraw;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 司机收支明细Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@Mapper
public interface WithdrawMapper 
{
    /**
     * 查询司机收支明细
     * 
     * @param withdrawId 司机收支明细ID
     * @return 司机收支明细
     */
    Withdraw selectWithdrawById(Long withdrawId);

    /**
     * 查询司机提现明细列表
     * 
     * @param withdraw 司机收支明细
     * @return 司机收支明细集合
     */
    List<Withdraw> selectWithdrawList(Withdraw withdraw);

    /**
     * 查询司机提现明细列表
     *
     * @param withdraw 司机收支明细
     * @return 司机收支明细集合
     */
    List<Withdraw> selectExpendList(Withdraw withdraw);

    /**
     * 查询司机收益明细列表
     *
     * @param withdraw 司机收支明细
     * @return 司机收支明细集合
     */
    List<Withdraw> selectIncomeList(Withdraw withdraw);

    /**
     * 新增司机收支明细
     * 
     * @param withdraw 司机收支明细
     * @return 结果
     */
    int insertWithdraw(Withdraw withdraw);

    /**
     * 修改司机收支明细
     * 
     * @param withdraw 司机收支明细
     * @return 结果
     */
    int updateWithdraw(Withdraw withdraw);

    /**
     * 删除司机收支明细
     * 
     * @param withdrawId 司机收支明细ID
     * @return 结果
     */
    int deleteWithdrawById(Long withdrawId);

    /**
     * 批量删除司机收支明细
     * 
     * @param withdrawIds 需要删除的数据ID
     * @return 结果
     */
    int deleteWithdrawByIds(String[] withdrawIds);

    /**
     * 批量审核司机提现
     *
     * @param withdraw
     * @return 结果
     */
    int auditByIds(Withdraw withdraw);
}
