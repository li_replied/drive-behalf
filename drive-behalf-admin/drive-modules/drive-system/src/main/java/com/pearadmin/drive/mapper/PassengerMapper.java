package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Passenger;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 乘客Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-07
 */
@Mapper
public interface PassengerMapper 
{
    /**
     * 查询乘客
     * 
     * @param userId 乘客ID
     * @return 乘客
     */
    Passenger selectPassengerById(Long userId);

    /**
     * 查询乘客
     *
     * @param userId 乘客ID
     * @return 乘客
     */
    String selectPassengerNameById(Long userId);

    /**
     * 查询乘客列表
     * 
     * @param passenger 乘客
     * @return 乘客集合
     */
    List<Passenger> selectPassengerList(Passenger passenger);

    /**
     * 修改数据
     * @param passenger
     * @return 操作结果
     */
    int updateById(Passenger passenger);

    /**
     * 批量开启免单特权
     *
     * @param ids 需要操作的数据ID
     * @return 结果
     */
    int startUpByIds(String[] ids);

    /**
     * 批量取消免单特权
     *
     * @param ids 需要操作的数据ID
     * @return 结果
     */
    int cancelByIds(String[] ids);

}
