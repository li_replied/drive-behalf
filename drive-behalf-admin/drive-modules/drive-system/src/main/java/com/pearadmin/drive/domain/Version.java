package com.pearadmin.drive.domain;

import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * APP版本对象 driver_version
 * 
 * @author huangtao
 * @date 2020-12-31
 */
@Data
public class Version extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long versionId;

    /** APP路径 */
    private String downloadUrl;

    /** 状态 */
    private String enableState;

    /** 版本信息 */
    private String information;

    /** 是否更新 */
    private String mustUpdate;

    /** 平台 */
    private String platform;

    /** 版本名称 */
    private String versionName;

    /** 版本号 */
    private String versionNumber;

    /** 来源 客户端：client,司机端：driver;*/
    private String origin;

}
