package com.pearadmin.drive.domain;

import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

/**
 * @Author huangtao
 * @Date 2021/01/08 15:49:34
 * @Description 发票抬头对象 driver_invoice_title
 */
@Data
public class InvoiceTitle extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long titleId;

    /** 抬头名称 */
    private String titleName;

    /** 单位税号 */
    private String unitTaxNumber;

    /** 企业地址 */
    private String address;

    /** 注册电话 */
    private String registerPhone;

    /** 开户银行 */
    private String bank;

    /** 银行账户 */
    private String bankAccount;

    /** 是否默认 */
    private String defaultTag;

    /** 手机号码 */
    private String phone;

    /** 邮箱 */
    private String mail;

    /** 删除标记 */
    private String deleteFlag;

}
