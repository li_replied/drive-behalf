package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.config.proprety.UploadProperty;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Version;
import com.pearadmin.drive.mapper.VersionMapper;
import com.pearadmin.drive.service.IVersionService;
import com.pearadmin.system.domain.SysFile;
import com.pearadmin.system.mapper.SysFileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * APP版本Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-31
 */
@Service
public class VersionServiceImpl implements IVersionService {
    @Autowired
    private VersionMapper versionMapper;
    @Autowired
    private SysFileMapper sysFileMapper;

    /**
     * 上 传 可 读 配 置
     */
    @Resource
    private UploadProperty uploadProperty;

    /**
     * 查询APP版本
     *
     * @param versionId APP版本ID
     * @return APP版本
     */
    @Override
    public Version getById(Long versionId) {
        return versionMapper.selectVersionById(versionId);
    }

    /**
     * 查询APP版本列表
     *
     * @param version APP版本
     * @return APP版本
     */
    @Override
    public List<Version> list(Version version) {
        return versionMapper.selectVersionList(version);
    }

    /**
     * 查询APP版本
     *
     * @param version    APP版本
     * @param pageDomain
     * @return APP版本 分页集合
     */
    @Override
    public PageInfo<Version> listByPage(Version version, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<Version> data = versionMapper.selectVersionList(version);
        return new PageInfo<>(data);
    }

    /**
     * 新增APP版本
     *
     * @param version APP版本
     * @return 结果
     */
    @Override
    public int save(Version version) {
        version.setCreatedUser(1L);
        version.setCreateTime(LocalDateTime.now());
        if (StringUtils.isNotEmpty(version.getDownloadUrl())) {
            SysFile file = sysFileMapper.selectById(version.getDownloadUrl());
            if (file != null) {
                version.setDownloadUrl(file.getFilePath().replace(uploadProperty.getUploadPath(), "/"));
            }
        }
        return versionMapper.insertVersion(version);
    }

    /**
     * 修改APP版本
     *
     * @param version APP版本
     * @return 结果
     */
    @Override
    public int update(Version version) {
        version.setModifiedTime(LocalDateTime.now());
        if (StringUtils.isNotEmpty(version.getDownloadUrl())) {
            SysFile newFile = sysFileMapper.selectById(version.getDownloadUrl());
            if (newFile != null) {
                Version versionObj = versionMapper.selectVersionById(version.getVersionId());
                if (versionObj != null && StringUtils.isNotEmpty(versionObj.getDownloadUrl())) {
                    SysFile oldFile = sysFileMapper.selectByPath(uploadProperty.getUploadPath() + versionObj.getDownloadUrl());
                    if (oldFile != null) {
                        sysFileMapper.deleteById(oldFile.getId());
                        new java.io.File(oldFile.getFilePath()).delete();
                    }
                }
                version.setDownloadUrl(newFile.getFilePath().replace(uploadProperty.getUploadPath(), "/"));
            }
        }
        return versionMapper.updateVersion(version);
    }

    /**
     * 删除APP版本对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return versionMapper.deleteVersionByIds(ids);
    }

    /**
     * 删除APP版本信息
     *
     * @param versionId APP版本ID
     * @return 结果
     */
    @Override
    public int remove(Long versionId) {
        return versionMapper.deleteVersionById(versionId);
    }

    /**
     * 修改状态
     *
     * @param version
     * @return 操作结果
     */
    @Override
    public boolean updateStatus(Version version) {
        Integer result = versionMapper.updateById(version);
        if (result > 0) {
            return true;
        }
        return false;
    }

    /**
     * 校验版本号是否唯一
     *
     * @param version 版本信息
     * @return
     */
    @Override
    public String checkVersionNumberUnique(Version version) {
        Long versionId = StringUtils.isNull(version.getVersionId()) ? -1L : version.getVersionId();
        Version info = versionMapper.checkVersionNumberUnique(version.getVersionNumber());
        if (StringUtils.isNotNull(info) && info.getVersionId().longValue() != versionId.longValue()) {
            return UserConstants.VERSION_NUMBER_NOT_UNIQUE;
        }
        return UserConstants.VERSION_NUMBER_UNIQUE;
    }
}
