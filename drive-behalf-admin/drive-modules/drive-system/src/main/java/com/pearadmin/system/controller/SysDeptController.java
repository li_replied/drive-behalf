package com.pearadmin.system.controller;

import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.common.web.domain.response.module.ResultTree;
import com.pearadmin.drive.domain.Region;
import com.pearadmin.drive.service.IRegionService;
import com.pearadmin.system.domain.SysDept;
import com.pearadmin.system.service.ISysDeptService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * Describe: 部门管理
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "dept")
public class SysDeptController extends BaseController {

    @Resource
    private ISysDeptService sysDeptService;

    @Resource
    private IRegionService regionService;

    /**
     * 基础路径
     */
    private static String MODULE_PATH = "system/dept/";

    /**
     * 获取部门列表视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/system/dept/main','sys:dept:main')")
    public ModelAndView main() {
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 获取部门列表数据
     * @param sysDept
     * @return 部门列表数据
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/system/dept/data','sys:dept:data')")
    public ResultTable data(SysDept sysDept) {
        List<SysDept> data = sysDeptService.list(sysDept);
        return dataTable(data);
    }

    /**
     * 获取部门树状数据结构
     * @param sysDept
     * @return ResultTree
     */
    @GetMapping("tree")
    public ResultTree tree(SysDept sysDept) {
        List<SysDept> data = sysDeptService.list(sysDept);
        return dataTree(data);
    }

    /**
     * 部门新增视图
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/system/dept/add','sys:dept:add')")
    public ModelAndView add() {
        return jumpPage(MODULE_PATH + "add");
    }

    /**
     * 部门新增保存
     * @param sysDept
     */
    @PostMapping("save")
    @ApiOperation(value = "保存部门数据")
    @PreAuthorize("hasPermission('/system/dept/add','sys:dept:add')")
    @Logging(title = "新增部门", describe = "新增部门", type = BusinessType.ADD)
    public Result save(@RequestBody SysDept sysDept) {
        if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(sysDeptService.checkDeptNameUnique(sysDept))) {
            return failure("新增部门'" + sysDept.getDeptName() + "'失败，部门名称已存在");
        }
        sysDept.setDeptId(SequenceUtil.makeStringId());
        sysDept.setMoney(BigDecimal.ZERO);
        return decide(sysDeptService.save(sysDept));
    }

    /**
     * 部门修改视图
     * @param deptId 部门ID
     */
    @GetMapping("edit")
    @PreAuthorize("hasPermission('/system/dept/edit','sys:dept:edit')")
    public ModelAndView edit(Model model, String deptId) {
        SysDept sysDept = sysDeptService.getById(deptId);
        model.addAttribute("sysDept", sysDept);
        if (DriveConstants.STATE_YES.equals(sysDept.getAgentFlag())) {
            if (DriveConstants.AGENT_LEVEL_PROVINCE.equals(sysDept.getAgentLevel())) {
                Region province = regionService.getById(sysDept.getRegionId());
                model.addAttribute("province", province.getName());
            } else if (DriveConstants.AGENT_LEVEL_CITY.equals(sysDept.getAgentLevel())) {
                Region city = regionService.getById(sysDept.getRegionId());
                if (city != null) {
                    model.addAttribute("city", city.getName());
                    Region province = regionService.getById(city.getParentId());
                    model.addAttribute("province", province.getName());
                }
            } else if (DriveConstants.AGENT_LEVEL_DISTRICT.equals(sysDept.getAgentLevel())) {
                Region region = regionService.getById(sysDept.getRegionId());
                if (region != null) {
                    model.addAttribute("county", region.getName());
                    Region city = regionService.getById(region.getParentId());
                    if (city != null) {
                        model.addAttribute("city", city.getName());
                        Region province = regionService.getById(city.getParentId());
                        model.addAttribute("province", province.getName());
                    }
                }
            }
        }
        return jumpPage(MODULE_PATH + "edit");
    }

    /**
     * 部门修改保存
     * @param sysDept
     */
    @PutMapping("update")
    @PreAuthorize("hasPermission('/system/dept/edit','sys:dept:edit')")
    @Logging(title = "修改部门", describe = "修改部门", type = BusinessType.EDIT)
    public Result update(@RequestBody SysDept sysDept) {
        if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(sysDeptService.checkDeptNameUnique(sysDept))) {
            return failure("修改部门'" + sysDept.getDeptName() + "'失败，部门名称已存在");
        } /*else if (sysDept.getParentId().equals(sysDept.getDeptId())) {
            return failure("修改部门'" + sysDept.getDeptName() + "'失败，上级部门不能是自己");
        }*/
        return decide(sysDeptService.update(sysDept));
    }

    /**
     * 部门删除
     * @param id 部门ID
     */
    @DeleteMapping("remove/{id}")
    @PreAuthorize("hasPermission('/system/dept/remove','sys:dept:remove')")
    @Logging(title = "删除部门", describe = "删除部门", type = BusinessType.REMOVE)
    public Result remove(@PathVariable String id) {
        if (sysDeptService.selectDeptCount(id) > 0) {
            return failure("存在下级部门,不允许删除");
        }
        if (sysDeptService.checkDeptExistUser(id)) {
            return failure("部门存在用户,不允许删除");
        }
        return decide(sysDeptService.remove(id));
    }

    /**
     * 部门批量删除
     * @param ids 部门ID集合
     */
    @DeleteMapping("batchRemove/{ids}")
    @PreAuthorize("hasPermission('/system/dept/remove','sys:dept:remove')")
    @Logging(title = "批量删除部门", describe = "批量删除部门", type = BusinessType.REMOVE)
    public Result batchRemove(@PathVariable String ids) {
        return decide(sysDeptService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 启用部门
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/system/dept/edit','sys:dept:edit')")
    @Logging(title = "修改部门", describe = "修改部门状态", type = BusinessType.EDIT)
    public Result enable(@RequestBody SysDept SysDept) {
        SysDept.setStatus("0");
        return decide(sysDeptService.update(SysDept));
    }

    /**
     * 禁用部门
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/system/dept/edit','sys:dept:edit')")
    @Logging(title = "修改部门", describe = "修改部门状态", type = BusinessType.EDIT)
    public Result disable(@RequestBody SysDept SysDept) {
        SysDept.setStatus("1");
        return decide(sysDeptService.update(SysDept));
    }
}