package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author huangtao
 * @Date 2021/01/19 15:25:05
 * @Description 分润提成比例配置对象 driver_distribution_ratio
 */
@Data
public class DistributionRatio extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long ratioId;

    /** 总部提成金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal hdRatio;

    /** 代理商提成比例 */
    private BigDecimal agRatio;

    /** 一级分销比例 */
    private BigDecimal oneLevelRatio;

    /** 二级分销比例 */
    private BigDecimal twoLevelRatio;

    /** 是否发放二级分销（YES是，NO否） */
    private String twoLevelState;

}
