package com.pearadmin.drive.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Apply;
import com.pearadmin.drive.mapper.ApplyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.pearadmin.drive.service.IApplyService;
import org.springframework.stereotype.Service;

/**
 * @Author huangtao
 * @Date 2021/03/18 09:43:47
 * @Description 【请填写功能名称】Service业务层处理
 */
@Service
public class ApplyServiceImpl implements IApplyService 
{
    @Autowired
    private ApplyMapper applyMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public Apply selectApplyById(Long id)
    {
        return applyMapper.selectApplyById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param apply 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Apply> selectApplyList(Apply apply)
    {
        return applyMapper.selectApplyList(apply);
    }

    /**
     * 查询【请填写功能名称】
     * @param apply 【请填写功能名称】
     * @param pageDomain
     * @return 【请填写功能名称】 分页集合
     */
    @Override
    public PageInfo<Apply> selectApplyPage(Apply apply, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<Apply> data = applyMapper.selectApplyList(apply);
        return new PageInfo<>(data);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param apply 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertApply(Apply apply)
    {
        return applyMapper.insertApply(apply);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param apply 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateApply(Apply apply)
    {
        return applyMapper.updateApply(apply);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteApplyByIds(String[] ids)
    {
        return applyMapper.deleteApplyByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteApplyById(Long id)
    {
        return applyMapper.deleteApplyById(id);
    }

    /**
     * 修改状态
     *
     * @param
     * @return 操作结果
     */
    @Override
    public boolean updateStatus(Apply apply) {
        apply.setModifiedTime(LocalDateTime.now());
        Integer result = applyMapper.updateById(apply);
        if (result > 0) {
            return true;
        }
        return false;
    }
}
