package com.pearadmin.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.tools.uuid.IdUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Distribution;
import com.pearadmin.drive.mapper.DistributionAgentMapper;
import com.pearadmin.system.domain.SysDept;
import com.pearadmin.system.mapper.SysDeptMapper;
import com.pearadmin.system.mapper.SysUserMapper;
import com.pearadmin.system.service.ISysDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class SysDeptServiceImpl implements ISysDeptService {

    @Resource
    private SysDeptMapper sysDeptMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private DistributionAgentMapper distributionAgentMapper;

    /**
     * 查询部门数据
     * @param param
     * @return 操作结果
     */
    @Override
    public List<SysDept> list(SysDept param) {
        return sysDeptMapper.selectList(param);
    }

    /**
     * 查询部门数据 分页
     * @param param pageDomain
     * @return 操作结果
     */
    @Override
    public PageInfo<SysDept> page(SysDept param, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<SysDept> list = sysDeptMapper.selectList(param);
        return new PageInfo<>(list);
    }

    /**
     * 保存部门数据
     * @param sysDept
     */
    @Override
    public boolean save(SysDept sysDept) {
        int result = sysDeptMapper.insert(sysDept);
        if (result > 0) {
            return true;
        }
        return false;
    }

    /**
     * 根据 ID 查询部门
     * @param id
     */
    @Override
    public SysDept getById(String id) {
        return sysDeptMapper.selectById(id);
    }

    /**
     * 修改用户数据
     * @param sysDept
     */
    @Override
    public boolean update(SysDept sysDept) {
        Integer result = sysDeptMapper.updateById(sysDept);
        if (result > 0) {
            return true;
        }
        return false;
    }

    /**
     * 根据 id 删除部门数据
     * @param id
     */
    @Override
    @Transactional
    public Boolean remove(String id) {
        sysDeptMapper.deleteById(id);
        sysUserMapper.resetDeptByDeptId(id);
        return true;
    }

    /**
     * 根据 id 批量删除部门数据
     * @param ids
     */
    @Override
    @Transactional
    public boolean batchRemove(String[] ids) {
        sysDeptMapper.deleteByIds(ids);
        sysUserMapper.resetDeptByDeptIds(ids);
        return true;
    }

    /**
     * 校验部门名称是否唯一
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public String checkDeptNameUnique(SysDept dept)
    {
        String deptId = StringUtils.isNull(dept.getDeptId()) ? "" : dept.getDeptId();
        SysDept info = sysDeptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
        if (StringUtils.isNotNull(info) && !info.getDeptId().equals(deptId))
        {
            return UserConstants.DEPT_NAME_NOT_UNIQUE;
        }
        return UserConstants.DEPT_NAME_UNIQUE;
    }

    /**
     * 查询部门人数
     * @param parentId 部门ID
     * @return 结果
     */
    @Override
    public int selectDeptCount(String parentId)
    {
        SysDept dept = new SysDept();
        dept.setParentId(parentId);
        return sysDeptMapper.selectDeptCount(dept);
    }

    /**
     * 查询部门是否存在用户
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(String deptId)
    {
        int result = sysDeptMapper.checkDeptExistUser(deptId);
        return result > 0 ? true : false;
    }

    /**
     * 查询代理商余额列表
     *
     * @param sysDept 司机
     * @return 代理商集合
     */
    @Override
    public List<SysDept> selectAgentMoneyList(SysDept sysDept) {
        return sysDeptMapper.selectAgentMoneyList(sysDept);
    }

    /**
     * 司机余额提现
     *
     * @param deptId 代理商ID
     * @param amount 代理商余额
     * @return 结果
     */
    @Override
    @Transactional
    public int updateAgentMoney(String deptId, BigDecimal amount) {
        // 司机账户余额清空
        SysDept sysDept = new SysDept();
        sysDept.setDeptId(deptId);
        sysDept.setModifiedTime(LocalDateTime.now());
        sysDept.setModifiedUser(0L);
        int result = sysDeptMapper.updateAgentMoney(sysDept);
        if (result > 0) {
            // 添加提现记录
            BigDecimal serviceCharge = amount.multiply(DriveConstants.SERVER_RATE);// 提现手续费
            BigDecimal actualAmount = amount.subtract(serviceCharge);// 实际提现金额
            Distribution distribution = new Distribution();
            distribution.setAmount(amount);
            distribution.setServiceRate(DriveConstants.SERVER_RATE);
            distribution.setServiceCharge(serviceCharge);
            distribution.setActualAmount(actualAmount);
            distribution.setUserId(Long.getLong(deptId));
            distribution.setOrderNo(IdUtils.fastSimpleUUID());
            distribution.setStatus(DriveConstants.STATE_AUDIT);
            distribution.setMethod(DriveConstants.TYPE_EXPEND);
            distribution.setCreatedUser(0L);
            distribution.setCreateTime(LocalDateTime.now());
            distributionAgentMapper.insertDistributionAgent(distribution);
        }
        return result;
    }

    /**
     * 根据 id 获取部门代理区域ID
     * @param id
     * @return 操作结果
     */
    @Override
    public String getRegionIdById(String id) {
        return sysDeptMapper.getRegionIdById(id);
    }
}
