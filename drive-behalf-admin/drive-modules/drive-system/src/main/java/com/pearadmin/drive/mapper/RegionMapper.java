package com.pearadmin.drive.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.drive.domain.Region;

/**
 * 代驾区域Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Mapper
public interface RegionMapper
{
    /**
     * 查询代驾区域
     * 
     * @param regionId 代驾区域ID
     * @return 代驾区域
     */
    Region selectRegionById(String regionId);

    /**
     * 查询代驾区域列表
     * 
     * @param region 代驾区域
     * @return 代驾区域集合
     */
    List<Region> selectRegionList(Region region);

    /**
     * 查询代驾区域列表树数据
     *
     * @return 代驾区域集合
     */
    List<Region> selectRegionTreeList();

    /**
     * 新增代驾区域
     * 
     * @param region 代驾区域
     * @return 结果
     */
    int insertRegion(Region region);

    /**
     * 修改代驾区域
     * 
     * @param region 代驾区域
     * @return 结果
     */
    int updateRegion(Region region);

    /**
     * 删除代驾区域
     * 
     * @param regionId 代驾区域ID
     * @return 结果
     */
    int deleteRegionById(String regionId);

    /**
     * 批量删除代驾区域
     * 
     * @param regionIds 需要删除的数据ID
     * @return 结果
     */
    int deleteRegionByIds(String[] regionIds);

}
