package com.pearadmin.drive.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.drive.domain.Recharge;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 客户充值Mapper接口
 */
@Mapper
public interface RechargeMapper 
{
    /**
     * 查询客户充值
     * 
     * @param rechargeId 客户充值ID
     * @return 客户充值
     */
    Recharge selectRechargeById(Long rechargeId);

    /**
     * 查询客户充值列表
     * 
     * @param recharge 客户充值
     * @return 客户充值集合
     */
    List<Recharge> selectRechargeList(Recharge recharge);

    /**
     * 新增客户充值
     * 
     * @param recharge 客户充值
     * @return 结果
     */
    int insertRecharge(Recharge recharge);

    /**
     * 修改客户充值
     * 
     * @param recharge 客户充值
     * @return 结果
     */
    int updateRecharge(Recharge recharge);

    /**
     * 删除客户充值
     * 
     * @param rechargeId 客户充值ID
     * @return 结果
     */
    int deleteRechargeById(Long rechargeId);

    /**
     * 批量删除客户充值
     * 
     * @param rechargeIds 需要删除的数据ID
     * @return 结果
     */
    int deleteRechargeByIds(String[] rechargeIds);

}
