package com.pearadmin.system.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.SysDept;

import java.math.BigDecimal;
import java.util.List;

/**
 * Describe: 部门服务接口类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
public interface ISysDeptService {

    /**
     * 查询部门数据
     * @param param
     * @return 操作结果
     */
    List<SysDept> list(SysDept param);

    /**
     * 分页查询部门数据
     * @param param
     * @param pageDomain
     * @return 操作结果
     */
    PageInfo<SysDept> page(SysDept param, PageDomain pageDomain);

    /**
     * 保存部门数据
     * @param SysDept
     * @return 操作结果
     */
    boolean save(SysDept SysDept);

    /**
     * 根据 id 获取部门信息
     * @param id
     * @return 操作结果
     */
    SysDept getById(String id);

    /**
     * 根据 id 修改用户数据
     * @param sysDept
     * @return 操作结果
     */
    boolean update(SysDept sysDept);

    /**
     * 根据 id 删除部门数据
     * @param id
     * @return 操作结果
     */
    Boolean remove(String id);

    /**
     * 根据 id 删除部门数据
     * @param ids
     * @return 操作结果
     */
    boolean batchRemove(String[] ids);

    /**
     * 校验部门名称是否唯一
     * @param dept 部门信息
     * @return 结果
     */
    String checkDeptNameUnique(SysDept dept);

    /**
     * 查询部门人数
     * @param parentId 父部门ID
     * @return 结果
     */
    int selectDeptCount(String parentId);

    /**
     * 查询部门是否存在用户
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    boolean checkDeptExistUser(String deptId);

    /**
     * 查询代理商余额列表
     *
     * @param sysDept 司机
     * @return 代理商集合
     */
    List<SysDept> selectAgentMoneyList(SysDept sysDept);

    /**
     *  代理商余额提现
     *
     * @param deptId 代理商ID
     * @param amount 代理商余额
     * @return 结果
     */
    int updateAgentMoney(String deptId, BigDecimal amount);

    /**
     * 根据 id 获取部门代理区域ID
     * @param id
     * @return 操作结果
     */
    String getRegionIdById(String id);
}
