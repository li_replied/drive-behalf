package com.pearadmin.drive.kafka;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.drive.domain.Distribution;
import com.pearadmin.drive.domain.DistributionRatio;
import com.pearadmin.drive.domain.Driver;
import com.pearadmin.drive.domain.TripOrder;
import com.pearadmin.drive.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaConsumer {

    @Autowired
    private IDistributionRatioService distributionRatioService;
    @Autowired
    private IDistributionHeadquartersService distributionHeadquartersService;
    @Autowired
    private IDistributionAgentService distributionAgentService;
    @Autowired
    private ITripOrderService tripOrderService;
    @Autowired
    private IDriverService driverService;

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * 处理总部订单提成
     *
     * @param record
     */
    @KafkaListener(topics = {"share"})
    public void hdProfit(ConsumerRecord<String, String> record) {
        if (log.isDebugEnabled()) {
            log.debug("接受订单数据", JSON.toJSONString(record.value()));
        }
        String json = record.value();
        if (StringUtils.isBlank(json)) {
            return;
        }
        ShareDto shareDto = null;
        shareDto = JSON.parseObject(json, ShareDto.class);
        if (shareDto == null) {
            return;
        }
        Distribution distribution = new Distribution();
        distribution.setOrderNo(shareDto.getOrderNumber());
        List<Distribution> distributionList = distributionHeadquartersService.list(distribution);
        if (distributionList.isEmpty()) {
            DistributionRatio ratio = distributionRatioService.getById(1l);
            BigDecimal profitAmount = shareDto.getAmount().subtract(ratio.getHdRatio());
            distribution.setAmount(ratio.getHdRatio());
            distribution.setUserId(1l);//分给总部
            distribution.setStatus(DriveConstants.STATE_FINISH);
            distribution.setMethod(DriveConstants.TYPE_INCOME);
            distribution.setCreatedUser(0l);
            distribution.setCreateTime(LocalDateTime.now());
            int result = distributionHeadquartersService.save(distribution);
            if (result > 0) {
                JSONObject resultJson = new JSONObject(true);
                resultJson.put("orderNumber", shareDto.getOrderNumber());
                resultJson.put("amount", profitAmount);
                kafkaTemplate.send("user-share", resultJson.toJSONString());
            }
        }
    }

    /**
     * 处理代理商订单提成
     *
     * @param record
     */
    @KafkaListener(topics = {"dealer-share"})
    public void agProfit(ConsumerRecord<String, String> record) {
        String json = record.value();
        if (StringUtils.isBlank(json)) {
            return;
        }
        ShareDto shareDto = null;
        shareDto = JSON.parseObject(json, ShareDto.class);
        if (shareDto == null) {
            return;
        }
        Distribution distribution = new Distribution();
        distribution.setOrderNo(shareDto.getOrderNumber());
        List<Distribution> distributionList = distributionAgentService.list(distribution);
        if (distributionList.isEmpty()) {
            TripOrder order = tripOrderService.selectTripOrderByOrderNo(shareDto.getOrderNumber());
            if (null != order) {
                Driver driver = driverService.getById(order.getDriverId());
                if (null != driver && StringUtils.isNotEmpty(driver.getAgentId())) {
                    distribution.setUserId(Long.getLong(driver.getAgentId()));
                } else {
                    //没有代理商时默认分给总部
                    distribution.setUserId(1l);
                }
                //profitAmount = amount.multiply(ratio.getAgRatio());
                distribution.setAmount(shareDto.getAmount());
                distribution.setStatus(DriveConstants.STATE_FINISH);
                distribution.setMethod(DriveConstants.TYPE_INCOME);
                distribution.setCreatedUser(0l);
                distribution.setCreateTime(LocalDateTime.now());
                distributionAgentService.save(distribution);
            }
        }
    }
}