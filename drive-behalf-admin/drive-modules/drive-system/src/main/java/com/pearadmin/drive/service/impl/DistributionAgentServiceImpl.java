package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Distribution;
import com.pearadmin.drive.mapper.DistributionAgentMapper;
import com.pearadmin.drive.service.IDistributionAgentService;
import com.pearadmin.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 代理商提成分润明细Service业务层处理
 */
@Service
public class DistributionAgentServiceImpl implements IDistributionAgentService 
{
    @Autowired
    private DistributionAgentMapper distributionAgentMapper;
    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询代理商提成分润明细
     * 
     * @param distributionId 代理商提成分润明细ID
     * @return 代理商提成分润明细
     */
    @Override
    public Distribution getById(Long distributionId)
    {
        return distributionAgentMapper.selectDistributionAgentById(distributionId);
    }

    /**
     * 查询代理商提成分润明细列表
     * 
     * @param distributionAgent 代理商提成分润明细
     * @return 代理商提成分润明细
     */
    @Override
    public List<Distribution> list(Distribution distributionAgent)
    {
        return distributionAgentMapper.selectDistributionAgentList(distributionAgent);
    }

    /**
     * 查询代理商提成分润明细
     * @param distributionAgent 代理商提成分润明细
     * @param pageDomain
     * @return 代理商提成分润明细 分页集合
     */
    @Override
    public PageInfo<Distribution> listByPage(Distribution distributionAgent, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        if (CurrentUserUtil.isNotAdmin()) {
            if (null != CurrentUserUtil.getDeptId()) {
                distributionAgent.setUserId(Long.valueOf(CurrentUserUtil.getDeptId()));
            }
        }
        List<Distribution> data = distributionAgentMapper.selectDistributionAgentList(distributionAgent);
        data.forEach(temp -> temp.setUserName(sysDeptMapper.getDeptNameById(temp.getUserId().toString())));
        return new PageInfo<>(data);
    }

    /**
     * 新增代理商提成分润明细
     * 
     * @param distributionAgent 代理商提成分润明细
     * @return 结果
     */
    @Override
    public int save(Distribution distributionAgent)
    {
        return distributionAgentMapper.insertDistributionAgent(distributionAgent);
    }

    /**
     * 修改代理商提成分润明细
     * 
     * @param distributionAgent 代理商提成分润明细
     * @return 结果
     */
    @Override
    public int update(Distribution distributionAgent)
    {
        return distributionAgentMapper.updateDistributionAgent(distributionAgent);
    }

    /**
     * 删除代理商提成分润明细对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return distributionAgentMapper.deleteDistributionAgentByIds(ids);
    }

    /**
     * 删除代理商提成分润明细信息
     * 
     * @param distributionId 代理商提成分润明细ID
     * @return 结果
     */
    @Override
    public int remove(Long distributionId)
    {
        return distributionAgentMapper.deleteDistributionAgentById(distributionId);
    }

    /**
     * 查询代理商提现明细
     *
     * @param withdraw 代理商收支明细
     * @param pageDomain
     * @return 代理商收支明细 分页集合
     */
    @Override
    public PageInfo<Distribution> selectExpendPage(Distribution withdraw, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        if (CurrentUserUtil.isNotAdmin()) {
            if (null != CurrentUserUtil.getDeptId()) {
                Map<String, Object> params = new HashMap<>();
                if (null != withdraw.getParams()) {
                    params = withdraw.getParams();
                }
                params.put("deptId", CurrentUserUtil.getDeptId());
                withdraw.setParams(params);
            }
        }
        List<Distribution> data = distributionAgentMapper.selectExpendList(withdraw);
        data.forEach(temp -> temp.setUserName(sysDeptMapper.getDeptNameById(temp.getUserId().toString())));
        return new PageInfo<>(data);
    }

    /**
     * 审核代理商提现
     *
     * @param withdrawId 数据ID
     * @return 结果
     */
    @Override
    public int audit(Long withdrawId) {
        Distribution distribution = new Distribution();
        distribution.setDistributionId(withdrawId);
        distribution.setStatus(DriveConstants.STATE_FINISH);
        distribution.setModifiedTime(LocalDateTime.now());
        distribution.setModifiedUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
        return distributionAgentMapper.updateDistributionAgent(distribution);
    }

    /**
     * 批量审核代理商提现
     *
     * @param ids 需要处理的数据ID
     * @return 结果
     */
    @Override
    public int auditByIds(String[] ids) {
        if (ids.length > 0) {
            Map<String, Object> params = new HashMap<>();
            params.put("ids", ids);
            Distribution distribution = new Distribution();
            distribution.setParams(params);
            distribution.setStatus(DriveConstants.STATE_FINISH);
            distribution.setModifiedTime(LocalDateTime.now());
            distribution.setModifiedUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
            return distributionAgentMapper.auditByIds(distribution);
        }
        return 0;
    }
}
