package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.config.proprety.UploadProperty;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.servlet.ServletUtil;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Banner;
import com.pearadmin.drive.service.IBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.nio.file.FileSystemNotFoundException;

/**
 * @Author huangtao
 * @Date 2021/01/09 10:55:41
 * @Description 广告Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "banner")
public class BannerController extends BaseController
{
    private String prefix = "drive/banner";

    @Autowired
    private IBannerService bannerService;
    @Resource
    private UploadProperty uploadProperty;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/banner/main','drive:banner:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询广告列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/banner/data','drive:banner:data')")
    public ResultTable list(@ModelAttribute Banner banner, PageDomain pageDomain)
    {
        PageInfo<Banner> pageInfo = bannerService.listByPage(banner,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增广告
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/banner/add','drive:banner:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存广告
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/banner/add','drive:banner:add')")
    @Logging(title = "新增广告",describe = "新增广告",type = BusinessType.ADD)
    public Result save(@RequestBody Banner banner)
    {
        return decide(bannerService.save(banner));
    }

    /**
     * 修改广告
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/banner/edit','drive:banner:edit')")
    public ModelAndView edit(Long bannerId, ModelMap mmap)
    {
        Banner banner = bannerService.getById(bannerId);
        mmap.put("banner", banner);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存广告
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/banner/edit','drive:banner:edit')")
    @Logging(title = "修改广告",describe = "修改广告",type = BusinessType.EDIT)
    public Result update(@RequestBody Banner banner)
    {
        return decide(bannerService.update(banner));
    }

    /**
     * 批量删除广告
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/banner/remove','drive:banner:remove')")
    @Logging(title = "批量删除广告",describe = "批量删除广告",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(bannerService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除广告
     */
    @DeleteMapping("/remove/{bannerId}")
    @PreAuthorize("hasPermission('/drive/banner/remove','drive:banner:remove')")
    @Logging(title = "批量删除广告",describe = "批量删除广告",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("bannerId") Long bannerId)
    {
        return decide(bannerService.remove(bannerId));
    }

    /**
     * 文件获取接口
     * @param bannerId
     * @return 文件流
     */
    @GetMapping("download/{bannerId}")
    public void download(@PathVariable("bannerId") Long bannerId) {
        try {
            Banner banner = bannerService.getById(bannerId);
            if (banner != null) {
                String filePath = uploadProperty.getUploadPath().substring(0, uploadProperty.getUploadPath().length() - 1) + banner.getPicPath();
                java.io.File files = new java.io.File(filePath);
                if (files.exists()) {
                    FileCopyUtils.copy(new FileInputStream(filePath), ServletUtil.getResponse().getOutputStream());
                }
            }
        } catch (Exception e) {
            throw new FileSystemNotFoundException();
        }
    }
}
