package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Withdraw;
import com.pearadmin.drive.mapper.WithdrawMapper;
import com.pearadmin.drive.service.IWithdrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 司机收支明细Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-16
 */
@Service
public class WithdrawServiceImpl implements IWithdrawService {
    @Autowired
    private WithdrawMapper withdrawMapper;

    /**
     * 查询司机收支明细
     *
     * @param withdrawId 司机收支明细ID
     * @return 司机收支明细
     */
    @Override
    public Withdraw getById(Long withdrawId) {
        return withdrawMapper.selectWithdrawById(withdrawId);
    }

    /**
     * 查询司机收支明细列表
     *
     * @param withdraw 司机收支明细
     * @return 司机收支明细
     */
    @Override
    public List<Withdraw> list(Withdraw withdraw) {
        return withdrawMapper.selectWithdrawList(withdraw);
    }

    /**
     * 查询司机提现明细
     *
     * @param withdraw 司机收支明细
     * @param pageDomain
     * @return 司机收支明细 分页集合
     */
    @Override
    public PageInfo<Withdraw> listByPage(Withdraw withdraw, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<Withdraw> data = withdrawMapper.selectWithdrawList(withdraw);
        return new PageInfo<>(data);
    }

    /**
     * 新增司机收支明细
     *
     * @param withdraw 司机收支明细
     * @return 结果
     */
    @Override
    public int save(Withdraw withdraw) {
        return withdrawMapper.insertWithdraw(withdraw);
    }

    /**
     * 修改司机收支明细
     *
     * @param withdraw 司机收支明细
     * @return 结果
     */
    @Override
    public int update(Withdraw withdraw) {
        return withdrawMapper.updateWithdraw(withdraw);
    }

    /**
     * 删除司机收支明细对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return withdrawMapper.deleteWithdrawByIds(ids);
    }

    /**
     * 删除司机收支明细信息
     *
     * @param withdrawId 司机收支明细ID
     * @return 结果
     */
    @Override
    public int remove(Long withdrawId) {
        return withdrawMapper.deleteWithdrawById(withdrawId);
    }

    /**
     * 查询司机提现明细
     *
     * @param withdraw 司机收支明细
     * @param pageDomain
     * @return 司机收支明细 分页集合
     */
    @Override
    public PageInfo<Withdraw> selectExpendPage(Withdraw withdraw, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        if (CurrentUserUtil.isNotAdmin()) {
            if (null != CurrentUserUtil.getDeptId()) {
                Map<String, Object> params = new HashMap<>();
                if (null != withdraw.getParams()) {
                    params = withdraw.getParams();
                }
                params.put("deptId", CurrentUserUtil.getDeptId());
                withdraw.setParams(params);
            }
        }
        List<Withdraw> data = withdrawMapper.selectExpendList(withdraw);
        return new PageInfo<>(data);
    }

    /**
     * 查询司机收益明细
     *
     * @param withdraw 司机收支明细
     * @param pageDomain
     * @return 司机收支明细 分页集合
     */
    @Override
    public PageInfo<Withdraw> selectIncomePage(Withdraw withdraw, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        if (CurrentUserUtil.isNotAdmin()) {
            if (null != CurrentUserUtil.getDeptId()) {
                Map<String, Object> params = new HashMap<>();
                if (null != withdraw.getParams()) {
                    params = withdraw.getParams();
                }
                params.put("deptId", CurrentUserUtil.getDeptId());
                withdraw.setParams(params);
            }
        }
        List<Withdraw> data = withdrawMapper.selectIncomeList(withdraw);
        return new PageInfo<>(data);
    }

    /**
     * 审核司机提现
     *
     * @param withdrawId 数据ID
     * @return 结果
     */
    @Override
    public int audit(Long withdrawId) {
        Withdraw withdraw = new Withdraw();
        withdraw.setWithdrawId(withdrawId);
        withdraw.setStatus(DriveConstants.STATE_FINISH);
        withdraw.setDealTime(LocalDateTime.now());
        withdraw.setModifiedTime(LocalDateTime.now());
        withdraw.setOperateUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
        return withdrawMapper.updateWithdraw(withdraw);
    }

    /**
     * 批量审核司机提现
     *
     * @param ids 需要处理的数据ID
     * @return 结果
     */
    @Override
    public int auditByIds(String[] ids) {
        if (ids.length > 0) {
            Map<String, Object> params = new HashMap<>();
            params.put("ids", ids);
            Withdraw withdraw = new Withdraw();
            withdraw.setParams(params);
            withdraw.setStatus(DriveConstants.STATE_FINISH);
            withdraw.setDealTime(LocalDateTime.now());
            withdraw.setModifiedTime(LocalDateTime.now());
            withdraw.setOperateUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
            return withdrawMapper.auditByIds(withdraw);
        }
        return 0;
    }
}
