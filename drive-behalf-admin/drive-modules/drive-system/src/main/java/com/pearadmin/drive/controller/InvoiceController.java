package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Invoice;
import com.pearadmin.drive.domain.InvoiceTitle;
import com.pearadmin.drive.service.IInvoiceTitleService;
import com.pearadmin.drive.service.ITripOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author huangtao
 * @Date 2021/01/08 15:49:34
 * @Description 发票抬头Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "invoice")
public class InvoiceController extends BaseController
{
    private String prefix = "drive/invoice";

    @Autowired
    private IInvoiceTitleService invoiceTitleService;
    @Autowired
    private ITripOrderService tripOrderService;

    /**
     * 发票抬头
     */
    @GetMapping("/title")
    @PreAuthorize("hasPermission('/drive/invoice/title','drive:invoice:title')")
    public ModelAndView title()
    {
        return jumpPage(prefix + "/title");
    }

    /**
     * 查询发票抬头列表
     */
    @GetMapping("/titleData")
    @PreAuthorize("hasPermission('/drive/invoice/titleData','drive:invoice:titleData')")
    public ResultTable titleData(@ModelAttribute InvoiceTitle invoiceTitle, PageDomain pageDomain)
    {
        PageInfo<InvoiceTitle> pageInfo = invoiceTitleService.selectInvoiceTitlePage(invoiceTitle,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 发票管理
     */
    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/invoice/main','drive:invoice:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询发票列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/invoice/data','drive:invoice:data')")
    public ResultTable data(@ModelAttribute Invoice invoice, PageDomain pageDomain)
    {
        PageInfo<Invoice> pageInfo = invoiceTitleService.selectInvoicePage(invoice,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 发票审核
     */
    @PutMapping("/audit/{invoiceId}")
    @PreAuthorize("hasPermission('/drive/invoice/edit','drive:invoice:edit')")
    @Logging(title = "发票审核",describe = "发票审核",type = BusinessType.EDIT)
    public Result audit(@PathVariable("invoiceId") Long invoiceId)
    {
        return decide(invoiceTitleService.audit(invoiceId));
    }
    
}
