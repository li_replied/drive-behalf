package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.tools.uuid.IdUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Coupon;
import com.pearadmin.drive.mapper.CouponMapper;
import com.pearadmin.drive.service.ICouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 代驾优惠券Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-02
 */
@Service
public class CouponServiceImpl implements ICouponService {
    @Autowired
    private CouponMapper couponMapper;

    /**
     * 查询代驾优惠券
     *
     * @param couponId 代驾优惠券ID
     * @return 代驾优惠券
     */
    @Override
    public Coupon getById(Long couponId) {
        return couponMapper.selectCouponById(couponId);
    }

    /**
     * 查询代驾优惠券列表
     *
     * @param coupon 代驾优惠券
     * @return 代驾优惠券
     */
    @Override
    public List<Coupon> list(Coupon coupon) {
        return couponMapper.selectCouponList(coupon);
    }

    /**
     * 查询代驾优惠券
     *
     * @param coupon     代驾优惠券
     * @param pageDomain
     * @return 代驾优惠券 分页集合
     */
    @Override
    public PageInfo<Coupon> listByPage(Coupon coupon, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<Coupon> data = couponMapper.selectCouponList(coupon);
        return new PageInfo<>(data);
    }

    /**
     * 新增代驾优惠券
     *
     * @param coupon 代驾优惠券
     * @return 结果
     */
    @Override
    public int save(Coupon coupon) {
        coupon.setCreateTime(LocalDateTime.now());
        coupon.setCouponNumber("" + IdUtils.fastSimpleUUID());
        coupon.setCouponStatus(DriveConstants.STATE_NO);
        return couponMapper.insertCoupon(coupon);
    }

    /**
     * 修改代驾优惠券
     *
     * @param coupon 代驾优惠券
     * @return 结果
     */
    @Override
    public int update(Coupon coupon) {
        coupon.setUpdateTime(LocalDateTime.now());
        return couponMapper.updateCoupon(coupon);
    }

    /**
     * 删除代驾优惠券对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return couponMapper.deleteCouponByIds(ids);
    }

    /**
     * 删除代驾优惠券信息
     *
     * @param couponId 代驾优惠券ID
     * @return 结果
     */
    @Override
    public int remove(Long couponId) {
        return couponMapper.deleteCouponById(couponId);
    }

    /**
     * 修改状态
     *
     * @param coupon
     * @return 操作结果
     */
    @Override
    public boolean updateStatus(Coupon coupon) {
        Integer result = couponMapper.updateById(coupon);
        if (result > 0) {
            return true;
        }
        return false;
    }
}
