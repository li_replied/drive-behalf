package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Distribution;
import com.pearadmin.drive.service.IDistributionAgentService;
import com.pearadmin.drive.service.IDistributionHeadquartersService;
import com.pearadmin.drive.service.IDistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author huangtao
 * @Date 2021/01/19 15:25:05
 * @Description 提成分润明细Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "distribution")
public class DistributionController extends BaseController
{
    private String prefix = "drive/distribution";

    @Autowired
    private IDistributionHeadquartersService headquartersService;
    @Autowired
    private IDistributionAgentService agentService;
    @Autowired
    private IDistributionService distributionService;

    /**
     * 总部提成收益
     */
    @GetMapping("/headquarters")
    @PreAuthorize("hasPermission('/drive/distribution/headquarters','drive:distribution:headquarters')")
    public ModelAndView headIncome()
    {
        return jumpPage(prefix + "/headIncome");
    }

    /**
     * 代理商提成收益
     */
    @GetMapping("/agent")
    @PreAuthorize("hasPermission('/drive/distribution/agent','drive:distribution:agent')")
    public ModelAndView agentIncome()
    {
        return jumpPage(prefix + "/agentIncome");
    }

    /**
     * 客户提成收益
     */
    @GetMapping("/customer")
    @PreAuthorize("hasPermission('/drive/distribution/customer','drive:distribution:customer')")
    public ModelAndView userIncome()
    {
        return jumpPage(prefix + "/userIncome");
    }

    /**
     * 总部提成收益明细
     * @param deptId
     */
    @GetMapping("/headDetail")
    public ModelAndView headDetail(Long deptId, ModelMap mmap)
    {
        mmap.put("deptId",deptId);
        return jumpPage(prefix + "/headMoneyDetail");
    }

    /**
     * 代理商提成收益明细
     * @param deptId
     */
    @GetMapping("/agentDetail")
    public ModelAndView agentDetail(Long deptId, ModelMap mmap)
    {
        mmap.put("deptId",deptId);
        return jumpPage(prefix + "/agentMoneyDetail");
    }

    /**
     * 查询总部提成分润明细列表
     */
    @GetMapping("/headquartersData")
    @PreAuthorize("hasPermission('/drive/distribution/headquartersData','drive:distribution:headquartersData')")
    public ResultTable headquartersData(@ModelAttribute Distribution distribution, PageDomain pageDomain)
    {
        PageInfo<Distribution> pageInfo = headquartersService.listByPage(distribution,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 查询代理商提成分润明细列表
     */
    @GetMapping("/agentData")
    @PreAuthorize("hasPermission('/drive/distribution/agentData','drive:distribution:agentData')")
    public ResultTable agentData(@ModelAttribute Distribution distribution, PageDomain pageDomain)
    {
        PageInfo<Distribution> pageInfo = agentService.listByPage(distribution,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 查询客户提成收益明细列表
     */
    @GetMapping("/customerData")
    @PreAuthorize("hasPermission('/drive/distribution/customerData','drive:distribution:customerData')")
    public ResultTable customerData(@ModelAttribute Distribution distribution, PageDomain pageDomain)
    {
        distribution.setProfitType(DriveConstants.PROFIT_TYPE_USER);
        PageInfo<Distribution> pageInfo = distributionService.listByPage(distribution,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 代理商提现
     */
    @GetMapping("/agentExpend")
    @PreAuthorize("hasPermission('/drive/distribution/agentExpend','drive:distribution:agentExpend')")
    public ModelAndView agentExpend() {
        return jumpPage(prefix + "/agentExpend");
    }

    /**
     * 查询代理商提现明细列表
     */
    @GetMapping("/agentExpendData")
    @PreAuthorize("hasPermission('/drive/distribution/agentExpendData','drive:distribution:agentExpendData')")
    public ResultTable agentExpendData(@ModelAttribute Distribution distribution, PageDomain pageDomain) {
        PageInfo<Distribution> pageInfo = agentService.selectExpendPage(distribution, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 审核代理商提现
     */
    @PutMapping("/agentExpendAudit/{withdrawId}")
    @PreAuthorize("hasPermission('drive:distribution:agentExpendAudit')")
    @Logging(title = "审核提现", describe = "审核代理商提现", type = BusinessType.EDIT)
    public Result agentExpendAudit(@PathVariable("withdrawId") Long withdrawId) {
        return decide(agentService.audit(withdrawId));
    }

    /**
     * 批量审核代理商提现
     */
    @PutMapping("/agentExpendBatchAudit")
    @PreAuthorize("hasPermission('drive:distribution:agentExpendAudit')")
    @Logging(title = "批量审核提现", describe = "批量审核代理商提现", type = BusinessType.EDIT)
    public Result agentExpendBatchAudit(String ids) {
        return decide(agentService.auditByIds(Convert.toStrArray(ids)));
    }
}