package com.pearadmin.common.utils;

import com.pearadmin.common.tools.bean.BeanUtils;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.system.domain.SysUser;
import org.springframework.security.core.Authentication;

/**
 * 当前用户信息工具类
 */
public class CurrentUserUtil {
    public static Authentication getSubject() {
        return SecurityUtil.currentUser();
    }

    public static SysUser getSysUser() {
        SysUser user = null;
        Object obj = getSubject().getPrincipal();
        if (StringUtils.isNotNull(obj)) {
            user = new SysUser();
            BeanUtils.copyBeanProp(user, obj);
        }
        return user;
    }

    public static String getUserId() {
        return getSysUser().getUserId();
    }

    public static String getUserName() {
        return getSysUser().getUsername();
    }

    public static String getDeptId() {
        return getSysUser().getDeptId();
    }

    public static boolean isAdmin() {
        return getSysUser().isAdmin();
    }

    public static boolean isNotAdmin() {
        if (null != getSubject() && !isAdmin() && StringUtils.isNotEmpty(getDeptId()) && !"0".equals(getDeptId())) {
            return true;
        }
        return false;
    }
}
