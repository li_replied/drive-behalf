package com.pearadmin.system.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.system.domain.SysNotice;

/**
 * 通知公告Service接口
 * 
 * @author huangtao
 * @date 2020-12-29
 */
public interface ISysNoticeService extends BaseService<SysNotice>
{

}
