package com.pearadmin.drive.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.drive.domain.CouponPassenger;

/**
 * 乘客优惠券使用Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Mapper
public interface CouponPassengerMapper
{
    /**
     * 查询乘客优惠券使用
     * 
     * @param passengerCouponId 乘客优惠券使用ID
     * @return 乘客优惠券使用
     */
    CouponPassenger selectCouponPassengerById(Long passengerCouponId);

    /**
     * 查询乘客优惠券使用列表
     * 
     * @param couponPassenger 乘客优惠券使用
     * @return 乘客优惠券使用集合
     */
    List<CouponPassenger> selectCouponPassengerList(CouponPassenger couponPassenger);

    /**
     * 新增乘客优惠券使用
     * 
     * @param couponPassenger 乘客优惠券使用
     * @return 结果
     */
    int insertCouponPassenger(CouponPassenger couponPassenger);

    /**
     * 修改乘客优惠券使用
     * 
     * @param couponPassenger 乘客优惠券使用
     * @return 结果
     */
    int updateCouponPassenger(CouponPassenger couponPassenger);

    /**
     * 删除乘客优惠券使用
     * 
     * @param passengerCouponId 乘客优惠券使用ID
     * @return 结果
     */
    int deleteCouponPassengerById(Long passengerCouponId);

    /**
     * 批量删除乘客优惠券使用
     * 
     * @param passengerCouponIds 需要删除的数据ID
     * @return 结果
     */
    int deleteCouponPassengerByIds(String[] passengerCouponIds);

}
