package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.config.proprety.UploadProperty;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Banner;
import com.pearadmin.drive.mapper.BannerMapper;
import com.pearadmin.drive.service.IBannerService;
import com.pearadmin.system.domain.SysFile;
import com.pearadmin.system.mapper.SysFileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/09 10:55:41
 * @Description 广告Service业务层处理
 */
@Service
public class BannerServiceImpl implements IBannerService 
{
    @Autowired
    private BannerMapper bannerMapper;
    @Autowired
    private SysFileMapper sysFileMapper;
    /**
     * 上 传 可 读 配 置
     */
    @Resource
    private UploadProperty uploadProperty;

    @Override
    public Banner getById(Long bannerId){
        return bannerMapper.selectBannerById(bannerId);
    }

    @Override
    public List<Banner> list(Banner banner) {
        return bannerMapper.selectBannerList(banner);
    }

    @Override
    public PageInfo<Banner> listByPage(Banner banner, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<Banner> data = bannerMapper.selectBannerList(banner);
        return new PageInfo<>(data);
    }

    @Override
    public int save(Banner banner) {
        banner.setCreateTime(LocalDateTime.now());
        if (StringUtils.isNotEmpty(banner.getPicPath())) {
            SysFile file = sysFileMapper.selectById(banner.getPicPath());
            if (file != null) {
                banner.setPicPath(file.getFilePath().replace(uploadProperty.getUploadPath(), "/"));
            }
        }
        return bannerMapper.insertBanner(banner);
    }

    @Override
    public int update(Banner banner) {
        banner.setModifiedTime(LocalDateTime.now());
        if (StringUtils.isNotEmpty(banner.getPicPath())) {
            SysFile newFile = sysFileMapper.selectById(banner.getPicPath());
            if (newFile != null) {
                Banner bannerObj = bannerMapper.selectBannerById(banner.getBannerId());
                if (bannerObj != null && StringUtils.isNotEmpty(bannerObj.getPicPath())) {
                    SysFile oldFile = sysFileMapper.selectByPath(uploadProperty.getUploadPath() + banner.getPicPath());
                    if (oldFile != null) {
                        sysFileMapper.deleteById(oldFile.getId());
                        new java.io.File(oldFile.getFilePath()).delete();
                    }
                }
                banner.setPicPath(newFile.getFilePath().replace(uploadProperty.getUploadPath(), "/"));
            }
        }
        return bannerMapper.updateBanner(banner);
    }

    @Override
    public int remove(Long bannerId) {
        return bannerMapper.deleteBannerById(bannerId);
    }

    @Override
    public int batchRemove(String[] ids) {
        return bannerMapper.deleteBannerByIds(ids);
    }

}