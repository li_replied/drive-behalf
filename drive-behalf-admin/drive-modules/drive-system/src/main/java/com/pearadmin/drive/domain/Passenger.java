package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 乘客对象 driver_passenger
 * 
 * @author huangtao
 * @date 2020-12-07
 */
@Data
public class Passenger extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 乘客ID */
    private Long userId;

    /** 乘客头像 */
    private String avatar;

    /** 出生日期 */
    private LocalDate birthday;

    /** 姓名 */
    private String name;

    /** 手机号码 */
    private String phone;

    /** 性别 */
    private String sex;

    /** 状态 */
    private String userEnable;

    /** 姓名是否显示 */
    private String nameShow;

    /** openid */
    private String openId;

    /** 是否有免单特权 */
    private String freeState;

    /** 上级ID */
    private Long parentId;

    /** 零钱余额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal money;

    /** 充值余额*/
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal retail;

    // 上级推荐人
    private String parentName;
}