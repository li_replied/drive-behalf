package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.BillingPrice;

/**
 * 代驾计费价格Service接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
public interface IBillingPriceService extends BaseService<BillingPrice>
{
    /**
     * 新增区域价目
     * @param billingPrice
     * @return 结果
     */
    boolean batchInsertBillingPrice(BillingPrice billingPrice);

}
