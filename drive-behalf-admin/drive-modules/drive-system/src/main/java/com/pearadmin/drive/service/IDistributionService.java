package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Distribution;

/**
 * @Author huangtao
 * @Date 2021/01/27 09:01:17
 * @Description 提成分润明细Service接口
 */
public interface IDistributionService extends BaseService<Distribution>
{

}
