package com.pearadmin.drive.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.drive.domain.OrderState;

/**
 * 订单状态流转Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-29
 */
@Mapper
public interface OrderStateMapper 
{
    /**
     * 查询订单状态流转
     * 
     * @param orderStateId 订单状态流转ID
     * @return 订单状态流转
     */
    OrderState selectOrderStateById(Long orderStateId);

    /**
     * 新增订单状态流转
     * 
     * @param orderState 订单状态流转
     * @return 结果
     */
    int insertOrderState(OrderState orderState);

    /**
     * 批量新增订单状态流转
     *
     * @param orderState 订单状态流转
     * @return 结果
     */
    int batchInsertOrderState(OrderState orderState);

    /**
     * 修改订单状态流转
     * 
     * @param orderState 订单状态流转
     * @return 结果
     */
    int updateOrderState(OrderState orderState);

    /**
     * 删除订单状态流转
     * 
     * @param orderStateId 订单状态流转ID
     * @return 结果
     */
    int deleteOrderStateById(Long orderStateId);

    /**
     * 批量删除订单状态流转
     * 
     * @param orderStateIds 需要删除的数据ID
     * @return 结果
     */
    int deleteOrderStateByIds(String[] orderStateIds);

}
