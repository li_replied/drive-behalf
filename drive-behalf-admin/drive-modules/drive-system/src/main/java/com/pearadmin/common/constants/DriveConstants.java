package com.pearadmin.common.constants;

import java.math.BigDecimal;

/**
 * 常量信息
 */
public class DriveConstants
{
    /** 状态码 */
    public final static String STATE_YES = "YES";
    public final static String STATE_NO = "NO";

    /** 代理商等级 */
    public final static String AGENT_LEVEL_PROVINCE = "PROVINCE"; // 省级
    public final static String AGENT_LEVEL_CITY = "CITY"; // 市级
    public final static String AGENT_LEVEL_DISTRICT = "DISTRICT"; // 区县级

    /** 分销用户类型 */
    public final static String PROFIT_TYPE_HO = "HO";// 总部
    public final static String PROFIT_TYPE_AG = "AG";// 代理商
    public final static String PROFIT_TYPE_USER = "USER";// 用户

    /** 收支状态 */
    public final static String STATE_AUDIT = "AUDIT";// 待审核
    public final static String STATE_FINISH= "FINISH";// 已完成

    /** 收支类型 */
    public final static String TYPE_INCOME = "INCOME";// 收入
    public final static String TYPE_EXPEND = "EXPEND";// 支出

    /** 提现手续费率-百分之0.6**/
    public final static BigDecimal SERVER_RATE = BigDecimal.valueOf(0.006);
}
