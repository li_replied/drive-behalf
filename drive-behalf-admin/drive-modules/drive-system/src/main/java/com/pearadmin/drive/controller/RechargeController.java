package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Recharge;
import com.pearadmin.drive.service.IRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 客户充值Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "recharge")
public class RechargeController extends BaseController
{
    private String prefix = "drive/recharge";

    @Autowired
    private IRechargeService rechargeService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/recharge/main','drive:recharge:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    @GetMapping("/detail")
    @PreAuthorize("hasPermission('/drive/recharge/main','drive:recharge:main')")
    public ModelAndView detail(Long passengerId, ModelMap mmap)
    {
        mmap.put("passengerId",passengerId);
        return jumpPage(prefix + "/moneyDetail");
    }

    /**
     * 查询客户充值列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/recharge/data','drive:recharge:data')")
    public ResultTable list(@ModelAttribute Recharge recharge, PageDomain pageDomain)
    {
        recharge.setIncome(true);
        PageInfo<Recharge> pageInfo = rechargeService.listByPage(recharge,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 修改客户充值
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/recharge/edit','drive:recharge:edit')")
    public ModelAndView edit(Long rechargeId, ModelMap mmap)
    {
        Recharge recharge = rechargeService.getById(rechargeId);
        mmap.put("recharge", recharge);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存客户充值
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/recharge/edit','drive:recharge:edit')")
    public Result update(@RequestBody Recharge recharge)
    {
        return decide(rechargeService.update(recharge));
    }

}
