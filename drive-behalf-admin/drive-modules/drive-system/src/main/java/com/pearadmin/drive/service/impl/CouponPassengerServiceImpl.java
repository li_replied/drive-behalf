package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.CouponPassenger;
import com.pearadmin.drive.mapper.CouponPassengerMapper;
import com.pearadmin.drive.service.ICouponPassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 乘客优惠券使用Service业务层处理
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Service
public class CouponPassengerServiceImpl implements ICouponPassengerService
{
    @Autowired
    private CouponPassengerMapper couponPassengerMapper;

    /**
     * 查询乘客优惠券使用
     * 
     * @param passengerCouponId 乘客优惠券使用ID
     * @return 乘客优惠券使用
     */
    @Override
    public CouponPassenger getById(Long passengerCouponId)
    {
        return couponPassengerMapper.selectCouponPassengerById(passengerCouponId);
    }

    /**
     * 查询乘客优惠券使用列表
     * 
     * @param couponPassenger 乘客优惠券使用
     * @return 乘客优惠券使用
     */
    @Override
    public List<CouponPassenger> list(CouponPassenger couponPassenger)
    {
        return couponPassengerMapper.selectCouponPassengerList(couponPassenger);
    }

    /**
     * 查询乘客优惠券使用
     * @param couponPassenger 乘客优惠券使用
     * @param pageDomain
     * @return 乘客优惠券使用 分页集合
     * */
    @Override
    public PageInfo<CouponPassenger> listByPage(CouponPassenger couponPassenger, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<CouponPassenger> data = couponPassengerMapper.selectCouponPassengerList(couponPassenger);
        return new PageInfo<>(data);
    }

    /**
     * 新增乘客优惠券使用
     * 
     * @param couponPassenger 乘客优惠券使用
     * @return 结果
     */

    @Override
    public int save(CouponPassenger couponPassenger)
    {
        return couponPassengerMapper.insertCouponPassenger(couponPassenger);
    }

    /**
     * 修改乘客优惠券使用
     * 
     * @param couponPassenger 乘客优惠券使用
     * @return 结果
     */
    @Override
    public int update(CouponPassenger couponPassenger)
    {
        return couponPassengerMapper.updateCouponPassenger(couponPassenger);
    }

    /**
     * 删除乘客优惠券使用对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return couponPassengerMapper.deleteCouponPassengerByIds(ids);
    }

    /**
     * 删除乘客优惠券使用信息
     * 
     * @param passengerCouponId 乘客优惠券使用ID
     * @return 结果
     */
    @Override
    public int remove(Long passengerCouponId)
    {
        return couponPassengerMapper.deleteCouponPassengerById(passengerCouponId);
    }
}
