package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Driver;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.drive.domain.Version;

/**
 * APP版本Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-31
 */
@Mapper
public interface VersionMapper 
{
    /**
     * 查询APP版本
     * 
     * @param versionId APP版本ID
     * @return APP版本
     */
    Version selectVersionById(Long versionId);

    /**
     * 查询APP版本列表
     * 
     * @param version APP版本
     * @return APP版本集合
     */
    List<Version> selectVersionList(Version version);

    /**
     * 新增APP版本
     * 
     * @param version APP版本
     * @return 结果
     */
    int insertVersion(Version version);

    /**
     * 修改APP版本
     * 
     * @param version APP版本
     * @return 结果
     */
    int updateVersion(Version version);

    /**
     * 删除APP版本
     * 
     * @param versionId APP版本ID
     * @return 结果
     */
    int deleteVersionById(Long versionId);

    /**
     * 批量删除APP版本
     * 
     * @param versionIds 需要删除的数据ID
     * @return 结果
     */
    int deleteVersionByIds(String[] versionIds);

    /**
     * 修改数据
     * @param version
     * @return 操作结果
     * */
    int updateById(Version version);

    /**
     * 校验版本号是否唯一
     *
     * @param versionNumber 版本号
     * @return 结果
     */
    Version checkVersionNumberUnique(String versionNumber);
}
