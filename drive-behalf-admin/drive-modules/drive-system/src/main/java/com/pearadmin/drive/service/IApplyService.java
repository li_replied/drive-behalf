package com.pearadmin.drive.service;

import java.util.List;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Apply;

/**
 * @Author huangtao
 * @Date 2021/03/18 09:43:47
 * @Description 【请填写功能名称】Service接口
 */
public interface IApplyService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    Apply selectApplyById(Long id);


    /**
    * 查询【请填写功能名称】
     * @param apply 【请填写功能名称】
     * @param pageDomain
     * @return 【请填写功能名称】 分页集合
     */
    PageInfo<Apply> selectApplyPage(Apply apply, PageDomain pageDomain);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param apply 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    List<Apply> selectApplyList(Apply apply);

    /**
     * 新增【请填写功能名称】
     * 
     * @param apply 【请填写功能名称】
     * @return 结果
     */
    int insertApply(Apply apply);

    /**
     * 修改【请填写功能名称】
     * 
     * @param apply 【请填写功能名称】
     * @return 结果
     */
    int updateApply(Apply apply);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteApplyByIds(String[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    int deleteApplyById(Long id);

    /**
     * 修改状态
     *
     * @param: driver
     * @return: 操作结果
     */
    boolean updateStatus(Apply apply);

}
