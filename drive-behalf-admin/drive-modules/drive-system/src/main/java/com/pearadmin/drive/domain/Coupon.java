package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 代驾优惠券对象 driver_coupon
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Data
public class Coupon extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 优惠券ID */
    private Long couponId;

    /** 商家ID */
    private Long shopId;

    /** 优惠券编号 */
    private String couponNumber;

    /** 优惠券状态，1启用，2关闭 */
    private String couponStatus;

    /** 优惠券类型：1代金券，2满减券，3折扣券 */
    private String couponType;

    /** 使用金额限制 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal limitAmount;

    /** 优惠券金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal couponAmount;

    /** 优惠折扣 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal couponDiscount;

    /** 优惠券标题 */
    private String couponTitle;

    /** 优惠券使用说明 */
    private String couponUseDes;

    /** 优惠券链接 */
    private String couponUrl;

    /** 使用时间类型：1有效时间段，2有效天数 */
    private String validType;

    /** 有效天数设置 */
    private Long validDay;

    /** 有效期起 */
    private LocalDate validTime;

    /** 有效期止 */
    private LocalDate invalidTime;

    /** 发放数量 */
    private Long sendCount;

    /** 领取数量 */
    private Long receiveCount;

    /** 使用次数 */
    private Long useCount;

}
