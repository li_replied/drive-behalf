package com.pearadmin.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.config.proprety.SecurityProperty;
import com.pearadmin.common.config.proprety.UploadProperty;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.*;
import com.pearadmin.system.mapper.*;
import com.pearadmin.system.service.ISysUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Describe: 用户服务实现类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Service
public class SysUserServiceImpl implements ISysUserService {

    /**
     * 注入用户服务
     */
    @Resource
    private SysUserMapper sysUserMapper;
    /**
     * 注入用户角色服务
     */
    @Resource
    private SysUserRoleMapper sysUserRoleMapper;
    /**
     * 注入角色服务
     */
    @Resource
    private SysRoleMapper sysRoleMapper;
    /**
     * 注入权限服务
     */
    @Resource
    private SysPowerMapper sysPowerMapper;
    /**
     * 注入部门服务
     */
    @Resource
    private SysDeptMapper sysDeptMapper;
    /**
     * 超级管理员配置
     */
    @Resource
    private SecurityProperty securityProperty;
    /**
     * 上 传 可 读 配 置
     */
    @Resource
    private UploadProperty uploadProperty;

    /**
     * 根据条件查询用户列表数据
     * @param param
     * @return 返回用户列表数据
     */
    @Override
    public List<SysUser> list(SysUser param) {
        List<SysUser> sysUsers = sysUserMapper.selectList(param);
        return sysUsers;
    }

    /**
     * 根据条件查询用户列表数据 分页
     * @param pageDomain
     * @param param
     * @return 返回分页用户列表数据
     */
    @Override
    public PageInfo<SysUser> page(SysUser param, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<SysUser> sysUsers = sysUserMapper.selectList(param);
        sysUsers.forEach(temp -> {
                if (StringUtils.isNotEmpty(temp.getDeptId())) {
                    temp.setDeptName(sysDeptMapper.getDeptNameById(temp.getDeptId()));
                }
            }
        );
        return new PageInfo<>(sysUsers);
    }

    /**
     * 根据 id 获取用户数据
     * @param id
     * @return 回用户信息
     */
    @Override
    public SysUser getById(String id) {
        return sysUserMapper.selectById(id);
    }

    /**
     * 根据 id 删除用户数据
     * @param id
     * @return 操作结果
     */
    @Override
    @Transactional
    public boolean remove(String id) {
        sysUserRoleMapper.deleteByUserId(id);
        sysUserMapper.deleteById(id);
        return true;
    }

    /**
     * 批量删除用户数据
     * @param ids
     * @return 操作结果
     */
    @Override
    @Transactional
    public boolean batchRemove(String[] ids) {
        sysUserMapper.deleteByIds(ids);
        sysUserRoleMapper.deleteByUserIds(ids);
        return true;
    }

    /**
     * 保存用户数据
     * @param sysUser
     * @return 操作结果
     */
    @Override
    public boolean save(SysUser sysUser) {
        int result = sysUserMapper.insert(sysUser);
        if(result>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 修改用户数据
     * @param sysUser
     * @return 操作结果
     */
    @Override
    public boolean update(SysUser sysUser) {
        Integer result = sysUserMapper.updateById(sysUser);
        if(result > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 保存用户角色数据
     * @param userId
     * @param roleIds
     * @return 操作结果
     */
    @Override
    public boolean saveUserRole(String userId, List<String> roleIds) {
        sysUserRoleMapper.deleteByUserId(userId);
        List<SysUserRole> sysUserRoles = new ArrayList<>();
        roleIds.forEach(roleId->{
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setId(SequenceUtil.makeStringId());
            sysUserRole.setRoleId(roleId);
            sysUserRole.setUserId(userId);
            sysUserRoles.add(sysUserRole);
        });
        int i = sysUserRoleMapper.batchInsert(sysUserRoles);
        if(i>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 获取用户角色数据
     * @param userId
     * @return 操作结果
     */
    public List<SysRole> getUserRole(String userId){
        List<SysRole> allRole = sysRoleMapper.selectList(null);
        List<SysUserRole> myRole = sysUserRoleMapper.selectByUserId(userId);
        allRole.forEach(sysRole->{
            myRole.forEach(sysUserRole->{
                if(sysRole.getRoleId().equals(sysUserRole.getRoleId()))sysRole.setChecked(true);
            });
        });
        return allRole;
    }

    /**
     * 获取用户菜单数据
     * @param username
     * @return 操作结果
     */
    @Override
    public List<SysMenu> getUserMenu(String username) {
        String name = !(securityProperty.isSuperAuthOpen() && username.equals(securityProperty.getSuperAdmin()))?username:"";
        return sysPowerMapper.selectMenuByUsername(name);
    }
    /**
     * 递归获取菜单tree
     * @param sysMenus
     * @param parentId
     * @return 操作结果
     */
    @Override
    public List<SysMenu> toUserMenu(List<SysMenu> sysMenus,String parentId) {
        List<SysMenu> list = new ArrayList<>();
        for (SysMenu menu : sysMenus) {
            if (parentId.equals(menu.getParentId())) {
                menu.setChildren(toUserMenu(sysMenus, menu.getId()));
                list.add(menu);
            }
        }
        return list;
    }

    /**
     * 用户头像上传
     * @param userId
     * @param file
     * @return 结果
     */
    @Override
    @Transactional
    public String uploadProfile(String userId, MultipartFile file) {
        try {
            String fileId = SequenceUtil.makeStringId();
            String name = file.getOriginalFilename();
            String suffixName = name.substring(name.lastIndexOf("."));
            String hash = fileId;
            String fileName = hash + suffixName;
            String parentPath = uploadProperty.getUploadPath();
            java.io.File filepath = new java.io.File(parentPath, fileName);
            if (!filepath.getParentFile().exists()) {
                filepath.getParentFile().mkdirs();
            }
            SysUser sysUser = new SysUser();
            file.transferTo(filepath);
            sysUser.setUserId(userId);
            sysUser.setAvatar(fileName);
            sysUser.setUpdateTime(LocalDateTime.now());
            int result = sysUserMapper.updateById(sysUser);
            if (result > 0) {
                return fileId;
            } else {
                return "";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 校验登录账号是否唯一
     * @param sysUser 数据
     * @return 结果
     */
    @Override
    public String checkUserNameUnique(SysUser sysUser) {
        String userId = StringUtils.isEmpty(sysUser.getUserId()) ? "" : sysUser.getUserId();
        SysUser info = sysUserMapper.checkUserNameUnique(sysUser.getUsername());
        if (StringUtils.isNotNull(info) && !info.getUserId().equals(userId))
        {
            return UserConstants.USER_NAME_NOT_UNIQUE;
        }
        return UserConstants.USER_NAME_UNIQUE;
    }

    /**
     * 校验手机号码是否唯一
     * @param sysUser 用户信息
     * @return
     */
    @Override
    public String checkPhoneUnique(SysUser sysUser)
    {
        String userId = StringUtils.isEmpty(sysUser.getUserId()) ? "" : sysUser.getUserId();
        SysUser info = sysUserMapper.checkPhoneUnique(sysUser.getPhone());
        if (StringUtils.isNotNull(info) && !info.getUserId().equals(userId))
        {
            return UserConstants.USER_PHONE_NOT_UNIQUE;
        }
        return UserConstants.USER_PHONE_UNIQUE;
    }

    /**
     * 校验email是否唯一
     * @param sysUser 用户信息
     * @return
     */
    @Override
    public String checkEmailUnique(SysUser sysUser)
    {
        String userId = StringUtils.isEmpty(sysUser.getUserId()) ? "" : sysUser.getUserId();
        SysUser info = sysUserMapper.checkEmailUnique(sysUser.getEmail());
        if (StringUtils.isNotNull(info) && !info.getUserId().equals(userId))
        {
            return UserConstants.USER_EMAIL_NOT_UNIQUE;
        }
        return UserConstants.USER_EMAIL_UNIQUE;
    }

    /**
     * 校验用户是否允许操作
     * @param sysUser 用户信息
     */
    @Override
    public String checkUserAllowed(SysUser sysUser)
    {
        if (StringUtils.isNotNull(sysUser.getUserId()) && sysUser.isAdmin())
        {
            return "不允许操作超级管理员用户";
        }
        return "";
    }
}
