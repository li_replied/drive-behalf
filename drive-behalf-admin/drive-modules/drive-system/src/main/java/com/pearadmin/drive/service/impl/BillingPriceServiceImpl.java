package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.BillingPrice;
import com.pearadmin.drive.domain.Region;
import com.pearadmin.drive.mapper.BillingPriceMapper;
import com.pearadmin.drive.mapper.RegionMapper;
import com.pearadmin.drive.service.IBillingPriceService;
import com.pearadmin.system.domain.SysDept;
import com.pearadmin.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代驾计费价格Service业务层处理
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Service
public class BillingPriceServiceImpl implements IBillingPriceService
{
    @Autowired
    private BillingPriceMapper billingPriceMapper;

    @Autowired
    private RegionMapper regionMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Override
    public BillingPrice getById(Long key) {
        return billingPriceMapper.selectBillingPriceById(key);
    }

    @Override
    public List<BillingPrice> list(BillingPrice billingPrice) {
        return billingPriceMapper.selectBillingPriceList(billingPrice);
    }

    @Override
    public PageInfo<BillingPrice> listByPage(BillingPrice billingPrice, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        if (CurrentUserUtil.isNotAdmin()) {
            SysDept dept = sysDeptMapper.getDeptAgentById(CurrentUserUtil.getDeptId());
            if (null != dept) {
                if (DriveConstants.AGENT_LEVEL_DISTRICT.equals(dept.getAgentLevel())) {
                    billingPrice.setRegionId(dept.getRegionId());
                } else {
                    Map<String, Object> params = new HashMap<>();
                    if (null != billingPrice.getParams()) {
                        params = billingPrice.getParams();
                    }
                    params.put("agentLevel", dept.getAgentLevel());
                    params.put("deptId", dept.getDeptId());
                    billingPrice.setParams(params);
                }
            }
        }
        List<BillingPrice> data = billingPriceMapper.selectBillingPriceList(billingPrice);
        data.forEach(temp -> {
            StringBuffer regionName = new StringBuffer();
            Region region = regionMapper.selectRegionById(temp.getRegionId());
            if (region != null) {
                Region city = regionMapper.selectRegionById(region.getParentId());
                if (city != null) {
                    Region province = regionMapper.selectRegionById(city.getParentId());
                    regionName.append(province.getName() + city.getName() + region.getName());
                }
                temp.setRegionName(regionName.toString());
            }
        });
        return new PageInfo<>(data);
    }

    @Override
    public int save(BillingPrice billingPrice) {
        int row = billingPriceMapper.selectPriceRowsByRegionId(billingPrice.getRegionId());
        if (row > 0) {
            return 0;
        }
        billingPrice.setCreateTime(LocalDateTime.now());
        return billingPriceMapper.insertBillingPrice(billingPrice);
    }

    @Override
    public int update(BillingPrice billingPrice) {
        billingPrice.setModifiedTime(LocalDateTime.now());
        return billingPriceMapper.updateBillingPrice(billingPrice);
    }

    @Override
    public int remove(Long key) {
        return billingPriceMapper.deleteBillingPriceById(key);
    }

    @Override
    public int batchRemove(String[] ids) {
        return billingPriceMapper.deleteBillingPriceByIds(ids);
    }

    /**
     * 新增代驾计费价格
     *
     * @param billingPrice 代驾计费价格
     * @return 结果
     */
    @Override
    public boolean batchInsertBillingPrice(BillingPrice billingPrice)
    {
        int row = billingPriceMapper.selectPriceRowsByRegionId(billingPrice.getRegionId());
        if (row > 0) {
            return false;
        }
        List<BillingPrice> billingPrices = new ArrayList<>();
        String timeSlots[] = billingPrice.getTimeSlots().split(",");
        String startPrices[] = billingPrice.getStartPrices().split(",");
        for (int i = 0; i < timeSlots.length; i++) {
            BillingPrice price = new BillingPrice();
            String slots[] = timeSlots[i].split(" - ");
            price.setStartTime(LocalTime.parse(slots[0]));
            price.setEndTime(LocalTime.parse(slots[1]));
            price.setStartingPrice(new BigDecimal(startPrices[i]));
            price.setRegionId(billingPrice.getRegionId());
            price.setStartingKilometre(billingPrice.getStartingKilometre());
            price.setMileageFee(billingPrice.getMileageFee());
            price.setCommissionRatio(billingPrice.getCommissionRatio());
            price.setFixDeduct(billingPrice.getFixDeduct());
            price.setCustomerPhone(billingPrice.getCustomerPhone());
            price.setWaitingTime(billingPrice.getWaitingTime());
            price.setWaitingFee(billingPrice.getWaitingFee());
            price.setCreateTime(LocalDateTime.now());
            billingPrices.add(price);
        }
        int result = billingPriceMapper.batchInsert(billingPrices);
        if (result > 0) {
            return true;
        }
        return false;
    }
}
