package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Score;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 客户积分收入提现Mapper接口
 */
@Mapper
public interface ScoreMapper 
{
    /**
     * 查询客户积分收入提现
     * 
     * @param id 
     * @return 客户积分收入提现
     */
    Score selectScoreById(Long id);

    /**
     * 查询客户积分收入提现列表
     * 
     * @param score 客户积分收入提现
     * @return 客户积分收入提现集合
     */
    List<Score> selectScoreList(Score score);

    /**
     * 新增客户积分收入提现
     * 
     * @param score 客户积分收入提现
     * @return 结果
     */
    int insertScore(Score score);

    /**
     * 修改客户积分收入提现
     * 
     * @param score 客户积分收入提现
     * @return 结果
     */
    int updateScore(Score score);

    /**
     * 删除客户积分收入提现
     * 
     * @param id 客户积分收入提现ID
     * @return 结果
     */
    int deleteScoreById(Long id);

    /**
     * 批量删除客户积分收入提现
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteScoreByIds(String[] ids);

    /**
     * 批量审核提现
     *
     * @param score
     * @return 结果
     */
    int auditByIds(Score score);

}
