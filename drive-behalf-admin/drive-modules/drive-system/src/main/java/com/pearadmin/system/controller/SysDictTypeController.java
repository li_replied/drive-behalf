package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.system.domain.SysDictType;
import com.pearadmin.system.service.ISysDictTypeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * Describe: 数据字典类型控制器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "dictType")
public class SysDictTypeController extends BaseController {

    @Resource
    private ISysDictTypeService sysDictTypeService;

    private String MODULE_PATH = "system/dict/";

    /**
     * 数据字典列表视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/system/dictType/main','sys:dictType:main')")
    public ModelAndView main() {
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 数据字典列表数据
     *
     * @param sysDictType
     * @param pageDomain
     * @return ResultTable
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/system/dictType/data','sys:dictType:data')")
    public ResultTable data(SysDictType sysDictType, PageDomain pageDomain) {
        PageInfo<SysDictType> pageInfo = sysDictTypeService.listByPage(sysDictType, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 根据条件查询用户列表数据
     *
     * @param sysDictType
     * @return ResultTable
     */
    @GetMapping("list")
    @PreAuthorize("hasPermission('/system/dictType/data','sys:dictType:data')")
    public ResultTable list(SysDictType sysDictType) {
        List<SysDictType> list = sysDictTypeService.list(sysDictType);
        return dataTable(list);
    }

    /**
     * 数据字典类型新增视图
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/system/dictType/add','sys:dictType:add')")
    public ModelAndView add() {
        return jumpPage(MODULE_PATH + "add");
    }

    /**
     * 新增字典类型
     *
     * @param sysDictType
     * @return Result
     */
    @PostMapping("save")
    @PreAuthorize("hasPermission('/system/dictType/add','sys:dictType:add')")
    @Logging(title = "新增字典",describe = "新增字典类型",type = BusinessType.ADD)
    public Result save(@RequestBody SysDictType sysDictType) {
        if (StringUtils.isNotEmpty(sysDictType.getTypeCode())
                && UserConstants.DICT_TYPE_NOT_UNIQUE.equals(sysDictTypeService.checkTypeCodeUnique(sysDictType))) {
            return failure("新增字典类型'" + sysDictType.getTypeName() + "，" + sysDictType.getTypeCode() + "'失败，字典编码已存在");
        }
        sysDictType.setId(SequenceUtil.makeStringId());
        int result = sysDictTypeService.save(sysDictType);
        return decide(result);
    }

    /**
     * 数据字典类型修改视图
     *
     * @param dictTypeId
     */
    @GetMapping("edit")
    @PreAuthorize("hasPermission('/system/dictType/edit','sys:dictType:edit')")
    public ModelAndView edit(Model model, String dictTypeId) {
        model.addAttribute("sysDictType", sysDictTypeService.getById(dictTypeId));
        return jumpPage(MODULE_PATH + "edit");
    }

    /**
     * 修改字典类型
     *
     * @param sysDictType
     * @return Result
     */
    @PutMapping("update")
    @PreAuthorize("hasPermission('/system/dictType/edit','sys:dictType:edit')")
    @Logging(title = "修改字典",describe = "修改字典类型",type = BusinessType.EDIT)
    public Result update(@RequestBody SysDictType sysDictType) {
        if (StringUtils.isNotEmpty(sysDictType.getTypeCode())
                && UserConstants.DICT_TYPE_NOT_UNIQUE.equals(sysDictTypeService.checkTypeCodeUnique(sysDictType))) {
            return failure("修改字典类型'" + sysDictType.getTypeName() + "，" + sysDictType.getTypeCode() + "'失败，字典编码已存在");
        }
        int result = sysDictTypeService.update(sysDictType);
        return decide(result);
    }

    /**
     * 数据字典删除
     *
     * @param id
     * @return Result
     */
    @DeleteMapping("remove/{id}")
    @PreAuthorize("hasPermission('/system/dictType/remove','sys:dictType:remove')")
    @Logging(title = "删除字典",describe = "删除字典",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("id") String id) {
        Boolean result = sysDictTypeService.remove(id);
        return decide(result);
    }

    /**
     * 批量删除
     *
     * @param typeCodes 需要删除的数据字典编码
     * @return Result
     */
    @DeleteMapping("batchRemove")
    @PreAuthorize("hasPermission('/system/dictType/remove','sys:dictType:remove')")
    @Logging(title = "批量删除字典",describe = "批量删除字典",type = BusinessType.REMOVE)
    public Result batchRemove(String typeCodes) {
        int result = sysDictTypeService.batchRemove(Convert.toStrArray(typeCodes));
        return decide(result);
    }

    /**
     * 启用字典
     *
     * @param sysDictType
     * @return Result
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/system/dictType/edit','sys:dictType:edit')")
    @Logging(title = "修改字典",describe = "修改字典状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody SysDictType sysDictType) {
        sysDictType.setEnable("0");
        return decide(sysDictTypeService.update(sysDictType));
    }

    /**
     * 禁用字典
     *
     * @param sysDictType
     * @return Result
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/system/dictType/edit','sys:dictType:edit')")
    @Logging(title = "修改字典",describe = "修改字典状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody SysDictType sysDictType) {
        sysDictType.setEnable("1");
        return decide(sysDictTypeService.update(sysDictType));
    }
}
