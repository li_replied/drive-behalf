package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Score;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 客户积分收入提现Service接口
 */
public interface IScoreService extends BaseService<Score>
{

    /**
     * 审核提现
     *
     * @param withdrawId 数据ID
     * @return 结果
     */
    int audit(Long withdrawId);

    /**
     * 批量审核提现
     *
     * @param ids 需要处理的数据ID
     * @return 结果
     */
    int auditByIds(String[] ids);
}
