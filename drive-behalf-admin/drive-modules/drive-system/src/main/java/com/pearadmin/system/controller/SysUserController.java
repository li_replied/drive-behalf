package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.plugins.repeat.annotation.RepeatSubmit;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.tools.servlet.ServletUtil;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.system.domain.SysMenu;
import com.pearadmin.system.domain.SysUser;
import com.pearadmin.system.service.ISysRoleService;
import com.pearadmin.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * Describe: 用户控制器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "user")
@Api(value="用户controller",tags={"用户操作接口"})
public class SysUserController extends BaseController {

    /**
     * 基础路径
     */
    private static String MODULE_PATH = "system/user/";

    /**
     * 用户模块服务
     */
    @Resource
    private ISysUserService sysUserService;

    /**
     * 角色模块服务
     */
    @Resource
    private ISysRoleService sysRoleService;

    /**
     * 获取用户列表视图
     */
    @GetMapping("main")
    @ApiOperation(value="获取用户列表视图")
    @PreAuthorize("hasPermission('/system/user/main','sys:user:main')")
    public ModelAndView main(){
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 获取用户列表数据
     * @param pageDomain
     * @param param
     * @return 用户列表数据
     */
    @GetMapping("data")
    @ApiOperation(value="获取用户列表数据")
    @PreAuthorize("hasPermission('/system/user/data','sys:user:data')")
    @Logging(title = "查询用户",describe = "查询用户",type = BusinessType.QUERY)
    public ResultTable data(PageDomain pageDomain, SysUser param){
        PageInfo<SysUser> pageInfo = sysUserService.page(param,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 用户新增视图
     * @return 返回用户新增视图
     */
    @GetMapping("add")
    @ApiOperation(value="获取用户新增视图")
    @PreAuthorize("hasPermission('/system/user/add','sys:user:add')")
    public ModelAndView add(Model model){
        model.addAttribute("sysRoles",sysRoleService.list(null));
        return jumpPage(MODULE_PATH+"add");
    }

    /**
     * 用户新增接口
     * @param sysUser
     * @return 操作结果
     */
    @RepeatSubmit
    @PostMapping("save")
    @ApiOperation(value="保存用户数据")
    @PreAuthorize("hasPermission('/system/user/add','sys:user:add')")
    @Logging(title = "新增用户",describe = "新增用户",type = BusinessType.ADD)
    public Result save(@RequestBody SysUser sysUser){
        if (StringUtils.isNotEmpty(sysUser.getUsername())
                && UserConstants.USER_NAME_NOT_UNIQUE.equals(sysUserService.checkUserNameUnique(sysUser))) {
            return failure("新增用户'" + sysUser.getRealName() + "，" + sysUser.getUsername() + "'失败，登录账号已存在");
        }
        else if (StringUtils.isNotEmpty(sysUser.getPhone())
                && UserConstants.USER_PHONE_NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(sysUser)))
        {
            return failure("新增用户'" + sysUser.getRealName() + "'失败，手机号码已存在");
        }
        else if (StringUtils.isNotEmpty(sysUser.getEmail())
                && UserConstants.USER_EMAIL_NOT_UNIQUE.equals(sysUserService.checkEmailUnique(sysUser)))
        {
            return failure("新增用户'" + sysUser.getRealName() + "'失败，邮箱账号已存在");
        }
        sysUser.setLogin("0");
        sysUser.setEnable("1");
        sysUser.setStatus("1");
        sysUser.setUserId(SequenceUtil.makeStringId());
        sysUser.setCreateTime(LocalDateTime.now());
        sysUser.setPassword(new BCryptPasswordEncoder().encode(sysUser.getPassword()));
        sysUserService.saveUserRole(sysUser.getUserId(), Arrays.asList(sysUser.getRoleIds().split(",")));
        Boolean result = sysUserService.save(sysUser);
        return decide(result);
    }

    /**
     * 用户修改视图
     * @param userId
     * @return 用户修改视图
     */
    @GetMapping("edit")
    @ApiOperation(value="获取用户修改视图")
    @PreAuthorize("hasPermission('/system/user/edit','sys:user:edit')")
    public ModelAndView edit(Model model, String userId){
        model.addAttribute("sysRoles",sysUserService.getUserRole(userId));
        SysUser sysUser = sysUserService.getById(userId);
        model.addAttribute("sysUser",sysUser);
        return jumpPage(MODULE_PATH + "edit");
    }

    /**
     * 用户修改接口
     * @param sysUser
     * @return 操作结果
     */
    @PutMapping("update")
    @ApiOperation(value="修改用户数据")
    @PreAuthorize("hasPermission('/system/user/edit','sys:user:edit')")
    @Logging(title = "修改用户",describe = "修改用户",type = BusinessType.EDIT)
    public Result update(@RequestBody SysUser sysUser){
        String allowed = sysUserService.checkUserAllowed(sysUser);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }
        if (StringUtils.isNotEmpty(sysUser.getUsername())
                && UserConstants.USER_NAME_NOT_UNIQUE.equals(sysUserService.checkUserNameUnique(sysUser))) {
            return failure("修改用户'" + sysUser.getRealName() + "，" + sysUser.getUsername() + "'失败，登录账号已存在");
        }
        else if (StringUtils.isNotEmpty(sysUser.getRealName())
                && UserConstants.USER_PHONE_NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(sysUser)))
        {
            return failure("修改用户'" + sysUser.getRealName() + "'失败，手机号码已存在");
        }
        else if (StringUtils.isNotEmpty(sysUser.getEmail())
                && UserConstants.USER_EMAIL_NOT_UNIQUE.equals(sysUserService.checkEmailUnique(sysUser)))
        {
            return failure("修改用户'" + sysUser.getRealName() + "'失败，邮箱账号已存在");
        }
        sysUserService.saveUserRole(sysUser.getUserId(), Arrays.asList(sysUser.getRoleIds().split(",")));
        boolean result = sysUserService.update(sysUser);
        return decide(result);
    }

    /**
     * 用户批量删除接口
     * @param ids
     * @return: 结果
     */
    @DeleteMapping("batchRemove/{ids}")
    @ApiOperation(value="批量删除用户")
    @PreAuthorize("hasPermission('/system/user/remove','sys:user:remove')")
    @Logging(title = "批量删除用户",describe = "批量删除用户",type = BusinessType.REMOVE)
    public Result batchRemove(@PathVariable String ids){
        String[] idArr = Convert.toStrArray(ids);
        boolean b = Arrays.asList(idArr).contains("1309861917694623744");
        if (b) {
            return failure("不允许操作超级管理员用户");
        }
        boolean result = sysUserService.batchRemove(idArr);
        return decide(result);
    }

    /**
     * 用户删除接口
     * @param id
     * @return 结果
     */
    @DeleteMapping("remove/{id}")
    @ApiOperation(value="删除用户数据")
    @PreAuthorize("hasPermission('/system/user/remove','sys:user:remove')")
    @Logging(title = "删除用户",describe = "删除用户",type = BusinessType.REMOVE)
    public Result remove(@PathVariable String id){
        boolean result  = sysUserService.remove(id);
        return decide(result);
    }

    /**
     * 根据 username 获取菜单数据
     * @return 执行结果
     */
    @GetMapping("getUserMenu")
    @ApiOperation(value = "获取用户菜单数据")
    public List<SysMenu> getUserMenu(){
        SysUser sysUser = (SysUser) SecurityUtil.currentUser().getPrincipal();
        List<SysMenu> menus = sysUserService.getUserMenu(sysUser.getUsername());
        return sysUserService.toUserMenu(menus,"0");
    }

    /**
     * 开启用户
     * @param sysUser
     * @return 执行结果
     */
    @PutMapping("enable")
    @ApiOperation(value = "开启用户登录")
    @PreAuthorize("hasPermission('/system/user/edit','sys:user:edit')")
    @Logging(title = "修改用户",describe = "修改用户状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody SysUser sysUser){
        String allowed = sysUserService.checkUserAllowed(sysUser);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }
        sysUser.setEnable("1");
        boolean result = sysUserService.update(sysUser);
        return decide(result);
    }

    /**
     * 禁用用户
     * @param sysUser
     * @return 执行结果
     */
    @PutMapping("disable")
    @ApiOperation(value = "禁用用户登录")
    @PreAuthorize("hasPermission('/system/user/edit','sys:user:edit')")
    @Logging(title = "修改用户",describe = "修改用户状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody SysUser sysUser){
        String allowed = sysUserService.checkUserAllowed(sysUser);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }
        sysUser.setEnable("0");
        boolean result = sysUserService.update(sysUser);
        return decide(result);
    }

    /**
     * 用户密码修改视图
     */
    @GetMapping("editPassword")
    public ModelAndView editPasswordView(){
        return jumpPage(MODULE_PATH + "editPassword");
    }

    /**
     * 用户密码修改接口
     * @param oldPassword
     * @param confirmPassword
     * @param newPassword
     * @return 操作结果
     */
    @PostMapping("editPassword")
    //@PreAuthorize("hasPermission('/system/user/edit','sys:user:edit')")
    @Logging(title = "修改用户",describe = "修改密码",type = BusinessType.EDIT)
    public Result editPassword(String oldPassword, String newPassword, String confirmPassword){
        SysUser sysUser = (SysUser) ServletUtil.getSession().getAttribute("currentUser");
        SysUser editUser = sysUserService.getById(sysUser.getUserId());
        /*String allowed = sysUserService.checkUserAllowed(editUser);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }*/
        if (Strings.isBlank(confirmPassword)
                || Strings.isBlank(newPassword)
                || Strings.isBlank(oldPassword)) {
            return failure("输入不能为空");
        }
        if(!new BCryptPasswordEncoder().matches(oldPassword,editUser.getPassword())){
            return failure("密码验证失败");
        }
        if(!newPassword.equals(confirmPassword)){
            return failure("两次密码输入不一致");
        }
        editUser.setPassword(new BCryptPasswordEncoder().encode(newPassword));
        boolean result = sysUserService.update(editUser);
        return decide(result,"修改成功","修改失败");
    }

    /**
     * 用户个人资料视图
     */
    @GetMapping("center")
    @ApiOperation(value = "个人资料")
    public ModelAndView center(Model model){
        SysUser sysUser = (SysUser) SecurityUtil.currentUser().getPrincipal();
        model.addAttribute("userInfo",sysUserService.getById(sysUser.getUserId()));
        return jumpPage(MODULE_PATH + "center");
    }

    /**
     * 用户修改个人资料
     * @param sysUser
     * @return 返回用户资料修改结果
     */
    @PutMapping("personalUpdate")
    @ApiOperation(value="修改用户个人资料")
    //@PreAuthorize("hasPermission('/system/user/edit','sys:user:edit')")
    @Logging(title = "修改用户",describe = "修改用户个人资料",type = BusinessType.EDIT)
    public Result personalUpdate(@RequestBody SysUser sysUser){
        /*String allowed = sysUserService.checkUserAllowed(sysUser);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }*/
        boolean result = sysUserService.update(sysUser);
        return decide(result);
    }

    /**
     * 用户头像修改视图
     */
    @GetMapping("uploadProfile")
    public ModelAndView uploadProfile(Model model) {
        SysUser sysUser = (SysUser) SecurityUtil.currentUser().getPrincipal();
        SysUser sysUserInfo = sysUserService.getById(sysUser.getUserId());
        model.addAttribute("userId", sysUserInfo != null ? sysUserInfo.getUserId() : sysUser.getUserId());
        return jumpPage(MODULE_PATH + "uploadProfile");
    }

    /**
     * 头像上传接口
     * @param userId
     * @param file
     * @return 结果
     */
    @PostMapping("uploadProfile")
    //@PreAuthorize("hasPermission('/system/user/edit','sys:user:edit')")
    @Logging(title = "修改用户",describe = "修改用户头像",type = BusinessType.EDIT)
    public Result upload(@RequestParam("userId") String userId, @RequestParam("file") MultipartFile file) {
        String result = sysUserService.uploadProfile(userId, file);
        if (Strings.isNotBlank(result)) {
            return success(0, "上传成功", result);
        }
        return failure("上传失败");
    }

}