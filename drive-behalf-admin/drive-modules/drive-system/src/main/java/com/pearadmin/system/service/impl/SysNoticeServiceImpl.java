package com.pearadmin.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.SysNotice;
import com.pearadmin.system.domain.SysUser;
import com.pearadmin.system.mapper.SysNoticeMapper;
import com.pearadmin.system.service.ISysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 通知公告Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-29
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService {
    @Autowired
    private SysNoticeMapper sysNoticeMapper;

    /**
     * 查询通知公告
     *
     * @param noticeId 通知公告ID
     * @return 通知公告
     */
    @Override
    public SysNotice getById(Long noticeId) {
        return sysNoticeMapper.selectSysNoticeById(noticeId);
    }

    /**
     * 查询通知公告列表
     *
     * @param sysNotice 通知公告
     * @return 通知公告
     */
    @Override
    public List<SysNotice> list(SysNotice sysNotice) {
        return sysNoticeMapper.selectSysNoticeList(sysNotice);
    }

    /**
     * 查询通知公告
     *
     * @param sysNotice  通知公告
     * @param pageDomain
     * @return 通知公告 分页集合
     */
    @Override
    public PageInfo<SysNotice> listByPage(SysNotice sysNotice, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<SysNotice> data = sysNoticeMapper.selectSysNoticeList(sysNotice);
        return new PageInfo<>(data);
    }

    /**
     * 新增通知公告
     *
     * @param sysNotice 通知公告
     * @return 结果
     */
    @Override
    public int save(SysNotice sysNotice) {
        sysNotice.setCreatedUser(null != SecurityUtil.currentUser() ? Long.valueOf(((SysUser) SecurityUtil.currentUser().getPrincipal()).getUserId()) : 0L);
        sysNotice.setCreateTime(LocalDateTime.now());
        return sysNoticeMapper.insertSysNotice(sysNotice);
    }

    /**
     * 修改通知公告
     *
     * @param sysNotice 通知公告
     * @return 结果
     */
    @Override
    public int update(SysNotice sysNotice) {
        sysNotice.setModifiedTime(LocalDateTime.now());
        return sysNoticeMapper.updateSysNotice(sysNotice);
    }

    /**
     * 删除通知公告对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return sysNoticeMapper.deleteSysNoticeByIds(ids);
    }

    /**
     * 删除通知公告信息
     *
     * @param noticeId 通知公告ID
     * @return 结果
     */
    @Override
    public int remove(Long noticeId) {
        return sysNoticeMapper.deleteSysNoticeById(noticeId);
    }

}
