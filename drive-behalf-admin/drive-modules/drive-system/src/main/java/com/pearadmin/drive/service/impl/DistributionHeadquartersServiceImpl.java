package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Distribution;
import com.pearadmin.drive.mapper.DistributionHeadquartersMapper;
import com.pearadmin.drive.service.IDistributionHeadquartersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 总部提成分润明细Service业务层处理
 */
@Service
public class DistributionHeadquartersServiceImpl implements IDistributionHeadquartersService 
{
    @Autowired
    private DistributionHeadquartersMapper distributionHeadquartersMapper;

    /**
     * 查询总部提成分润明细
     * 
     * @param distributionId 总部提成分润明细ID
     * @return 总部提成分润明细
     */
    @Override
    public Distribution getById(Long distributionId)
    {
        return distributionHeadquartersMapper.selectDistributionHeadquartersById(distributionId);
    }

    /**
     * 查询总部提成分润明细列表
     * 
     * @param distributionHeadquarters 总部提成分润明细
     * @return 总部提成分润明细
     */
    @Override
    public List<Distribution> list(Distribution distributionHeadquarters)
    {
        return distributionHeadquartersMapper.selectDistributionHeadquartersList(distributionHeadquarters);
    }

    /**
     * 查询总部提成分润明细
     * @param distributionHeadquarters 总部提成分润明细
     * @param pageDomain
     * @return 总部提成分润明细 分页集合
     */
    @Override
    public PageInfo<Distribution> listByPage(Distribution distributionHeadquarters, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<Distribution> data = distributionHeadquartersMapper.selectDistributionHeadquartersList(distributionHeadquarters);
        return new PageInfo<>(data);
    }

    /**
     * 新增总部提成分润明细
     * 
     * @param distributionHeadquarters 总部提成分润明细
     * @return 结果
     */
    @Override
    public int save(Distribution distributionHeadquarters)
    {
        return distributionHeadquartersMapper.insertDistributionHeadquarters(distributionHeadquarters);
    }

    /**
     * 修改总部提成分润明细
     * 
     * @param distributionHeadquarters 总部提成分润明细
     * @return 结果
     */
    @Override
    public int update(Distribution distributionHeadquarters)
    {
        return distributionHeadquartersMapper.updateDistributionHeadquarters(distributionHeadquarters);
    }

    /**
     * 删除总部提成分润明细对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return distributionHeadquartersMapper.deleteDistributionHeadquartersByIds(ids);
    }

    /**
     * 删除总部提成分润明细信息
     * 
     * @param distributionId 总部提成分润明细ID
     * @return 结果
     */
    @Override
    public int remove(Long distributionId)
    {
        return distributionHeadquartersMapper.deleteDistributionHeadquartersById(distributionId);
    }
}
