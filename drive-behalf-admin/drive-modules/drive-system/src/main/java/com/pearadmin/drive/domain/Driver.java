package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 司机对象 driver_driver
 * 
 * @author huangtao
 * @date 2020-12-07
 */
@Data
public class Driver extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 司机ID */
    private Long userId;

    /** 头像 */
    private String avatar;

    /** 出生日期 */
    private LocalDate birthday;

    /** 姓名 */
    private String name;

    /** 手机号码 */
    private String phone;

    /** 性别 */
    private String sex;

    /** 状态 */
    private String userEnable;

    /** 余额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal money;

    /** 是否在线 */
    private String online;

    /** 代理商ID */
    private String agentId;

    /** 押金状态 */
    private String depositStatus;

    /** 押金金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal depositMoney;

    private Integer likes;
    private Double lastLongitude;
    private Double lastLatitude;
    private String lastAddress;
    private Long lastOnlineTime;
    private Integer totalOnlineTime;
    private Integer todayOnlineTime;
}