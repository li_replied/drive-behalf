package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Coupon;

/**
 * 代驾优惠券Service接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
public interface ICouponService extends BaseService<Coupon>
{
    /**
     * 修改状态
     * @param: coupon
     * @return: 操作结果
     * */
    boolean updateStatus(Coupon coupon);

}
