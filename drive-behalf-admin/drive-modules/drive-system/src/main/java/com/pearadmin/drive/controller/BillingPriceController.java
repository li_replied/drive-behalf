package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.BillingPrice;
import com.pearadmin.drive.service.IBillingPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 代驾计费价格Controller
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "price")
public class BillingPriceController extends BaseController
{
    private String prefix = "drive/price";

    @Autowired
    private IBillingPriceService billingPriceService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/price/main','drive:price:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询代驾计费价格列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/price/data','drive:price:data')")
    public ResultTable list(@ModelAttribute BillingPrice billingPrice, PageDomain pageDomain)
    {
        PageInfo<BillingPrice> pageInfo = billingPriceService.listByPage(billingPrice,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增代驾计费价格
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/price/add','drive:price:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存代驾计费价格
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/price/add','drive:price:add')")
    @Logging(title = "新增计费价格",describe = "新增计费价格",type = BusinessType.ADD)
    public Result save(@RequestBody BillingPrice billingPrice)
    {
        return decide(billingPriceService.batchInsertBillingPrice(billingPrice),"保存成功","保存失败，该区域价格已存在");
    }

    /**
     * 修改代驾计费价格
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/price/edit','drive:price:edit')")
    public ModelAndView edit(Long billId, String regionName, ModelMap mmap)
    {
        BillingPrice billingPrice = billingPriceService.getById(billId);
        mmap.put("billingPrice", billingPrice);
        mmap.put("regionName", regionName);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存代驾计费价格
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/price/edit','drive:price:edit')")
    @Logging(title = "修改计费价格",describe = "修改计费价格",type = BusinessType.EDIT)
    public Result update(@RequestBody BillingPrice billingPrice)
    {
        return decide(billingPriceService.update(billingPrice));
    }

    /**
     * 删除代驾计费价格
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/price/remove','drive:price:remove')")
    @Logging(title = "批量删除计费价格",describe = "批量删除计费价格",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(billingPriceService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{billId}")
    @PreAuthorize("hasPermission('/drive/price/remove','drive:price:remove')")
    @Logging(title = "删除计费价格",describe = "删除计费价格",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("billId") Long billId)
    {
        return decide(billingPriceService.remove(billId));
    }

}
