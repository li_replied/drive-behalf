package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.TripOrder;
import com.pearadmin.drive.service.ITripOrderService;
import com.pearadmin.system.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 代驾订单Controller
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "order")
public class TripOrderController extends BaseController
{
    private String prefix = "drive/order";

    @Autowired
    private ITripOrderService tripOrderService;
    @Autowired
    private ISysDeptService deptService;

    /**
     * 全部订单
     */
    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/order/main','drive:order:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 待分配订单
     */
    @GetMapping("/assigned")
    @PreAuthorize("hasPermission('/drive/order/assigned','drive:order:assigned')")
    public ModelAndView assigned()
    {
        return jumpPage(prefix + "/assigned");
    }

    /**
     * 已接单订单
     */
    @GetMapping("/accepted")
    @PreAuthorize("hasPermission('/drive/order/accepted','drive:order:accepted')")
    public ModelAndView accepted()
    {
        return jumpPage(prefix + "/accepted");
    }

    /**
     * 代价中订单
     */
    @GetMapping("/driving")
    @PreAuthorize("hasPermission('/drive/order/driving','drive:order:driving')")
    public ModelAndView driving()
    {
        return jumpPage(prefix + "/driving");
    }

    /**
     * 待支付订单
     */
    @GetMapping("/payment")
    @PreAuthorize("hasPermission('/drive/order/payment','drive:order:payment')")
    public ModelAndView payment()
    {
        return jumpPage(prefix + "/payment");
    }

    /**
     * 已完成订单
     */
    @GetMapping("/complete")
    @PreAuthorize("hasPermission('/drive/order/complete','drive:order:complete')")
    public ModelAndView complete()
    {
        return jumpPage(prefix + "/complete");
    }

    /**
     * 查询代驾订单列表数据
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/order/data','drive:order:data')")
    public ResultTable list(@ModelAttribute TripOrder tripOrder, PageDomain pageDomain)
    {
        PageInfo<TripOrder> pageInfo = tripOrderService.listByPage(tripOrder,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增代驾订单
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/order/add','drive:order:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存代驾订单
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/order/add','drive:order:add')")
    public Result save(@RequestBody TripOrder tripOrder)
    {
        return decide(tripOrderService.save(tripOrder));
    }

    /**
     * 修改代驾订单
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/order/edit','drive:order:edit')")
    public ModelAndView edit(Long tripOrderId, ModelMap mmap)
    {
        TripOrder tripOrder = tripOrderService.getById(tripOrderId);
        mmap.put("tripOrder", tripOrder);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存代驾订单
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/order/edit','drive:order:edit')")
    public Result update(@RequestBody TripOrder tripOrder)
    {
        return decide(tripOrderService.update(tripOrder));
    }

    /**
     * 批量删除
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/order/remove','drive:order:remove')")
    public Result batchRemove(String ids)
    {
        return decide(tripOrderService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{tripOrderId}")
    @PreAuthorize("hasPermission('/drive/order/remove','drive:order:remove')")
    public Result remove(@PathVariable("tripOrderId") Long tripOrderId)
    {
        return decide(tripOrderService.remove(tripOrderId));
    }

    /**
     * 查看订单明细
     */
    @GetMapping("/detail")
    @PreAuthorize("hasPermission('/drive/order/detail','drive:order:detail')")
    public ModelAndView detail(String orderNo, ModelMap mmap)
    {
        TripOrder tripOrder = tripOrderService.selectTripOrderByOrderNo(orderNo);
        mmap.put("tripOrder", tripOrder);
        return jumpPage(prefix + "/detail");
    }

    /**
     * 取消订单
     */
    @PutMapping("/cancel/{orderNumber}")
    @PreAuthorize("hasPermission('/drive/order/cancel','drive:order:cancel')")
    @Logging(title = "修改订单",describe = "取消订单",type = BusinessType.EDIT)
    public Result cancel(@PathVariable("orderNumber") String orderNumber)
    {
        return decide(tripOrderService.cancel(orderNumber));
    }

    /**
     * 批量取消代驾订单
     */
    @PutMapping( "/batchCancel")
    @PreAuthorize("hasPermission('/drive/order/cancel','drive:order:cancel')")
    @Logging(title = "修改订单",describe = "批量取消订单",type = BusinessType.EDIT)
    public Result batchCancel(String orderNumbers)
    {
        return decide(tripOrderService.cancelByIds(Convert.toStrArray(orderNumbers)));
    }
}
