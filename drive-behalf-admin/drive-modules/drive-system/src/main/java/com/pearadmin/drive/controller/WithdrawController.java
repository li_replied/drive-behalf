package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Withdraw;
import com.pearadmin.drive.service.IWithdrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 司机收支明细Controller
 *
 * @author huangtao
 * @date 2020-12-16
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "withdraw")
public class WithdrawController extends BaseController {
    private String prefix = "drive/withdraw";

    @Autowired
    private IWithdrawService withdrawService;

    /**
     * 司机收支余额明细
     */
    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/withdraw/main','drive:withdraw:main')")
    public ModelAndView main(Long driverId, ModelMap mmap)
    {
        mmap.put("driverId",driverId);
        return jumpPage(prefix + "/moneyDetail");
    }

    /**
     * 司机收支余额明细列表数据
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/withdraw/data','drive:withdraw:data')")
    public ResultTable data(@ModelAttribute Withdraw withdraw, PageDomain pageDomain) {
        PageInfo<Withdraw> pageInfo = withdrawService.listByPage(withdraw, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 提现明细
     */
    @GetMapping("/expend")
    @PreAuthorize("hasPermission('/drive/withdraw/expend','drive:withdraw:expend')")
    public ModelAndView expend() {
        return jumpPage(prefix + "/expend");
    }

    /**
     * 收入明细
     */
    @GetMapping("/income")
    @PreAuthorize("hasPermission('/drive/withdraw/income','drive:withdraw:income')")
    public ModelAndView income() {
        return jumpPage(prefix + "/income");
    }

    /**
     * 查询司机提现明细列表
     */
    @GetMapping("/expendData")
    @PreAuthorize("hasPermission('/drive/withdraw/expendData','drive:withdraw:expendData')")
    public ResultTable expendData(@ModelAttribute Withdraw withdraw, PageDomain pageDomain) {
        PageInfo<Withdraw> pageInfo = withdrawService.selectExpendPage(withdraw, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 查询司机收入明细列表
     */
    @GetMapping("/incomeData")
    @PreAuthorize("hasPermission('/drive/withdraw/incomeData','drive:withdraw:incomeData')")
    public ResultTable incomeData(@ModelAttribute Withdraw withdraw, PageDomain pageDomain) {
        PageInfo<Withdraw> pageInfo = withdrawService.selectIncomePage(withdraw, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 审核司机提现
     */
    @PutMapping("/audit/{withdrawId}")
    @PreAuthorize("hasPermission('/drive/withdraw/audit','drive:withdraw:audit')")
    @Logging(title = "审核提现", describe = "审核司机提现", type = BusinessType.EDIT)
    public Result audit(@PathVariable("withdrawId") Long withdrawId) {
        return decide(withdrawService.audit(withdrawId));
    }

    /**
     * 批量审核司机提现
     */
    @PutMapping("/batchAudit")
    @PreAuthorize("hasPermission('/drive/withdraw/audit','drive:withdraw:audit')")
    @Logging(title = "批量审核提现", describe = "批量审核司机提现", type = BusinessType.EDIT)
    public Result batchAudit(String ids) {
        return decide(withdrawService.auditByIds(Convert.toStrArray(ids)));
    }
}
