package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Apply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.pearadmin.drive.service.IApplyService;

/**
 * @Author huangtao
 * @Date 2021/03/18 09:43:47
 * @Description 【请填写功能名称】Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "apply")
public class ApplyController extends BaseController
{
    private String prefix = "drive/apply";

    @Autowired
    private IApplyService applyService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/apply/main','drive:apply:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/apply/data','drive:apply:data')")
    public ResultTable list(@ModelAttribute Apply apply, PageDomain pageDomain)
    {
        PageInfo<Apply> pageInfo = applyService.selectApplyPage(apply,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/apply/add','drive:apply:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/apply/add','drive:apply:add')")
    public Result save(@RequestBody Apply apply)
    {
        return decide(applyService.insertApply(apply));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/apply/edit','drive:apply:edit')")
    public ModelAndView edit(Long id, ModelMap mmap)
    {
        Apply apply = applyService.selectApplyById(id);
        mmap.put("apply", apply);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/apply/edit','drive:apply:edit')")
    public Result update(@RequestBody Apply apply)
    {
        return decide(applyService.updateApply(apply));
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/apply/remove','drive:apply:remove')")
    public Result batchRemove(String ids)
    {
        return decide(applyService.deleteApplyByIds(Convert.toStrArray(ids)));
    }

    /**
     * 删除【请填写功能名称】
     */
    @DeleteMapping("/remove/{id}")
    @PreAuthorize("hasPermission('/drive/apply/remove','drive:apply:remove')")
    public Result remove(@PathVariable("id") Long id)
    {
        return decide(applyService.deleteApplyById(id));
    }


    /**
     * 审批通过
     * @param
     * @return 执行结果
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/drive/apply/edit','drive:apply:edit')")
    @Logging(title = "修改司机",describe = "修改司机状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody Apply apply){
        apply.setStatus("enable");
        return decide(applyService.updateStatus(apply));
    }

    /**
     * 审批未通过
     * @param
     * @return 执行结果
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/drive/apply/edit','drive:apply:edit')")
    @Logging(title = "修改司机",describe = "修改司机状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody Apply apply){
        apply.setStatus("disable");
        return decide(applyService.updateStatus(apply));
    }
}
