package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Score;
import com.pearadmin.drive.service.IScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 客户积分收入提现Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "score")
public class ScoreController extends BaseController
{
    private String prefix = "drive/score";

    @Autowired
    private IScoreService scoreService;

    /**
     * 客户积分收入提现
     */
    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/score/main','drive:score:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询客户积分收入提现列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/score/data','drive:score:data')")
    public ResultTable list(@ModelAttribute Score score, PageDomain pageDomain)
    {
        PageInfo<Score> pageInfo = scoreService.listByPage(score,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 客户积分收支明细
     * @param passengerId
     */
    @GetMapping("/detail")
    //@PreAuthorize("hasPermission('/drive/score/main','drive:score:main')")
    public ModelAndView detail(Long passengerId, ModelMap mmap)
    {
        mmap.put("passengerId",passengerId);
        return jumpPage(prefix + "/moneyDetail");
    }

    /**
     * 审核司机提现
     */
    @PutMapping("/audit/{withdrawId}")
    @PreAuthorize("hasPermission('/drive/score/audit','drive:score:audit')")
    @Logging(title = "审核提现", describe = "审核客户提现", type = BusinessType.EDIT)
    public Result audit(@PathVariable("withdrawId") Long withdrawId) {
        return decide(scoreService.audit(withdrawId));
    }

    /**
     * 批量审核司机提现
     */
    @PutMapping("/batchAudit")
    @PreAuthorize("hasPermission('/drive/score/audit','drive:score:audit')")
    @Logging(title = "批量审核提现", describe = "批量审核客户提现", type = BusinessType.EDIT)
    public Result batchAudit(String ids) {
        return decide(scoreService.auditByIds(Convert.toStrArray(ids)));
    }
}