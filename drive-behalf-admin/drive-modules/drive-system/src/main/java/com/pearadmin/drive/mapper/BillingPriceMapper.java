package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.BillingPrice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 代驾计费价格Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Mapper
public interface BillingPriceMapper
{
    /**
     * 查询代驾计费价格
     * 
     * @param billId 代驾计费价格ID
     * @return 代驾计费价格
     */
    BillingPrice selectBillingPriceById(Long billId);

    /**
     * 查询代驾计费价格列表
     * 
     * @param billingPrice 代驾计费价格
     * @return 代驾计费价格集合
     */
    List<BillingPrice> selectBillingPriceList(BillingPrice billingPrice);

    /**
     * 新增代驾计费价格
     * 
     * @param billingPrice 代驾计费价格
     * @return 结果
     */
    int insertBillingPrice(BillingPrice billingPrice);

    /**
     * 修改代驾计费价格
     * 
     * @param billingPrice 代驾计费价格
     * @return 结果
     */
    int updateBillingPrice(BillingPrice billingPrice);

    /**
     * 删除代驾计费价格
     * 
     * @param billId 代驾计费价格ID
     * @return 结果
     */
    int deleteBillingPriceById(Long billId);

    /**
     * 批量删除代驾计费价格
     * 
     * @param billIds 需要删除的数据ID
     * @return 结果
     */
    int deleteBillingPriceByIds(String[] billIds);

    /**
     * 查询代驾计费价格
     *
     * @param regionId 区域ID
     * @return 代驾计费价格
     */
    int selectPriceRowsByRegionId(String regionId);

    /**
     * 新增区域价目
     * @param billingPrices
     * @return 结果
     */
    int batchInsert(List<BillingPrice> billingPrices);
}
