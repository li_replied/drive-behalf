package com.pearadmin.drive.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Invoice;
import com.pearadmin.drive.domain.InvoiceTitle;

/**
 * @Author huangtao
 * @Date 2021/01/08 15:49:34
 * @Description 发票抬头Service接口
 */
public interface IInvoiceTitleService
{
    /**
     * 查询发票抬头
     * 
     * @param titleId 发票抬头ID
     * @return 发票抬头
     */
    InvoiceTitle selectInvoiceTitleById(Long titleId);

    /**
    * 查询发票抬头
     * @param invoiceTitle 发票抬头
     * @param pageDomain
     * @return 发票抬头 分页集合
     */
    PageInfo<InvoiceTitle> selectInvoiceTitlePage(InvoiceTitle invoiceTitle, PageDomain pageDomain);

    /**
     * 查询发票
     *
     * @param invoiceId 发票ID
     * @return 发票
     */
    Invoice selectInvoiceById(Long invoiceId);

    /**
     * 查询发票
     * @param invoice 发票
     * @param pageDomain
     * @return 发票 分页集合
     */
    PageInfo<Invoice> selectInvoicePage(Invoice invoice, PageDomain pageDomain);

    /**
     * 发票审核
     * @param: invoiceId 发票ID
     * @return: 操作结果
     */
    boolean audit(Long invoiceId);
}
