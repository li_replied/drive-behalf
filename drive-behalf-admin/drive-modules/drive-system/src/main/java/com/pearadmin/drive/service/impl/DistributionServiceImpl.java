package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Distribution;
import com.pearadmin.drive.mapper.DistributionMapper;
import com.pearadmin.drive.mapper.PassengerMapper;
import com.pearadmin.drive.service.IDistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/27 09:01:17
 * @Description 提成分润明细Service业务层处理
 */
@Service
public class DistributionServiceImpl implements IDistributionService 
{
    @Autowired
    private DistributionMapper distributionMapper;
    @Autowired
    private PassengerMapper passengerMapper;

    /**
     * 查询提成分润明细
     *
     * @param distributionId 提成分润明细ID
     * @return 提成分润明细
     */
    @Override
    public Distribution getById(Long distributionId)
    {
        return distributionMapper.selectDistributionById(distributionId);
    }

    /**
     * 查询提成分润明细列表
     *
     * @param distribution 提成分润明细
     * @return 提成分润明细
     */
    @Override
    public List<Distribution> list(Distribution distribution)
    {
        return distributionMapper.selectDistributionList(distribution);
    }

    /**
     * 查询提成分润明细
     * @param distribution 提成分润明细
     * @param pageDomain
     * @return 提成分润明细 分页集合
     */
    @Override
    public PageInfo<Distribution> listByPage(Distribution distribution, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<Distribution> data = distributionMapper.selectDistributionList(distribution);
        data.forEach(temp->temp.setUserName(passengerMapper.selectPassengerNameById(temp.getUserId())));
        return new PageInfo<>(data);
    }

    /**
     * 新增提成分润明细
     *
     * @param distribution 提成分润明细
     * @return 结果
     */
    @Override
    public int save(Distribution distribution)
    {
        return distributionMapper.insertDistribution(distribution);
    }

    /**
     * 修改提成分润明细
     *
     * @param distribution 提成分润明细
     * @return 结果
     */
    @Override
    public int update(Distribution distribution)
    {
        return distributionMapper.updateDistribution(distribution);
    }

    /**
     * 删除提成分润明细对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return distributionMapper.deleteDistributionByIds(ids);
    }

    /**
     * 删除提成分润明细信息
     *
     * @param distributionId 提成分润明细ID
     * @return 结果
     */
    @Override
    public int remove(Long distributionId)
    {
        return distributionMapper.deleteDistributionById(distributionId);
    }
}
