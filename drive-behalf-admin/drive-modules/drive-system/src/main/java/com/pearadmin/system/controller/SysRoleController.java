package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.common.web.domain.response.module.ResultTree;
import com.pearadmin.system.domain.SysRole;
import com.pearadmin.system.service.ISysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * Describe: 角 色 控 制 器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "role")
@Api(value="角色controller",tags={"角色操作接口"})
public class SysRoleController extends BaseController {

    /**
     * 基础路径
     */
    private static String MODULE_PATH = "system/role/";

    /**
     * 角色模块服务
     */
    @Resource
    private ISysRoleService sysRoleService;

    /**
     * 获取角色列表视图
     */
    @GetMapping("main")
    @ApiOperation(value="获取角色列表视图")
    @PreAuthorize("hasPermission('/system/role/main','sys:role:main')")
    public ModelAndView main(){
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 获取角色列表数据
     * Param SysRole PageDomain
     * Return 角色列表数据
     */
    @GetMapping("data")
    @ApiOperation(value="获取角色列表数据")
    @PreAuthorize("hasPermission('/system/role/data','sys:role:data')")
    public ResultTable data(PageDomain pageDomain, SysRole param){
       PageInfo<SysRole> pageInfo = sysRoleService.page(param,pageDomain);
       return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 角色新增视图
     */
    @GetMapping("add")
    @ApiOperation(value="获取角色新增视图")
    @PreAuthorize("hasPermission('/system/role/add','sys:role:add')")
    public ModelAndView add(){
        return jumpPage(MODULE_PATH + "add");
    }

    /**
     * 角色信息新增保存
     * Param SysRole
     * Return 执行结果
     */
    @PostMapping("save")
    @ApiOperation(value="保存角色数据")
    @PreAuthorize("hasPermission('/system/role/add','sys:role:add')")
    @Logging(title = "新增角色",describe = "新增角色",type = BusinessType.ADD)
    public Result save(@RequestBody SysRole sysRole){
        if (UserConstants.ROLE_NAME_NOT_UNIQUE.equals(sysRoleService.checkRoleNameUnique(sysRole)))
        {
            return failure("新增角色'" + sysRole.getRoleName() + "'失败，角色名称已存在");
        }
        else if (UserConstants.ROLE_KEY_NOT_UNIQUE.equals(sysRoleService.checkRoleKeyUnique(sysRole)))
        {
            return failure("新增角色'" + sysRole.getRoleName() + "'失败，角色权限已存在");
        }
        sysRole.setRoleId(SequenceUtil.makeStringId());
        boolean result = sysRoleService.save(sysRole);
        return decide(result);
    }

    /**
     * 角色修改视图
     * Param roleId
     */
    @GetMapping("edit")
    @ApiOperation(value="获取角色修改视图")
    @PreAuthorize("hasPermission('/system/role/edit','sys:role:edit')")
    public ModelAndView edit(ModelAndView modelAndView,String roleId){
        modelAndView.addObject("sysRole",sysRoleService.getById(roleId));
        modelAndView.setViewName(MODULE_PATH + "edit");
        return modelAndView;
    }

    /**
     * 修改角色信息
     * Param SysRole
     * Return 执行结果
     */
    @PutMapping("update")
    @ApiOperation(value="修改角色数据")
    @PreAuthorize("hasPermission('/system/role/edit','sys:role:edit')")
    @Logging(title = "修改角色",describe = "修改角色",type = BusinessType.EDIT)
    public Result update(@RequestBody SysRole sysRole){
        String allowed = sysRoleService.checkRoleAllowed(sysRole);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }
        if (UserConstants.ROLE_NAME_NOT_UNIQUE.equals(sysRoleService.checkRoleNameUnique(sysRole)))
        {
            return failure("修改角色'" + sysRole.getRoleName() + "'失败，角色名称已存在");
        }
        else if (UserConstants.ROLE_KEY_NOT_UNIQUE.equals(sysRoleService.checkRoleKeyUnique(sysRole)))
        {
            return failure("修改角色'" + sysRole.getRoleName() + "'失败，角色权限已存在");
        }
        boolean result = sysRoleService.update(sysRole);
        return decide(result);
    }

    /**
     * 角色授权视图
     * Param roleId
     */
    @GetMapping("power")
    @ApiOperation(value="获取分配角色权限视图")
    @PreAuthorize("hasPermission('/system/role/power','sys:role:power')")
    public ModelAndView power(Model model, String roleId){
        model.addAttribute("roleId",roleId);
        return jumpPage(MODULE_PATH + "power");
    }

    /**
     * 保存角色权限
     * Param RoleId PowerIds
     * Return 结果
     */
    @PutMapping("saveRolePower")
    @ApiOperation(value="保存角色权限数据")
    @PreAuthorize("hasPermission('/system/role/power','sys:role:power')")
    @Logging(title = "修改角色",describe = "保存角色权限",type = BusinessType.EDIT)
    public Result saveRolePower(String roleId, String powerIds){
        SysRole sysRole = new SysRole();
        sysRole.setRoleId(roleId);
        String allowed = sysRoleService.checkRoleAllowed(sysRole);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }
        boolean result = sysRoleService.saveRolePower(roleId, Arrays.asList(powerIds.split(",")));
        return decide(result);
    }

    /**
     * 获取角色权限
     * Param RoleId
     * Return ResultTree
     */
    @GetMapping("getRolePower")
    @ApiOperation(value="获取角色权限数据")
    @PreAuthorize("hasPermission('/system/role/power','sys:role:power')")
    public ResultTree getRolePower(String roleId){
        return dataTree(sysRoleService.getRolePower(roleId));
    }

    /**
     * 删除
     * Param: id
     * Return: 结果
     */
    @DeleteMapping("remove/{id}")
    @ApiOperation(value="删除角色数据")
    @PreAuthorize("hasPermission('/system/role/remove','sys:role:remove')")
    @Logging(title = "删除角色",describe = "删除角色",type = BusinessType.REMOVE)
    public Result remove(@PathVariable String id){
        boolean result  = sysRoleService.remove(id);
        return decide(result);
    }

    /**
     * 批量删除
     * Param: ids
     * Return: 结果
     */
    @DeleteMapping("batchRemove/{ids}")
    @ApiOperation(value="批量删除角色数据")
    @PreAuthorize("hasPermission('/system/role/remove','sys:role:remove')")
    @Logging(title = "批量删除角色",describe = "批量删除角色",type = BusinessType.REMOVE)
    public Result batchRemove(@PathVariable String ids){
        String[] idArr = Convert.toStrArray(ids);
        boolean b = Arrays.asList(idArr).contains("1309851245195821056");
        if (b) {
            return failure("不允许操作超级管理员角色");
        }
        boolean result = sysRoleService.batchRemove(idArr);
        return decide(result);
    }

    /**
     * 启用角色
     * Param: sysRole
     * Return: 结果
     */
    @PutMapping("enable")
    @ApiOperation(value="启用角色")
    @PreAuthorize("hasPermission('/system/role/edit','sys:role:edit')")
    @Logging(title = "修改角色",describe = "修改角色状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody SysRole sysRole){
        String allowed = sysRoleService.checkRoleAllowed(sysRole);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }
        sysRole.setEnable("0");
        boolean result =  sysRoleService.update(sysRole);
        return decide(result);
    }

    /**
     * 禁用角色
     * Param: sysRole
     * Return: 结果
     */
    @PutMapping("disable")
    @ApiOperation(value="禁用角色")
    @PreAuthorize("hasPermission('/system/role/edit','sys:role:edit')")
    @Logging(title = "修改角色",describe = "修改角色状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody SysRole sysRole){
        String allowed = sysRoleService.checkRoleAllowed(sysRole);
        if (StringUtils.isNotEmpty(allowed)) {
            return failure(allowed);
        }
        sysRole.setEnable("1");
        boolean result =  sysRoleService.update(sysRole);
        return decide(result);
    }

}