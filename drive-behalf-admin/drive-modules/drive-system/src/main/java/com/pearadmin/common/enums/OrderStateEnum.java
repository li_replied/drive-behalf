package com.pearadmin.common.enums;

public enum OrderStateEnum {
    //ASSIGNED, REDISTRIBUTION, OVERTIME, CANCEL, ACCEPTED, CONFIRMDRIVE, DRIVE, PAYMENT, PAYING, COMPLETE;

    /**
     * 待分配
     */
    ASSIGNED,
    /**
     * 再分配
     */
    REDISTRIBUTION,
    /**
     * 超时
     */
    OVERTIME,
    /**
     * 撤销
     */
    CANCEL,
    /**
     * 已接单
     */
    ACCEPTED,
    /**
     * 确认代驾
     */
    CONFIRMDRIVE,
    /**
     * 开始代驾
     */
    DRIVE,
    /**
     * 待支付
     */
    PAYMENT,
    /**
     * 支付中
     */
    PAYING,
    /**
     * 完成
     */
    COMPLETE

}
