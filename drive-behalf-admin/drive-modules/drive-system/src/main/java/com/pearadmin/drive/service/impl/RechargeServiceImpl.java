package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Recharge;
import com.pearadmin.drive.mapper.PassengerMapper;
import com.pearadmin.drive.mapper.RechargeMapper;
import com.pearadmin.drive.service.IRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 客户充值Service业务层处理
 */
@Service
public class RechargeServiceImpl implements IRechargeService 
{
    @Autowired
    private RechargeMapper rechargeMapper;
    @Autowired
    private PassengerMapper passengerMapper;

    /**
     * 查询客户充值
     * 
     * @param rechargeId 客户充值ID
     * @return 客户充值
     */
    @Override
    public Recharge getById(Long rechargeId)
    {
        return rechargeMapper.selectRechargeById(rechargeId);
    }

    /**
     * 查询客户充值列表
     * 
     * @param recharge 客户充值
     * @return 客户充值
     */
    @Override
    public List<Recharge> list(Recharge recharge)
    {
        return rechargeMapper.selectRechargeList(recharge);
    }

    /**
     * 查询客户充值
     * @param recharge 客户充值
     * @param pageDomain
     * @return 客户充值 分页集合
     */
    @Override
    public PageInfo<Recharge> listByPage(Recharge recharge, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<Recharge> data = rechargeMapper.selectRechargeList(recharge);
        data.forEach(temp->temp.setPassengerName(passengerMapper.selectPassengerNameById(temp.getPassengerId())));
        return new PageInfo<>(data);
    }

    /**
     * 新增客户充值
     * 
     * @param recharge 客户充值
     * @return 结果
     */
    @Override
    public int save(Recharge recharge)
    {
        return rechargeMapper.insertRecharge(recharge);
    }

    /**
     * 修改客户充值
     * 
     * @param recharge 客户充值
     * @return 结果
     */
    @Override
    public int update(Recharge recharge)
    {
        recharge.setModifiedTime(LocalDateTime.now());
        return rechargeMapper.updateRecharge(recharge);
    }

    /**
     * 删除客户充值对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return rechargeMapper.deleteRechargeByIds(ids);
    }

    /**
     * 删除客户充值信息
     * 
     * @param rechargeId 客户充值ID
     * @return 结果
     */
    @Override
    public int remove(Long rechargeId)
    {
        return rechargeMapper.deleteRechargeById(rechargeId);
    }
}
