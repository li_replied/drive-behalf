package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Distribution;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 总部提成分润明细Mapper接口
 */
@Mapper
public interface DistributionHeadquartersMapper 
{
    /**
     * 查询总部提成分润明细
     * 
     * @param distributionId 总部提成分润明细ID
     * @return 总部提成分润明细
     */
    Distribution selectDistributionHeadquartersById(Long distributionId);

    /**
     * 查询总部提成分润明细列表
     * 
     * @param distributionHeadquarters 总部提成分润明细
     * @return 总部提成分润明细集合
     */
    List<Distribution> selectDistributionHeadquartersList(Distribution distributionHeadquarters);

    /**
     * 新增总部提成分润明细
     * 
     * @param distributionHeadquarters 总部提成分润明细
     * @return 结果
     */
    int insertDistributionHeadquarters(Distribution distributionHeadquarters);

    /**
     * 修改总部提成分润明细
     * 
     * @param distributionHeadquarters 总部提成分润明细
     * @return 结果
     */
    int updateDistributionHeadquarters(Distribution distributionHeadquarters);

    /**
     * 删除总部提成分润明细
     * 
     * @param distributionId 总部提成分润明细ID
     * @return 结果
     */
    int deleteDistributionHeadquartersById(Long distributionId);

    /**
     * 批量删除总部提成分润明细
     * 
     * @param distributionIds 需要删除的数据ID
     * @return 结果
     */
    int deleteDistributionHeadquartersByIds(String[] distributionIds);

}
