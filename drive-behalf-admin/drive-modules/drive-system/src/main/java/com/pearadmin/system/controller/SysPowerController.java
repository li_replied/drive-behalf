package com.pearadmin.system.controller;

import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.common.web.domain.response.module.ResultTree;
import com.pearadmin.system.domain.SysPower;
import com.pearadmin.system.service.ISysPowerService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Describe: 权 限 控 制 器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "power")
public class SysPowerController extends BaseController {

    /**
     * 基础路径
     */
    private static String MODULE_PATH = "system/power/";

    /**
     * 权限模块服务
     */
    @Resource
    private ISysPowerService sysPowerService;

    /**
     * 获取权限列表视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/system/power/main','sys:power:main')")
    public ModelAndView main(){
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 获取权限列表数据
     * Param sysPower
     * Return 权限列表数据
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/system/power/data','sys:power:data')")
    public ResultTable data(SysPower sysPower){
        return treeTable(sysPowerService.list(sysPower));
    }

    /**
     * 权限新增视图
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/system/power/add','sys:power:add')")
    public ModelAndView add(){
        return jumpPage(MODULE_PATH + "add");
    }

    /**
     * 权限修改视图
     * Param powerId
     */
    @GetMapping("edit")
    @PreAuthorize("hasPermission('/system/power/edit','sys:power:edit')")
    public ModelAndView edit(Model model, String powerId){
        model.addAttribute("sysPower",sysPowerService.getById(powerId));
        return jumpPage(MODULE_PATH + "edit");
    }

    /**
     * 权限菜单新增保存
     * Param: SysPower
     * Return: ResuBean
     */
    @PostMapping("save")
    @PreAuthorize("hasPermission('/system/power/add','sys:power:add')")
    @Logging(title = "新增权限菜单",describe = "新增权限菜单",type = BusinessType.ADD)
    public Result save(@RequestBody SysPower sysPower){
        if(Strings.isBlank(sysPower.getParentId())){
            return failure("请选择上级菜单");
        }
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(sysPowerService.checkMenuNameUnique(sysPower)))
        {
            return failure("新增菜单'" + sysPower.getPowerName() + "'失败，菜单名称已存在");
        }
        sysPower.setPowerId(SequenceUtil.makeStringId());
        boolean result = sysPowerService.save(sysPower);
        return decide(result);
    }

    /**
     * 权限菜单修改保存
     * Param SysPower
     * Return 执行结果
     */
    @PutMapping("update")
    @PreAuthorize("hasPermission('/system/power/edit','sys:power:edit')")
    @Logging(title = "修改权限菜单",describe = "修改权限菜单",type = BusinessType.EDIT)
    public Result update(@RequestBody SysPower sysPower){
        if(Strings.isBlank(sysPower.getParentId())){
            return failure("请选择上级菜单");
        }
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(sysPowerService.checkMenuNameUnique(sysPower)))
        {
            return failure("修改菜单'" + sysPower.getPowerName() + "'失败，菜单名称已存在");
        }
        boolean result = sysPowerService.update(sysPower);
        return decide(result);
    }

    /**
     * 根据 id 进行删除
     * Param id
     * Return Result
     */
    @DeleteMapping("remove/{id}")
    @PreAuthorize("hasPermission('/system/power/remove','sys:power:remove')")
    @Logging(title = "删除权限菜单",describe = "删除权限菜单",type = BusinessType.REMOVE)
    public Result remove(@PathVariable String id){
        boolean result = sysPowerService.remove(id);
        return decide(result);
    }

    /**
     * 获取父级权限选择数据
     * Param sysPower
     * Return ResultTree
     */
    @GetMapping("selectParent")
    public ResultTree selectParent(SysPower sysPower){
        Map<String, Object> params = new HashMap<>();
        params.put("exclusionType", "2");
        sysPower.setParams(params);
        List<SysPower> list = sysPowerService.list(sysPower);
        SysPower basePower = new SysPower();
        basePower.setPowerName("顶级菜单");
        basePower.setPowerId("0");
        basePower.setParentId("-1");
        list.add(basePower);
        return dataTree(list);
    }

    /**
     * 开启用户
     * Param sysPower
     * Return Result
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/system/power/edit','sys:power:edit')")
    @Logging(title = "修改权限菜单",describe = "修改权限菜单状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody SysPower sysPower){
        sysPower.setEnable(true);
        boolean result = sysPowerService.update(sysPower);
        return decide(result);
    }

    /**
     * 禁用用户
     * Param sysPower
     * Return Result
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/system/power/edit','sys:power:edit')")
    @Logging(title = "修改权限菜单",describe = "修改权限菜单状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody SysPower sysPower){
        sysPower.setEnable(false);
        boolean result = sysPowerService.update(sysPower);
        return decide(result);
    }
}
