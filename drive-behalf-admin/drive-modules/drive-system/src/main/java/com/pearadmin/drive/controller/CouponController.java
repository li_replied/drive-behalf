package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Coupon;
import com.pearadmin.drive.service.ICouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 代驾优惠券Controller
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "coupon")
public class CouponController extends BaseController
{
    private String prefix = "drive/coupon";

    @Autowired
    private ICouponService couponService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/coupon/main','drive:coupon:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询代驾优惠券列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/coupon/data','drive:coupon:data')")
    public ResultTable list(@ModelAttribute Coupon coupon, PageDomain pageDomain)
    {
        PageInfo<Coupon> pageInfo = couponService.listByPage(coupon,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增代驾优惠券
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/coupon/add','drive:coupon:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存代驾优惠券
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/coupon/add','drive:coupon:add')")
    @Logging(title = "新增优惠券",describe = "新增优惠券",type = BusinessType.ADD)
    public Result save(@RequestBody Coupon coupon)
    {
        return decide(couponService.save(coupon));
    }

    /**
     * 修改代驾优惠券
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/coupon/edit','drive:coupon:edit')")
    public ModelAndView edit(Long couponId, ModelMap mmap)
    {
        Coupon coupon = couponService.getById(couponId);
        mmap.put("coupon", coupon);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存代驾优惠券
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/coupon/edit','drive:coupon:edit')")
    @Logging(title = "修改优惠券",describe = "修改优惠券",type = BusinessType.EDIT)
    public Result update(@RequestBody Coupon coupon)
    {
        return decide(couponService.update(coupon));
    }

    /**
     * 批量删除
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/coupon/remove','drive:coupon:remove')")
    @Logging(title = "批量删除优惠券",describe = "批量删除优惠券",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(couponService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{couponId}")
    @PreAuthorize("hasPermission('/drive/coupon/remove','drive:coupon:remove')")
    @Logging(title = "删除优惠券",describe = "删除优惠券",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("couponId") Long couponId)
    {
        return decide(couponService.remove(couponId));
    }

    /**
     * 开启优惠券
     * @param coupon
     * @return 执行结果
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/drive/coupon/edit','drive:coupon:edit')")
    @Logging(title = "修改状态",describe = "修改状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody Coupon coupon){
        coupon.setCouponStatus(DriveConstants.STATE_YES);
        return decide(couponService.updateStatus(coupon));
    }

    /**
     * 禁用优惠券
     * @param coupon
     * @return 执行结果
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/drive/coupon/edit','drive:coupon:edit')")
    @Logging(title = "修改状态",describe = "修改状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody Coupon coupon){
        coupon.setCouponStatus(DriveConstants.STATE_NO);
        return decide(couponService.updateStatus(coupon));
    }
}
