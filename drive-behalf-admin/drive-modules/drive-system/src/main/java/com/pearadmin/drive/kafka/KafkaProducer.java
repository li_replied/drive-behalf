package com.pearadmin.drive.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Service
@Slf4j
public class KafkaProducer {
    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * 发送消息
     *
     * @param kafkaMessage
     */
    public void subscribe(String address, LocalDateTime date) {
        kafkaTemplate.send("subscribe", "{\"address\":\"" + address + "\",\"date\":\"" + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\"}");
    }
}
