package com.pearadmin.drive.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 数据统计Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-24
 */
@Mapper
public interface DataCountMapper
{
    /**
     * 统计近七天内乘客/司机注册量
     * @param params
     * @return
     */
    List<Map<String, Object>> countUsersNum(Map<String,Object> params);

    /**
     * 统计时间段内订单量
     * @param params
     * @return
     */
    List<Map<String, Object>> countOrdersNum(Map<String,Object> params);

    /**
     * 统计每月的订单量
     * @param params
     * @return
     */
    List<Map<String, Object>> countOrdersNumByMonths(Map<String,Object> params);

    /**
     * 统计时间段内订单收益
     * @param params
     * @return
     */
    List<Map<String, Object>> countOrdersProfit(Map<String,Object> params);

    /**
     * 统计一周内订单量
     */
    List<Map<String, Object>> countOrdersByWeek(Map<String,Object> params);

    /**
     * 统计首页订单相关数据
     */
    Map<String, Object> countHomeOrders(Map<String,Object> params);
}
