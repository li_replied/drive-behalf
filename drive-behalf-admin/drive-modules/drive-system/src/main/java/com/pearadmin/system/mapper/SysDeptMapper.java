package com.pearadmin.system.mapper;

import com.pearadmin.system.domain.SysDept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Describe: 部门接口
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Mapper
public interface SysDeptMapper {

    /**
     * Describe: 查询部门列表
     * Param: SysDept
     * Return: List<SysDept>
     * */
    List<SysDept> selectList(SysDept param);

    /**
     * Describe: 添加部门数据
     * Param: SysDept
     * Return: 执行结果
     * */
    Integer insert(SysDept sysDept);

    /**
     * Describe: 根据 Id 查询部门
     * Param: id
     * Return: SysDept
     * */
    SysDept selectById(@Param("id") String id);

    /**
     * Describe: 根据 Id 修改部门
     * Param: SysDept
     * Return: Integer
     * */
    Integer updateById(SysDept sysDept);

    /**
     * Describe: 根据 Id 删除部门
     * Param: id
     * Return: Integer
     * */
    Integer deleteById(String id);

    /**
     * Describe: 根据 Id 批量删除
     * Param: ids
     * Return: Integer
     * */
    Integer deleteByIds(String[] ids);

    /**
     * 校验部门名称是否唯一
     * @param deptName 部门名称
     * @param parentId 父部门ID
     * @return 结果
     */
    SysDept checkDeptNameUnique(@Param("deptName") String deptName, @Param("parentId") String parentId);

    /**
     * 查询部门人数
     * @param dept 部门信息
     * @return 结果
     */
    int selectDeptCount(SysDept dept);

    /**
     * 查询部门是否存在用户
     * @param deptId 部门ID
     * @return 结果
     */
    int checkDeptExistUser(String deptId);

    /**
     * 获取部门名称
     * @param deptId
     */
    @Select("select dept_name from sys_dept where dept_id = #{deptId}")
    String getDeptNameById(@Param("deptId") String deptId);

    /**
     * 获取部门名称
     * @param deptId
     */
    @Select("select dept_id deptId, agent_level agentLevel, agent_alone agentAlone, region_id regionId from sys_dept where dept_id = #{deptId} \n" +
            "and agent_flag = 'YES' and agent_level is not null ")
    SysDept getDeptAgentById(@Param("deptId") String deptId);

    /**
     * 查询代理商余额列表
     *
     * @param sysDept 代理商
     * @return 代理商集合
     */
    List<SysDept> selectAgentMoneyList(SysDept sysDept);

    /**
     * 代理商余额提现
     *
     * @param sysDept 代理商
     * @return 结果
     */
    int updateAgentMoney(SysDept sysDept);

    /**
     * 获取区域ID
     * @param deptId
     */
    @Select("select region_id from sys_dept where dept_id = #{deptId} and agent_flag = 'YES'")
    String getRegionIdById(@Param("deptId") String deptId);
}
