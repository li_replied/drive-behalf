package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Invoice;
import com.pearadmin.drive.domain.InvoiceTitle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/08 15:49:34
 * @Description 发票抬头Mapper接口
 */
@Mapper
public interface InvoiceTitleMapper 
{
    /**
     * 查询发票抬头
     * 
     * @param titleId 发票抬头ID
     * @return 发票抬头
     */
    InvoiceTitle selectInvoiceTitleById(Long titleId);

    /**
     * 查询发票抬头列表
     * 
     * @param invoiceTitle 发票抬头
     * @return 发票抬头集合
     */
    List<InvoiceTitle> selectInvoiceTitleList(InvoiceTitle invoiceTitle);

    /**
     * 查询发票
     *
     * @param invoiceId 发票ID
     * @return 发票
     */
    Invoice selectInvoiceById(Long invoiceId);

    /**
     * 查询发票列表
     *
     * @param invoice 发票
     * @return 发票抬头集合
     */
    List<Invoice> selectInvoiceList(Invoice invoice);

    /**
     * 发票审核
     * @param: invoice 发票
     * @return: 操作结果
     */
    int audit(Invoice invoice);
}
