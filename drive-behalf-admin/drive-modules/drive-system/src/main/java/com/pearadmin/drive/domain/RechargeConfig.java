package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 充值金额配置对象 driver_recharge_config
 */
@Data
public class RechargeConfig extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long configId;

    /** 充值金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal rechargeAmount;

    /** 赠送金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal giftAmount;

    /** 状态（YES是，NO否） */
    private String state;

}
