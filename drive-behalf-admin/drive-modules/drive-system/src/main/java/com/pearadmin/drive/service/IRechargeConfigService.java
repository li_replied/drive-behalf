package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.RechargeConfig;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 充值金额配置Service接口
 */
public interface IRechargeConfigService extends BaseService<RechargeConfig>
{

}
