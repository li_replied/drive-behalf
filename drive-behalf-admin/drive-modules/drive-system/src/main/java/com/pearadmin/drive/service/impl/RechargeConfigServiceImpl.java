package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.RechargeConfig;
import com.pearadmin.drive.mapper.RechargeConfigMapper;
import com.pearadmin.drive.service.IRechargeConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 充值金额配置Service业务层处理
 */
@Service
public class RechargeConfigServiceImpl implements IRechargeConfigService 
{
    @Autowired
    private RechargeConfigMapper rechargeConfigMapper;

    /**
     * 查询充值金额配置
     * 
     * @param configId 充值金额配置ID
     * @return 充值金额配置
     */
    @Override
    public RechargeConfig getById(Long configId)
    {
        return rechargeConfigMapper.selectRechargeConfigById(configId);
    }

    /**
     * 查询充值金额配置列表
     * 
     * @param rechargeConfig 充值金额配置
     * @return 充值金额配置
     */
    @Override
    public List<RechargeConfig> list(RechargeConfig rechargeConfig)
    {
        return rechargeConfigMapper.selectRechargeConfigList(rechargeConfig);
    }

    /**
     * 查询充值金额配置
     * @param rechargeConfig 充值金额配置
     * @param pageDomain
     * @return 充值金额配置 分页集合
     */
    @Override
    public PageInfo<RechargeConfig> listByPage(RechargeConfig rechargeConfig, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<RechargeConfig> data = rechargeConfigMapper.selectRechargeConfigList(rechargeConfig);
        return new PageInfo<>(data);
    }

    /**
     * 新增充值金额配置
     * 
     * @param rechargeConfig 充值金额配置
     * @return 结果
     */
    @Override
    public int save(RechargeConfig rechargeConfig)
    {
        return rechargeConfigMapper.insertRechargeConfig(rechargeConfig);
    }

    /**
     * 修改充值金额配置
     * 
     * @param rechargeConfig 充值金额配置
     * @return 结果
     */
    @Override
    public int update(RechargeConfig rechargeConfig)
    {
        return rechargeConfigMapper.updateRechargeConfig(rechargeConfig);
    }

    /**
     * 删除充值金额配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return rechargeConfigMapper.deleteRechargeConfigByIds(ids);
    }

    /**
     * 删除充值金额配置信息
     * 
     * @param configId 充值金额配置ID
     * @return 结果
     */
    @Override
    public int remove(Long configId)
    {
        return rechargeConfigMapper.deleteRechargeConfigById(configId);
    }
}
