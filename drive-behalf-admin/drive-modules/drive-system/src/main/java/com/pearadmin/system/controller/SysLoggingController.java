package com.pearadmin.system.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.enums.LoggingType;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.system.domain.SysLog;
import com.pearadmin.system.service.ISysLogService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * Describe: 日 志 控 制 器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "logging")
public class SysLoggingController extends BaseController {

    private String MODULE_PATH = "system/logging/";

    @Resource
    private ISysLogService sysLogService;

    /**
     * 系统日志视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/system/logging/main','sys:logging:main')")
    public ModelAndView main() {
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 操作日志数据
     * Param: pageDomain
     * Return: ResultTable
     */
    @GetMapping("operateLog")
    @PreAuthorize("hasPermission('/system/logging/operateLog','sys:logging:operateLog')")
    public ResultTable operateLog(PageDomain pageDomain, LocalDateTime startTime, LocalDateTime endTime) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        PageInfo<SysLog> pageInfo = new PageInfo<>(sysLogService.data(LoggingType.OPERATE,startTime,endTime));
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 登录日志数据
     * Param: pageDomain
     * Return: ResultTable
     */
    @GetMapping("loginLog")
    @PreAuthorize("hasPermission('/system/logging/loginLog','sys:logging:loginLog')")
    public ResultTable loginLog(PageDomain pageDomain, LocalDateTime startTime, LocalDateTime endTime) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        PageInfo<SysLog> pageInfo = new PageInfo<>(sysLogService.data(LoggingType.LOGIN, startTime, endTime));
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 日志明细视图
     */
    @GetMapping("details/{logId}")
    public ModelAndView details(@PathVariable("logId") String logId,  ModelMap mmap){
        mmap.put("log", sysLogService.getById(logId));
        return jumpPage(MODULE_PATH + "details");
    }
}
