package com.pearadmin.drive.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.drive.domain.DistributionRatio;

/**
 * @Author huangtao
 * @Date 2021/01/19 15:25:05
 * @Description 提成分润比例配置Mapper接口
 */
@Mapper
public interface DistributionRatioMapper 
{
    /**
     * 查询提成分润比例配置
     * 
     * @param ratioId 提成分润比例配置ID
     * @return 提成分润比例配置
     */
    DistributionRatio selectDistributionRatioById(Long ratioId);

    /**
     * 查询提成分润比例配置列表
     * 
     * @param distributionRatio 提成分润比例配置
     * @return 提成分润比例配置集合
     */
    List<DistributionRatio> selectDistributionRatioList(DistributionRatio distributionRatio);

    /**
     * 新增提成分润比例配置
     * 
     * @param distributionRatio 提成分润比例配置
     * @return 结果
     */
    int insertDistributionRatio(DistributionRatio distributionRatio);

    /**
     * 修改提成分润比例配置
     * 
     * @param distributionRatio 提成分润比例配置
     * @return 结果
     */
    int updateDistributionRatio(DistributionRatio distributionRatio);

    /**
     * 删除提成分润比例配置
     * 
     * @param ratioId 提成分润比例配置ID
     * @return 结果
     */
    int deleteDistributionRatioById(Long ratioId);

    /**
     * 批量删除提成分润比例配置
     * 
     * @param ratioIds 需要删除的数据ID
     * @return 结果
     */
    int deleteDistributionRatioByIds(String[] ratioIds);

}
