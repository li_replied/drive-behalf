package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.PriceIncreased;

import java.util.List;

/**
 * 加倍Service接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
public interface IPriceIncreasedService extends BaseService<PriceIncreased>
{
    /**
     * 查询价格加倍列表
     *
     * @param billingPriceId 价格表id
     * @return 价格加倍集合
     */
    List<PriceIncreased> selectPriceIncreasedListByBillId(Long billingPriceId);

}
