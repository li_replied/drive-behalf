package com.pearadmin.drive.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Withdraw;

/**
 * 司机收支明细Service接口
 * 
 * @author huangtao
 * @date 2020-12-16
 */
public interface IWithdrawService extends BaseService<Withdraw>
{
    /**
     * 查询司机提现明细
     * @param withdraw 司机收支明细
     * @param pageDomain
     * @return 司机收支明细 分页集合
     */
    PageInfo<Withdraw> selectExpendPage(Withdraw withdraw, PageDomain pageDomain);

    /**
     * 查询司机收益明细
     * @param withdraw 司机收支明细
     * @param pageDomain
     * @return 司机收支明细 分页集合
     */
    PageInfo<Withdraw> selectIncomePage(Withdraw withdraw, PageDomain pageDomain);

    /**
     * 审核司机提现
     *
     * @param withdrawId 数据ID
     * @return 结果
     */
    int audit(Long withdrawId);

    /**
     * 批量审核司机提现
     *
     * @param ids 需要处理的数据ID
     * @return 结果
     */
    int auditByIds(String[] ids);

}
