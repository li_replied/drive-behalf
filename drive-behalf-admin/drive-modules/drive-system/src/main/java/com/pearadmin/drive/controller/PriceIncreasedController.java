package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.PriceIncreased;
import com.pearadmin.drive.service.IPriceIncreasedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 加倍Controller
 *
 * @author huangtao
 * @date 2020-12-02
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "increased")
public class PriceIncreasedController extends BaseController {
    private String prefix = "drive/increased";

    @Autowired
    private IPriceIncreasedService priceIncreasedService;

    /**
     * 临时加倍
     */
    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/increased/main','drive:increased:main')")
    public ModelAndView main(Long billingPriceId, ModelMap mmap) {
        mmap.put("billingPriceId", billingPriceId);
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询加倍列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/increased/data','drive:increased:data')")
    public ResultTable list(@ModelAttribute PriceIncreased priceIncreased, PageDomain pageDomain) {
        PageInfo<PriceIncreased> pageInfo = priceIncreasedService.listByPage(priceIncreased, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 新增加倍
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/increased/add','drive:increased:add')")
    public ModelAndView add(Long billingPriceId, ModelMap mmap) {
        mmap.put("billingPriceId", billingPriceId);
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存加倍
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/increased/add','drive:increased:add')")
    @Logging(title = "新增价格加倍", describe = "新增价格加倍", type = BusinessType.ADD)
    public Result save(@RequestBody PriceIncreased priceIncreased) {
        return decide(priceIncreasedService.save(priceIncreased));
    }

    /**
     * 修改加倍
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/increased/edit','drive:increased:edit')")
    public ModelAndView edit(Long priceIncreasedId, ModelMap mmap) {
        PriceIncreased priceIncreased = priceIncreasedService.getById(priceIncreasedId);
        mmap.put("priceIncreased", priceIncreased);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存加倍
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/increased/edit','drive:increased:edit')")
    @Logging(title = "修改价格加倍", describe = "修改价格加倍", type = BusinessType.EDIT)
    public Result update(@RequestBody PriceIncreased priceIncreased) {
        return decide(priceIncreasedService.update(priceIncreased));
    }

    /**
     * 删除加倍
     */
    @DeleteMapping("/batchRemove")
    @PreAuthorize("hasPermission('/drive/increased/remove','drive:increased:remove')")
    @Logging(title = "批量删除价格加倍", describe = "批量删除价格加倍", type = BusinessType.REMOVE)
    public Result batchRemove(String ids) {
        return decide(priceIncreasedService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{priceIncreasedId}")
    @PreAuthorize("hasPermission('/drive/increased/remove','drive:increased:remove')")
    @Logging(title = "删除价格加倍", describe = "删除价格加倍", type = BusinessType.REMOVE)
    public Result remove(@PathVariable("priceIncreasedId") Long priceIncreasedId) {
        return decide(priceIncreasedService.remove(priceIncreasedId));
    }
}
