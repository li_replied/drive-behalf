package com.pearadmin.drive.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.kafka.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pearadmin.drive.mapper.SubscribeMapper;
import com.pearadmin.drive.domain.Subscribe;
import com.pearadmin.drive.service.ISubscribeService;

/**
 * @Author huangtao
 * @Date 2021/03/19 14:52:48
 * @Description 【请填写功能名称】Service业务层处理
 */
@Service
public class SubscribeServiceImpl implements ISubscribeService {
    @Autowired
    private SubscribeMapper subscribeMapper;
    @Autowired
    private KafkaProducer kafkaProducer;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public Subscribe selectSubscribeById(Long id) {
        return subscribeMapper.selectSubscribeById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param subscribe 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Subscribe> selectSubscribeList(Subscribe subscribe) {
        return subscribeMapper.selectSubscribeList(subscribe);
    }

    /**
     * 查询【请填写功能名称】
     *
     * @param subscribe  【请填写功能名称】
     * @param pageDomain
     * @return 【请填写功能名称】 分页集合
     */
    @Override
    public PageInfo<Subscribe> selectSubscribePage(Subscribe subscribe, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<Subscribe> data = subscribeMapper.selectSubscribeList(subscribe);
        return new PageInfo<>(data);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param subscribe 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertSubscribe(Subscribe subscribe) {
        subscribe.setCreatedUser(Long.valueOf(CurrentUserUtil.getUserId()));
        subscribe.setCreateTime(LocalDateTime.now());
        subscribe.setState("NO");
        int count= subscribeMapper.insertSubscribe(subscribe);
        kafkaProducer.subscribe(subscribe.getStartPoint(), subscribe.getAppointmentTime());
        return count;
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param subscribe 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateSubscribe(Subscribe subscribe) {
        return subscribeMapper.updateSubscribe(subscribe);
    }

    /**
     * 删除【请填写功能名称】对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSubscribeByIds(String[] ids) {
        return subscribeMapper.deleteSubscribeByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteSubscribeById(Long id) {
        return subscribeMapper.deleteSubscribeById(id);
    }
}
