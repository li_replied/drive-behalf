package com.pearadmin.drive.service;

import java.util.List;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Subscribe;

/**
 * @Author huangtao
 * @Date 2021/03/19 14:52:48
 * @Description 【请填写功能名称】Service接口
 */
public interface ISubscribeService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    Subscribe selectSubscribeById(Long id);


    /**
    * 查询【请填写功能名称】
     * @param subscribe 【请填写功能名称】
     * @param pageDomain
     * @return 【请填写功能名称】 分页集合
     */
    PageInfo<Subscribe> selectSubscribePage(Subscribe subscribe, PageDomain pageDomain);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param subscribe 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    List<Subscribe> selectSubscribeList(Subscribe subscribe);

    /**
     * 新增【请填写功能名称】
     * 
     * @param subscribe 【请填写功能名称】
     * @return 结果
     */
    int insertSubscribe(Subscribe subscribe);

    /**
     * 修改【请填写功能名称】
     * 
     * @param subscribe 【请填写功能名称】
     * @return 结果
     */
    int updateSubscribe(Subscribe subscribe);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteSubscribeByIds(String[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    int deleteSubscribeById(Long id);

}
