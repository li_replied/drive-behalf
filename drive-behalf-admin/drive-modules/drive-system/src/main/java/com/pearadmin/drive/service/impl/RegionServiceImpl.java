package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Region;
import com.pearadmin.drive.mapper.RegionMapper;
import com.pearadmin.drive.service.IRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 代驾区域Service业务层处理
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Service
public class RegionServiceImpl implements IRegionService
{
    @Autowired
    private RegionMapper regionMapper;

    /**
     * 查询代驾区域
     * 
     * @param regionId 代驾区域ID
     * @return 代驾区域
     */
    @Override
    public Region getById(String regionId)
    {
        return regionMapper.selectRegionById(regionId);
    }

    @Override
    public Region getById(Long key) {
        return null;
    }

    @Override
    public int remove(Long key) {
        return 0;
    }

    /**
     * 查询代驾区域列表
     * 
     * @param region 代驾区域
     * @return 代驾区域
     */
    @Override
    public List<Region> list(Region region)
    {
        return regionMapper.selectRegionList(region);
    }

    /**
     * 查询代驾区域列表树数据
     *
     * @return 代驾区域
     */
    @Override
    public List<Region> selectRegionTreeList()
    {
        return regionMapper.selectRegionTreeList();
    }

    /**
     * 查询代驾区域
     * @param region 代驾区域
     * @param pageDomain
     * @return 代驾区域 分页集合
     */
    @Override
    public PageInfo<Region> listByPage(Region region, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<Region> data = regionMapper.selectRegionList(region);
        return new PageInfo<>(data);
    }

    /**
     * 新增代驾区域
     * 
     * @param region 代驾区域
     * @return 结果
     */
    @Override
    public int save(Region region)
    {
        return regionMapper.insertRegion(region);
    }

    /**
     * 修改代驾区域
     * 
     * @param region 代驾区域
     * @return 结果
     */
    @Override
    public int update(Region region)
    {
        return regionMapper.updateRegion(region);
    }

    /**
     * 删除代驾区域对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids)
    {
        return regionMapper.deleteRegionByIds(ids);
    }

    /**
     * 删除代驾区域信息
     * 
     * @param regionId 代驾区域ID
     * @return 结果
     */
    @Override
    public int remove(String regionId)
    {
        return regionMapper.deleteRegionById(regionId);
    }
}
