package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.drive.domain.Subscribe;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.pearadmin.drive.service.ISubscribeService;

/**
 * @Author huangtao
 * @Date 2021/03/19 14:52:48
 * @Description 【请填写功能名称】Controller
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "subscribe")
public class SubscribeController extends BaseController {
    private String prefix = "drive/subscribe";

    @Autowired
    private ISubscribeService subscribeService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/subscribe/main','drive:subscribe:main')")
    public ModelAndView main() {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/subscribe/data','drive:subscribe:data')")
    public ResultTable list(@ModelAttribute Subscribe subscribe, PageDomain pageDomain) {
        PageInfo<Subscribe> pageInfo = subscribeService.selectSubscribePage(subscribe, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/subscribe/add','drive:subscribe:add')")
    public ModelAndView add() {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/subscribe/add','drive:subscribe:add')")
    public Result save(@RequestBody Subscribe subscribe) {
        return decide(subscribeService.insertSubscribe(subscribe));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/subscribe/edit','drive:subscribe:edit')")
    public ModelAndView edit(Long id, ModelMap mmap) {
        Subscribe subscribe = subscribeService.selectSubscribeById(id);
        mmap.put("subscribe", subscribe);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/subscribe/edit','drive:subscribe:edit')")
    public Result update(@RequestBody Subscribe subscribe) {
        return decide(subscribeService.updateSubscribe(subscribe));
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @DeleteMapping("/batchRemove")
    @PreAuthorize("hasPermission('/drive/subscribe/remove','drive:subscribe:remove')")
    public Result batchRemove(String ids) {
        return decide(subscribeService.deleteSubscribeByIds(Convert.toStrArray(ids)));
    }

    /**
     * 删除【请填写功能名称】
     */
    @DeleteMapping("/remove/{id}")
    @PreAuthorize("hasPermission('/drive/subscribe/remove','drive:subscribe:remove')")
    public Result remove(@PathVariable("id") Long id) {
        return decide(subscribeService.deleteSubscribeById(id));
    }
}
