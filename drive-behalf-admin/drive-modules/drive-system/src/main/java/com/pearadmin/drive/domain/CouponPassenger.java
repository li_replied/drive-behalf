package com.pearadmin.drive.domain;

import java.util.Date;
import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 乘客优惠券使用对象 driver_coupon_passenger
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Data
public class CouponPassenger extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 乘客优惠券ID */
    private Long passengerCouponId;

    /** 乘客ID */
    private Long passengerId;

    /** 优惠券ID */
    private Long couponId;

    /** 使用状态，1未使用，2已使用，3已过期 */
    private String useStatus;

    /** 领取时间 */
    private Date receiveTime;

    /** 使用时间 */
    private Date usedTime;

}
