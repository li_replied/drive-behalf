package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 司机收支明细对象 driver_withdraw
 *
 * @author huangtao
 * @date 2020-12-16
 */
@Data
public class Withdraw extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    private Long withdrawId;

    /** 司机ID */
    private Long driverId;

    /** 订单/提现编号 */
    private String orderNumber;

    /** 收支金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal amount;

    /** 实际提现金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal actualAmount;

    /** 提现手续费比例 */
    private BigDecimal serviceRate;

    /** 提现手续费用 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal serviceCharge;

    /** 收支状态（AUDIT:待审核，FINISH:已完成） */
    private String status;

    /** 收支类型（INCOME:收入，EXPEND:支出） */
    private String method;

    /** 确认提现时间 */
    private LocalDateTime dealTime;

    /** 后台操作用户ID */
    private Long operateUser;

    // 司机名称和手机号码
    private String driverName;

    // 乘客名称和手机号码
    private String passengerName;

    // 订单支付金额
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal payAmount;

    // 司机提成收益
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal commission;

    // 司机提成比例
    private BigDecimal commissionRatio;
}
