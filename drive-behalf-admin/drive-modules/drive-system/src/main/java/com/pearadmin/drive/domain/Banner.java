package com.pearadmin.drive.domain;

import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

/**
 * @Author huangtao
 * @Date 2021/01/09 10:55:41
 * @Description 广告对象 driver_banner
 */
@Data
public class Banner extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long bannerId;

    /** 广告名称 */
    private String bannerName;

    /** 广告类型 */
    private String bannerType;

    /** 文件路径 */
    private String picPath;

    /** 状态 */
    private String status;

    /** 排序 */
    private Long sort;

}
