package com.pearadmin.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.SysConfig;
import com.pearadmin.system.mapper.SysConfigMapper;
import com.pearadmin.system.service.ISysConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Describe: 系统配置服务接口实现
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
@Service
public class SysConfigServiceImpl implements ISysConfigService {

    /**
     * 系统配置数据库操作接口
     */
    @Resource
    private SysConfigMapper sysConfigMapper;

    /**
     * 根据 ID 查询系统配置
     * Param: id
     */
    @Override
    public SysConfig getById(String id) {
        return sysConfigMapper.selectById(id);
    }

    @Override
    public SysConfig getById(Long key) {
        return null;
    }

    @Override
    public int remove(Long key) {
        return 0;
    }

    /**
     * Describe: 根据条件查询系统配置列表数据
     * Param: SysConfig
     * Return: List<SysConfig>
     */
    @Override
    public List<SysConfig> list(SysConfig param) {
        return sysConfigMapper.selectList(param);
    }

    /**
     * Describe: 根据条件查询系统配置列表数据 分页
     * Param: SysConfig
     * Return: PageInfo<SysConfig>
     */
    @Override
    public PageInfo<SysConfig> listByPage(SysConfig param, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<SysConfig> list = sysConfigMapper.selectList(param);
        return new PageInfo<>(list);
    }

    /**
     * Describe: 保存系统配置数据
     * Param: SysConfig
     * Return: Boolean
     */
    @Override
    public int save(SysConfig sysConfig) {
        sysConfig.setCreateTime(LocalDateTime.now());
        return sysConfigMapper.insert(sysConfig);
    }

    /**
     * 修改系统配置
     * Param: SysConfig
     */
    @Override
    public int update(SysConfig sysConfig) {
        sysConfig.setUpdateTime(LocalDateTime.now());
        return sysConfigMapper.updateById(sysConfig);
    }

    /**
     * 根据 ID 删除系统配置
     * Param: id
     */
    @Override
    public int remove(String id) {
        return sysConfigMapper.deleteById(id);
    }

    /**
     * 批量删除系统配置
     * Param: ids
     */
    @Override
    public int batchRemove(String[] ids) {
        return sysConfigMapper.deleteByIds(ids);
    }

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数配置信息
     * @return 结果
     */
    @Override
    public String checkConfigKeyUnique(SysConfig config) {
        String configId = StringUtils.isNotEmpty(config.getConfigId()) ? "" : config.getConfigId();
        SysConfig info = sysConfigMapper.checkConfigKeyUnique(config.getConfigCode());
        if (StringUtils.isNotNull(info) && !info.getConfigId().equals(configId)) {
            return UserConstants.CONFIG_KEY_NOT_UNIQUE;
        }
        return UserConstants.CONFIG_KEY_UNIQUE;
    }
}
