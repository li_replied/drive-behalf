package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.common.web.domain.response.module.ResultTree;
import com.pearadmin.drive.domain.Region;
import com.pearadmin.drive.service.IRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 代驾区域Controller
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "region")
public class RegionController extends BaseController
{
    private String prefix = "drive/region";

    @Autowired
    private IRegionService regionService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/region/main','drive:region:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 树结构数据
     */
    @GetMapping("/tree")
    public ResultTree tree(){
        List<Region> data = regionService.selectRegionTreeList();
        return dataTree(data);
    }

    /**
     * 查询代驾区域列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/region/data','drive:region:data')")
    public ResultTable list(@ModelAttribute Region region, PageDomain pageDomain)
    {
        PageInfo<Region> pageInfo = regionService.listByPage(region,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增代驾区域
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/drive/region/add','drive:region:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存代驾区域
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/drive/region/add','drive:region:add')")
    @Logging(title = "新增区域",describe = "新增区域",type = BusinessType.ADD)
    public Result save(@RequestBody Region region)
    {
        return decide(regionService.save(region));
    }

    /**
     * 修改代驾区域
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/drive/region/edit','drive:region:edit')")
    public ModelAndView edit(Long regionId, ModelMap mmap)
    {
        Region region = regionService.getById(regionId);
        mmap.put("region", region);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存代驾区域
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/drive/region/edit','drive:region:edit')")
    @Logging(title = "修改区域",describe = "修改区域",type = BusinessType.EDIT)
    public Result update(@RequestBody Region region)
    {
        return decide(regionService.update(region));
    }

    /**
     * 删除代驾区域
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/drive/region/remove','drive:region:remove')")
    @Logging(title = "批量删除区域",describe = "批量删除区域",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(regionService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{regionId}")
    @PreAuthorize("hasPermission('/drive/region/remove','drive:region:remove')")
    @Logging(title = "删除区域",describe = "删除区域",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("regionId") Long regionId)
    {
        return decide(regionService.remove(regionId));
    }
}
