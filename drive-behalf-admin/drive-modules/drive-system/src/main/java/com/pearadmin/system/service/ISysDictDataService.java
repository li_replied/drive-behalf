package com.pearadmin.system.service;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.SysDictData;
import java.util.List;

/**
 * Describe: 字典值服务类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
public interface ISysDictDataService {

    /**
     * 根据条件查询字典类型列表数据
     * @param sysDictData
     * @return List<SysDictData>
     */
    List<SysDictData> list(SysDictData sysDictData);

    /**
     * 根据字典code获取可用的字典列表数据
     * @param typeCode
     * @return List<SysDictData>
     */
    List<SysDictData> selectByCode(String typeCode);

    /**
     * 刷新字典缓存
     * @param typeCode
     */
    void refreshCacheTypeCode(String typeCode);

    /**
     * 根据条件查询字典类型列表数据 分页
     * @param sysDictData
     * @param pageDomain
     * @return PageInfo<SysDictData>
     */
    PageInfo<SysDictData> page(SysDictData sysDictData, PageDomain pageDomain);

    /**
     * 插入 SysDictData 数据
     * @param sysDictData
     * @return Boolean
     */
    Boolean save(SysDictData sysDictData);

    /**
     * 根据 Id 查询字典类型
     * @param id
     * @return SysDictData
     */
    SysDictData getById(String id);

    /**
     * 批量删除
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int removeByIds(String[] ids);

    /**
     * 删除 SysDictData 数据
     * @param id
     * @return Boolean
     */
    Boolean remove(String id);

    /**
     * 修改 SysDictData 数据
     * @param sysDictData
     * @return Boolean
     */
    Boolean updateById(SysDictData sysDictData);
}
