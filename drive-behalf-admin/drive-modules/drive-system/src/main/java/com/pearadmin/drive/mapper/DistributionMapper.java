package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Distribution;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/27 09:01:17
 * @Description 提成分润明细Mapper接口
 */
@Mapper
public interface DistributionMapper 
{
    /**
     * 查询提成分润明细
     * 
     * @param distributionId 提成分润明细ID
     * @return 提成分润明细
     */
    Distribution selectDistributionById(Long distributionId);

    /**
     * 查询提成分润明细列表
     * 
     * @param distribution 提成分润明细
     * @return 提成分润明细集合
     */
    List<Distribution> selectDistributionList(Distribution distribution);

    /**
     * 新增提成分润明细
     * 
     * @param distribution 提成分润明细
     * @return 结果
     */
    int insertDistribution(Distribution distribution);

    /**
     * 修改提成分润明细
     * 
     * @param distribution 提成分润明细
     * @return 结果
     */
    int updateDistribution(Distribution distribution);

    /**
     * 删除提成分润明细
     * 
     * @param distributionId 提成分润明细ID
     * @return 结果
     */
    int deleteDistributionById(Long distributionId);

    /**
     * 批量删除提成分润明细
     * 
     * @param distributionIds 需要删除的数据ID
     * @return 结果
     */
    int deleteDistributionByIds(String[] distributionIds);

}
