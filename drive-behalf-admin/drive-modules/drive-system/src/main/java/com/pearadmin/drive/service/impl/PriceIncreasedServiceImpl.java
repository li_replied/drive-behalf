package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.PriceIncreased;
import com.pearadmin.drive.mapper.PriceIncreasedMapper;
import com.pearadmin.drive.service.IPriceIncreasedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 加倍Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-02
 */
@Service
public class PriceIncreasedServiceImpl implements IPriceIncreasedService {
    @Autowired
    private PriceIncreasedMapper priceIncreasedMapper;

    /**
     * 查询加倍
     *
     * @param priceIncreasedId 加倍ID
     * @return 加倍
     */
    @Override
    public PriceIncreased getById(Long priceIncreasedId) {
        return priceIncreasedMapper.selectPriceIncreasedById(priceIncreasedId);
    }

    /**
     * 查询加倍列表
     *
     * @param priceIncreased 加倍
     * @return 加倍
     */
    @Override
    public List<PriceIncreased> list(PriceIncreased priceIncreased) {
        return priceIncreasedMapper.selectPriceIncreasedList(priceIncreased);
    }

    /**
     * 查询加倍
     *
     * @param priceIncreased 加倍
     * @param pageDomain
     * @return 加倍 分页集合
     */
    @Override
    public PageInfo<PriceIncreased> listByPage(PriceIncreased priceIncreased, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<PriceIncreased> data = priceIncreasedMapper.selectPriceIncreasedList(priceIncreased);
        return new PageInfo<>(data);
    }

    /**
     * 新增加倍
     *
     * @param priceIncreased 加倍
     * @return 结果
     */
    @Override
    public int save(PriceIncreased priceIncreased) {
        priceIncreased.setCreateTime(LocalDateTime.now());
        return priceIncreasedMapper.insertPriceIncreased(priceIncreased);
    }

    /**
     * 修改加倍
     *
     * @param priceIncreased 加倍
     * @return 结果
     */
    @Override
    public int update(PriceIncreased priceIncreased) {
        priceIncreased.setModifiedTime(LocalDateTime.now());
        return priceIncreasedMapper.updatePriceIncreased(priceIncreased);
    }

    /**
     * 删除加倍对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return priceIncreasedMapper.deletePriceIncreasedByIds(ids);
    }

    /**
     * 删除加倍信息
     *
     * @param priceIncreasedId 加倍ID
     * @return 结果
     */
    @Override
    public int remove(Long priceIncreasedId) {
        return priceIncreasedMapper.deletePriceIncreasedById(priceIncreasedId);
    }

    /**
     * 查询价格加倍列表
     *
     * @param billingPriceId 价格表id
     * @return 价格加倍集合
     */
    @Override
    public List<PriceIncreased> selectPriceIncreasedListByBillId(Long billingPriceId) {
        return priceIncreasedMapper.selectPriceIncreasedListByBillId(billingPriceId);
    }
}
