package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.DistributionRatio;

/**
 * @Author huangtao
 * @Date 2021/01/19 15:25:05
 * @Description 提成分润比例配置Service接口
 */
public interface IDistributionRatioService extends BaseService<DistributionRatio>
{

}
