package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.RechargeConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author huangtao
 * @Date 2021/01/21 11:54:05
 * @Description 充值金额配置Mapper接口
 */
@Mapper
public interface RechargeConfigMapper 
{
    /**
     * 查询充值金额配置
     * 
     * @param configId 充值金额配置ID
     * @return 充值金额配置
     */
    RechargeConfig selectRechargeConfigById(Long configId);

    /**
     * 查询充值金额配置列表
     * 
     * @param rechargeConfig 充值金额配置
     * @return 充值金额配置集合
     */
    List<RechargeConfig> selectRechargeConfigList(RechargeConfig rechargeConfig);

    /**
     * 新增充值金额配置
     * 
     * @param rechargeConfig 充值金额配置
     * @return 结果
     */
    int insertRechargeConfig(RechargeConfig rechargeConfig);

    /**
     * 修改充值金额配置
     * 
     * @param rechargeConfig 充值金额配置
     * @return 结果
     */
    int updateRechargeConfig(RechargeConfig rechargeConfig);

    /**
     * 删除充值金额配置
     * 
     * @param configId 充值金额配置ID
     * @return 结果
     */
    int deleteRechargeConfigById(Long configId);

    /**
     * 批量删除充值金额配置
     * 
     * @param configIds 需要删除的数据ID
     * @return 结果
     */
    int deleteRechargeConfigByIds(String[] configIds);

}
