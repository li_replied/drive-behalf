package com.pearadmin.drive.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DriveAgent implements Serializable {

    private static final long serialVersionUID = -7958966981070185784L;

    private List<DriverAddress> addresses;

}