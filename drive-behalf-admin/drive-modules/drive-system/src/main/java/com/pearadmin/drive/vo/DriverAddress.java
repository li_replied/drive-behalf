package com.pearadmin.drive.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DriverAddress implements Serializable {

    private static final long serialVersionUID = -2202831609572645039L;

    /** 司机ID */
    private Long driverId;

    /** 司机姓名 */
    private String realName;

    /** 司机手机号码 */
    private String phone;

    /** 司机地址 */
    private String address;

    /** 纬度 */
    private String lat;

    /** 经度 */
    private String lng;

    /** 时间戳 */
    private Long timestamp;
}