package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Coupon;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

import com.pearadmin.drive.domain.Driver;

/**
 * 司机Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-07
 */
@Mapper
public interface DriverMapper 
{
    /**
     * 查询司机
     * 
     * @param userId 司机ID
     * @return 司机
     */
    Driver selectDriverById(Long userId);

    /**
     * 查询5分钟内在线的司机
     *
     * @param agentId 代理区域
     * @return 司机
     */
    List<Driver> selectDriverListByAgentId(String agentId);

    /**
     * 查询司机
     *
     * @param userId 司机ID
     * @return 司机
     */
    String selectDriverNameById(Long userId);

    /**
     * 查询司机列表
     * 
     * @param driver 司机
     * @return 司机集合
     */
    List<Driver> selectDriverList(Driver driver);

    /**
     * 查询司机余额列表
     *
     * @param driver 司机
     * @return 司机集合
     */
    List<Driver> selectDriverMoneyList(Driver driver);

    /**
     * 新增司机
     * 
     * @param driver 司机
     * @return 结果
     */
    int insertDriver(Driver driver);

    /**
     * 修改司机
     * 
     * @param driver 司机
     * @return 结果
     */
    int updateDriver(Driver driver);

    /**
     * 删除司机
     * 
     * @param userId 司机ID
     * @return 结果
     */
    int deleteDriverById(Long userId);

    /**
     * 批量删除司机
     * 
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    int deleteDriverByIds(String[] userIds);

    /**
     * 司机余额提现
     *
     * @param driver 司机
     * @return 结果
     */
    int updateDriverMoney(Driver driver);

    /**
     * 校验手机号码是否唯一
     *
     * @param phone 手机号码
     * @return 结果
     */
    Driver checkPhoneUnique(String phone);

    /**
     * 修改数据
     * @param driver
     * @return 操作结果
     * */
    int updateById(Driver driver);

}
