package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.PriceIncreased;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 加倍Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Mapper
public interface PriceIncreasedMapper
{
    /**
     * 查询加倍
     * 
     * @param priceIncreasedId 加倍ID
     * @return 加倍
     */
    PriceIncreased selectPriceIncreasedById(Long priceIncreasedId);

    /**
     * 查询加倍列表
     * 
     * @param priceIncreased 加倍
     * @return 加倍集合
     */
    List<PriceIncreased> selectPriceIncreasedList(PriceIncreased priceIncreased);

    /**
     * 新增加倍
     * 
     * @param priceIncreased 加倍
     * @return 结果
     */
    int insertPriceIncreased(PriceIncreased priceIncreased);

    /**
     * 修改加倍
     * 
     * @param priceIncreased 加倍
     * @return 结果
     */
    int updatePriceIncreased(PriceIncreased priceIncreased);

    /**
     * 删除加倍
     * 
     * @param priceIncreasedId 加倍ID
     * @return 结果
     */
    int deletePriceIncreasedById(Long priceIncreasedId);

    /**
     * 批量删除加倍
     * 
     * @param priceIncreasedIds 需要删除的数据ID
     * @return 结果
     */
    int deletePriceIncreasedByIds(String[] priceIncreasedIds);

    /**
     * 查询价格加倍列表
     *
     * @param billingPriceId 价格表id
     * @return 价格加倍集合
     */
    List<PriceIncreased> selectPriceIncreasedListByBillId(Long billingPriceId);
}
