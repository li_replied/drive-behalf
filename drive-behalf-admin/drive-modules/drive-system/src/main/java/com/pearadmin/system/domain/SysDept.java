package com.pearadmin.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;

/**
 * Describe: 字典值领域模型
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Data
@Alias("SysDept")
public class SysDept extends BaseDomain {

    /** 部门名称 */
    private String deptId;

    /** 父级编号 */
    private String parentId;

    /** 部门名称 */
    private String deptName;

    /** 排序 */
    private Long sort;

    /** 负责人 */
    private String leader;

    /** 联系方式 */
    private String phone;

    /** 邮箱 */
    private String email;

    /** 部门状态 */
    private String status;

    /** 详细地址 */
    private String address;

    /** 是否代理商 */
    private String agentFlag;

    /** 是否独立代理商 */
    private String agentAlone;

    /** 代理商等级 */
    private String agentLevel;

    /** 区域编号 */
    private String regionId;

    /** 余额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal money;

}
