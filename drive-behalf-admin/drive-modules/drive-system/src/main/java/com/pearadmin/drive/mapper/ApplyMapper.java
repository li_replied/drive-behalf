package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Driver;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.drive.domain.Apply;

/**
 * @Author huangtao
 * @Date 2021/03/18 09:43:47
 * @Description 【请填写功能名称】Mapper接口
 */
@Mapper
public interface ApplyMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    Apply selectApplyById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param apply 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    List<Apply> selectApplyList(Apply apply);

    /**
     * 新增【请填写功能名称】
     * 
     * @param apply 【请填写功能名称】
     * @return 结果
     */
    int insertApply(Apply apply);

    /**
     * 修改【请填写功能名称】
     * 
     * @param apply 【请填写功能名称】
     * @return 结果
     */
    int updateApply(Apply apply);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    int deleteApplyById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteApplyByIds(String[] ids);

    /**
     * 修改数据
     * @param
     * @return 操作结果
     * */
    int updateById(Apply apply);
}
