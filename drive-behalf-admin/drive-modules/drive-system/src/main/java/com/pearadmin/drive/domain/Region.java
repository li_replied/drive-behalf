package com.pearadmin.drive.domain;

import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 代驾区域对象 driver_region
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Data
public class Region extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 区域编号 */
    private String regionId;

    /** 区域名称 */
    private String name;

    /** 简称 */
    private String shortName;

    /** 上级编号 */
    private String parentId;

    /** 级别0国家，1省级，2市级，3区县级 */
    private String levelType;

}
