package com.pearadmin.system.domain;

import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 通知公告对象 sys_notice
 * 
 * @author huangtao
 * @date 2020-12-29
 */
@Data
public class SysNotice extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 公告ID */
    private Integer noticeId;

    /** 公告标题 */
    private String noticeTitle;

    /** 公告类型（NOTIFY通知 NOTICE公告） */
    private String noticeType;

    /** 公告内容 */
    private String noticeContent;

    /** 公告状态（YES启用 NO关闭） */
    private String status;

}
