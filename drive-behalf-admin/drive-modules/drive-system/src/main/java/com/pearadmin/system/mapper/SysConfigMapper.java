package com.pearadmin.system.mapper;

import com.pearadmin.system.domain.SysConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * Describe: 系统配置接口
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Mapper
public interface SysConfigMapper {

    /**
     * Describe: 查询系统配置信息
     * Param: SysConfig
     * Return: 执行结果
     */
    List<SysConfig> selectList(SysConfig param);

    /**
     * Describe: 添加系统配置
     * Param: SysConfig
     * Return: 执行结果
     */
    int insert(SysConfig sysConfig);

    /**
     * Describe: 根据 Id 查询系统配置
     * Param: id
     * Return: SysConfig
     */
    SysConfig selectById(@Param("id") String id);

    /**
     * Describe: 根据 Id 修改系统配置
     * Param: SysConfig
     * Return: Boolean
     */
    int updateById(SysConfig sysConfig);

    /**
     * Describe: 根据 Id 删除系统配置
     * Param: id
     * Return: SysConfig
     */
    int deleteById(String id);

    /**
     * Describe: 根据 Id 删除系统配置
     * Param: id
     * Return: SysConfig
     */
    int deleteByIds(String[] id);

    /**
     * 根据键名查询参数配置信息
     * @param configCode 参数键名
     * @return 参数配置信息
     */
    SysConfig checkConfigKeyUnique(String configCode);
}
