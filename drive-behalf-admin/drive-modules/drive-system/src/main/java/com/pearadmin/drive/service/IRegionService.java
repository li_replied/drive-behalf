package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Region;

import java.util.List;

/**
 * 代驾区域Service接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
public interface IRegionService extends BaseService<Region>
{
    /**
     * 查询区域
     *
     * @param key
     * @return
     */
    Region getById(String key);

    /**
     * 删除区域
     *
     * @param key
     * @return
     */
    int remove(String key);

    /**
     * 查询代驾区域列表树数据
     *
     * @return 代驾区域集合
     */
    List<Region> selectRegionTreeList();

}
