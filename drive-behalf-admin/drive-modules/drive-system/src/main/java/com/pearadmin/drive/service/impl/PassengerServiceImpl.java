package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Passenger;
import com.pearadmin.drive.mapper.PassengerMapper;
import com.pearadmin.drive.service.IPassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 乘客Service业务层处理
 *
 * @author huangtao
 * @date 2020-12-07
 */
@Service
public class PassengerServiceImpl implements IPassengerService {
    @Autowired
    private PassengerMapper passengerMapper;

    @Resource
    private RedisTemplate<String, Serializable> redisTemplate;

    /**
     * 查询乘客
     *
     * @param userId 乘客ID
     * @return 乘客
     */
    @Override
    public Passenger getById(Long userId) {
        return passengerMapper.selectPassengerById(userId);
    }

    /**
     * 查询乘客列表
     *
     * @param passenger 乘客
     * @return 乘客
     */
    @Override
    public List<Passenger> list(Passenger passenger) {
        return passengerMapper.selectPassengerList(passenger);
    }

    /**
     * 查询乘客
     *
     * @param passenger  乘客
     * @param pageDomain
     * @return 乘客 分页集合
     */
    @Override
    public PageInfo<Passenger> listByPage(Passenger passenger, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<Passenger> data = passengerMapper.selectPassengerList(passenger);
        data.forEach(temp ->
                temp.setParentName(StringUtils.isNotNull(temp.getParentId()) ? passengerMapper.selectPassengerNameById(temp.getParentId()) : "无")
        );
        return new PageInfo<>(data);
    }

    @Override
    public int save(Passenger entity) {
        return 0;
    }

    @Override
    public int update(Passenger entity) {
        return 0;
    }

    @Override
    public int remove(Long key) {
        return 0;
    }

    @Override
    public int batchRemove(String[] ids) {
        return 0;
    }

    /**
     * 修改状态
     *
     * @param passenger
     * @return 操作结果
     */
    @Override
    public boolean updateStatus(Passenger passenger) {
        if (DriveConstants.STATE_NO.equals(passenger.getUserEnable())) {
            boolean b = redisTemplate.delete("user:token:CLIENT" + passenger.getUserId());
        }
        passenger.setModifiedTime(LocalDateTime.now());
        Integer result = passengerMapper.updateById(passenger);
        if (result > 0) {
            return true;
        }
        return false;
    }

    /**
     * 批量开启特权
     *
     * @param ids 需要操作的数据ID
     * @return 结果
     */
    @Override
    public int startUpByIds(String[] ids) {
        return passengerMapper.startUpByIds(ids);
    }

    /**
     * 批量取消特权
     *
     * @param ids 需要操作的数据ID
     * @return 结果
     */
    @Override
    public int cancelByIds(String[] ids) {
        return passengerMapper.cancelByIds(ids);
    }
}
