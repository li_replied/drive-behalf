package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.system.domain.SysConfig;
import com.pearadmin.system.service.ISysConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * Describe: 系 统 配 置 控 制 器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "config")
public class SysConfigController extends BaseController {

    @Resource
    private ISysConfigService sysConfigService;

    /**
     * 基础路径
     */
    private String MODULE_PATH = "system/config/";

    /**
     * 系统配置视图
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/system/config/main','sys:config:main')")
    public ModelAndView main(){
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * 系统配置列表数据
     * @param param
     * @param pageDomain
     * @return ResultTable
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/system/config/data','sys:config:data')")
    public ResultTable data(SysConfig param, PageDomain pageDomain){
        PageInfo<SysConfig> pageInfo = sysConfigService.listByPage(param,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 系统配置新增视图
     * @return ModelAndView
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/system/config/add','sys:config:add')")
    public ModelAndView add(){
        return jumpPage(MODULE_PATH + "add");
    }

    /**
     * 系统配置新增保存
     * @param sysConfig
     * @return Result
     */
    @PostMapping("save")
    @PreAuthorize("hasPermission('/system/config/add','sys:config:add')")
    @Logging(title = "新增系统配置",describe = "新增系统配置",type = BusinessType.ADD)
    public Result save(@RequestBody SysConfig sysConfig){
        if (UserConstants.CONFIG_KEY_NOT_UNIQUE.equals(sysConfigService.checkConfigKeyUnique(sysConfig)))
        {
            return failure("新增参数'" + sysConfig.getConfigName() + "'失败，参数键名已存在");
        }
        sysConfig.setConfigId(SequenceUtil.makeStringId());
        sysConfig.setCreateTime(LocalDateTime.now());
        return decide(sysConfigService.save(sysConfig));
    }

    /**
     * 系统配置修改视图
     * @param configId
     * @return ModelAndView
     */
    @GetMapping("edit")
    @PreAuthorize("hasPermission('/system/config/edit','sys:config:edit')")
    public ModelAndView edit(Model model, String configId){
        model.addAttribute("sysConfig",sysConfigService.getById(configId));
        return jumpPage(MODULE_PATH + "edit");
    }

    /**
     * 系统配置修改保存
     * @param sysConfig
     * @return Result
     */
    @PutMapping("update")
    @PreAuthorize("hasPermission('/system/config/edit','sys:config:edit')")
    @Logging(title = "修改系统配置",describe = "修改系统配置",type = BusinessType.EDIT)
    public Result update(@RequestBody SysConfig sysConfig){
        if (UserConstants.CONFIG_KEY_NOT_UNIQUE.equals(sysConfigService.checkConfigKeyUnique(sysConfig)))
        {
            return failure("修改参数'" + sysConfig.getConfigName() + "'失败，参数键名已存在");
        }
        sysConfig.setUpdateTime(LocalDateTime.now());
        return decide(sysConfigService.update(sysConfig));
    }

    /**
     * 删除
     * @param id
     * @return Result
     */
    @DeleteMapping("remove/{id}")
    @PreAuthorize("hasPermission('/system/config/remove','sys:config:remove')")
    @Logging(title = "删除系统配置",describe = "删除系统配置",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("id") String id){
        return decide(sysConfigService.remove(id));
    }

    /**
     * 批量删除
     * @param ids
     * @return Result
     */
    @DeleteMapping("batchRemove/{ids}")
    @PreAuthorize("hasPermission('/system/config/remove','sys:config:remove')")
    @Logging(title = "批量删除系统配置",describe = "批量删除系统配置",type = BusinessType.REMOVE)
    public Result batchRemove(@PathVariable String ids){
        return decide(sysConfigService.batchRemove(Convert.toStrArray(ids)));
    }

}
