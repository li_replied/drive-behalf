package com.pearadmin.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.config.proprety.UploadProperty;
import com.pearadmin.common.tools.file.FileUtil;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.tools.servlet.ServletUtil;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.SysFile;
import com.pearadmin.system.mapper.SysFileMapper;
import com.pearadmin.system.service.ISysFileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Describe: 文件服务接口实现
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Service
public class SysFileServiceImpl implements ISysFileService {

    /**
     * 引 入 服 务
     * */
    @Resource
    private SysFileMapper sysFileMapper;

    /**
     * 上 传 可 读 配 置
     * */
    @Resource
    private UploadProperty uploadProperty;

    /**
     * Describe: 文 件 夹 列 表
     * Param: File
     * Return: id
     * */
    @Override
    public List<String> fileDirs(){
        List<String> fileDirs = new ArrayList<>();
        java.io.File file = new java.io.File("/home/upload");
        if(file.isDirectory()){
          java.io.File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                   String dirName = files[i].getName();
                   fileDirs.add(dirName);
                }
            }
        }
        return fileDirs;
    }

    /**
     * Describe: 文件上传
     * Param: File
     * Return: id
     * */
    @Override
    @Transactional
    public String upload(MultipartFile file) {
        try {
            String fileId = SequenceUtil.makeStringId();
            String name = file.getOriginalFilename();
            String suffixName = name.substring(name.lastIndexOf("."));
            String hash = fileId;
            String fileName = hash + suffixName;
            String fileDir = LocalDate.now().toString();
            String parentPath = uploadProperty.getUploadPath() + fileDir;
            java.io.File filepath = new java.io.File(parentPath, fileName);
            if (!filepath.exists())            {
                if (!filepath.getParentFile().exists()) {
                    filepath.getParentFile().mkdirs();
                }
            }
            SysFile fileDomain = new SysFile();
            file.transferTo(filepath);
            fileDomain.setId(fileId);
            fileDomain.setFileDesc(name);
            fileDomain.setFileName(fileName);
            fileDomain.setTargetDate(LocalDate.now());
            fileDomain.setFilePath(filepath.getPath());
            fileDomain.setCreateTime(LocalDateTime.now());
            fileDomain.setFileSize(FileUtil.getPrintSize(filepath.length()));
            fileDomain.setFileType(suffixName.replace(".", ""));
            int result = sysFileMapper.insert(fileDomain);
            if (result > 0) {
                return fileId;
            } else {
                return "";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Describe: 根据 Id 下载文件
     * Param: id
     * Return: IO
     * */
    @Override
    public void download(String id) {
        try {
            SysFile file = sysFileMapper.selectById(id);
            if(null!=file && StringUtils.isNotEmpty(file.getFilePath())){
                java.io.File files = new java.io.File(file.getFilePath());
                if (files.exists()) {
                    FileCopyUtils.copy(new FileInputStream(file.getFilePath()), ServletUtil.getResponse().getOutputStream());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Describe: 查 询 文 件 列 表
     * Param: id
     * Return: File
     * */
    @Override
    public List<SysFile> data(SysFile file) {
        return sysFileMapper.selectList(file);
    }

    /**
     * 查询文件
     * @param file 文件
     * @param pageDomain
     * @return 文件 分页集合
     * */
    @Override
    public PageInfo<SysFile> selectFilePage(SysFile file, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<SysFile> data = sysFileMapper.selectList(file);
        return new PageInfo<>(data);
    }

    /**
     * Describe: 根据 Id 删除文件信息
     * Param: id
     * Return: int
     * */
    @Override
    public boolean remove(String id) {
        SysFile file = sysFileMapper.selectById(id);
        new java.io.File(file.getFilePath()).delete();
        int removeInfo = sysFileMapper.deleteById(id);
        if(removeInfo>0){
            return true;
        }
        return false;
    }

    /**
     * 批量删除
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public boolean removeByIds(String[] ids)
    {
        int removes = 0;
        if (ids.length > 0) {
            for (int i = 0; i < ids.length; i++) {
                SysFile file = sysFileMapper.selectById(ids[i]);
                new java.io.File(file.getFilePath()).delete();
                sysFileMapper.deleteById(ids[i]);
                removes++;
            }
        }
        if (removes > 0) {
            return true;
        }
        return false;
    }
}
