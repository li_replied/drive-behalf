package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.Passenger;

/**
 * 乘客Service接口
 * 
 * @author huangtao
 * @date 2020-12-07
 */
public interface IPassengerService extends BaseService<Passenger>
{
    /**
     * 修改状态
     * @param: passenger
     * @return: 操作结果
     */
    boolean updateStatus(Passenger passenger);

    /**
     * 批量开启特权
     *
     * @param ids 需要操作的数据编号
     * @return 结果
     */
    int startUpByIds(String[] ids);

    /**
     * 批量取消特权
     *
     * @param ids 需要操作的数据编号
     * @return 结果
     */
    int cancelByIds(String[] ids);

}
