package com.pearadmin.drive.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * @Author huangtao
 * @Date 2021/03/18 09:43:47
 * @Description 【请填写功能名称】对象 driver_apply
 */
@Data
public class Apply extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    private Long createdUser;

    /** $column.columnComment */
    private Long modifiedUser;

    /** $column.columnComment */
    private LocalDate birthday;

    /** $column.columnComment */
    private String name;

    /** $column.columnComment */
    private String phone;

    /** $column.columnComment */
    private String sex;

    /** $column.columnComment */
    private String status;

    /** $column.columnComment */
    private Long years;

}
