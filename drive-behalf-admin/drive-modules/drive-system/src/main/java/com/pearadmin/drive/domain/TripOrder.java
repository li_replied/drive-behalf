package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 代驾订单对象 driver_trip_order
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Data
public class TripOrder extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 订单ID */
    private Long tripOrderId;

    /** 乘客ID */
    private Long passengerId;

    /** 司机ID */
    private Long driverId;

    /** 行程起点 */
    private String startPoint;

    /** 行程终点 */
    private String endPoint;

    /** 行程时间 */
    private String drivingTime;

    /** 预约时间 */
    private LocalDateTime appointmentTime;

    /** 订单编号 */
    private String orderNumber;

    /** 订单状态 */
    private String orderStatus;

    /** 取消原因 */
    private String cancelReason;

    /** 支付方式 */
    private String payMethod;

    /** 支付流水号ID */
    private String transactionId;

    /** 支付时间 */
    private LocalDateTime payTime;

    /** 行程预估价 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal estimatedPrice;

    /** 实付金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal payAmount;

    /** 起步价 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal startingPrice;

    /** 里程费 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal mileageFee;

    /** 等待费 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal waitingFee;

    /** 加价倍数 */
    private BigDecimal priceMultiple;

    /** 优惠金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal couponAmount;

    /** 优惠券ID */
    private Long couponId;

    /** 终点纬度 */
    private String endLat;

    /** 终点经度 */
    private String endLng;

    /** 起点纬度 */
    private String startLat;

    /** 起点经度 */
    private String startLng;

    /** 返程费 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal backFee;

    /** 其他费用 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal otherFee;

    /** 里程 */
    private Long mileage;

    /** 订单状态流转时间 */
    private LocalDateTime transactionDate;

    /** 司机提成收益 */
    private BigDecimal commission;

    /** 司机提成比例 */
    private BigDecimal commissionRatio;

    /** 区域编号 */
    private String regionId;

    // 乘客名称
    @Transient
    private String passengerName;

    // 司机姓名
    @Transient
    private String driverName;
}