package com.pearadmin.drive.kafka;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class ShareDto extends KafkaMessage implements Serializable {
    private String orderNumber;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal amount;

    public ShareDto() {
        super("share");
    }

}
