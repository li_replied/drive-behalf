package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.system.domain.SysNotice;
import com.pearadmin.system.service.ISysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 通知公告Controller
 * 
 * @author huangtao
 * @date 2020-12-29
 */
@RestController
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "notice")
public class SysNoticeController extends BaseController
{
    private String prefix = "system/notice";

    @Autowired
    private ISysNoticeService sysNoticeService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/system/notice/main','sys:notice:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询通知公告列表-分页
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/system/notice/data','sys:notice:data')")
    public ResultTable data(@ModelAttribute SysNotice sysNotice, PageDomain pageDomain)
    {
        PageInfo<SysNotice> pageInfo = sysNoticeService.listByPage(sysNotice,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增通知公告
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/system/notice/add','sys:notice:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存通知公告
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/system/notice/add','sys:notice:add')")
    @Logging(title = "新增公告",describe = "新增公告",type = BusinessType.ADD)
    public Result save(@RequestBody SysNotice sysNotice)
    {
        return decide(sysNoticeService.save(sysNotice));
    }

    /**
     * 修改通知公告
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/system/notice/edit','sys:notice:edit')")
    public ModelAndView edit(Long noticeId, ModelMap mmap)
    {
        SysNotice sysNotice = sysNoticeService.getById(noticeId);
        mmap.put("sysNotice", sysNotice);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存通知公告
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/system/notice/edit','sys:notice:edit')")
    @Logging(title = "修改公告",describe = "修改公告",type = BusinessType.EDIT)
    public Result update(@RequestBody SysNotice sysNotice)
    {
        return decide(sysNoticeService.update(sysNotice));
    }

    /**
     * 批量删除
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/system/notice/remove','sys:notice:remove')")
    @Logging(title = "批量删除公告",describe = "批量删除公告",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(sysNoticeService.batchRemove(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{noticeId}")
    @PreAuthorize("hasPermission('/system/notice/remove','sys:notice:remove')")
    @Logging(title = "删除公告",describe = "删除公告",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("noticeId") Long noticeId)
    {
        return decide(sysNoticeService.remove(noticeId));
    }

    /**
     * 启用
     * @param notice
     * @return 执行结果
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/system/notice/edit','sys:notice:edit')")
    @Logging(title = "修改公告",describe = "修改公告状态",type = BusinessType.EDIT)
    public Result enable(@RequestBody SysNotice notice){
        notice.setStatus(DriveConstants.STATE_YES);
        return decide(sysNoticeService.update(notice));
    }

    /**
     * 关闭
     * @param notice
     * @return 执行结果
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/system/notice/edit','sys:notice:edit')")
    @Logging(title = "修改公告",describe = "修改公告状态",type = BusinessType.EDIT)
    public Result disable(@RequestBody SysNotice notice){
        notice.setStatus(DriveConstants.STATE_NO);
        return decide(sysNoticeService.update(notice));
    }

    /**
     * 获取通知公告列表
     */
    @GetMapping("/list")
    //@PreAuthorize("hasPermission('/system/notice/data','sys:notice:data')")
    public Result list(@ModelAttribute SysNotice sysNotice)
    {
        List<SysNotice> list = sysNoticeService.list(sysNotice);
        return success("获取成功",list);
    }

    /**
     * 公告详情
     */
    @GetMapping("/detail/{noticeId}")
    public ModelAndView detail(@PathVariable("noticeId") Long noticeId, ModelMap mmap)
    {
        mmap.put("notice", sysNoticeService.getById(noticeId));
        return jumpPage(prefix + "/detail");
    }
}
