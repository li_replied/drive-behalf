package com.pearadmin.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.UserConstants;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.SysDictType;
import com.pearadmin.system.mapper.SysDictDataMapper;
import com.pearadmin.system.mapper.SysDictTypeMapper;
import com.pearadmin.system.service.ISysDictDataService;
import com.pearadmin.system.service.ISysDictTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典类型服务实现类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@Service
public class SysDictTypeServiceImpl implements ISysDictTypeService {

    @Resource
    private SysDictTypeMapper sysDictTypeMapper;

    @Resource
    private ISysDictDataService iSysDictDataService;

    @Resource
    private SysDictDataMapper sysDictDataMapper;

    @Override
    public SysDictType getById(Long key) {
        return null;
    }

    @Override
    public int remove(Long key) {
        return 0;
    }

    /**
     * 根据条件查询用户列表数据
     * @param sysDictType
     * @return List<SysDictType>
     */
    @Override
    public List<SysDictType> list(SysDictType sysDictType) {
        return sysDictTypeMapper.selectList(sysDictType);
    }

    /**
     * 根据条件查询用户列表数据 分页
     * @param sysDictType
     * @param pageDomain
     * @return PageInfo<SysDictType>
     */
    @Override
    public PageInfo<SysDictType> listByPage(SysDictType sysDictType, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<SysDictType> list = sysDictTypeMapper.selectList(sysDictType);
        return new PageInfo<>(list);
    }

    /**
     * 保存字典数据
     * @param sysDictType
     * @return Boolean
     */
    @Override
    public int save(SysDictType sysDictType) {
        Integer result = sysDictTypeMapper.insert(sysDictType);
        if(result > 0){
            iSysDictDataService.refreshCacheTypeCode(sysDictType.getTypeCode());
        }
        return result;
    }

    /**
     * 根据 ID 查询字典类型
     * @param id
     * @return 返回字典类型信息
     */
    @Override
    public SysDictType getById(String id) {
        return sysDictTypeMapper.selectById(id);
    }

    /**
     * 根据 ID 修改字典类型
     * @param sysDictType
     * @return Boolean
     */
    @Override
    public int update(SysDictType sysDictType) {
        int result = sysDictTypeMapper.updateById(sysDictType);
        if(result > 0){
            iSysDictDataService.refreshCacheTypeCode(sysDictType.getTypeCode());
        }
        return result;
    }

    /**
     * 根据 ID 删除字典类型
     * @param id
     * @return Boolean
     */
    @Override
    public Boolean remove(String id) {
        SysDictType sysDictType = sysDictTypeMapper.selectById(id);
        if (sysDictType != null) {
            sysDictTypeMapper.deleteById(id);
            sysDictDataMapper.deleteByCode(sysDictType.getTypeCode());
            iSysDictDataService.refreshCacheTypeCode(sysDictType.getTypeCode());
        }
        return true;
    }

    /**
     * 批量删除
     * @param typeCodes 需要删除的数据字典编码
     * @return int
     */
    @Override
    @Transactional
    public int batchRemove(String[] typeCodes)
    {
        int result = sysDictTypeMapper.deleteByIds(typeCodes);
        if (result > 0) {
            // 删除字典数据
            sysDictDataMapper.deleteByTypeCodes(typeCodes);
            // 刷新字典缓存
            for (String typeCode : typeCodes) {
                iSysDictDataService.refreshCacheTypeCode(typeCode);
            }
        }
        return result;
    }

    /**
     * 校验字典编码是否唯一
     *
     * @param sysDictType 数据
     * @return 结果
     */
    @Override
    public String checkTypeCodeUnique(SysDictType sysDictType) {
        String typeId = StringUtils.isEmpty(sysDictType.getId()) ? "" : sysDictType.getId();
        SysDictType info = sysDictTypeMapper.checkTypeCodeUnique(sysDictType.getTypeCode());
        if (StringUtils.isNotNull(info) && !info.getId().equals(typeId))
        {
            return UserConstants.DICT_TYPE_NOT_UNIQUE;
        }
        return UserConstants.DICT_TYPE_UNIQUE;
    }
}
