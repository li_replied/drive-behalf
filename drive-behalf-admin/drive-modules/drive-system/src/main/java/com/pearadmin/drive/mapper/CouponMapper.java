package com.pearadmin.drive.mapper;

import com.pearadmin.drive.domain.Coupon;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 代驾优惠券Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
@Mapper
public interface CouponMapper
{
    /**
     * 查询代驾优惠券
     * 
     * @param couponId 代驾优惠券ID
     * @return 代驾优惠券
     */
    Coupon selectCouponById(Long couponId);

    /**
     * 查询代驾优惠券列表
     * 
     * @param coupon 代驾优惠券
     * @return 代驾优惠券集合
     */
    List<Coupon> selectCouponList(Coupon coupon);

    /**
     * 新增代驾优惠券
     * 
     * @param coupon 代驾优惠券
     * @return 结果
     */
    int insertCoupon(Coupon coupon);

    /**
     * 修改代驾优惠券
     * 
     * @param coupon 代驾优惠券
     * @return 结果
     */
    int updateCoupon(Coupon coupon);

    /**
     * 删除代驾优惠券
     * 
     * @param couponId 代驾优惠券ID
     * @return 结果
     */
    int deleteCouponById(Long couponId);

    /**
     * 批量删除代驾优惠券
     * 
     * @param couponIds 需要删除的数据ID
     * @return 结果
     */
    int deleteCouponByIds(String[] couponIds);

    /**
     * 修改数据
     * @param coupon
     * @return 操作结果
     * */
    int updateById(Coupon coupon);

}
