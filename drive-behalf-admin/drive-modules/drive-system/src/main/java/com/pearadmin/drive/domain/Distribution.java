package com.pearadmin.drive.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pearadmin.common.web.base.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author huangtao
 * @Date 2021/01/19 15:25:05
 * @Description 分润提成明细对象 driver_distribution
 */
@Data
public class Distribution extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long distributionId;

    /** 订单编号/提现编号 */
    private String orderNo;

    /** 总部/代理商/用户ID */
    private Long userId;

    /** 收益/提现金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal amount;

    /** 实际提现金额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal actualAmount;

    /** 提现手续费比例 */
    private BigDecimal serviceRate;

    /** 提现手续费用 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#.00")
    private BigDecimal serviceCharge;

    /** 提成类型（HO总部，AG代理商，USER用户） */
    private String profitType;

    /** 收支状态（AUDIT:待审核，FINISH:已完成） */
    private String status;

    /** 收支类型（INCOME:收入，EXPEND:支出） */
    private String method;

    // 总部/代理商/用户名称
    private String userName;
}
