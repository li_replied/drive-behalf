package com.pearadmin.drive.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.drive.domain.Passenger;
import com.pearadmin.drive.service.IPassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 乘客Controller
 *
 * @author huangtao
 * @date 2020-12-07
 */
@RestController
@RequestMapping(ControllerConstant.API_DRIVE_PREFIX + "passenger")
public class PassengerController extends BaseController {
    private String prefix = "drive/passenger";

    @Autowired
    private IPassengerService passengerService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/drive/passenger/main','drive:passenger:main')")
    public ModelAndView main() {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询乘客列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/drive/passenger/data','drive:passenger:data')")
    public ResultTable list(@ModelAttribute Passenger passenger, PageDomain pageDomain) {
        PageInfo<Passenger> pageInfo = passengerService.listByPage(passenger, pageDomain);
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * 启用
     */
    @PutMapping("enable")
    @PreAuthorize("hasPermission('/drive/passenger/edit','drive:passenger:edit')")
    @Logging(title = "修改乘客", describe = "乘客状态启用", type = BusinessType.EDIT)
    public Result enable(@RequestBody Passenger passenger) {
        passenger.setUserEnable(DriveConstants.STATE_YES);
        return decide(passengerService.updateStatus(passenger));
    }

    /**
     * 禁用
     */
    @PutMapping("disable")
    @PreAuthorize("hasPermission('/drive/passenger/edit','drive:passenger:edit')")
    @Logging(title = "修改乘客", describe = "乘客状态禁用", type = BusinessType.EDIT)
    public Result disable(@RequestBody Passenger passenger) {
        passenger.setUserEnable(DriveConstants.STATE_NO);
        return decide(passengerService.updateStatus(passenger));
    }

    /**
     * 开启免单特权
     */
    @PutMapping("startUp")
    @PreAuthorize("hasPermission('/drive/passenger/edit','drive:passenger:edit')")
    @Logging(title = "修改乘客特权", describe = "开启免单特权", type = BusinessType.EDIT)
    public Result startUp(@RequestBody Passenger passenger) {
        passenger.setFreeState(DriveConstants.STATE_YES);
        return decide(passengerService.updateStatus(passenger));
    }

    /**
     * 取消免单特权
     */
    @PutMapping("close")
    @PreAuthorize("hasPermission('/drive/passenger/edit','drive:passenger:edit')")
    @Logging(title = "修改乘客特权", describe = "关闭免单特权", type = BusinessType.EDIT)
    public Result close(@RequestBody Passenger passenger) {
        passenger.setFreeState(DriveConstants.STATE_NO);
        return decide(passengerService.updateStatus(passenger));
    }

    /**
     * 批量开启免单特权
     */
    @PutMapping("/batchStartUp")
    @PreAuthorize("hasPermission('/drive/passenger/edit','drive:passenger:edit')")
    @Logging(title = "修改乘客特权", describe = "批量开启免单特权", type = BusinessType.EDIT)
    public Result batchStartUp(String ids) {
        return decide(passengerService.startUpByIds(Convert.toStrArray(ids)));
    }

    /**
     * 批量取消免单特权
     */
    @PutMapping("/batchCancel")
    @PreAuthorize("hasPermission('/drive/passenger/edit','drive:passenger:edit')")
    @Logging(title = "修改乘客特权", describe = "批量取消免单特权", type = BusinessType.EDIT)
    public Result batchCancel(String ids) {
        return decide(passengerService.cancelByIds(Convert.toStrArray(ids)));
    }
}
