package com.pearadmin.drive.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constants.DriveConstants;
import com.pearadmin.common.utils.CurrentUserUtil;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.drive.domain.Score;
import com.pearadmin.drive.mapper.PassengerMapper;
import com.pearadmin.drive.mapper.ScoreMapper;
import com.pearadmin.drive.service.IScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author huangtao
 * @Date 2021/01/25 16:03:56
 * @Description 客户积分收入提现Service业务层处理
 */
@Service
public class ScoreServiceImpl implements IScoreService {
    @Autowired
    private ScoreMapper scoreMapper;
    @Autowired
    private PassengerMapper passengerMapper;

    /**
     * 查询客户积分收入提现
     *
     * @param id 客户积分收入提现ID
     * @return 客户积分收入提现
     */
    @Override
    public Score getById(Long id) {
        return scoreMapper.selectScoreById(id);
    }

    /**
     * 查询客户积分收入提现列表
     *
     * @param score 客户积分收入提现
     * @return 客户积分收入提现
     */
    @Override
    public List<Score> list(Score score) {
        return scoreMapper.selectScoreList(score);
    }

    /**
     * 查询客户积分收入提现
     *
     * @param score      客户积分收入提现
     * @param pageDomain
     * @return 客户积分收入提现 分页集合
     */
    @Override
    public PageInfo<Score> listByPage(Score score, PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        List<Score> data = scoreMapper.selectScoreList(score);
        data.forEach(temp->temp.setUserName(passengerMapper.selectPassengerNameById(temp.getUserId())));
        return new PageInfo<>(data);
    }

    /**
     * 新增客户积分收入提现
     *
     * @param score 客户积分收入提现
     * @return 结果
     */
    @Override
    public int save(Score score) {
        return scoreMapper.insertScore(score);
    }

    /**
     * 修改客户积分收入提现
     *
     * @param score 客户积分收入提现
     * @return 结果
     */
    @Override
    public int update(Score score) {
        return scoreMapper.updateScore(score);
    }

    /**
     * 删除客户积分收入提现对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchRemove(String[] ids) {
        return scoreMapper.deleteScoreByIds(ids);
    }

    /**
     * 删除客户积分收入提现信息
     *
     * @param id 客户积分收入提现ID
     * @return 结果
     */
    @Override
    public int remove(Long id) {
        return scoreMapper.deleteScoreById(id);
    }

    /**
     * 审核提现
     *
     * @param withdrawId 数据ID
     * @return 结果
     */
    @Override
    public int audit(Long withdrawId) {
        Score score = new Score();
        score.setId(withdrawId);
        score.setState(DriveConstants.STATE_YES);
        score.setModifiedTime(LocalDateTime.now());
        score.setModifiedUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
        return scoreMapper.updateScore(score);
    }

    /**
     * 批量审核提现
     *
     * @param ids 需要处理的数据ID
     * @return 结果
     */
    @Override
    public int auditByIds(String[] ids) {
        if (ids.length > 0) {
            Map<String, Object> params = new HashMap<>();
            params.put("ids", ids);
            Score score = new Score();
            score.setParams(params);
            score.setState(DriveConstants.STATE_YES);
            score.setModifiedTime(LocalDateTime.now());
            score.setModifiedUser(null != CurrentUserUtil.getUserId() ? Long.valueOf(CurrentUserUtil.getUserId()) : 0);
            return scoreMapper.auditByIds(score);
        }
        return 0;
    }
}
