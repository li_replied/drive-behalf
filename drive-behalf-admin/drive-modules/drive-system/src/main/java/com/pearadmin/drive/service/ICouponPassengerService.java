package com.pearadmin.drive.service;

import com.pearadmin.common.web.base.BaseService;
import com.pearadmin.drive.domain.CouponPassenger;

/**
 * 乘客优惠券使用Service接口
 * 
 * @author huangtao
 * @date 2020-12-02
 */
public interface ICouponPassengerService extends BaseService<CouponPassenger>
{

}
