package com.pearadmin.schedule.mapper;

import com.pearadmin.schedule.domain.SysJob;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 定时任务调度Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@Mapper
public interface SysJobMapper 
{
    /**
     * 查询定时任务调度
     * 
     * @param jobId 定时任务调度ID
     * @return 定时任务调度
     */
    SysJob selectSysJobById(Long jobId);

    /**
     * 查询定时任务调度列表
     * 
     * @param sysJob 定时任务调度
     * @return 定时任务调度集合
     */
    List<SysJob> selectSysJobList(SysJob sysJob);

    /**
     * 新增定时任务调度
     * 
     * @param sysJob 定时任务调度
     * @return 结果
     */
    int insertSysJob(SysJob sysJob);

    /**
     * 修改定时任务调度
     * 
     * @param sysJob 定时任务调度
     * @return 结果
     */
    int updateSysJob(SysJob sysJob);

    /**
     * 删除定时任务调度
     * 
     * @param jobId 定时任务调度ID
     * @return 结果
     */
    int deleteSysJobById(Long jobId);

    /**
     * 批量删除定时任务调度
     * 
     * @param jobIds 需要删除的数据ID
     * @return 结果
     */
    int deleteSysJobByIds(String[] jobIds);

}
