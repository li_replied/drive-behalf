package com.pearadmin.schedule.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.tools.text.StringUtils;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.schedule.domain.SysJob;
import com.pearadmin.schedule.domain.SysJobLog;
import com.pearadmin.schedule.service.ISysJobLogService;
import com.pearadmin.schedule.service.ISysJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 定时任务调度日志Controller
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@RestController
@RequestMapping(ControllerConstant.API_SCHEDULE_PREFIX + "log")
public class SysJobLogController extends BaseController
{
    private String prefix = "schedule/log";

    @Autowired
    private ISysJobService jobService;
    @Autowired
    private ISysJobLogService jobLogService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/schedule/log/main','schedule:log:main')")
    public ModelAndView main(@RequestParam(value = "jobId", required = false) Long jobId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(jobId))
        {
            SysJob job = jobService.selectJobById(jobId);
            mmap.put("job", job);
        }
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询定时任务调度日志列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/schedule/log/data','schedule:log:data')")
    public ResultTable list(@ModelAttribute SysJobLog sysJobLog, PageDomain pageDomain)
    {
        PageInfo<SysJobLog> pageInfo = jobLogService.selectSysJobLogPage(sysJobLog,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 任务日志详细
     */
    @GetMapping("/detail/{jobLogId}")
    @PreAuthorize("hasPermission('/schedule/log/detail','schedule:log:detail')")
    public ModelAndView detail(@PathVariable("jobLogId") Long jobLogId, ModelMap mmap)
    {
        mmap.put("name", "jobLog");
        mmap.put("jobLog", jobLogService.selectSysJobLogById(jobLogId));
        return jumpPage("schedule/job/detail");
    }

    /**
     * 批量删除
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/schedule/log/remove','schedule:log:remove')")
    @Logging(title = "调度任务日志",describe = "批量删除调度任务日志",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)
    {
        return decide(jobLogService.deleteSysJobLogByIds(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove/{jobLogId}")
    @PreAuthorize("hasPermission('/schedule/log/remove','schedule:log:remove')")
    @Logging(title = "调度任务日志",describe = "删除调度任务日志",type = BusinessType.REMOVE)
    public Result remove(@PathVariable("jobLogId") Long jobLogId)
    {
        return decide(jobLogService.deleteSysJobLogById(jobLogId));
    }

    /**
     * 清空日志
     */
    @PostMapping("/clean")
    @PreAuthorize("hasPermission('/schedule/log/clean','schedule:log:remove')")
    @Logging(title = "调度任务日志",describe = "清空调度任务日志",type = BusinessType.REMOVE)
    public Result clean()
    {
        jobLogService.cleanJobLog();
        return success();
    }

}
