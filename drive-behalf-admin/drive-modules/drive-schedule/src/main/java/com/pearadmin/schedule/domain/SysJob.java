package com.pearadmin.schedule.domain;

import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 定时任务调度对象 sys_job
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@Data
public class SysJob extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 任务ID */
    private Long jobId;

    /** 任务名称 */
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 调用目标字符串 */
    private String invokeTarget;

    /** cron执行表达式 */
    private String cronExpression;

    /** 计划执行错误策略（1立即执行 2执行一次 3放弃执行） */
    private String misfirePolicy;

    /** 是否并发执行（0允许 1禁止） */
    private String concurrent;

    /** 状态（0正常 1暂停） */
    private String status;

}
