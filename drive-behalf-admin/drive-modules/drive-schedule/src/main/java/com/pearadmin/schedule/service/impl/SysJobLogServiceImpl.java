package com.pearadmin.schedule.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.schedule.domain.SysJobLog;
import com.pearadmin.schedule.mapper.SysJobLogMapper;
import com.pearadmin.schedule.service.ISysJobLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 定时任务调度日志Service业务层处理
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@Service
public class SysJobLogServiceImpl implements ISysJobLogService
{
    @Autowired
    private SysJobLogMapper sysJobLogMapper;

    /**
     * 查询定时任务调度日志
     * 
     * @param jobLogId 定时任务调度日志ID
     * @return 定时任务调度日志
     */
    @Override
    public SysJobLog selectSysJobLogById(Long jobLogId)
    {
        return sysJobLogMapper.selectSysJobLogById(jobLogId);
    }

    /**
     * 查询定时任务调度日志列表
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 定时任务调度日志
     */
    @Override
    public List<SysJobLog> selectSysJobLogList(SysJobLog sysJobLog)
    {
        return sysJobLogMapper.selectSysJobLogList(sysJobLog);
    }

    /**
     * 查询定时任务调度日志
     * @param sysJobLog 定时任务调度日志
     * @param pageDomain
     * @return 定时任务调度日志 分页集合
     * */
    @Override
    public PageInfo<SysJobLog> selectSysJobLogPage(SysJobLog sysJobLog, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<SysJobLog> data = sysJobLogMapper.selectSysJobLogList(sysJobLog);
        return new PageInfo<>(data);
    }

    /**
     * 新增定时任务调度日志
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 结果
     */
    @Override
    public int insertSysJobLog(SysJobLog sysJobLog)
    {
        sysJobLog.setCreateTime(LocalDateTime.now());
        return sysJobLogMapper.insertSysJobLog(sysJobLog);
    }

    /**
     * 修改定时任务调度日志
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 结果
     */
    @Override
    public int updateSysJobLog(SysJobLog sysJobLog)
    {
        return sysJobLogMapper.updateSysJobLog(sysJobLog);
    }

    /**
     * 删除定时任务调度日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysJobLogByIds(String[] ids)
    {
        return sysJobLogMapper.deleteSysJobLogByIds(ids);
    }

    /**
     * 删除定时任务调度日志信息
     * 
     * @param jobLogId 定时任务调度日志ID
     * @return 结果
     */
    @Override
    public int deleteSysJobLogById(Long jobLogId)
    {
        return sysJobLogMapper.deleteSysJobLogById(jobLogId);
    }

    /**
     * 清空定时任务调度日志信息
     *
     * @return 结果
     */
    @Override
    public void cleanJobLog()
    {
        sysJobLogMapper.cleanJobLog();
    }
}
