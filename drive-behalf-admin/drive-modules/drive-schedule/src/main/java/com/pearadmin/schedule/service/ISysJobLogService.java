package com.pearadmin.schedule.service;

import java.util.List;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.schedule.domain.SysJobLog;

/**
 * 定时任务调度日志Service接口
 * 
 * @author huangtao
 * @date 2020-12-16
 */
public interface ISysJobLogService 
{
    /**
     * 查询定时任务调度日志
     * 
     * @param jobLogId 定时任务调度日志ID
     * @return 定时任务调度日志
     */
    SysJobLog selectSysJobLogById(Long jobLogId);


    /**
    * 查询定时任务调度日志
     * @param sysJobLog 定时任务调度日志
     * @param pageDomain
     * @return 定时任务调度日志 分页集合
     * */
    PageInfo<SysJobLog> selectSysJobLogPage(SysJobLog sysJobLog, PageDomain pageDomain);

    /**
     * 查询定时任务调度日志列表
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 定时任务调度日志集合
     */
    List<SysJobLog> selectSysJobLogList(SysJobLog sysJobLog);

    /**
     * 新增定时任务调度日志
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 结果
     */
    int insertSysJobLog(SysJobLog sysJobLog);

    /**
     * 修改定时任务调度日志
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 结果
     */
    int updateSysJobLog(SysJobLog sysJobLog);

    /**
     * 批量删除定时任务调度日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteSysJobLogByIds(String[] ids);

    /**
     * 删除定时任务调度日志信息
     * 
     * @param jobLogId 定时任务调度日志ID
     * @return 结果
     */
    int deleteSysJobLogById(Long jobLogId);

    /**
     * 清空定时任务调度日志信息
     *
     * @return 结果
     */
    void cleanJobLog();
}
