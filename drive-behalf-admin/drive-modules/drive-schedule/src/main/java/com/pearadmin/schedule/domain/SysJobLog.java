package com.pearadmin.schedule.domain;

import lombok.Data;
import com.pearadmin.common.web.base.BaseDomain;

import java.util.Date;

/**
 * 定时任务调度日志对象 sys_job_log
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@Data
public class SysJobLog extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 任务日志ID */
    private Long jobLogId;

    /** 任务名称 */
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 调用目标字符串 */
    private String invokeTarget;

    /** 日志信息 */
    private String jobMessage;

    /** 执行状态（0正常 1失败） */
    private String status;

    /** 异常信息 */
    private String exceptionInfo;

    /** 开始时间 */
    private Date startTime;

    /** 结束时间 */
    private Date endTime;
}
