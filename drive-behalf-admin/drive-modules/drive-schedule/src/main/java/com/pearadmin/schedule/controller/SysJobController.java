package com.pearadmin.schedule.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.text.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.schedule.domain.SysJob;
import com.pearadmin.schedule.service.ISysJobService;
import com.pearadmin.schedule.util.CronUtils;
import com.pearadmin.schedule.util.TaskException;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;

/**
 * 定时任务调度Controller
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@RestController
@RequestMapping(ControllerConstant.API_SCHEDULE_PREFIX + "job")
public class SysJobController extends BaseController
{
    private String prefix = "schedule/job";

    @Autowired
    private ISysJobService sysJobService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/schedule/job/main','schedule:job:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询定时任务调度列表
     */
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/schedule/job/data','schedule:job:data')")
    public ResultTable list(@ModelAttribute SysJob sysJob, PageDomain pageDomain)
    {
        PageInfo<SysJob> pageInfo = sysJobService.selectSysJobPage(sysJob,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增定时任务调度
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/schedule/job/add','schedule:job:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存定时任务调度
     */
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/schedule/job/add','schedule:job:add')")
    @Logging(title = "新增调度任务",describe = "新增调度任务",type = BusinessType.ADD)
    public Result save(@RequestBody SysJob sysJob)throws SchedulerException, TaskException
    {
        if (!CronUtils.isValid(sysJob.getCronExpression()))
        {
            return failure("cron表达式不正确");
        }
        sysJob.setCreateTime(LocalDateTime.now());
        return decide(sysJobService.insertJob(sysJob));
    }

    /**
     * 修改定时任务调度
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/schedule/job/edit','schedule:job:edit')")
    public ModelAndView edit(Long jobId, ModelMap mmap)
    {
        SysJob sysJob = sysJobService.selectJobById(jobId);
        mmap.put("sysJob", sysJob);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存定时任务调度
     */
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/schedule/job/edit','schedule:job:edit')")
    @Logging(title = "修改调度任务",describe = "修改调度任务",type = BusinessType.EDIT)
    public Result update(@RequestBody SysJob sysJob)throws SchedulerException, TaskException
    {
        if (!CronUtils.isValid(sysJob.getCronExpression()))
        {
            return failure("cron表达式不正确");
        }
        sysJob.setUpdateTime(LocalDateTime.now());
        return decide(sysJobService.updateJob(sysJob));
    }

    /**
     * 批量删除
     */
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/schedule/job/remove','schedule:job:remove')")
    @Logging(title = "批量删除调度任务",describe = "批量删除调度任务",type = BusinessType.REMOVE)
    public Result batchRemove(String ids)throws SchedulerException
    {
        sysJobService.deleteJobByIds(Convert.toStrArray(ids));
        return success();
    }

    /**
     * 删除
     */
    @DeleteMapping("/remove")
    @PreAuthorize("hasPermission('/schedule/job/remove','schedule:job:remove')")
    @Logging(title = "删除调度任务",describe = "删除调度任务",type = BusinessType.REMOVE)
    public Result remove(SysJob job)throws SchedulerException
    {
        return decide(sysJobService.deleteJob(job));
    }

    /**
     * 执行一次定时任务
     * @param job
     * @return  执行结果
     */
    @RequestMapping("/run")
    @PreAuthorize("hasPermission('/schdule/job/run','schedule:job:run')")
    @Logging(title = "执行调度任务",describe = "执行调度任务",type = BusinessType.EDIT)
    public Result run(SysJob job) throws SchedulerException{
        sysJobService.run(job);
        return success("运行成功") ;
    }

    /**
     * 任务详细
     */
    @GetMapping("/detail/{jobId}")
    @PreAuthorize("hasPermission('/schedule/job/detail','schedule:job:detail')")
    public ModelAndView detail(@PathVariable("jobId") Long jobId, ModelMap mmap)
    {
        mmap.put("name", "job");
        mmap.put("job", sysJobService.selectJobById(jobId));
        return jumpPage(prefix + "/detail");
    }

    /**
     * 任务调度状态修改
     */
    @PostMapping("/changeStatus")
    @PreAuthorize("hasPermission('/schedule/job/edit','schedule:job:edit')")
    @Logging(title = "修改调度任务",describe = "修改调度任务状态",type = BusinessType.EDIT)
    public Result changeStatus(SysJob job) throws SchedulerException
    {
        SysJob newJob = sysJobService.selectJobById(job.getJobId());
        newJob.setStatus(job.getStatus());
        return decide(sysJobService.changeStatus(newJob));
    }

}
