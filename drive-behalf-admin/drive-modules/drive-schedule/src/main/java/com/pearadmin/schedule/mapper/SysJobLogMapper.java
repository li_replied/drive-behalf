package com.pearadmin.schedule.mapper;

import com.pearadmin.schedule.domain.SysJobLog;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * 定时任务调度日志Mapper接口
 * 
 * @author huangtao
 * @date 2020-12-16
 */
@Mapper
public interface SysJobLogMapper 
{
    /**
     * 查询定时任务调度日志
     * 
     * @param jobLogId 定时任务调度日志ID
     * @return 定时任务调度日志
     */
    SysJobLog selectSysJobLogById(Long jobLogId);

    /**
     * 查询定时任务调度日志列表
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 定时任务调度日志集合
     */
    List<SysJobLog> selectSysJobLogList(SysJobLog sysJobLog);

    /**
     * 新增定时任务调度日志
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 结果
     */
    int insertSysJobLog(SysJobLog sysJobLog);

    /**
     * 修改定时任务调度日志
     * 
     * @param sysJobLog 定时任务调度日志
     * @return 结果
     */
    int updateSysJobLog(SysJobLog sysJobLog);

    /**
     * 删除定时任务调度日志
     * 
     * @param jobLogId 定时任务调度日志ID
     * @return 结果
     */
    int deleteSysJobLogById(Long jobLogId);

    /**
     * 批量删除定时任务调度日志
     * 
     * @param jobLogIds 需要删除的数据ID
     * @return 结果
     */
    int deleteSysJobLogByIds(String[] jobLogIds);

    /**
     * 清空定时任务调度日志信息
     *
     * @return 结果
     */
    void cleanJobLog();

}
