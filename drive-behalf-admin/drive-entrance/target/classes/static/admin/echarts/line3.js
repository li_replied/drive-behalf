layui.use(['form', 'laydate', 'jquery', 'echarts'], function () {
	let form = layui.form;
	let $ = layui.jquery;
	let laydate = layui.laydate;
	let echarts = layui.echarts;

	let prefix = "/drive/count/";

	form.on('submit(count-query)', function (data) {
		return false;
	});

	let insStart = laydate.render({
		elem: '#startTime',
		max: $('#endTime').val(),
		theme: 'molv',
		trigger: 'click',
		done: function(value, date) {
			// 结束时间大于开始时间
			if (value !== '') {
				insEnd.config.min.year = date.year;
				insEnd.config.min.month = date.month - 1;
				insEnd.config.min.date = date.date;
			} else {
				insEnd.config.min.year = '';
				insEnd.config.min.month = '';
				insEnd.config.min.date = '';
			}
		}
	});

	let insEnd = laydate.render({
		elem: '#endTime',
		min: $('#startTime').val(),
		theme: 'molv',
		trigger: 'click',
		done: function(value, date) {
			// 开始时间小于结束时间
			if (value !== '') {
				insStart.config.max.year = date.year;
				insStart.config.max.month = date.month - 1;
				insStart.config.max.date = date.date;
			} else {
				insStart.config.max.year = '2099';
				insStart.config.max.month = '12';
				insStart.config.max.date = '31';
			}
		}
	});

	var line3 = echarts.init(document.getElementById('line3'));

	var option = {
		backgroundColor: '#fff',
		title: {
			text: "用户量",
			left: "18px",
			top: "0",
			textStyle: {
				color: "#999",
				fontSize: 12,
				fontWeight: '400'
			}
		},
		color: ['#73A0FA', '#73DEB3'],
		tooltip: {
			trigger: 'axis',
			axisPointer: {
				type: 'cross',
				crossStyle: {
					color: '#999'
				},
				lineStyle: {
					type: 'dashed'
				}
			}
		},
		grid: {
			left: '25',
			right: '25',
			bottom: '25',
			top: '75',
			containLabel: true
		},
		legend: {
			data: [],
			orient: 'horizontal',
			icon: "rect",
			show: true,
			//left: 20,
			//top: 25,
		},
		xAxis: {
			type: 'category',
			//boundaryGap: false,//坐标轴两边留白
			data: [],
			splitLine: {
				show: true,
				lineStyle: {
					color: '#E5E9ED',
					// 	opacity:0.1
				}
			},
			axisTick: {
				show: false
			},
			axisLine: {
				show: false
			}
		},
		yAxis: {
			type: 'value',
			// max: max_value>=100? max_value + 100: max_value+10,
			// max: max_value > 100 ? max_value * 2 : max_value + 10,
			// interval: 10,
			// nameLocation: "center",
			axisLabel: {
				color: '#999',
				textStyle: {
					fontSize: 12
				}
			},
			splitLine: {
				show: true,
				lineStyle: {
					color: '#F3F4F4'
				}
			},
			axisTick: {
				show: false
			},
			axisLine: {
				show: false
			}
		},
		series: [{
				name: '乘客',
				type: 'line',
				smooth: true,
				data: []
			},
			{
				name: '司机',
				type: 'line',
				smooth: true,
				data: []
			}
		]
	};

	line3.setOption(option);

	$(function () {
		$.ajax({
			url: prefix + "line3",
			type: 'get',
			dataType: 'json',
			success: function (result) {
				option.legend.data = ['乘客', '司机'];// 设置图例
				option.xAxis.data = result.data.dayArr;// 设置X轴数据
				option.series[0].data = result.data.passengerArr; // 设置Y轴图表1
				option.series[1].data = result.data.driverArr; // 设置Y轴图表2
				line3.setOption(option);// 重新加载图表
			}
		})
	})

	window.addEventListener("resize", function () {
		line3.resize();
	});
})