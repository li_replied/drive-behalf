layui.use(['form', 'laydate', 'jquery', 'echarts'], function () {
    let form = layui.form;
    let $ = layui.jquery;
    let laydate = layui.laydate;
    let echarts = layui.echarts;

    let prefix = "/drive/count/";

    laydate.render({
        elem: '#year',
        type: 'year',
        theme: 'molv',
        trigger: 'click'
    });

    var column1 = echarts.init(document.getElementById('column1'));
    var option1 = {
        title: {
            text: '每月订单量',
            left: 'center',
            top: 2
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow',
                textStyle: {
                    color: '#fff',
                    fontSize: '26'
                }
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '10%',
            containLabel: true
        },
        legend: {
            top: '6%',
            right: '10%',
            data: [],
            textStyle: {
                fontSize: 12,
                color: '#808080'
            },
            icon: 'roundRect'
        },
        xAxis: [{
            type: 'category',
            boundaryGap: true,//坐标轴两边留白
            axisLabel: {
                color: '#4D4D4D',
                fontSize: 14,
                margin: 21,
                fontWeight: 'bold'
            },
            axisTick: {
                alignWithLabel: true,
                lineStyle:{
                    color:'#4D4D4D'
                }
            },
            axisLine: {
                lineStyle:{
                    color:'#4D4D4D'
                }
            },
            data: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"]
        }],
        yAxis: [{
            name: '订单数',
            type: 'value',
            nameTextStyle: {
                color: '#808080',
                fontSize: 12,
                padding: [0, 0, 0, -5]
            },
            /*max: function (value) {
                if (value.max < 5) {
                    return 5
                } else {
                    return value.max
                }
            },*/
            axisLabel: {
                color: '#808080',
                fontSize: 12,
                margin: 5
            },
            axisLine: {
                show: true,
                lineStyle:{
                    color:'#4D4D4D'
                }
            },
            axisTick: {
                alignWithLabel: true,
                lineStyle:{
                    color:'#4D4D4D'
                }
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#E5E9ED'
                }
            }
        }],
        series: [
            {
                name: '总订单',
                type: 'bar',
                label: {
                    show: true,
                    position: 'top',
                    fontSize: 14,
                    color: '#3DC3F0',
                    fontWeight: 'bold'
                },
                barMaxWidth: 60,
                itemStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#3DC3F0' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#CCF2FF' // 100% 处的颜色
                        }]
                    }
                },
                data: []
            },
            {
                name: '已完成订单',
                type: 'bar',
                label: {
                    show: true,
                    position: 'top',
                    fontSize: 14,
                    color: '#3D8BF0',
                    fontWeight: 'bold'
                },
                barMaxWidth: 60,
                itemStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [{
                            offset: 0, color: '#3D8BF0' // 0% 处的颜色
                        }, {
                            offset: 1, color: '#CCE2FF' // 100% 处的颜色
                        }]
                    }
                },
                data: []
            }
        ]
    };

    column1.setOption(option1);

    window.loadColumn1 = function(obj) {
        let data = {};
        if (obj != null) {
            data = {"year": obj.field.year};
        }
        $.ajax({
            url: prefix + "column1",
            type: 'get',
            dataType: 'json',
            data: data,
            success: function (result) {
                option1.legend.data = ['总订单', '已完成订单'];// 设置图例
                option1.xAxis.data = result.data.monthArr;// 设置X轴数据
                option1.series[0].data = result.data.allOrderArr; // 设置Y轴图表1
                option1.series[1].data = result.data.orderArr; // 设置Y轴图表2
                column1.setOption(option1);// 重新加载图表
            }
        });
    }

    $(function () {
        window.loadColumn1(null);
    })

    form.on('submit(count-query)', function (data) {
        window.loadColumn1(data);
        return false;
    });

    window.addEventListener("resize", function () {
        column1.resize();
    });
})
