layui.use(['form', 'laydate', 'jquery', 'echarts'], function () {
	let form = layui.form;
	let $ = layui.jquery;
	let laydate = layui.laydate;
	let echarts = layui.echarts;

	let prefix = "/drive/count/";

	let insStart = laydate.render({
		elem: '#startTime',
		max: $('#endTime').val(),
		theme: 'molv',
		trigger: 'click',
		done: function(value, date) {
			// 结束时间大于开始时间
			if (value !== '') {
				insEnd.config.min.year = date.year;
				insEnd.config.min.month = date.month - 1;
				insEnd.config.min.date = date.date;
			} else {
				insEnd.config.min.year = '';
				insEnd.config.min.month = '';
				insEnd.config.min.date = '';
			}
		}
	});

	let insEnd = laydate.render({
		elem: '#endTime',
		min: $('#startTime').val(),
		theme: 'molv',
		trigger: 'click',
		done: function(value, date) {
			// 开始时间小于结束时间
			if (value !== '') {
				insStart.config.max.year = date.year;
				insStart.config.max.month = date.month - 1;
				insStart.config.max.date = date.date;
			} else {
				insStart.config.max.year = '2099';
				insStart.config.max.month = '12';
				insStart.config.max.date = '31';
			}
		}
	});

	var line4 = echarts.init(document.getElementById('line4'));

	const colorList = ["#9E87FF", '#73DDFF', '#fe9a8b', '#F56948', '#9E87FF']
	var option = {
	    title: {
	        text: '订单量',
			left: "18px",
			top: "0",
			textStyle: {
				color: "#999",
				fontSize: 12,
				fontWeight: '400'
			}
		},
	    tooltip: {
	        trigger: 'axis',
			axisPointer: {
				type: 'cross',
				animation: false,
				label: {
					backgroundColor: '#505765'
				}
			}
	    },
	    legend: {
	        data: []
	    },
	    grid: {
	        left: '25',
	        right: '25',
	        bottom: '25',
	        containLabel: true
	    },
	    toolbox: {
	        feature: {
				dataZoom: {
					yAxisIndex: 'none'
				},
				restore: {},
				saveAsImage: {}
	        }
	    },
	    xAxis: {
			type: 'category',
		    boundaryGap: false,//坐标轴两边留白
			data: [],
			axisLabel: { //坐标轴刻度标签的相关设置。
				interval: 1,//设置为 1，表示『隔一个标签显示一个标签』
			//	margin:15,
				textStyle: {
					color: '#1B253A',
					fontStyle: 'normal',
					fontFamily: '微软雅黑',
					fontSize: 12,
				}/*,
				formatter:function(params) {
	                var newParamsName = "";
	                var paramsNameNumber = params.length;
	                var provideNumber = 4;  //一行显示几个字
	                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
	                if (paramsNameNumber > provideNumber) {
	                    for (var p = 0; p < rowNumber; p++) {
	                        var tempStr = "";
	                        var start = p * provideNumber;
	                        var end = start + provideNumber;
	                        if (p == rowNumber - 1) {
	                            tempStr = params.substring(start, paramsNameNumber);
	                        } else {
	                            tempStr = params.substring(start, end) + "\n";
	                        }
	                        newParamsName += tempStr;
	                    }
	
	                } else {
	                    newParamsName = params;
	                }
	                return newParamsName
	            },*/
				//rotate:50,
			},
			axisTick:{//坐标轴刻度相关设置。
				show: false
			},
			axisLine:{//坐标轴轴线相关设置
				lineStyle:{
					color:'#E5E9ED',
					// opacity:0.2
				}
			},
			splitLine: { //坐标轴在 grid 区域中的分隔线。
				show: true,
				lineStyle: {
					color: '#E5E9ED',
				// 	opacity:0.1
				}
			}
	    },
	    yAxis: [
			{
				name: '订单数',
				type: 'value',
				splitNumber: 5,
				axisLabel: {
					textStyle: {
						color: '#a8aab0',
						fontStyle: 'normal',
						fontFamily: '微软雅黑',
						fontSize: 12
					}
				},
				axisLine:{
					show: false
				},
				axisTick:{
					show: false
				},
				splitLine: {
					show: true,
					lineStyle: {
						color: '#E5E9ED',
					// 	opacity:0.1
					}
				}
			},
			{
				name: '订单金额(元)',
				type: 'value',
				splitNumber: 5,
				axisLabel: {
					textStyle: {
						color: '#a8aab0',
						fontStyle: 'normal',
						fontFamily: '微软雅黑',
						fontSize: 12,
					}
				},
				axisLine:{
					show: false
				},
				axisTick:{
					show: false
				},
				splitLine: {
					show: true,
					lineStyle: {
						color: '#E5E9ED',
						// 	opacity:0.1
					}
				}
			}
		],
	    series: [
	        {
	            name: '订单数',
	            type: 'line',
	            itemStyle: {
			        normal: {
						color:'#3A84FF',
			            lineStyle: {
							color: "#3A84FF",
							width:1
			            },
			            areaStyle: { 
							color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [{
								offset: 0,
								color: 'rgba(58,132,255,0)'
							}, {
								offset: 1,
								color: 'rgba(58,132,255,0.35)'
							}]),
			            }
			        }
				},
	            data: []
	        },
	        {
	            name: '金额',
	            type: 'bar',
				yAxisIndex: 1,
	            itemStyle: {
			        normal: {
			            color:'rgba(255,80,124,1)',
			            lineStyle: {
							color: "rgba(255,80,124,1)",
							width:1
			            },
			            areaStyle: { 
							color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [{
								offset: 0,
								color: 'rgba(255,80,124,0)'
							}, {
								offset: 1,
								color: 'rgba(255,80,124,0.35)'
							}]),
			            }
			        }
				},
	            data: []
	        }
	    ]
	};


	line4.setOption(option);

	window.loadEcharts = function(obj) {
		let data = {};
		if (obj != null) {
			data = {"beginTime": obj.field.beginTime, "endTime": obj.field.endTime};
		}
		$.ajax({
			url: prefix + "line4",
			type: 'get',
			dataType: 'json',
			data: data,
			success: function (result) {
				option.legend.data = ['订单数', '金额'];// 设置图例
				option.xAxis.data = result.data.dayArr;// 设置X轴数据
				option.series[0].data = result.data.orderArr; // 设置Y轴图表1
				option.series[1].data = result.data.amountArr; // 设置Y轴图表2
				line4.setOption(option);// 重新加载图表
			}
		});
	}

	$(function () {
		window.loadEcharts(null);
	})

	form.on('submit(count-query)', function (data) {
		window.loadEcharts(data);
		return false;
	});

	window.addEventListener("resize", function () {
		line4.resize();
	});

})
