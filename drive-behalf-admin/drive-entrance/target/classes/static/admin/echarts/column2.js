layui.use(['form', 'laydate', 'jquery', 'echarts'], function () {
	let form = layui.form;
	let $ = layui.jquery;
	let laydate = layui.laydate;
	let echarts = layui.echarts;

	let prefix = "/drive/count/";

	laydate.render({
		elem: '#year',
		type: 'year',
		theme: 'molv',
		trigger: 'click'
	});

	var column2 = echarts.init(document.getElementById('column2'));

	var option2 = {
	    backgroundColor: '#ffffff',
	    title: {
	        text: '订单收益情况',
	        left: 'center',
	        top: 2
	    },
	    color: ['#fed46b','#2194ff','#9E87FF'],
	    tooltip: {
	        trigger: 'axis',
	        axisPointer: { // 坐标轴指示器，坐标轴触发有效
	            type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
	        }
	    },
	    grid: {
	        left: '3%',
	        right: '4%',
	        bottom: '10%',
	        containLabel: true
	    },
	    legend: {
	        left: 'center',
	        bottom: '2%',
	        data: []
	    },
	    xAxis: {
	        type: 'category',
	        data: [],
			axisLabel: { //坐标轴刻度标签的相关设置。
				interval: 2,//设置为 1，表示『隔一个标签显示一个标签』
				margin: 15,
				textStyle: {
					color: '#1B253A',
					fontStyle: 'normal',
					fontFamily: '微软雅黑',
					fontSize: 12
				}
			},
	        axisTick: {
	            alignWithLabel: true
	        }
	    },
	    yAxis: [{
			name: '金额(元)',
	        type: 'value'
	    }],
	    //barMaxWidth: '30',
	    label:{
	        show:true,
	        position:'top',
	        formatter:function(params){
				if (params.value == '0.00') {
					return 0
				}
				return params.value
	        }
	    },
	    series: [
	        {
	            name: '订单金额',
	            type: 'bar',
	            data: []
	        },
	        {
	            name: '平台收益',
	            type: 'bar',
	            data: []
	        },
			{
				name: '司机收益',
				type: 'bar',
				data: []
			}
	    ]
	};
	column2.setOption(option2);

	window.loadColumn2 = function(obj) {
		let data = {};
		if (obj != null) {
			data = {"year": obj.field.year};
		}
		$.ajax({
			url: prefix + "column2",
			type: 'get',
			dataType: 'json',
			data: data,
			success: function (result) {
				option2.legend.data = ['订单金额', '平台收益', '司机收益'];// 设置图例
				option2.xAxis.data = result.data.dayArr;// 设置X轴数据
				option2.series[0].data = result.data.payAmountArr; // 设置Y轴图表1
				option2.series[1].data = result.data.platformProfitArr; // 设置Y轴图表2
				option2.series[2].data = result.data.driverProfitArr; // 设置Y轴图表3
				column2.setOption(option2);// 重新加载图表
			}
		});
	}

	$(function () {
		window.loadColumn2(null);
	})

	form.on('submit(count-query)', function (data) {
		window.loadColumn2(data);
		return false;
	});


	window.addEventListener("resize", function () {
		column2.resize();
	});
})
