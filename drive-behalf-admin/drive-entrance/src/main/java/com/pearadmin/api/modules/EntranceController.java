package com.pearadmin.api.modules;

import com.pearadmin.common.plugins.logging.aop.annotation.Logging;
import com.pearadmin.common.plugins.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.secure.session.SecureSessionService;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Describe: 入 口 控 制 器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@RestController
@RequestMapping
public class EntranceController extends BaseController {

    @Resource
    private SessionRegistry sessionRegistry;

    /**
     * 获取登录页
     */
    @GetMapping("login")
    public ModelAndView login(HttpServletRequest request) {
        if (SecurityUtil.isAuthentication()) {
            SecureSessionService.expiredSession(request, sessionRegistry);
            return jumpPage("index");
        } else {
            return jumpPage("login");
        }
    }

    /**
     * 获取主页
     */
    @GetMapping("index")
    @Logging(title = "主页", describe = "返回 Index 主页视图", type = BusinessType.ADD)
    public ModelAndView index()
    {
        return jumpPage("index");
    }

    /**
     * 获取主页视图
     */
    @GetMapping("console")
    public ModelAndView home()
    {
        return jumpPage("console/console");
    }

    /**
     * 无权限页面
     * @return 403页面
     */
    @GetMapping("error/403")
    public ModelAndView noPermission(){
        return jumpPage("error/403");
    }

    /**
     * 找不到页面
     * @return 404页面
     */
    @GetMapping("error/404")
    public ModelAndView notFound(){
        return jumpPage("error/404");
    }

    /**
     * 异常处理页
     * @return 500页面
     */
    @GetMapping("error/500")
    public ModelAndView onException(){
        return jumpPage("error/500");
    }

}
