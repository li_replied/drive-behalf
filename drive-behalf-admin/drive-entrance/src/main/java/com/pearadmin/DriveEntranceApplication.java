package com.pearadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * Describe: 入 口 启 动 类
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
@ServletComponentScan
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, SecurityAutoConfiguration.class})
public class DriveEntranceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DriveEntranceApplication.class, args);
    }

}
