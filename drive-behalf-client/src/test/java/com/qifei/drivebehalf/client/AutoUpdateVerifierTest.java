package com.qifei.drivebehalf.client;

import com.wechat.pay.contrib.apache.httpclient.WechatPayHttpClientBuilder;
import com.wechat.pay.contrib.apache.httpclient.WechatPayUploadHttpPost;
import com.wechat.pay.contrib.apache.httpclient.auth.AutoUpdateCertificatesVerifier;
import com.wechat.pay.contrib.apache.httpclient.auth.PrivateKeySigner;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Credentials;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Validator;
import com.wechat.pay.contrib.apache.httpclient.util.PemUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.URI;
import java.security.PrivateKey;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AutoUpdateVerifierTest {

    private static String mchId = "1604658983"; // 商户号
    private static String mchSerialNo = "505FC6238A9B70251AC44B238B41B2E1915372B5"; // 商户证书序列号
    private static String apiV3Key = ""; // api密钥
    // 你的商户私钥
    private static String privateKey = "-----BEGIN PRIVATE KEY-----\n" +
            "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDQg2LOOBr6FDKR\n" +
            "2MJUMX3cfAFeu2cupXiPubyuxyVx4mGb1ayY7vlduNqtyLxj67ALi5D+m5As1SsV\n" +
            "LiSDlBBdXFTNVjbQqDI/9HbF+VJk94rl8VFOceD34pPm1Zt5zlL0/8Kg856m6hxg\n" +
            "kVXvaRLSk6Q33Cj9agmsNPf27jbu+q9JUS1WuEyYdlUVqnW3jtAI9Jb9WFFru3LW\n" +
            "fgdvwmmdIpeCp2l2TmK+7IhKa1qITn+NdBbmFMKtgZII8kL9irREKA1cJ8lD2LHP\n" +
            "K3xGzoY4d1kA0z5lijrTFR6Y55IDKxsr4JF4T7AOjQIXy6GWxKskCH/xwdTWu0Yk\n" +
            "p2g/gloTAgMBAAECggEBAMx4+wx00meIJOsG39IGUoFDjWhjZ9TDaY2vu0q9eYfL\n" +
            "UqKpj77NIfZ5TuB6Z9uohRNdQsuaq3B7fesgO6hYKxASegp+T/Sf697xkQOk8dqX\n" +
            "MTs6QNwnKlCLfTQxJmz1Gj6tDCQfouXV0AwWmWnT5SRgqfopSHzxXhjkPIGGqpH/\n" +
            "t4CI41EUL+/zEiofT2Penhyo+D8PEtma8+iCr8EwKbBBwf84QaQcpRfV93Tv154e\n" +
            "3oKpc5c2pVE4CQ/OKVPHMB3ZGhf77zlNK0KBzJHPjcOJoUtdkxxN0jjXReWv8FiF\n" +
            "sxaR30bFi5ydm6mUWPG5DWoDAtt34uETA1Qg6JG2PgECgYEA55hd0MVjN7WlDtIy\n" +
            "ZDteT3irjTyv7hNKHPFxCq+ldG7ZQ61tbkMLPRlhTpHP++ioCjIAFUJbJ+veRaZ5\n" +
            "4DVIuCvoTIZpMe8oi3g7SpZUuGIYMV22B7fIgbRR8JHgvpFa0dRh7aJbm+oTFCr6\n" +
            "q8ZO1mLeNzdfdiDyvdGlcvG6FQECgYEA5nxZOKNCcXqq/mP/2Qkwbz8q2jb7m/dv\n" +
            "qx4J4hkfQrg6qujkNSgv9leSqUm+hVB6g+YA14M8USwDP7vrkZ14Xspz5V2+uMU2\n" +
            "i/0kAXG2kHP/HLf+2rX9m3D7PzdFIkYcnBFMuInvbrpZo5UcMDLNUUpuSfYEOa9J\n" +
            "b+fSZdkLyxMCgYEA0FUF/6mwiQrDwkNfxT6Kph6847513EpJ3yNqNtyuj8CvfOvj\n" +
            "hs6Uq4/LMuyEFe3CS/mSjY01hwHs3ubUt9X25oXI4B4BP0QOSvgLDhuRdpP3g2Dz\n" +
            "QJoYqmTVWP1BGOyUsDDHLwdgNS8pnnjTKfaoML6bezw6iX82HX8bv/ynEgECgYEA\n" +
            "rbH2Vx2J3BUyueXIkNpA+wVimPKoqUBOkmlfz1lZWqGV7Oj32iuAAai7aQKfJr6g\n" +
            "pbf22HMZSJ7fj+9htuoyJVVzA96i9YIB+nt53/C/QZt5HT54PIC8L6F0Zo6TdiHa\n" +
            "O6853zRsEUk0dnfPt28Blgp+lRRm2MYFlOTihUiNf6sCgYAdQ+FskStWwOaDjd9r\n" +
            "PzdVoOUg7iQz6LK5UDrY2+w833GrbqcSl3Ulc+0XLZF4SFrPpbgV2u6qV3WH9Jk7\n" +
            "bp1xNZQZGscV99zV2PGy9MSTzpxVevpcow4OkPzMU9VJrYS8RUxoaGnGg4zhxyHb\n" +
            "bbjE7tSxOZoztVRm1pA390SI8g==\n" +
            "-----END PRIVATE KEY-----\n";

    //测试AutoUpdateCertificatesVerifier的verify方法参数
    private static String serialNumber = "";
    private static String message = "";
    private static String signature = "";
    private CloseableHttpClient httpClient;
    private AutoUpdateCertificatesVerifier verifier;

    @Before
    public void setup() throws IOException {
        PrivateKey merchantPrivateKey = PemUtil.loadPrivateKey(
                new ByteArrayInputStream(privateKey.getBytes("utf-8")));

        //使用自动更新的签名验证器，不需要传入证书
        verifier = new AutoUpdateCertificatesVerifier(
                new WechatPay2Credentials(mchId, new PrivateKeySigner(mchSerialNo, merchantPrivateKey)),
                apiV3Key.getBytes("utf-8"));

        httpClient = WechatPayHttpClientBuilder.create()
                .withMerchant(mchId, mchSerialNo, merchantPrivateKey)
                .withValidator(new WechatPay2Validator(verifier))
                .build();
    }

    @After
    public void after() throws IOException {
        httpClient.close();
    }


    public void autoUpdateVerifierTest() throws Exception {
        assertTrue(verifier.verify(serialNumber, message.getBytes("utf-8"), signature));
    }

    @Test
    public void getCertificateTest() throws Exception {
        URIBuilder uriBuilder = new URIBuilder("https://api.mch.weixin.qq.com/v3/certificates");
        HttpGet httpGet = new HttpGet(uriBuilder.build());
        httpGet.addHeader("Accept", "application/json");
        CloseableHttpResponse response1 = httpClient.execute(httpGet);
        assertEquals(200, response1.getStatusLine().getStatusCode());
        try {
            HttpEntity entity1 = response1.getEntity();
            // do something useful with the response body
            // and ensure it is fully consumed
            EntityUtils.consume(entity1);
        } finally {
            response1.close();
        }
    }


    public void uploadImageTest() throws Exception {
        String filePath = "/your/home/hellokitty.png";

        URI uri = new URI("https://api.mch.weixin.qq.com/v3/merchant/media/upload");

        File file = new File(filePath);
        try (FileInputStream s1 = new FileInputStream(file)) {
            String sha256 = DigestUtils.sha256Hex(s1);
            try (InputStream s2 = new FileInputStream(file)) {
                WechatPayUploadHttpPost request = new WechatPayUploadHttpPost.Builder(uri)
                        .withImage(file.getName(), sha256, s2)
                        .build();

                CloseableHttpResponse response1 = httpClient.execute(request);
                assertEquals(200, response1.getStatusLine().getStatusCode());
                try {
                    HttpEntity entity1 = response1.getEntity();
                    // do something useful with the response body
                    // and ensure it is fully consumed
                    String s = EntityUtils.toString(entity1);
                    System.out.println(s);
                } finally {
                    response1.close();
                }
            }
        }
    }
}

