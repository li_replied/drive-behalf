package com.qifei.drivebehalf.client.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(
        prefix = "ali.oss",
        ignoreUnknownFields = true
)
public class AliYunOSSProperties {
    private String bucket;
    private String endpoint;
    private String ossUrl;
    private String folder = "uploads";
}
