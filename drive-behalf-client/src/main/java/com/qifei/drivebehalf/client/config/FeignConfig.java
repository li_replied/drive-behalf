package com.qifei.drivebehalf.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class FeignConfig {

    @Bean
    public MappingJackson2HttpMessageConverter customMappingJackson2HttpMessageConverter() {
        return new CustomMappingJackson2HttpMessageConverter();
    }

    static class CustomMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {
        public CustomMappingJackson2HttpMessageConverter() {
            List<MediaType> list = new ArrayList<>();
            list.add(MediaType.TEXT_PLAIN);
            list.add(MediaType.APPLICATION_JSON);
            setSupportedMediaTypes(list);
        }
    }
}
