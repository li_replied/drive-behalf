package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import com.qifei.drivebehalf.client.dao.DriverOrderStateDao;
import com.qifei.drivebehalf.client.entity.DriverOrderStateEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DriverOrderStateService {
    private final DriverOrderStateDao driverOrderStateDao;

    public void log(String orderNumber, OrderStateEnum orderStatus, String reason) throws Exception {
        DriverOrderStateEntity driverOrderStateEntity = new DriverOrderStateEntity();
        driverOrderStateEntity.setOrderNumber(orderNumber);
        driverOrderStateEntity.setOrderStatus(orderStatus);
        driverOrderStateEntity.setReason(reason);
        driverOrderStateEntity.setCreate();
        driverOrderStateDao.save(driverOrderStateEntity);
    }
}
