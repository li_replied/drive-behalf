package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(
        name = "driver_driver",
        indexes = {
                @Index(name = "idx_driver_driver_phone", columnList = "phone"),
        }
)
public class DriverEntity extends UserEntity {
    @Column(name = "money", precision = 8, scale = 2)
    private BigDecimal money = new BigDecimal(0);
    @Column(name = "years")
    private Integer years;
    @Column(name = "likes")
    private Integer likes;

    @Column(name = "last_longitude")
    private Double lastLongitude;
    @Column(name = "last_latitude")
    private Double lastLatitude;
    @Column(name = "last_address")
    private String lastAddress;
    @Column(name = "last_update_time")
    private Long lastUpdateTime;

    @Column(name = "agent_id")
    private String agentId;
    @Column(name = "online")
    @Enumerated(EnumType.STRING)
    private EnableEnum online = EnableEnum.NO;
    @Column(name = "last_online_time")
    private Long lastOnlineTime;
    @Column(name = "total_online_time")
    private Integer totalOnlineTime;
    @Column(name = "today_online_time")
    private Integer todayOnlineTime;
}
