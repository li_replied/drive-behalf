package com.qifei.drivebehalf.client.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalTime;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "driver_billing_price")
public class DriverBillingPriceEntity extends BaseEntity {

    /**
     * 价格表
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bill_id")
    private Long billId;

    /**
     * 区域编号
     */
    @Column(name = "region_id")
    private String regionId;

    /**
     * 起步公里数(3km)
     */
    @Column(name = "starting_kilometre")
    private int startingKilometre = 0;

    /**
     * 起步价格(3km以内)
     */
    @Column(name = "starting_price")
    private BigDecimal startingPrice = new BigDecimal(0);

    /**
     * 里程费(超过起步里程后按X元每Y公里计算，不足Y公里的部分按Y公里计算)
     */
    @Column(name = "mileage_fee")
    private BigDecimal mileageFee = new BigDecimal(0);

    /**
     * 等待时间(X分钟)
     */
    @Column(name = "waiting_time")
    private int waitingTime = 0;

    /**
     * 等待费(每等待1分钟加收X元，不足1分钟的部分按1分钟计算)
     */
    @Column(name = "waiting_fee")
    private BigDecimal waitingFee = new BigDecimal(0);
    /**
     * 提成比例
     */
    @Column(name = "commission_ratio")
    private BigDecimal commissionRatio = new BigDecimal(0);
    /**
     * 固定抽成
     */
    @Column(name = "fix_deduct")
    private BigDecimal fixDeduct = new BigDecimal(0);
    /**
     * 客服热线
     */
    @Column(name = "customer_phone")
    private String customerPhone;
    /**
     * 开始时间段
     */
    @Column(name = "start_time")
    private LocalTime startTime;
    /**
     * 结束时间段
     */
    @Column(name = "end_time")
    private LocalTime endTime;

}
