package com.qifei.drivebehalf.client.service;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.qifei.drivebehalf.client.common.enums.AudioFileTypeEnum;
import com.qifei.drivebehalf.client.common.enums.PictureFileTypeEnum;
import com.qifei.drivebehalf.client.common.properties.AliYunOSSProperties;
import com.qifei.drivebehalf.client.common.properties.AliYunProperties;
import com.qifei.drivebehalf.client.common.utils.UUIDUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;

@Service
@Slf4j
public class OSSService {

    private static String PATTERN = "yyyyMMdd";

    private AliYunProperties aliYunProperties;

    @Autowired
    public void setAliYunProperties(AliYunProperties aliYunProperties) {
        this.aliYunProperties = aliYunProperties;
    }

    private AliYunOSSProperties aliYunOSSProperties;

    @Autowired
    public void setAliYunOSSProperties(AliYunOSSProperties aliYunOSSProperties) {
        this.aliYunOSSProperties = aliYunOSSProperties;
    }

    private OSS ossClient = null;

    @PostConstruct
    public void initOssClient() {
        log.info("初始化OssClient");
        ossClient = new OSSClientBuilder().build(aliYunOSSProperties.getEndpoint(), aliYunProperties.getAccessKeyId(), aliYunProperties.getAccessSecret());
    }

    @PreDestroy
    public void destroyOssClient() {
        log.info("销毁OssClient");
        if (ossClient != null) {
            ossClient.shutdown();
        }
    }

    public String putImage(PictureFileTypeEnum type, MultipartFile file) throws Exception {
        return putObject(type.getSuffix(), file.getInputStream());
    }

    public String putAudio(AudioFileTypeEnum type, MultipartFile file) throws Exception {
        return putObject(type.getSuffix(), file.getInputStream());
    }

    public String putObject(String suffix, InputStream inputStream) throws Exception {
        String fileKey = UUID.randomUUID().toString().replaceAll("-", "") + suffix;
        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN);
        String path = dateFormat.format(new Date());
        if (aliYunOSSProperties.getFolder().startsWith("/") || aliYunOSSProperties.getFolder().startsWith("\\")) {
            aliYunOSSProperties.setFolder(aliYunOSSProperties.getFolder().substring(1, aliYunOSSProperties.getFolder().length()));
        }
        if (!aliYunOSSProperties.getFolder().endsWith("/") && !aliYunOSSProperties.getFolder().endsWith("\\")) {
            path = "/" + path;
        }
        fileKey = aliYunOSSProperties.getFolder() + path + "/" + fileKey;
        ossClient.putObject(aliYunOSSProperties.getBucket(), fileKey, inputStream);
        return fileKey;
    }

    public String putObject(String suffix, File file) throws Exception {
        String fileKey = UUIDUtils.uuid() + suffix;
        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN);
        String path = dateFormat.format(new Date());
        if (aliYunOSSProperties.getFolder().startsWith("/") || aliYunOSSProperties.getFolder().startsWith("\\")) {
            aliYunOSSProperties.setFolder(aliYunOSSProperties.getFolder().substring(1, aliYunOSSProperties.getFolder().length()));
        }
        if (!aliYunOSSProperties.getFolder().endsWith("/") && !aliYunOSSProperties.getFolder().endsWith("\\")) {
            path = "/" + path;
        }
        fileKey = aliYunOSSProperties.getFolder() + path + "/" + fileKey;
        ossClient.putObject(aliYunOSSProperties.getBucket(), fileKey, file);
        return fileKey;
    }

    public String getUrl(String fileKey) throws Exception {
        // 设置URL过期时间为10年  3600l* 1000*24*365*10
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expiration = now.plusYears(10);
        Long milli = expiration.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        // 生成URL
        URL url = ossClient.generatePresignedUrl(aliYunOSSProperties.getBucket(), fileKey, new Date(milli));
        if (url != null) {
            String path = url.toString();
            return path.substring(0, path.indexOf("?"));
        }
        return null;
    }

    public void deleteObject(String fileKey) throws Exception {
        ossClient.deleteObject(aliYunOSSProperties.getBucket(), fileKey);
    }

    @Data
    public static class OSSImageInfo {
        private String pictureKey;
        private String pictureUrl;
    }
}


