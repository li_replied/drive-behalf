package com.qifei.drivebehalf.client.controller.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "下单参数")
public class PlaceOrderParam implements Serializable {
    @NotEmpty(message = "算价id不能为空")
    @ApiModelProperty("算价id")
    private String uuid;
}
