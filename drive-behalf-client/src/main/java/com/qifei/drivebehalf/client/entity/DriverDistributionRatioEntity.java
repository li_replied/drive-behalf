package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@Entity
@Table(name = "driver_distribution_ratio")
public class DriverDistributionRatioEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ratio_id", unique = true, nullable = false)
    private Long ratioId;
    @Column(name = "hd_ratio", precision = 10, scale = 2)
    private BigDecimal hdRatio = new BigDecimal(0);
    @Column(name = "ag_ratio", precision = 10, scale = 2)
    private BigDecimal agRatio = new BigDecimal(0);
    @Column(name = "one_level_ratio", precision = 10, scale = 2)
    private BigDecimal oneLevelRatio = new BigDecimal(0);
    @Column(name = "two_level_ratio", precision = 10, scale = 2)
    private BigDecimal twoLevelRatio = new BigDecimal(0);
    @Column(name = "two_level_state")
    @Enumerated(EnumType.STRING)
    private EnableEnum twoLevelState;
}
