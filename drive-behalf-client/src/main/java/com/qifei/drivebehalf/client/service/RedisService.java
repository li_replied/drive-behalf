package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    public void setRedisTemplate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }


    /**
     * 生成key
     *
     * @param keyEnum
     * @param key
     * @return
     */
    private String generateKey(RedisKeyEnum keyEnum, String key) {
        return keyEnum.getKey() + key;
    }

    /**
     * 获取值
     *
     * @param keyEnum
     * @param key
     * @return
     */
    public String getKey(RedisKeyEnum keyEnum, String key) {
        return redisTemplate.opsForValue().get(generateKey(keyEnum, key));
    }

    /**
     * 获取值
     *
     * @param keyEnum
     * @return
     */
    public String getKey(RedisKeyEnum keyEnum) {
        return redisTemplate.opsForValue().get(keyEnum.getKey());
    }

    /**
     * key是否存在
     *
     * @param keyEnum
     * @param key
     * @return
     */
    public boolean hasKey(RedisKeyEnum keyEnum, String key) {
        return redisTemplate.hasKey(generateKey(keyEnum, key));
    }

    public boolean hasKey(RedisKeyEnum keyEnum) {
        return redisTemplate.hasKey(keyEnum.getKey());
    }

    /**
     * 保存值
     *
     * @param keyEnum
     * @param key
     * @param value
     */
    public void setKey(RedisKeyEnum keyEnum, String key, String value) {
        redisTemplate.opsForValue().set(generateKey(keyEnum, key), value);
    }

    /**
     * 保存值
     *
     * @param keyEnum
     * @param value
     */
    public void setKey(RedisKeyEnum keyEnum, String value) {
        redisTemplate.opsForValue().set(keyEnum.getKey(), value);
    }

    /**
     * 保存key并设置过期时间
     *
     * @param key
     * @param value
     */
    public void setEx(RedisKeyEnum keyEnum, String key, String value) {
        redisTemplate.opsForValue().set(generateKey(keyEnum, key), value, keyEnum.getTtl(), TimeUnit.SECONDS);
    }

    public void setEx(RedisKeyEnum keyEnum, String value) {
        redisTemplate.opsForValue().set(keyEnum.getKey(), value, keyEnum.getTtl(), TimeUnit.SECONDS);
    }

    /**
     * 保存key并设置过期时间
     *
     * @param key
     * @param value
     * @param timeout
     */
    public void setEx(RedisKeyEnum keyEnum, String key, String value, long timeout) {
        redisTemplate.opsForValue().set(generateKey(keyEnum, key), value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 设置生存时间
     *
     * @param keyEnum
     * @param key
     */
    public void expire(RedisKeyEnum keyEnum, String key) {
        redisTemplate.expire(generateKey(keyEnum, key), keyEnum.getTtl(), TimeUnit.SECONDS);
    }


    /**
     * 删除key
     *
     * @param keyEnum
     * @param key
     */
    public boolean delKey(RedisKeyEnum keyEnum, String key) {
        return redisTemplate.delete(generateKey(keyEnum, key));
    }

    public boolean delKey(RedisKeyEnum keyEnum) {
        return redisTemplate.delete(keyEnum.getKey());
    }

    public String hGet(RedisKeyEnum keyEnum, String key) {
        return (String) redisTemplate.opsForHash().get(keyEnum.getKey(), key);
    }

    public void hSet(RedisKeyEnum keyEnum, String key, String value) {
        redisTemplate.opsForHash().put(keyEnum.getKey(), key, value);
    }

    public boolean hExists(RedisKeyEnum keyEnum, String key) {
        return redisTemplate.opsForHash().hasKey(keyEnum.getKey(), key);
    }

    public boolean geoAdd(RedisKeyEnum keyEnum, double lng, double lat, String name) {
        Point point = new Point(lng, lat);
        return redisTemplate.opsForGeo().add(keyEnum.getKey(), point, name) > 0;
    }

    public boolean geoRemove(RedisKeyEnum keyEnum, String name) {
        return redisTemplate.opsForGeo().remove(keyEnum.getKey(), name) > 0;
    }

    public GeoResults<RedisGeoCommands.GeoLocation<String>> geoRadius(RedisKeyEnum keyEnum, double lng, double lat) {
        Distance distance = new Distance(10D, Metrics.KILOMETERS);
        Point point = new Point(lng, lat);
        Circle circle = new Circle(point, distance);
        return redisTemplate.opsForGeo().radius(keyEnum.getKey(), circle);
    }

    public GeoResults<RedisGeoCommands.GeoLocation<String>> geoRadius(RedisKeyEnum keyEnum, double lng, double lat, double range) {
        return redisTemplate.opsForGeo().radius(
                keyEnum.getKey(),
                new Circle(new Point(lng, lat), new Distance(range, Metrics.KILOMETERS)),
                RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().sortAscending());
    }

}
