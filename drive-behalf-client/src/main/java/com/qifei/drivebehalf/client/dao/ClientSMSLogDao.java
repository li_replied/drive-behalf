package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverSMSLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ClientSMSLogDao extends JpaRepository<DriverSMSLogEntity, Long>, JpaSpecificationExecutor<DriverSMSLogEntity> {
}
