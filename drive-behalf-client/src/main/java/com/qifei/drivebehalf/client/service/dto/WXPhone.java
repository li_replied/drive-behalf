package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WXPhone implements Serializable {
    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
}
