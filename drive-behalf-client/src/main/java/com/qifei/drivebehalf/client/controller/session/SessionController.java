package com.qifei.drivebehalf.client.controller.session;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "会话")
@RestController
@RequestMapping("/session")
public class SessionController {
    @ValidToken
    @ApiOperation("检查token是否合法")
    @GetMapping("/check")
    public ApiResponse<Void> check() throws Exception {
        return ApiResponse.createBySuccess();
    }
}
