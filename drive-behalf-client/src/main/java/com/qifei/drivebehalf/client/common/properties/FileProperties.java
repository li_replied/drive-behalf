package com.qifei.drivebehalf.client.common.properties;

import cn.hutool.core.date.LocalDateTimeUtil;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Component
@ConfigurationProperties(
        prefix = "file",
        ignoreUnknownFields = true
)
public class FileProperties {
    private String path;

    public String path(String fileName) {
        fileName = fileName.substring(fileName.lastIndexOf("."), fileName.length());
        String path = "/" +
                LocalDateTimeUtil.format(LocalDate.now(), "yyyy-MM-dd") + "/" + UUID.randomUUID().toString() + fileName;
        return path;
    }
}
