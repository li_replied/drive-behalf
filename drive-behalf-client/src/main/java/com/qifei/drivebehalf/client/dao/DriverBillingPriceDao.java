package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverBillingPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalTime;
import java.util.List;

public interface DriverBillingPriceDao extends JpaRepository<DriverBillingPriceEntity, Long>, JpaSpecificationExecutor<DriverBillingPriceEntity> {
    List<DriverBillingPriceEntity> queryDriverBillingPriceEntityByRegionIdOrderByStartTime(String regionId);

    DriverBillingPriceEntity queryFirstByRegionIdAndEndTimeGreaterThanEqualAndStartTimeLessThanEqual(String regionId, LocalTime end, LocalTime start);
}
