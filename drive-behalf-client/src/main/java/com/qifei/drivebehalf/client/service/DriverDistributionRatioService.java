package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.ProfitType;
import com.qifei.drivebehalf.client.dao.DriverDistributionDao;
import com.qifei.drivebehalf.client.dao.DriverDistributionRatioDao;
import com.qifei.drivebehalf.client.entity.DriverDistributionEntity;
import com.qifei.drivebehalf.client.entity.DriverDistributionRatioEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DriverDistributionRatioService {
    private final DriverDistributionRatioDao driverDistributionRatioDao;
    private final DriverDistributionDao driverDistributionDao;

    public DriverDistributionRatioEntity find() throws Exception {
        List<DriverDistributionRatioEntity> list = driverDistributionRatioDao.findAll();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public long count(String orderNo, Long userId) throws Exception {
        return driverDistributionDao.countByOrderNoAndUserId(orderNo, userId);
    }

    @Transactional
    public void distribution(String orderNo, Long userId, BigDecimal amount) throws Exception {
        DriverDistributionEntity driverDistributionEntity = new DriverDistributionEntity();
        driverDistributionEntity.setOrderNo(orderNo);
        driverDistributionEntity.setUserId(userId);
        driverDistributionEntity.setAmount(amount);
        driverDistributionEntity.setProfitType(ProfitType.USER);
        driverDistributionEntity.setCreate();
        driverDistributionDao.save(driverDistributionEntity);
    }
}
