package com.qifei.drivebehalf.client.pay;

import lombok.Data;

@Data
public class AlipayBean {
    /**
     * 商户订单号，必填
     *
     */
    private String outTradeNo;
    /**
     * 订单名称，必填
     */
    private String subject;
    /**
     * 付款金额，必填
     * 根据支付宝接口协议，必须使用下划线
     */
    private String totalAmount;
    /**
     * 商品描述，可空
     */
    private String body;
    /**
     * 超时时间参数
     */
    private String timeoutExpress = "10m";
    /**
     * 产品编号
     */
    private String productCode = "QUICK_MSECURITY_PAY";
 
}