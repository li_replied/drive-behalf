package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.PayAction;
import com.qifei.drivebehalf.client.common.enums.PayType;
import com.qifei.drivebehalf.client.common.enums.UserType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@Entity
@Table(
        name = "driver_pay",
        indexes = {
                @Index(name = "idx_driver_pay_order_number", columnList = "order_number"),
                @Index(name = "idx_driver_order_state_created_user", columnList = "created_user"),
        }
)
@DynamicUpdate
@GenericGenerator(name = "system-uuid", strategy = "uuid")
public class DriverPayEntity extends BaseEntity implements Serializable {
    /**
     * 支付id
     */
    @Id
    @GeneratedValue(generator = "system-uuid")
    @Column(name = "pay_id")
    private String id;
    @Column(name = "order_number", nullable = false)
    private String orderNumber;
    @Column(name = "passenger_id", nullable = false)
    private Long passengerId;
    @Column(name = "pay_amount", updatable = false)
    private BigDecimal payAmount = new BigDecimal(0);
    @Column(name = "prepay_id",length = 2000)
    private String prepayId;
    @Column(name = "pay_type")
    @Enumerated(EnumType.STRING)
    private PayType payType;
    @Column(name = "action")
    @Enumerated(EnumType.STRING)
    private PayAction action;
    @Column(name = "transactions_state")
    @Enumerated(EnumType.STRING)
    private EnableEnum transactionsState;
    @Column(name = "pay_state")
    @Enumerated(EnumType.STRING)
    private EnableEnum payState;
    @Column(name = "param", length = 2000)
    private String param;
    @Column(name = "result", length = 2000)
    private String result;
    @Column(name = "user_type")
    @Enumerated(EnumType.STRING)
    private UserType userType;

}
