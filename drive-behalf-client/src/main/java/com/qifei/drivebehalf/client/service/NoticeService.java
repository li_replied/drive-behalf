package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.DateUtil;
import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.base.impl.ListMessage;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.exceptions.PushSingleException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.style.Style0;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.common.utils.NumberFormatUtils;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.service.dto.WXSendMessage;
import com.qifei.drivebehalf.client.service.dto.WXSendMessageResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class NoticeService implements InitializingBean {
    private final RedisService redisService;
    private final MPWeiXinService mpWeiXinService;
    private final SmService smService;
    private final DriverService driverService;

    private IGtPush push;

    @Value("${weixin.accepted-message-id}")
    private String acceptedMessageId;
    @Value("${weixin.confirm-drive-id}")
    private String confirmDriveId;
    @Value("${weixin.confirm-delivered-id}")
    private String confirmDeliveredId;
    @Value("${ge-tui.app-id}")
    private String appId;
    @Value("${ge-tui.app-key}")
    private String appKey;
    @Value("${ge-tui.master-secret}")
    private String masterSecret;

    /**
     * 发送接单消息
     *
     * @param driverTripOrderEntity
     * @throws Exception
     */
    public void sendAccepted(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        PassengerEntity passengerEntity = driverTripOrderEntity.getPassenger();
        String userPhone = passengerEntity.getPhone();
        String title = "代驾师傅已接单";
        String orderNumber = driverTripOrderEntity.getOrderNumber();
        String name = driverTripOrderEntity.getDriver().getName();
        String phone = driverTripOrderEntity.getDriver().getPhone();
        String customerPhone = driverTripOrderEntity.getCustomerPhone();
        if (StringUtils.isEmpty(passengerEntity.getOpenId())) {
            smService.accepted(
                    userPhone,
                    orderNumber,
                    name,
                    phone,
                    customerPhone
            );
            return;
        }
        Map<String, WXSendMessage.DataMap> map = new HashMap<>();
        //接单信息
        map.put("thing1", new WXSendMessage.DataMap(title + "(" + orderNumber + ")"));
        //代驾司机
        map.put("thing2", new WXSendMessage.DataMap(name));
        //司机电话
        map.put("phone_number3", new WXSendMessage.DataMap(phone));
        //客服热线
        map.put("phone_number4", new WXSendMessage.DataMap(customerPhone));
        boolean result = send(
                passengerEntity.getOpenId(),
                acceptedMessageId,
                "/pages/order/orderdetails/orderdetails?ordernum=" + driverTripOrderEntity.getOrderNumber(),
                map
        );
        if (!result) {
            smService.accepted(
                    userPhone,
                    orderNumber,
                    name,
                    phone,
                    customerPhone
            );
        }
    }

    /**
     * 发送确认代驾开始
     *
     * @param driverTripOrderEntity
     * @throws Exception
     */
    public void sendConfirmDrive(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        PassengerEntity passengerEntity = driverTripOrderEntity.getPassenger();
        String title = "您好，代驾服务已开始。请确认您已上车。";
        String date = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        String name = driverTripOrderEntity.getDriver().getName() + "(" +
                driverTripOrderEntity.getDriver().getPhone() + ")";
        String customerPhone = driverTripOrderEntity.getCustomerPhone();
        if (StringUtils.isEmpty(passengerEntity.getOpenId())) {
            smService.confirmDrive(
                    passengerEntity.getPhone(),
                    title,
                    date,
                    name,
                    customerPhone
            );
            return;
        }
        Map<String, WXSendMessage.DataMap> map = new HashMap<>();
        //接单信息
        map.put("thing1", new WXSendMessage.DataMap(title));
        //时间
        map.put("time2", new WXSendMessage.DataMap(date));
        //司机
        map.put("thing3", new WXSendMessage.DataMap(name));
        //客服热线
        map.put("phone_number4", new WXSendMessage.DataMap(customerPhone));
        boolean result = send(
                passengerEntity.getOpenId(),
                confirmDriveId,
                "/pages/index/index",
                map
        );
        if (!result) {
            smService.confirmDrive(
                    passengerEntity.getPhone(),
                    title,
                    date,
                    name,
                    customerPhone
            );
        }
    }

    /**
     * 确认结束代驾
     *
     * @param driverTripOrderEntity
     * @throws Exception
     */
    public void sendConfirmDelivered(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        PassengerEntity passengerEntity = driverTripOrderEntity.getPassenger();
        String endPoint = driverTripOrderEntity.getEndPoint();
        String phone = driverTripOrderEntity.getDriver().getPhone();
        String amount = NumberFormatUtils.interceptTwoBits(driverTripOrderEntity.getPayAmount());
        String mileage = String.valueOf(driverTripOrderEntity.getMileage());
        String message = "请进入小程序确认并支付。";
        if (StringUtils.isEmpty(passengerEntity.getOpenId())) {
            smService.confirmDelivered(
                    passengerEntity.getPhone(),
                    endPoint,
                    phone,
                    amount,
                    mileage
            );
            return;
        }
        Map<String, WXSendMessage.DataMap> map = new HashMap<>();
        map.put("thing3", new WXSendMessage.DataMap(endPoint));
        map.put("phone_number2", new WXSendMessage.DataMap(phone));
        map.put("amount4", new WXSendMessage.DataMap(amount));
        map.put("thing5", new WXSendMessage.DataMap(mileage));
        map.put("thing9", new WXSendMessage.DataMap(message));
        boolean result = send(
                passengerEntity.getOpenId(),
                confirmDeliveredId,
                "/pages/index/index",
                map
        );
        if (!result) {
            smService.confirmDelivered(
                    passengerEntity.getPhone(),
                    endPoint,
                    phone,
                    amount,
                    mileage
            );
        }

    }

    public void sendAssigned(String address, String date) throws Exception {
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);
        Style0 style = new Style0();
        // 设置通知栏标题与内容
        style.setTitle("抢单通知");
        //用户确认通知
        //到账通知
        StringBuffer text = new StringBuffer();
        text.append("出发地：")
                .append(address)
                .append(" ")
                .append("出发时间：")
                .append(date);
        style.setText(text.toString());
        // 设置通知是否响铃，震动，或者可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        style.setChannelLevel(3); //设置通知渠道重要性
        style.setBadgeAddNum(1);
        template.setStyle(style);
        AppMessage message = new AppMessage();
        List<String> appIdList = new ArrayList<>();
        appIdList.add(appId);
        message.setAppIdList(appIdList);
        message.setData(template);
        // 设置消息离线，并设置离线时间
        message.setOffline(true);
        // 离线有效时间，单位为毫秒
        message.setOfflineExpireTime(24 * 1000 * 3600);
        // 厂商通道下发策略
        message.setStrategyJson("{\"default\":4,\"ios\":4,\"st\":4}");
        try {
            IPushResult result = push.pushMessageToApp(message);
            log.info(JsonUtils.writeValue(result));
        } catch (Exception e) {
            log.error("消息发送错误", e);
        }
    }

    /**
     * 抢单通知
     *
     * @param driverTripOrderEntity
     * @throws Exception
     */
    public void sendAssigned(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);
        Style0 style = new Style0();
        // 设置通知栏标题与内容
        style.setTitle("抢单通知");
        //用户确认通知
        //到账通知
        StringBuffer text = new StringBuffer();
        text.append("出发地：")
                .append(driverTripOrderEntity.getStartPoint())
                .append(" ")
                .append("目的地：")
                .append(driverTripOrderEntity.getEndPoint())
                .append(" ")
                .append("出发时间：")
                .append(DateUtil.format(driverTripOrderEntity.getAppointmentTime(), "yyyy-MM-dd HH:mm:ss"))
                .append(" ")
                .append("佣金：")
                .append(NumberFormatUtils.interceptTwoBits(driverTripOrderEntity.getCommission()));
        style.setText(text.toString());
        // 设置通知是否响铃，震动，或者可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        style.setChannelLevel(3); //设置通知渠道重要性
        style.setBadgeAddNum(1);
        template.setStyle(style);
        AppMessage message = new AppMessage();
        List<String> appIdList = new ArrayList<>();
        appIdList.add(appId);
        message.setAppIdList(appIdList);
        message.setData(template);
        // 设置消息离线，并设置离线时间
        message.setOffline(true);
        // 离线有效时间，单位为毫秒
        message.setOfflineExpireTime(24 * 1000 * 3600);
        // 厂商通道下发策略
        message.setStrategyJson("{\"default\":4,\"ios\":4,\"st\":4}");
        try {
            IPushResult result = push.pushMessageToApp(message);
            log.info(JsonUtils.writeValue(result));
        } catch (Exception e) {
            log.error("消息发送错误", e);
        }
    }

    /**
     * 确认通知
     *
     * @param driverTripOrderEntity
     * @throws Exception
     */
    public void sendDrive(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        DriverEntity driverEntity = driverTripOrderEntity.getDriver();
        if (driverEntity == null) {
            return;
        }
        if (StringUtils.isEmpty(driverEntity.getClientId())) {
            return;
        }
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);
        Style0 style = new Style0();
        // 设置通知栏标题与内容
        style.setTitle("用户确认通知");
        //用户确认通知
        //到账通知
        StringBuffer text = new StringBuffer();
        text.append("订单号：")
                .append(driverTripOrderEntity.getOrderNumber())
                .append(" ")
                .append("确认时间：")
                .append(DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        style.setText(text.toString());
        // 设置通知是否响铃，震动，或者可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        style.setChannelLevel(3); //设置通知渠道重要性
        style.setBadgeAddNum(1);
        template.setStyle(style);
        SingleMessage message = new SingleMessage();
        message.setData(template);
        // 设置消息离线，并设置离线时间
        message.setOffline(true);
        // 离线有效时间，单位为毫秒
        message.setOfflineExpireTime(24 * 1000 * 3600);
        // 厂商通道下发策略
        message.setStrategyJson("{\"default\":4,\"ios\":4,\"st\":4}");
        Target target = new Target();
        target.setAppId(appId);
        target.setClientId(driverEntity.getClientId());
        try {
            push.pushMessageToSingle(message, target);
        } catch (PushSingleException e) {
            log.error("发送消息错误", e);
        }
    }

    /**
     * 支付通知
     *
     * @param driverTripOrderEntity
     * @throws Exception
     */
    public void sendPayment(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        DriverEntity driverEntity = driverTripOrderEntity.getDriver();
        if (driverEntity == null) {
            return;
        }
        if (StringUtils.isEmpty(driverEntity.getClientId())) {
            return;
        }
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);
        Style0 style = new Style0();
        // 设置通知栏标题与内容
        style.setTitle("到账通知");
        //用户确认通知
        //到账通知
        StringBuffer text = new StringBuffer();
        text.append("订单号：")
                .append(driverTripOrderEntity.getOrderNumber())
                .append(" ")
                .append("金额：")
                .append(NumberFormatUtils.interceptTwoBits(driverTripOrderEntity.getCommission()) + "元");
        style.setText(text.toString());
        // 设置通知是否响铃，震动，或者可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        style.setChannelLevel(3); //设置通知渠道重要性
        style.setBadgeAddNum(1);
        template.setStyle(style);
        SingleMessage message = new SingleMessage();
        message.setData(template);
        // 设置消息离线，并设置离线时间
        message.setOffline(true);
        // 离线有效时间，单位为毫秒
        message.setOfflineExpireTime(24 * 1000 * 3600);
        // 厂商通道下发策略
        message.setStrategyJson("{\"default\":4,\"ios\":4,\"st\":4}");
        Target target = new Target();
        target.setAppId(appId);
        target.setClientId(driverEntity.getClientId());
        try {
            push.pushMessageToSingle(message, target);
        } catch (PushSingleException e) {
            log.error("发送消息错误", e);
        }
    }

    public void sendCancel(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        DriverEntity driverEntity = driverTripOrderEntity.getDriver();
        if (driverEntity == null) {
            return;
        }
        if (StringUtils.isEmpty(driverEntity.getClientId())) {
            return;
        }
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);
        Style0 style = new Style0();
        // 设置通知栏标题与内容
        style.setTitle("取消通知");
        //用户确认通知
        //到账通知
        StringBuffer text = new StringBuffer();
        text.append("订单号：")
                .append(driverTripOrderEntity.getOrderNumber())
                .append("出发地：")
                .append(driverTripOrderEntity.getStartPoint())
                .append(" ")
                .append("目的地：")
                .append(driverTripOrderEntity.getEndPoint())
                .append(" ")
                .append("出发时间：")
                .append(DateUtil.format(driverTripOrderEntity.getAppointmentTime(), "yyyy-MM-dd HH:mm:ss"));
        style.setText(text.toString());
        // 设置通知是否响铃，震动，或者可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        style.setChannelLevel(3); //设置通知渠道重要性
        style.setBadgeAddNum(1);
        template.setStyle(style);
        SingleMessage message = new SingleMessage();
        message.setData(template);
        // 设置消息离线，并设置离线时间
        message.setOffline(true);
        // 离线有效时间，单位为毫秒
        message.setOfflineExpireTime(24 * 1000 * 3600);
        // 厂商通道下发策略
        message.setStrategyJson("{\"default\":4,\"ios\":4,\"st\":4}");
        Target target = new Target();
        target.setAppId(appId);
        target.setClientId(driverEntity.getClientId());
        try {
            push.pushMessageToSingle(message, target);
        } catch (PushSingleException e) {
            log.error("发送消息错误", e);
        }
    }

    private boolean send(String openId, String templateId, String page, Map<String, WXSendMessage.DataMap> param) throws Exception {
        WXSendMessage wxSendMessage = new WXSendMessage();
        wxSendMessage.setTouser(openId);
        wxSendMessage.setTemplateId(templateId);
        wxSendMessage.setPage(page);
        wxSendMessage.setData(param);
        String accessToken = redisService.getKey(RedisKeyEnum.AccessToken);
        WXSendMessageResult sendMessage = mpWeiXinService.sendMessage(accessToken, JsonUtils.writeValue(wxSendMessage));
        return sendMessage.getErrcode() == 0;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        push = new IGtPush("https://api.getui.com/apiex.htm", appKey, masterSecret);
    }

}
