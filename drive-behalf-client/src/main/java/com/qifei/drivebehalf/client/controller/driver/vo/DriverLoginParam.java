package com.qifei.drivebehalf.client.controller.driver.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "司机绑定参数")
public class DriverLoginParam implements Serializable {
    @NotEmpty(message = "手机号不能为空")
    @Length(max = 11, min = 11, message = "手机号不正确")
    private String phone;
    @NotEmpty(message = "短信验证码不能为空")
    @Length(max = 5, min = 5, message = "短信验证码不正确")
    private String captcha;
    @NotEmpty(message = "设备标识不能为空")
    @Length(max = 32, min = 32, message = "设备标识无效")
    private String cid;
}