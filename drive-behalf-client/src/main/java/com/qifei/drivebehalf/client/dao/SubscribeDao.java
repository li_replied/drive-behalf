package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.SubscribeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface SubscribeDao extends JpaRepository<SubscribeEntity, Long>, JpaSpecificationExecutor<SubscribeEntity> {
    List<SubscribeEntity> queryAllByStateOrderByAppointmentTimeDesc(EnableEnum state);

    List<SubscribeEntity> queryAllByDriverAndStateOrderByAppointmentTimeDesc(DriverEntity driverEntity, EnableEnum state);
}
