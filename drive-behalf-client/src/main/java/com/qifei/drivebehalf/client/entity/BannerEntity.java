package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "driver_banner")
public class BannerEntity extends BaseEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "banner_id")
    private Long id;

    /** 广告名称 */
    @Column(name = "banner_name")
    private String bannerName;

    /** 广告类型 */
    @Column(name = "banner_type")
    private String bannerType;

    /** 文件路径 */
    @Column(name = "pic_path")
    private String picPath;

    /** 状态 */
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private EnableEnum status = EnableEnum.NO;

    /** 排序 */
    @Column(name = "sort")
    private Integer sort;

    @Column(name = "period")
    private Integer period;
}
