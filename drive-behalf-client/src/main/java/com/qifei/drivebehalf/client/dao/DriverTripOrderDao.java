package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface DriverTripOrderDao extends JpaRepository<DriverTripOrderEntity, Long>, JpaSpecificationExecutor<DriverTripOrderEntity> {

    long countByPassengerAndOrderStatusIn(PassengerEntity passengerEntity, OrderStateEnum[] orderStates);

    DriverTripOrderEntity findTopByPassengerAndOrderStatusIn(PassengerEntity passengerEntity, OrderStateEnum[] orderStates);

    DriverTripOrderEntity findTopByDriverAndOrderStatus(DriverEntity driver, OrderStateEnum status);

    long countByDriverAndOrderStatusIn(DriverEntity driverEntity, OrderStateEnum[] orderStates);

    List<DriverTripOrderEntity> findByDriverAndOrderStatusIn(DriverEntity driverEntity, OrderStateEnum[] orderStates);

    List<DriverTripOrderEntity> findByAppointmentTimeBeforeAndOrderStatusIn(Date date, OrderStateEnum[] orderStates);

    Page<DriverTripOrderEntity> queryAllByPassengerOrderByCreateTimeDesc(PassengerEntity passengerEntity, Pageable pageable);

    Page<DriverTripOrderEntity> queryAllByDriverOrderByCreateTimeDesc(DriverEntity driverEntity, Pageable pageable);

    DriverTripOrderEntity queryDriverTripOrderEntityByOrderNumber(String orderNumber);

    @Query(value = "select sum(commission) from driver_trip_order where driver_id=? and order_status='COMPLETE' and create_time>=date(now())", nativeQuery = true)
    BigDecimal sumTodayMoney(Long driverId);

    @Query(value = "select count(1) from driver_trip_order where driver_id=? and create_time>=date(now())", nativeQuery = true)
    int countTodayOrder(Long driverId);

    @Modifying
    @Query("update DriverTripOrderEntity set orderStatus=:orderState, " +
            "cancelReason=:cancelReason where orderNumber=:orderNumber")
    void updateOrderState(
            @Param("orderNumber") String orderNumber,
            @Param("orderState") OrderStateEnum orderState,
            @Param("cancelReason") String cancelReason
    );

    @Modifying
    @Query("update DriverTripOrderEntity set orderStatus=:orderState " +
            " where orderNumber=:orderNumber")
    void updateOrderState(
            @Param("orderNumber") String orderNumber,
            @Param("orderState") OrderStateEnum orderState
    );

    @Modifying
    @Query("update DriverTripOrderEntity set payMethod=:payType, orderStatus=:orderStatus,transactionDate=:transactionDate, payMethod=:payMethod  " +
            " where orderNumber=:orderNumber")
    void updatePayType(
            @Param("orderNumber") String orderNumber,
            @Param("orderStatus") OrderStateEnum orderStatus,
            @Param("payType") String payType,
            @Param("transactionDate") Date transactionDate,
            @Param("payMethod") String payMethod
    );

    @Modifying
    @Query("update DriverTripOrderEntity set orderStatus=:orderState, " +
            "updateTime=:updateTime, cancelReason=:reason where orderNumber=:orderNumber")
    @Transactional
    void updateOrderState(
            @Param("orderNumber") String orderNumber,
            @Param("orderState") OrderStateEnum orderState,
            @Param("updateTime") Date updateTime,
            @Param("reason") String reason
    );

    @Modifying
    @Query("update DriverTripOrderEntity set payId=:payId " +
            " where orderNumber=:orderNumber")
    void updatePayId(@Param("orderNumber") String orderNumber, @Param("payId") String payId);

    @Modifying
    @Query("update DriverTripOrderEntity set payTime=:payTime, " +
            "transactionId=:transactionId,orderStatus=:orderStatus " +
            "where orderNumber=:orderNumber")
    void updatePayState(
            @Param("orderNumber") String orderNumber,
            @Param("payTime") Date payTime,
            @Param("transactionId") String transactionId,
            @Param("orderStatus") OrderStateEnum orderState
    );

    @Modifying
    @Query("update DriverTripOrderEntity set driver=:driver, " +
            "orderStatus=:orderStatus " +
            "where orderNumber=:orderNumber")
    void updateDriverAndOrderStatus(
            @Param("orderNumber") String orderNumber,
            @Param("orderStatus") OrderStateEnum orderState,
            @Param("driver") DriverEntity driver
    );

    @Modifying
    @Query("update DriverTripOrderEntity set endLng=:endLng, endLat=:endLat,endPoint=:endPoint," +
            "mileage=:mileage,drivingTime=:drivingTime,estimatedPrice=:price,payAmount=:price,commission=:commission " +
            " where orderNumber=:orderNumber")
    void updateTransferOrder(
            @Param("orderNumber") String orderNumber,
            @Param("endLng") String endLng,
            @Param("endLat") String endLat,
            @Param("endPoint") String endPoint,
            @Param("mileage") Integer mileage,
            @Param("drivingTime") String drivingTime,
            @Param("price") BigDecimal price,
            @Param("commission") BigDecimal commission
    );

    @Modifying
    @Query("update DriverTripOrderEntity set invoiceState=:invoiceState where orderNumber=:orderNumber")
    void updateInvoice(@Param("orderNumber") String orderNumber, @Param("invoiceState") EnableEnum invoiceState);

    List<DriverTripOrderEntity> queryByOrderStatusAndTransactionDateBetween(OrderStateEnum orderState, Date begin, Date end);

    List<DriverTripOrderEntity> queryAllByOrderStatusInOrderByCreateTimeDesc(OrderStateEnum... stateEnum);

    List<DriverTripOrderEntity> queryAllByOrderNumberIn(String... orderNumber);

    DriverTripOrderEntity findByOrderNumber(String orderNumber);

    // 根据手机号查询指定乘客最近一笔订单
    DriverTripOrderEntity findTopByPassengerPhoneOrderByCreateTimeDesc(String phone);
}
