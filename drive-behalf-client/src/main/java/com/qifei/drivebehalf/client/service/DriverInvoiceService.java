package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.controller.invoice.vo.DrawInvoiceParam;
import com.qifei.drivebehalf.client.controller.invoice.vo.InvoiceTitleParam;
import com.qifei.drivebehalf.client.controller.invoice.vo.InvoiceTitleResult;
import com.qifei.drivebehalf.client.dao.DriverInvoiceDao;
import com.qifei.drivebehalf.client.dao.DriverInvoiceTitleDao;
import com.qifei.drivebehalf.client.entity.DriverInvoiceEntity;
import com.qifei.drivebehalf.client.entity.DriverInvoiceTitleEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DriverInvoiceService {
    private final DriverInvoiceTitleDao driverInvoiceTitleDao;
    private final DriverInvoiceDao driverInvoiceDao;
    private final DriverTripOrderService orderService;

    public List<DriverInvoiceTitleEntity> query() throws Exception {
        UserEntity entity = UserLoginUtils.getLogin();
        return driverInvoiceTitleDao.queryAllByUserIdAndDeleteFlag(entity.getId(), EnableEnum.NO);
    }

    public DriverInvoiceTitleEntity queryDefault() throws Exception {
        UserEntity entity = UserLoginUtils.getLogin();
        return driverInvoiceTitleDao.queryFirstByUserIdAndDefaultTagAndDeleteFlag(entity.getId(), EnableEnum.YES, EnableEnum.NO);
    }

    public DriverInvoiceTitleEntity find(Long id) throws Exception {
        return driverInvoiceTitleDao.findById(id).orElse(null);
    }

    @Transactional
    public void save(InvoiceTitleParam param) throws Exception {
        UserEntity entity = UserLoginUtils.getLogin();
        DriverInvoiceTitleEntity driverInvoiceTitleEntity = null;
        if (param.getId() != null) {
            Optional<DriverInvoiceTitleEntity> optional = driverInvoiceTitleDao.findById(param.getId());
            driverInvoiceTitleEntity = optional.orElse(null);
            if (driverInvoiceTitleEntity == null) {
                driverInvoiceTitleEntity = new DriverInvoiceTitleEntity();
                driverInvoiceTitleEntity.setCreate();
            }
            driverInvoiceTitleEntity.setModified();
        } else {
            driverInvoiceTitleEntity = new DriverInvoiceTitleEntity();
            driverInvoiceTitleEntity.setCreate();
        }
        driverInvoiceTitleEntity.setUserId(entity.getId());
        driverInvoiceTitleEntity.setTitleName(param.getTitleName());
        driverInvoiceTitleEntity.setUnitTaxNumber(param.getUnitTaxNumber());
        driverInvoiceTitleEntity.setMail(param.getMail());
        driverInvoiceTitleEntity.setPhone(param.getPhone());
        driverInvoiceTitleEntity.setBank(param.getBank());
        driverInvoiceTitleEntity.setBankAccount(param.getBankAccount());
        driverInvoiceTitleEntity.setAddress(param.getAddress());
        driverInvoiceTitleEntity.setRegisterPhone(param.getRegisterPhone());
        driverInvoiceTitleEntity.setDefaultTag(param.getDefaultTag());
        driverInvoiceTitleDao.save(driverInvoiceTitleEntity);
        if (driverInvoiceTitleEntity.getDefaultTag() == EnableEnum.YES) {
            driverInvoiceTitleDao.updateDefaultTag(entity.getId(), driverInvoiceTitleEntity.getId());
        }
    }

    @Transactional
    public void delete(Long id) throws Exception {
        driverInvoiceTitleDao.updateInvoiceTitleDeleteFlag(id);
    }

    public InvoiceTitleResult order(String orderNumber) throws Exception {
        UserEntity entity = UserLoginUtils.getLogin();
        DriverInvoiceEntity driverInvoiceEntity = driverInvoiceDao.queryFirstByUserIdAndOrderNo(entity.getId(), orderNumber);
        if (driverInvoiceEntity == null) {
            return null;
        }
        DriverInvoiceTitleEntity driverInvoiceTitleEntity = driverInvoiceTitleDao.findById(driverInvoiceEntity.getTitleId()).orElse(null);
        if (driverInvoiceTitleEntity == null) {
            return null;
        }
        InvoiceTitleResult invoiceTitleResult = new InvoiceTitleResult();
        BeanUtils.copyProperties(driverInvoiceTitleEntity, invoiceTitleResult);
        invoiceTitleResult.setState(driverInvoiceEntity.getState());
        return invoiceTitleResult;
    }

    @Transactional
    public void draw(DrawInvoiceParam param) throws Exception {
        UserEntity entity = UserLoginUtils.getLogin();
        DriverInvoiceEntity driverInvoiceEntity = driverInvoiceDao.queryFirstByUserIdAndOrderNo(entity.getId(), param.getOrderNumber());
        if (driverInvoiceEntity == null) {
            driverInvoiceEntity = new DriverInvoiceEntity();
            driverInvoiceEntity.setCreate();
        } else {
            Assert.isTrue(driverInvoiceEntity.getState() == EnableEnum.NO, "发票已发送邮箱，不允许修改。");
            driverInvoiceEntity.setModified();
        }
        driverInvoiceEntity.setTitleId(param.getTitleId());
        driverInvoiceEntity.setOrderNo(param.getOrderNumber());
        driverInvoiceEntity.setUserId(entity.getId());
        driverInvoiceDao.save(driverInvoiceEntity);
        orderService.updateInvoice(param.getOrderNumber(), EnableEnum.YES);
    }

    @Transactional
    public void cancel(String orderNumber) throws Exception {
        UserEntity entity = UserLoginUtils.getLogin();
        DriverInvoiceEntity driverInvoiceEntity = driverInvoiceDao.queryFirstByUserIdAndOrderNo(entity.getId(), orderNumber);
        Assert.notNull(driverInvoiceEntity, "未开发票。");
        Assert.isTrue(driverInvoiceEntity.getState() == EnableEnum.NO, "发票已发送邮箱，不允许撤销。");
        driverInvoiceDao.deleteByOrderNo(orderNumber);
        orderService.updateInvoice(orderNumber, EnableEnum.NO);
    }
}
