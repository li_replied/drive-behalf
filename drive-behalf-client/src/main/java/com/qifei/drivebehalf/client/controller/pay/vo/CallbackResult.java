package com.qifei.drivebehalf.client.controller.pay.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CallbackResult implements Serializable {
    private String code;

    private String message;
}
