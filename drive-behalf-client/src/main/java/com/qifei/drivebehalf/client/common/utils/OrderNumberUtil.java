package com.qifei.drivebehalf.client.common.utils;

import cn.hutool.core.date.DateUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class OrderNumberUtil {
    public static String orderNumber(String phone, int number) {
        Date now = DateUtil.beginOfDay(new Date());
        String date = DateUtil.format(now, "yyyyMMdd");
        String code = String.valueOf(Math.abs(phone.hashCode()));
        if (code.length() > 2) {
            code = code.substring(0, 2);
        }
        code = StringUtils.leftPad(code, 2, '0');
        String numberStr = String.valueOf(number);
        numberStr = StringUtils.leftPad(numberStr, 6, '0');
        return date + code + numberStr;
    }
}
