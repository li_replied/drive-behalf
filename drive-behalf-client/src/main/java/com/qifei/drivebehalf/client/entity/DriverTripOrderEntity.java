package com.qifei.drivebehalf.client.entity;

import javax.persistence.*;
import java.io.Serializable;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(
        name = "driver_trip_order",
        indexes = {
                @Index(name = "idx_driver_trip_order_passenger_id", columnList = "passenger_id"),
                @Index(name = "idx_driver_trip_order_driver_id", columnList = "driver_id"),
                @Index(name = "idx_driver_trip_order_create_time", columnList = "create_time"),
                @Index(name = "idx_driver_trip_order_order_number", columnList = "order_number", unique = true),
                @Index(name = "idx_driver_trip_order_coupon_id", columnList = "coupon_id")
        }
)
public class DriverTripOrderEntity extends BaseEntity implements Serializable {

    /**
     * 订单ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trip_order_id", unique = true, nullable = false)
    private Long tripOrderId;

    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "passenger_id", nullable = false)
    private PassengerEntity passenger;

    /**
     * 司机ID
     */
    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "driver_id")
    private DriverEntity driver;

    /**
     * 行程起点
     */
    @Column(name = "start_point", nullable = false)
    private String startPoint;

    /**
     * 行程终点
     */
    @Column(name = "end_point", nullable = false)
    private String endPoint;

    /**
     * 行程起点经纬度
     */
    @Column(name = "start_lng", nullable = false)
    private String startLng;

    /**
     * 行程起点经纬度
     */
    @Column(name = "start_lat", nullable = false)
    private String startLat;

    /**
     * 行程终点经纬度
     */
    @Column(name = "end_lng", nullable = false)
    private String endLng;

    /**
     * 行程终点经纬度
     */
    @Column(name = "end_lat", nullable = false)
    private String endLat;


    /**
     * 行驶时长
     */
    @Column(name = "driving_time")
    private String drivingTime;

    /**
     * 预约时间
     */
    @Column(name = "appointment_time", nullable = false)
    private Date appointmentTime;

    /**
     * 订单编号
     */
    @Column(name = "order_number", nullable = false)
    private String orderNumber;

    /**
     * 订单状态，1待接单，2已接单，3乘客已上车，4已完成，5已取消，6已超时，7已删除
     */
    @Column(name = "order_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStateEnum orderStatus;

    /**
     * 取消原因
     */
    @Column(name = "cancel_reason", length = 2000)
    private String cancelReason;

    /**
     * 支付方式，1微信，2支付宝
     */
    @Column(name = "pay_method")
    private String payMethod;

    /**
     * 支付流水号ID
     */
    @Column(name = "transaction_id")
    private String transactionId;
    /**
     * 下单时间
     */
    @Column(name = "transaction_date")
    private Date transactionDate;

    /**
     * 支付时间
     */
    @Column(name = "pay_time")
    private Date payTime;

    /**
     * 行程预估价
     */
    @Column(name = "estimated_price", nullable = false)
    private BigDecimal estimatedPrice = new BigDecimal(0);

    /**
     * 实付金额
     */
    @Column(name = "pay_amount")
    private BigDecimal payAmount = new BigDecimal(0);

    /**
     * 起步价
     */
    @Column(name = "starting_price")
    private BigDecimal startingPrice = new BigDecimal(0);

    /**
     * 起步公里数(3km)
     */
    @Column(name = "starting_kilometre")
    private Integer startingKilometre;

    /**
     * 里程费
     */
    @Column(name = "mileage_fee")
    private BigDecimal mileageFee = new BigDecimal(0);

    /**
     * 里程
     */
    @Column(name = "mileage", nullable = false)
    private Integer mileage;

    /**
     * 等待费
     */
    @Column(name = "waiting_fee")
    private BigDecimal waitingFee = new BigDecimal(0);

    /**
     * 返程费
     */
    @Column(name = "back_fee")
    private BigDecimal backFee = new BigDecimal(0);

    /**
     * 其他费用
     */
    @Column(name = "other_fee")
    private BigDecimal otherFee = new BigDecimal(0);

    /**
     * 加价倍数
     */
    @Column(name = "price_multiple")
    private BigDecimal priceMultiple = new BigDecimal(0);

    /**
     * 优惠金额
     */
    @Column(name = "coupon_amount")
    private BigDecimal couponAmount = new BigDecimal(0);

    /**
     * 优惠券ID
     */
    @Column(name = "coupon_id")
    private Long couponId;

    @Column(name = "region_id")
    private String regionId;
    /**
     * 客服热线
     */
    @Column(name = "customer_phone")
    private String customerPhone;
    /**
     * 佣金
     */
    @Column(name = "commission", precision = 10, scale = 2)
    private BigDecimal commission = new BigDecimal(0);

    @Column(name = "commission_ratio")
    private BigDecimal commissionRatio = new BigDecimal(0);
    /**
     * 固定抽成
     */
    @Column(name = "fix_deduct")
    private BigDecimal fixDeduct = new BigDecimal(0);

    /**
     * 发票状态
     */
    @Column(name = "invoice_state")
    @Enumerated(EnumType.STRING)
    private EnableEnum invoiceState = EnableEnum.NO;
    @Column(name = "pay_id")
    private String payId;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "waiting_time")
    private Integer waitingTime;

    @Column(name = "waiting_price")
    private BigDecimal waitingPrice = new BigDecimal(0);

    @Transient
    private Double distance;
}
