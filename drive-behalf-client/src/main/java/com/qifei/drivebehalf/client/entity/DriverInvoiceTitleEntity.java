package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "driver_invoice_title",
        indexes = {
                @Index(name = "idx_driver_invoice_title_user_id", columnList = "user_id"),
        })
public class DriverInvoiceTitleEntity extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "title_id")
    private Long id;
    /**
     * 用户
     */
    @Column(name = "user_id", nullable = false)
    private Long userId;
    /**
     * 抬头名称
     */
    @Column(name = "title_name", nullable = false, length = 500)
    private String titleName;
    /**
     * 单位税号
     */
    @Column(name = "unit_tax_number", nullable = false)
    private String unitTaxNumber;
    /**
     * 邮箱
     */
    @Column(name = "mail", nullable = false)
    private String mail;
    /**
     * 注册电话
     */
    @Column(name = "phone")
    private String phone;
    /**
     * 开户银行
     */
    @Column(name = "bank", length = 500)
    private String bank;
    /**
     * 银行账户
     */
    @Column(name = "bank_account")
    private String bankAccount;
    /**
     * 注册地址
     */
    @Column(name = "address")
    private String address;
    /**
     * 企业电话
     */
    @Column(name = "register_phone")
    private String registerPhone;
    /**
     * 默认标识
     */
    @Column(name = "default_tag")
    @Enumerated(EnumType.STRING)
    private EnableEnum defaultTag = EnableEnum.NO;

    @Column(name = "delete_flag")
    @Enumerated(EnumType.STRING)
    private EnableEnum deleteFlag = EnableEnum.NO;
}
