package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class WXSendMessageResult implements Serializable {
    private Integer errcode;
    private String errmsg;
}
