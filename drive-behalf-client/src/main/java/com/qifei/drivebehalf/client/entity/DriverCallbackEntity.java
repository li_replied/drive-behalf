package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "driver_callback")
public class DriverCallbackEntity extends BaseEntity {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "callback_id", unique = true, nullable = false)
    private Long callbackId;
    @Column(name = "wechatpay_serial")
    private String wechatpaySerial;
    @Column(name = "wechatpay_timestamp")
    private String wechatpayTimestamp;
    @Column(name = "wechatpay_nonce")
    private String wechatpayNonce;
    @Column(name = "wechatpay_signature",length = 2000)
    private String wechatpaySignature;
    @Column(name = "body", length = 2000)
    private String body;
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private EnableEnum state = EnableEnum.NO;
    @Column(name = "payCallback", length = 2000)
    private String payCallback;
}
