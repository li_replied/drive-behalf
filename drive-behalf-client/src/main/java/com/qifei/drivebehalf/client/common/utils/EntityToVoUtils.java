package com.qifei.drivebehalf.client.common.utils;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class EntityToVoUtils {

    public static <T> T copyProperties(BaseEntity entity, Class<T> cls) throws Exception {
        T vo = cls.newInstance();
        BeanUtils.copyProperties(entity, vo);
        Field[] fields = cls.getDeclaredFields();
        if (fields == null || fields.length == 0) {
            return vo;
        }
        for (Field field : fields) {
            Class fieldCls = field.getType();
            if (BeanUtils.isSimpleValueType(fieldCls)) {
                continue;
            }
            String getMethodName = "get" + StringUtils.capitalize(field.getName());
            Method getMethod = BeanUtils.findMethod(entity.getClass(), getMethodName);
            if (getMethod == null) {
                continue;
            }
            Object sourceProperties = getMethod.invoke(entity);
            if (sourceProperties == null) {
                continue;
            }
            String setMethodName = "set" + StringUtils.capitalize(field.getName());
            Method setMethod = BeanUtils.findMethod(cls, setMethodName, fieldCls);
            if (setMethod == null) {
                continue;
            }
            Object properties = null;
            if (fieldCls.isArray() && sourceProperties.getClass().isArray()) {
                List<BaseEntity> list = (List<BaseEntity>) sourceProperties;
                Class propertiesCls = actualType(field);
                if (propertiesCls == null) {
                    continue;
                }
                properties = copyProperties(list, propertiesCls);
            } else {
                properties = fieldCls.newInstance();
                BeanUtils.copyProperties(sourceProperties, properties);
            }
            setMethod.invoke(vo, properties);
        }
        return vo;
    }

    public static <T> List<T> copyProperties(List<? extends BaseEntity> list, Class<T> cls) throws Exception {
        if (list == null || list.isEmpty()) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (BaseEntity entity : list) {
            T vo = copyProperties(entity, cls);
            result.add(vo);
        }
        return result;
    }

    private static Class actualType(Field field) throws Exception {
        Type type = field.getGenericType();
        if (type == null) {
            return null;
        }
        if (!(type instanceof ParameterizedType)) {
            return null;
        }
        ParameterizedType pt = (ParameterizedType) type;
        return pt.getActualTypeArguments()[0].getClass();
    }
}
