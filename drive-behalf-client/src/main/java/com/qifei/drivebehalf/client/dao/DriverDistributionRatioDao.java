package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverDistributionRatioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverDistributionRatioDao extends JpaRepository<DriverDistributionRatioEntity, Long>, JpaSpecificationExecutor<DriverDistributionRatioEntity> {
}
