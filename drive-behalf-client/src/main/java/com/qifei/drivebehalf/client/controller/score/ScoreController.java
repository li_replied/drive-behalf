package com.qifei.drivebehalf.client.controller.score;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.model.PageResult;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.score.vo.ExchangeParam;
import com.qifei.drivebehalf.client.controller.score.vo.RecentlyResult;
import com.qifei.drivebehalf.client.controller.score.vo.ScoreResult;
import com.qifei.drivebehalf.client.controller.score.vo.ScoreTotalResult;
import com.qifei.drivebehalf.client.entity.ScoreEntity;
import com.qifei.drivebehalf.client.service.ScoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "积分管理")
@RestController
@RequestMapping("/score")
public class ScoreController {
    @Resource
    private ScoreService scoreService;

    @ValidToken
    @ApiOperation("积分统计")
    @GetMapping("/total")
    public ApiResponse<ScoreTotalResult> total() throws Exception {
        ScoreTotalResult result = scoreService.total();
        return ApiResponse.createBySuccess(result);
    }


    @ValidToken
    @ApiOperation("积分记录")
    @GetMapping("/query")
    public ApiResponse<PageResult<ScoreResult>> query(@Validated PageParam param) throws Exception {
        Page<ScoreEntity> page = scoreService.query(param);
        PageResult<ScoreResult> result = PageResult.create(page, ScoreResult.class);
        return ApiResponse.createBySuccess(result);
    }

    @ValidToken
    @ApiOperation("兑换积分")
    @PostMapping("/exchange")
    public ApiResponse<RecentlyResult> exchange(@Valid @RequestBody ExchangeParam param) throws Exception {
        scoreService.exchange(param);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("最近记录")
    @GetMapping("/recently")
    public ApiResponse<RecentlyResult> recently() throws Exception {
        ScoreEntity scoreEntity = scoreService.recently();
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(scoreEntity, RecentlyResult.class));
    }
}
