package com.qifei.drivebehalf.client.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(
        prefix = "weixin"
)
public class WeiXinProperties {
    private String appId;
    private String androidClientId;
    private String androidDriverId;
    private String secret;
    private String mchId;
    private String serialNo;
    private String apiV3Key;
    private String apiKey;
    private String notifyUrl;
}
