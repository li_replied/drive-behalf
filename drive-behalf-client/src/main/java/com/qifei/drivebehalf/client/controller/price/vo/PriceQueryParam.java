package com.qifei.drivebehalf.client.controller.price.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "查询价格")
public class PriceQueryParam implements Serializable {
    @ApiModelProperty("区域id")
    @NotEmpty(message = "区域id不能为空。")
    private String regionId;
}
