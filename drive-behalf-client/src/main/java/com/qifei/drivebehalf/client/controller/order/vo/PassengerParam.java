package com.qifei.drivebehalf.client.controller.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "呼叫代驾参数")
public class PassengerParam implements Serializable {
    @ApiModelProperty("起点")
    @NotEmpty(message = "起点地址不能为空")
    private String startPlace;
    @NotNull(message = "起点纬度不能为空")
    @ApiModelProperty("开始纬度")
    @Range(min = 18, max = 52, message = "当前位置不支持")
    private Double startLat;
    @NotNull(message = "起点经度不能为空")
    @ApiModelProperty("开始经度")
    @Range(min = 72, max = 132, message = "当前位置不支持")
    private Double startLng;
    @NotNull(message = "代驾人数")
    @Range(min = 1, max = 10)
    @ApiModelProperty("代驾人数")
    private Integer needDriverCount;
}
