package com.qifei.drivebehalf.client.controller.passenger.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ApiModel(description = "用户信息")
public class PassengerResult implements Serializable {
    @ApiModelProperty("id")
    protected Long id;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("是否显示姓名")
    private EnableEnum nameShow = EnableEnum.NO;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("生日")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    @ApiModelProperty("性别")
    private SexEnum sex = SexEnum.M;
    @ApiModelProperty("充值金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal money = new BigDecimal(0);
    @ApiModelProperty("分销金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal retail = new BigDecimal(0);
    @ApiModelProperty("绑定时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date relationTime;
}
