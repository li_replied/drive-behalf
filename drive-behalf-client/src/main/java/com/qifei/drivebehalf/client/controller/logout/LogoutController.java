package com.qifei.drivebehalf.client.controller.logout;

import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "注销用户")
@RestController
@RequiredArgsConstructor
public class LogoutController {
    private final RedisService redisService;

    @ApiOperation("注销用户")
    @PostMapping("/logout")
    public ApiResponse<Void> logout() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        if (userEntity != null) {
            redisService.delKey(RedisKeyEnum.UserToken, userEntity.getUserType() + String.valueOf(userEntity.getId()));
        }
        return ApiResponse.createBySuccess();
    }
}
