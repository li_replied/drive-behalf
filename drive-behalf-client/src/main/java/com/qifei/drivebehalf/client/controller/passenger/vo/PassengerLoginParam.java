package com.qifei.drivebehalf.client.controller.passenger.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "用户登录参数")
public class PassengerLoginParam implements Serializable {
    @ApiModelProperty("手机号")
    @NotEmpty(message = "手机号不能为空")
    @Length(max = 11, min = 11, message = "手机号不正确")
    private String phone;
    @ApiModelProperty("短信验证码")
    @NotEmpty(message = "短信验证码不能为空")
    @Length(max = 6, min = 5, message = "短信验证码不正确")
    private String smsCode;
    @ApiModelProperty("微信公众号/小程序登录code")
    private String jsCode;
}
