package com.qifei.drivebehalf.client.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(
        prefix = "ali",
        ignoreUnknownFields = true
)
public class AliYunProperties {
    private String regionId = "cn-hangzhou";
    private String accessKeyId;
    private String accessSecret;
}
