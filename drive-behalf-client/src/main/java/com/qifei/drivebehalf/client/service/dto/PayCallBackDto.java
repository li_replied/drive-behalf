package com.qifei.drivebehalf.client.service.dto;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class PayCallBackDto implements Serializable {
    private String payId;
    private String orderNumber;
    private String transactionId;
    private Date successTime;
    private EnableEnum payState;
    private BigDecimal payAmount;
    private Long passengerId;
}
