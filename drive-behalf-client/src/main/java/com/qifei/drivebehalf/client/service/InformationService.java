package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.dao.NoticeDao;
import com.qifei.drivebehalf.client.entity.NoticeEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InformationService {
    private final NoticeDao noticeDao;

    public NoticeEntity info() {
        return noticeDao.findFirstByStatusOrderByCreateTimeDesc(EnableEnum.YES);
    }

    public List<NoticeEntity> list() {
        return noticeDao.findNoticeEntitiesByStatusOrderByCreateTimeDesc(EnableEnum.YES);
    }
}
