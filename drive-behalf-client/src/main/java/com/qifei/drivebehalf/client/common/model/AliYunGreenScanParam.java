package com.qifei.drivebehalf.client.common.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 阿里云文字安全校验参数
 */
@Data
public class AliYunGreenScanParam {
    private String bizType;
    private List<String> scenes = Arrays.asList("antispam");
    private List<AliYunGreenScanTaskParam> tasks = new ArrayList<>();
}
