package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.qifei.drivebehalf.client.common.enums.*;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.properties.WeiXinProperties;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.common.utils.MD5;
import com.qifei.drivebehalf.client.common.utils.UUIDUtils;
import com.qifei.drivebehalf.client.controller.pay.vo.AppSign;
import com.qifei.drivebehalf.client.controller.pay.vo.BalanceParam;
import com.qifei.drivebehalf.client.controller.pay.vo.JsapiSign;
import com.qifei.drivebehalf.client.controller.pay.vo.RechargeParam;
import com.qifei.drivebehalf.client.dao.DriverCallbackDao;
import com.qifei.drivebehalf.client.dao.DriverPayDao;
import com.qifei.drivebehalf.client.entity.*;
import com.qifei.drivebehalf.client.service.dto.*;
import com.wechat.pay.contrib.apache.httpclient.WechatPayHttpClientBuilder;
import com.wechat.pay.contrib.apache.httpclient.auth.AutoUpdateCertificatesVerifier;
import com.wechat.pay.contrib.apache.httpclient.auth.PrivateKeySigner;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Credentials;
import com.wechat.pay.contrib.apache.httpclient.auth.WechatPay2Validator;
import com.wechat.pay.contrib.apache.httpclient.util.AesUtil;
import com.wechat.pay.contrib.apache.httpclient.util.PemUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Base64;
import java.util.Date;

@Slf4j
@Service
@RequiredArgsConstructor
public class WxPayService implements InitializingBean {

    private final DriverTripOrderService driverTripOrderService;

    private final DriverPayDao driverPayDao;

    private final DriverCallbackDao driverCallbackDao;

    private final PassengerService passengerService;

    private final KafkaProducer kafkaProducer;

    private final DriverRechargeService driverRechargeService;

    private final ShortMessageService shortMessageService;

    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";


    @Resource
    private RedisService redisService;


    @Resource
    private WeiXinProperties weiXinProperties;

    @Value("${weixin.transactions-url}")
    private String transactionsUrl;
    @Value("${weixin.query-out-trade-no-url}")
    private String queryOutTradeNoUrl;

    private PrivateKey privateKey;

    private AutoUpdateCertificatesVerifier verifier;

    private CloseableHttpClient httpClient;

    @Override
    public void afterPropertiesSet() throws Exception {
        DefaultResourceLoader resourceLoader = new DefaultResourceLoader();
        InputStream inputStream = resourceLoader.getResource("classpath:qfkj.pem").getInputStream();
        privateKey = PemUtil.loadPrivateKey(inputStream);
        //使用自动更新的签名验证器，不需要传入证书
        verifier = new AutoUpdateCertificatesVerifier(
                new WechatPay2Credentials(weiXinProperties.getMchId(), new PrivateKeySigner(weiXinProperties.getSerialNo(), privateKey)),
                weiXinProperties.getApiV3Key().getBytes("utf-8")
        );
        httpClient = WechatPayHttpClientBuilder.create()
                .withMerchant(weiXinProperties.getMchId(), weiXinProperties.getSerialNo(), privateKey)
                .withValidator(new WechatPay2Validator(verifier))
                .build();
    }

    @Transactional(dontRollbackOn = {ServiceWorkingException.class, IllegalArgumentException.class})
    public String placeOrder(PayType type, String orderNumber) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, orderNumber)) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderService.info(orderNumber);
            Assert.notNull(driverTripOrderEntity, "订单不存在。");
            if (OrderStateEnum.PAYMENT != driverTripOrderEntity.getOrderStatus()) {
                if (OrderStateEnum.PAYING == driverTripOrderEntity.getOrderStatus()) {
                    WXCallback wxCallback = payState(driverTripOrderEntity.getPayId());
                    if ("SUCCESS".equals(wxCallback.getTradeState())) {
                        throw new ServiceWorkingException("订单已经支付。请到我的订单查询订单状态。");
                    }
                } else if (OrderStateEnum.COMPLETE == driverTripOrderEntity.getOrderStatus()) {
                    throw new ServiceWorkingException("订单已经支付。请到我的订单查询订单状态。");
                } else {
                    throw new ServiceWorkingException("订单不可支付。");
                }
            }
            String paying = redisService.getKey(RedisKeyEnum.Paying, driverTripOrderEntity.getOrderNumber());
            if (StringUtils.isNotEmpty(paying)) {
                return paying;
            }
            // 免单
//            PassengerEntity userEntity = (PassengerEntity) UserLoginUtils.getLogin();
//            userEntity = passengerService.info(userEntity.getId());
//            Assert.notNull(userEntity, "用户不存在。");
//            if (EnableEnum.YES == userEntity.getFreeState()) {
//                driverTripOrderService.updatePay(
//                        driverTripOrderEntity.getOrderNumber(),
//                        new Date(),
//                        driverTripOrderEntity.getOrderNumber(),
//                        EnableEnum.YES
//                );
//                return null;
//            }
            String[] payId = null;
            switch (type) {
                case jsapi:
                    payId = jsapiPlaceOrder(orderNumber, driverTripOrderEntity.getPayAmount(), PayAction.consume);
                    break;
                case app:
                    payId = appPlaceOrder(orderNumber, driverTripOrderEntity.getPayAmount(), PayAction.consume);
                    break;
                default:
                    throw new ServiceWorkingException("支付类型错误。");
            }
            driverTripOrderService.updatePayId(orderNumber, payId[1]);
            driverTripOrderService.updatePayType(orderNumber, OrderStateEnum.PAYING, type.name(), PayMethod.WEBCHAT.name());
            return payId[0];
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, orderNumber);
        }
    }

    public String[] jsapiPlaceOrder(String orderNumber, BigDecimal amount, PayAction payAction) throws Exception {
        PassengerEntity userEntity = (PassengerEntity) UserLoginUtils.getLogin();
        Assert.isTrue(StringUtils.isNotEmpty(userEntity.getOpenId()), "openid不存在");
        String logId = log(PayType.jsapi, orderNumber, payAction, amount, userEntity.getId(), userEntity.getUserType());
        WXTransactions wxTransactions = new WXTransactions();
        wxTransactions.setAppid(weiXinProperties.getAppId());
        wxTransactions.setMchid(weiXinProperties.getMchId());
        wxTransactions.setDescription("红色代驾");
        wxTransactions.setOutTradeNo(logId);
        Date timeExpire = DateUtil.offset(new Date(), DateField.HOUR_OF_DAY, 2);
        wxTransactions.setTimeExpire(DateUtil.format(timeExpire, "yyyy-MM-dd'T'HH:mm:ssXXX"));
        wxTransactions.setNotifyUrl(weiXinProperties.getNotifyUrl());
        BigDecimal payAmount = amount.multiply(new BigDecimal(100));
        wxTransactions.setAmount(new WXAmount(payAmount.intValue()));
        wxTransactions.setPayer(new WXPayer(userEntity.getOpenId()));
        wxTransactions.setAttach(orderNumber);
        String json = JsonUtils.writeValue(wxTransactions);
        updateParam(logId, json);
        HttpPost httpPost = new HttpPost(String.format(transactionsUrl, PayType.jsapi.name()));
        StringEntity stringEntity = new StringEntity(json, "UTF-8");
        httpPost.setEntity(stringEntity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("User-Agent", "qfdj-1.0.0");
        httpPost.setHeader("Content-Type", CONTENT_TYPE);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        String body = getBody(response);
        if (response.getStatusLine().getStatusCode() != 200) {
            this.update(logId, null, body);
            throw new ServiceWorkingException("支付异常。");
        }
        WXTransactionsResult result = JsonUtils.readValue(body, WXTransactionsResult.class);
        this.update(logId, result.getPrepayId(), body);
        redisService.setKey(RedisKeyEnum.Paying, logId, result.getPrepayId());
        return new String[]{result.getPrepayId(), logId};
    }

    public String[] appPlaceOrder(String orderNumber, BigDecimal amount, PayAction payAction) throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        String logId = log(PayType.app, orderNumber, payAction, amount, userEntity.getId(), userEntity.getUserType());
        WXTransactions wxTransactions = new WXTransactions();
        if (userEntity.getUserType() == UserType.DRIVER) {
            wxTransactions.setAppid(weiXinProperties.getAndroidDriverId());
        } else {
            wxTransactions.setAppid(weiXinProperties.getAndroidClientId());
        }
        wxTransactions.setMchid(weiXinProperties.getMchId());
        wxTransactions.setDescription("红色代驾");
        wxTransactions.setOutTradeNo(logId);
        Date timeExpire = DateUtil.offset(new Date(), DateField.HOUR_OF_DAY, 2);
        wxTransactions.setTimeExpire(DateUtil.format(timeExpire, "yyyy-MM-dd'T'HH:mm:ssXXX"));
        wxTransactions.setNotifyUrl(weiXinProperties.getNotifyUrl());
        BigDecimal payAmount = amount.multiply(new BigDecimal(100)); // 微信支付金额只支持整数，这里要精确到分，换算
        wxTransactions.setAmount(new WXAmount(payAmount.intValue()));
        wxTransactions.setAttach(orderNumber);
        String json = JsonUtils.writeValue(wxTransactions);
        updateParam(logId, json);
        StringEntity stringEntity = new StringEntity(json, "UTF-8");
        HttpPost httpPost = new HttpPost(String.format(transactionsUrl, PayType.app.name()));
        httpPost.setEntity(stringEntity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("User-Agent", "qfdj-1.0.0");
        httpPost.setHeader("Content-Type", CONTENT_TYPE);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        String body = getBody(response);
        if (response.getStatusLine().getStatusCode() != 200) {
            this.update(logId, null, body);
            throw new ServiceWorkingException("支付异常。");
        }
        WXTransactionsResult result = JsonUtils.readValue(body, WXTransactionsResult.class);
        this.update(logId, result.getPrepayId(), body);
        redisService.setKey(RedisKeyEnum.Paying, logId, result.getPrepayId());
        return new String[]{result.getPrepayId(), logId};
    }

    public JsapiSign jsapiSign(String prepayId) throws Exception {
        JsapiSign payResult = new JsapiSign();
        StringBuffer message = new StringBuffer();
        payResult.setAppId(weiXinProperties.getAppId());
        message.append(payResult.getAppId() + "\n");
        payResult.setTimeStamp(String.valueOf(System.currentTimeMillis() / 1000L));
        message.append(payResult.getTimeStamp() + "\n");
        payResult.setNonceStr(UUIDUtils.uuid());
        message.append(payResult.getNonceStr() + "\n");
        payResult.setPackageStr(String.format("prepay_id=%s", prepayId));
        message.append(payResult.getPackageStr() + "\n");
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(privateKey);
        sign.update(message.toString().getBytes("UTF-8"));
        String signStr = Base64.getEncoder().encodeToString(sign.sign());
        payResult.setPaySign(signStr);
        return payResult;
    }

    public AppSign appSign(String prepayId) throws Exception {
        AppSign payResult = new AppSign();
        payResult.setPartnerid(weiXinProperties.getMchId());
        if (UserLoginUtils.getLogin().getUserType() == UserType.DRIVER) {
            payResult.setAppid(weiXinProperties.getAndroidDriverId());
        } else {
            payResult.setAppid(weiXinProperties.getAndroidClientId());
        }
        // 支付参数签名
        StringBuffer message = new StringBuffer();
        message.append(payResult.getAppid() + "\n");
        payResult.setTimestamp(String.valueOf(System.currentTimeMillis() / 1000L));
        message.append(payResult.getTimestamp() + "\n");
        payResult.setNoncestr(UUIDUtils.uuid());
        message.append(payResult.getNoncestr() + "\n");
        payResult.setPrepayid(prepayId);
        message.append(prepayId + "\n");
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(privateKey);
        sign.update(message.toString().getBytes("UTF-8"));
        String signStr = Base64.getEncoder().encodeToString(sign.sign());
        payResult.setSign(signStr);
        return payResult;
    }

    /**
     * 取消支付
     *
     * @param orderNumber
     * @throws Exception
     */
    @Transactional
    public void cancel(String orderNumber) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, orderNumber)) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderService.info(orderNumber);
            Assert.notNull(driverTripOrderEntity, "订单不存在");
            if (OrderStateEnum.PAYING == driverTripOrderEntity.getOrderStatus()) {
                driverTripOrderService.cancelPay(orderNumber);
            }
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, orderNumber);
        }
    }

    @Transactional(dontRollbackOn = {ServiceWorkingException.class, IllegalArgumentException.class})
    public String recharge(RechargeParam param) throws Exception {
        DriverRechargeEntity DriverRechargeEntity = driverRechargeService.order(param.getOrderNumber());
        Assert.notNull(DriverRechargeEntity, "订单不存在");
        redisService.setKey(RedisKeyEnum.Paying, param.getOrderNumber());
        String payId[] = null;
        switch (param.getPayType()) {
            case jsapi:
                payId = jsapiPlaceOrder(param.getOrderNumber(), DriverRechargeEntity.getAmount(), PayAction.recharge);
                break;
            case app:
                payId = appPlaceOrder(param.getOrderNumber(), DriverRechargeEntity.getAmount(), PayAction.recharge);
                break;
            default:
                throw new ServiceWorkingException("支付类型错误。");
        }
        driverRechargeService.updatePayId(param.getOrderNumber(), payId[0], payId[1]);
        return payId[0];
    }

    public void callback(
            String wechatpaySerial,
            String wechatpayTimestamp,
            String wechatpayNonce,
            String wechatpaySignature,
            String body
    ) throws Exception {
        Long logId = callbackLog(wechatpaySerial, wechatpayTimestamp, wechatpayNonce, wechatpaySignature, body);
        EnableEnum state = EnableEnum.NO;
        String text = null;
        try {
            Assert.hasText(wechatpaySerial, "Wechatpay-Serial 为空");
            Assert.hasText(wechatpayTimestamp, "Wechatpay-Timestamp-Serial 为空");
            Assert.hasText(wechatpayNonce, "Wechatpay-Nonce 为空");
            Assert.hasText(wechatpaySignature, "Wechatpay-Signature 为空");
            Assert.hasText(body, "body 为空");
            state = EnableEnum.YES;
            StringBuffer sig = new StringBuffer();
            sig.append(wechatpayTimestamp + "\n");
            sig.append(wechatpayNonce + "\n");
            sig.append(body + "\n");
            boolean verifierBol = verifier.verify(wechatpaySerial, sig.toString().getBytes("UTF-8"), wechatpaySignature);
            if (!verifierBol) {
                throw new ServiceWorkingException("签名错误");
            }
            WXCallbackMessage message = JsonUtils.readValue(body, WXCallbackMessage.class);
            Assert.notNull(message, "消息体为空");
            Assert.notNull(message.getResource(), "通知资源为空");
            WXCallbackResource resource = message.getResource();
            Assert.hasText(resource.getCiphertext(), "密文数据为空");
            Assert.hasText(resource.getAssociatedData(), "附加数据为空");
            Assert.hasText(resource.getNonce(), "随机串为空");
            AesUtil aesUtil = new AesUtil(weiXinProperties.getApiV3Key().getBytes("UTF-8"));
            text = aesUtil.decryptToString(
                    resource.getAssociatedData().getBytes("UTF-8"),
                    resource.getNonce().getBytes("UTF-8"),
                    resource.getCiphertext()
            );
            if (StringUtils.isEmpty(text)) {
                throw new ServiceWorkingException("解密错误");
            }
            WXCallback wxCallback = JsonUtils.readValue(text, WXCallback.class);
            Assert.notNull(wxCallback, "密文转换错误");
            kafkaProducer.payCallBack(wxCallback);
        } finally {
            updateCallbackLog(logId, state, text);
        }
    }

    @Transactional
    public void payCallBack(WXCallback wxCallback) throws Exception {
        DriverPayEntity driverPayEntity = find(wxCallback.getOutTradeNo());
        Assert.notNull(driverPayEntity, "支付记录为空");
        EnableEnum payState = EnableEnum.NO;
        if ("SUCCESS".equals(wxCallback.getTradeState())) {
            payState = EnableEnum.YES;
            redisService.delKey(RedisKeyEnum.Paying, wxCallback.getOutTradeNo());
        }
        driverPayEntity.setPayState(payState);
        driverPayEntity.setModified();
        save(driverPayEntity);
        PayCallBackDto payCallBackDto = new PayCallBackDto();
        payCallBackDto.setPayId(driverPayEntity.getPrepayId());
        payCallBackDto.setOrderNumber(wxCallback.getAttach());
        payCallBackDto.setPayState(payState);
        payCallBackDto.setTransactionId(wxCallback.getTransactionId());
        payCallBackDto.setSuccessTime(wxCallback.getSuccessTime());
        payCallBackDto.setPayAmount(driverPayEntity.getPayAmount());
        payCallBackDto.setPassengerId(driverPayEntity.getPassengerId());
        PayAction payAction = driverPayEntity.getAction();
        switch (payAction) {
            case consume:
                driverTripOrderService.payCallBack(payCallBackDto);
                break;
            case recharge:
                driverRechargeService.payCallBack(payCallBackDto);
                break;
            default:
        }
    }

    public WXCallback payState(String orderNumber) throws Exception {
        URI uri = new URIBuilder(String.format(queryOutTradeNoUrl, orderNumber)).addParameter("mchid", weiXinProperties.getMchId()).build();
        HttpGet httpGet = new HttpGet(uri);
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("User-Agent", "qfdj-1.0.0");
        httpGet.setHeader("Content-Type", "application/json");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        String body = getBody(response);
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new ServiceWorkingException("查询异常。");
        }
        return JsonUtils.readValue(body, WXCallback.class);
    }

    public DriverPayEntity find(String id) throws Exception {
        return driverPayDao.findById(id).orElse(null);
    }

    @Transactional
    public void balance(BalanceParam param) throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        PassengerEntity passengerEntity = passengerService.info(user.getId());
        Assert.notNull(passengerEntity, "用户不存在");
        DriverTripOrderEntity driverTripOrderEntity = driverTripOrderService.find(param.getOrderNumber());
        Assert.notNull(driverTripOrderEntity, "订单不存在");
        if (redisService.hasKey(RedisKeyEnum.OrderLock, param.getOrderNumber())) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            driverTripOrderService.updatePayType(param.getOrderNumber(), OrderStateEnum.PAYING, PayMethod.BALANCE.name(), PayMethod.BALANCE.name());
            BigDecimal payAmount = driverTripOrderEntity.getPayAmount();
            Assert.isTrue(passengerEntity.getMoney().compareTo(payAmount) > -1, "账户余额不足");
            driverRechargeService.deduction(user.getId(), driverTripOrderEntity.getOrderNumber(), payAmount);
            driverTripOrderService.updatePay(
                    driverTripOrderEntity.getOrderNumber(),
                    new Date(),
                    "",
                    EnableEnum.YES
            );
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, param.getOrderNumber());
        }
    }

    @Transactional
    public void save(DriverPayEntity payEntity) throws Exception {
        driverPayDao.save(payEntity);
    }

    @Transactional
    public void updateCallbackLog(Long id, EnableEnum state, String body) throws Exception {
        DriverCallbackEntity driverCallbackEntity = driverCallbackDao.findById(id).orElse(null);
        if (driverCallbackEntity == null) {
            return;
        }
        driverCallbackEntity.setState(state);
        driverCallbackEntity.setPayCallback(body);
        driverCallbackEntity.setModified();
        driverCallbackDao.save(driverCallbackEntity);
    }

    @Transactional
    public Long callbackLog(
            String wechatpaySerial,
            String wechatpayTimestamp,
            String wechatpayNonce,
            String wechatpaySignature,
            String body
    ) throws Exception {
        DriverCallbackEntity driverCallbackEntity = new DriverCallbackEntity();
        driverCallbackEntity.setWechatpaySerial(wechatpaySerial);
        driverCallbackEntity.setWechatpayTimestamp(wechatpayTimestamp);
        driverCallbackEntity.setWechatpayNonce(wechatpayNonce);
        driverCallbackEntity.setWechatpaySignature(wechatpaySignature);
        driverCallbackEntity.setBody(body);
        driverCallbackEntity.setCreate();
        driverCallbackDao.save(driverCallbackEntity);
        return driverCallbackEntity.getCallbackId();
    }

    private String getBody(CloseableHttpResponse response) throws Exception {
        HttpEntity httpEntity = response.getEntity();
        InputStream inputStream = httpEntity.getContent();
        int count = 0;
        byte[] bytes = new byte[1024];
        StringBuffer buffer = new StringBuffer();
        while ((count = inputStream.read(bytes)) > 0) {
            buffer.append(new String(bytes, 0, count));
        }
        inputStream.close();
        return buffer.toString();
    }

    public String log(
            PayType type,
            String orderNumber,
            PayAction payAction,
            BigDecimal amount,
            Long id,
            UserType userType
    ) throws Exception {
        DriverPayEntity driverPayEntity = new DriverPayEntity();
        driverPayEntity.setOrderNumber(orderNumber);
        driverPayEntity.setPayType(type);
        driverPayEntity.setPayState(EnableEnum.NO);
        driverPayEntity.setAction(payAction);
        driverPayEntity.setPayAmount(amount);
        driverPayEntity.setPassengerId(id);
        driverPayEntity.setUserType(userType);
        driverPayEntity.setCreate();
        driverPayDao.saveAndFlush(driverPayEntity);
        return driverPayEntity.getId();
    }

    public void updateParam(String id, String param) throws Exception {
        DriverPayEntity driverPayEntity = driverPayDao.findById(id).orElse(null);
        if (driverPayEntity == null) {
            return;
        }
        driverPayEntity.setParam(param);
        driverPayEntity.setModified();
        driverPayDao.saveAndFlush(driverPayEntity);
    }

    public void update(String id, String prepayId, String result) throws Exception {
        DriverPayEntity driverPayEntity = driverPayDao.findById(id).orElse(null);
        if (driverPayEntity == null) {
            return;
        }
        if (StringUtils.isEmpty(prepayId)) {
            driverPayEntity.setTransactionsState(EnableEnum.NO);
        } else {
            driverPayEntity.setTransactionsState(EnableEnum.YES);
            driverPayEntity.setPrepayId(prepayId);
        }
        driverPayEntity.setResult(result);
        driverPayEntity.setModified();
        driverPayDao.saveAndFlush(driverPayEntity);
    }
}
