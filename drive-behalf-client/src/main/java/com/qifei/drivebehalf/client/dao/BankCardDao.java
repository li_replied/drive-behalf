package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.BankCardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface BankCardDao extends JpaRepository<BankCardEntity, Long>, JpaSpecificationExecutor<BankCardEntity> {
    List<BankCardEntity> queryAllByUserId(Long userId);
}
