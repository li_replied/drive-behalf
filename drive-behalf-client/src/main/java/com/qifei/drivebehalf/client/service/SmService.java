package com.qifei.drivebehalf.client.service;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.qifei.drivebehalf.client.common.enums.AliYunParameter;
import com.qifei.drivebehalf.client.common.enums.AliYunSmsParameter;
import com.qifei.drivebehalf.client.common.model.Message;
import com.qifei.drivebehalf.client.common.properties.AliYunProperties;
import com.qifei.drivebehalf.client.common.properties.AliYunSmsProperties;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.model.AliYunResponse;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.dao.ClientSMSLogDao;
import com.qifei.drivebehalf.client.entity.DriverSMSLogEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


/**
 * 发送短信息服务
 */
@Service
@RequiredArgsConstructor
public class SmService {

    private final AliYunProperties aliYunProperties;

    private final AliYunSmsProperties aliYunSmsProperties;

    private final ClientSMSLogDao clientSMSLogDao;
    @Value("${ali.sms.accepted}")
    private String accepted;
    @Value("${ali.sms.check-code}")
    private String checkCode;
    @Value("${ali.sms.confirm-drive}")
    private String confirmDrive;

    @Value("${ali.sms.confirm-delivered}")
    private String confirmDelivered;

    /**
     * 发送短信验证码
     *
     * @param phoneNum
     * @param code
     * @throws Exception
     */
    public Long sendSms(String phoneNum, String code) throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("code", code);
        return send(phoneNum, checkCode, param);
    }

    public Long accepted(String userPhone, String orderNumber, String name, String phone, String customerPhone) throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("orderNumber", orderNumber);
        param.put("name", name);
        param.put("phone", phone);
        param.put("customerPhone", customerPhone);
        return send(userPhone, accepted, param);
    }

    public Long confirmDrive(String userPhone, String message, String date, String name, String customerPhone) throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("message", message);
        param.put("date", date);
        param.put("name", name);
        param.put("customerPhone", customerPhone);
        return send(userPhone, confirmDrive, param);
    }

    public Long confirmDelivered(String userPhone, String endPoint, String phone, String amount, String mileage) throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("endPoint", endPoint);
        param.put("phone", phone);
        param.put("amount", amount);
        param.put("mileage", mileage);
        return send(userPhone, confirmDelivered, param);
    }

    private Long send(String phoneNum, String templateCode, Map<String, String> param) throws Exception {
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(aliYunSmsProperties.getDomain());
        request.setSysVersion(aliYunSmsProperties.getSignatureVersion());
        request.setSysAction(aliYunSmsProperties.getAction());
        request.putQueryParameter(AliYunParameter.RegionId.name(), aliYunProperties.getRegionId());
        request.putQueryParameter(AliYunSmsParameter.PhoneNumbers.name(), phoneNum);
        request.putQueryParameter(AliYunSmsParameter.SignName.name(), aliYunSmsProperties.getSignName());
        request.putQueryParameter(AliYunSmsParameter.TemplateCode.name(), templateCode);
        String json = JsonUtils.writeValue(param);
        request.putQueryParameter(AliYunSmsParameter.TemplateParam.name(), json);
        request.putQueryParameter(AliYunSmsParameter.Version.name(), aliYunSmsProperties.getVersion());
        CommonResponse response = request(request);
        Long logId = addLog(phoneNum, json, request, response);
        if (response.getHttpStatus() != 200) {
            throw new ServiceWorkingException("短信发送失败！");
        }
        AliYunResponse result = JsonUtils.readValue(response.getData(), AliYunResponse.class);
        if (!"OK".equals(result.getCode())) {
            throw new ServiceWorkingException("短信发送失败！");
        }
        return logId;
    }

    /**
     * 请求阿里云
     *
     * @param request
     * @return
     * @throws Exception
     */
    private CommonResponse request(CommonRequest request) throws Exception {
        DefaultProfile profile = DefaultProfile.getProfile(aliYunProperties.getRegionId(), aliYunProperties.getAccessKeyId(), aliYunProperties.getAccessSecret());
        IAcsClient client = new DefaultAcsClient(profile);
        return client.getCommonResponse(request);
    }

    private Long addLog(String phoneNum, String code, CommonRequest request, CommonResponse response) throws Exception {
        DriverSMSLogEntity driverSMSLogEntity = new DriverSMSLogEntity();
        driverSMSLogEntity.setPhone(phoneNum);
        driverSMSLogEntity.setMessage(code);
        driverSMSLogEntity.setParam(JsonUtils.writeValue(request));
        driverSMSLogEntity.setResult(response.getData());
        driverSMSLogEntity.setCreate();
        clientSMSLogDao.save(driverSMSLogEntity);
        return driverSMSLogEntity.getId();
    }
}
