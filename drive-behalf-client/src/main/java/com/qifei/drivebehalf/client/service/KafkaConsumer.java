package com.qifei.drivebehalf.client.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.service.dto.PayBackMessage;
import com.qifei.drivebehalf.client.service.dto.ShareMessageDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaConsumer {
    private final WxPayService wxPayService;

    private final PassengerService passengerService;

    private final NoticeService noticeService;

    @KafkaListener(topics = {"pay-back"})
    public void onPayBack(ConsumerRecord<String, String> record) {
        if (log.isDebugEnabled()) {
            try {
                log.debug("接受支付回调", JsonUtils.writeValue(record));
            } catch (JsonProcessingException e) {
                log.error("json转换错误", e);
            }
        }
        String json = record.value();
        if (StringUtils.isBlank(json)) {
            return;
        }
        PayBackMessage payBackMessage = null;
        try {
            payBackMessage = JsonUtils.readValue(json, PayBackMessage.class);
        } catch (JsonProcessingException e) {
            log.error("json转换错误", e);
        }
        if (payBackMessage == null) {
            return;
        }
        try {
            wxPayService.payCallBack(payBackMessage.getCallback());
        } catch (Exception e) {
            log.error("更新支付状态错误", e);
        }
    }

    @KafkaListener(topics = {"user-share"})
    public void onUserShare(ConsumerRecord<String, String> record) {
        if (log.isDebugEnabled()) {
            try {
                log.debug("客户分成回调", JsonUtils.writeValue(record));
            } catch (JsonProcessingException e) {
                log.error("json转换错误", e);
            }
        }
        String json = record.value();
        if (StringUtils.isBlank(json)) {
            return;
        }
        ShareMessageDto shareMessageDto = null;
        try {
            shareMessageDto = JsonUtils.readValue(json, ShareMessageDto.class);
        } catch (JsonProcessingException e) {
            log.error("json转换错误", e);
        }
        if (shareMessageDto == null) {
            return;
        }
        try {
            passengerService.share(shareMessageDto);
        } catch (Exception e) {
            log.error("客户分成错误", e);
        }
    }

    @KafkaListener(topics = {"subscribe"})
    public void onSubscribe(ConsumerRecord<String, String> record) {
        if (log.isDebugEnabled()) {
            try {
                log.debug("预约回调", JsonUtils.writeValue(record));
            } catch (JsonProcessingException e) {
                log.error("json转换错误", e);
            }
        }
        String json = record.value();
        if (StringUtils.isBlank(json)) {
            return;
        }
        Map<String, String> message = null;
        try {
            message = JsonUtils.readValue(json, HashMap.class);
        } catch (JsonProcessingException e) {
            log.error("json转换错误", e);
        }
        if (message == null) {
            return;
        }
        try {
            noticeService.sendAssigned(message.get("address"), message.get("date"));
        } catch (Exception e) {
            log.error("预约回调错误", e);
        }
    }
}
