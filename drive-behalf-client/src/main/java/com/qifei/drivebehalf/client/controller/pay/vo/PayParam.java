package com.qifei.drivebehalf.client.controller.pay.vo;

import com.qifei.drivebehalf.client.common.enums.PayType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "JSAPI 支付参数")
public class PayParam implements Serializable {
    @NotEmpty(message = "订单编号不能为空")
    @ApiModelProperty("订单编号")
    private String orderNumber;
    @NotNull(message = "支付方式不能为空")
    @ApiModelProperty("支付方式")
    private PayType payType;
}
