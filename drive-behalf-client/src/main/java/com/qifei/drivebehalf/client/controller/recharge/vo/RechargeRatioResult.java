package com.qifei.drivebehalf.client.controller.recharge.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ApiModel(description = "充值规则")
public class RechargeRatioResult implements Serializable {
    @ApiModelProperty("规则ID")
    private Long configId;
    @ApiModelProperty("充值金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal rechargeAmount;
    @ApiModelProperty("赠送金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal giftAmount;
}
