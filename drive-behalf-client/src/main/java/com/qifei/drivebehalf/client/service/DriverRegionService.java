package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.enums.RegionType;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.controller.region.vo.RegionResult;
import com.qifei.drivebehalf.client.dao.DriverRegionDao;
import com.qifei.drivebehalf.client.entity.DriverRegionEntity;
import com.qifei.drivebehalf.client.service.dto.GeocoderResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;

import javax.transaction.Transactional;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DriverRegionService {
    private final DriverRegionDao driverRegionDao;

    private final RedisService redisService;

    private final TencentMapService tencentMapService;
    @Value("${tencent-map.key}")
    private String key;

    public DriverRegionEntity queryRegion(String regionId) throws Exception {
        return driverRegionDao.findById(regionId).orElse(null);
    }

    public List<RegionResult> queryAll() throws Exception {
        String region = redisService.getKey(RedisKeyEnum.Region);
        List<RegionResult> results = null;
        if (StringUtils.isEmpty(region)) {
            List<DriverRegionEntity> provinceList = queryRegionByType(RegionType.PROVINCE);
            if (provinceList != null) {
                results = new ArrayList<>();
                for (DriverRegionEntity province : provinceList) {
                    RegionResult provinceResult = BeanCopyUtils.copyProperties(province, RegionResult.class);
                    results.add(provinceResult);
                    List<RegionResult> data = queryData(province.getRegionId());
                    provinceResult.setData(data);
                }
                redisService.setKey(RedisKeyEnum.Region, JsonUtils.writeValue(results));
            }
        } else {
            results = JsonUtils.readValue(region);
        }
        return results;
    }

    public RegionResult info(String lat, String lng) throws Exception {
        String location = String.format("%s,%s", lat, lng);
        GeocoderResult geocoder = tencentMapService.geocoder(location, "radius=1;policy=1", key);
        Assert.isTrue(geocoder.getStatus() == 0, "地址换算错误。");
        log.info(JsonUtils.writeValue(geocoder));
        RegionResult regionResult = new RegionResult();
        regionResult.setRegionId(geocoder.getResult().getAdInfo().getAdcode());
        regionResult.setName(geocoder.getResult().getAdInfo().getDistrict());
        return regionResult;
    }

    private List<RegionResult> queryData(String id) throws Exception {
        List<DriverRegionEntity> list = driverRegionDao.queryByParentId(id);
        List<RegionResult> results = null;
        if (list != null) {
            results = new ArrayList<>();
            for (DriverRegionEntity region : list) {
                RegionResult regionResult = BeanCopyUtils.copyProperties(region, RegionResult.class);
                results.add(regionResult);
                List<RegionResult> data = queryData(region.getRegionId());
                regionResult.setData(data);
            }
        }
        return results;
    }

    public List<DriverRegionEntity> queryRegionByType(RegionType type) throws Exception {
        return driverRegionDao.queryByLevelType(type);
    }

    @Transactional(rollbackOn = Exception.class)
    public void saveAll(List<DriverRegionEntity> list) throws Exception {
        driverRegionDao.saveAll(list);
    }

    @Transactional(rollbackOn = Exception.class)
    public void save(DriverRegionEntity driverRegionEntity) throws Exception {
        driverRegionDao.save(driverRegionEntity);
    }

    @Transactional(rollbackOn = Exception.class)
    public void deleteAll() throws Exception {
        driverRegionDao.deleteAll();
        DriverRegionEntity driverRegionEntity = new DriverRegionEntity();
        driverRegionEntity.setRegionId("100000");
        driverRegionEntity.setLevelType(RegionType.NATION);
        driverRegionEntity.setParentId("0");
        String name = "中国";
        driverRegionEntity.setName(name);
        driverRegionEntity.setShortName(name);
        driverRegionEntity.setMergerName(name);
        driverRegionEntity.setMergerShortName(name);
        driverRegionEntity.setCreate();
        driverRegionDao.save(driverRegionEntity);
    }

}
