package com.qifei.drivebehalf.client.controller.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@ApiModel(description = "计算价格")
public class SumMoneyResult implements Serializable {
    @ApiModelProperty("订单时间")
    private String datetime;
    @ApiModelProperty("起点")
    private String startPoint;
    @ApiModelProperty("终点")
    private String endPoint;
    @ApiModelProperty("开始纬度")
    private String startLat;
    @ApiModelProperty("开始经度")
    private String startLng;
    @ApiModelProperty("结束纬度")
    private String endLat;
    @ApiModelProperty("结束经度")
    private String endLng;
    @ApiModelProperty("总金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal total;
    @ApiModelProperty("起步价格")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal startingPrice;
    @ApiModelProperty("起步公里")
    private int startingKilometre;
    @ApiModelProperty("里程费")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal mileageFee;
    @ApiModelProperty("收费里程")
    private int chargedMileage;
    @ApiModelProperty("行驶时间")
    private String duration;
    @ApiModelProperty("总里程")
    private int distance;
    @ApiModelProperty("倍数")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.0")
    private BigDecimal multiple;
    @ApiModelProperty("加倍金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal multipleFee;
    @ApiModelProperty("加倍原因")
    private String reason;
    @ApiModelProperty("算价id,下单使用")
    private String uuid;
    @ApiModelProperty("客服热线")
    private String customerPhone;
    @ApiModelProperty("佣金")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal commission;
    @ApiModelProperty("佣金比例")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal commissionRatio;
    @ApiModelProperty("地区id")
    private String regionId;
    @ApiModelProperty("中转次数")
    private int transferCount;
}
