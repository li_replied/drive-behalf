package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.dao.ClientSMSLogDao;
import com.qifei.drivebehalf.client.entity.DriverSMSLogEntity;
import com.qifei.drivebehalf.client.service.dto.SmsCodeDto;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShortMessageService {

    private final SmService smService;


    private final RedisService redisService;

    private final ClientSMSLogDao clientSMSLogDao;


    /**
     * 发送短信
     *
     * @param mobile
     * @throws Exception
     */
    public void sendSm(String mobile) throws Exception {
        //校验是否可以发
        canSend(mobile);
        String code = RandomStringUtils.randomNumeric(5);
        Long logId = smService.sendSms(mobile, code);
        cacheSmsCode(mobile, code, logId);
    }

    /**
     * 是否可以发短信
     *
     * @param phoneNum
     * @return
     */
    private void canSend(String phoneNum) throws Exception {
        String countString = redisService.getKey(RedisKeyEnum.SmsSendCount, phoneNum);
        if (StringUtils.isNotEmpty(countString)) {
            int count = NumberUtils.toInt(countString, 0);
            if (count > 5) {
                throw new ServiceWorkingException("超出短信发送次数，请稍后再试。");
            }
        }
        if (redisService.hasKey(RedisKeyEnum.SmsExpire, phoneNum)) {
            throw new ServiceWorkingException("短信发送太频繁，请稍后再试。");
        }
    }

    /**
     * 缓存短信校验码
     *
     * @param phoneNum
     * @param code
     * @throws Exception
     */
    private void cacheSmsCode(String phoneNum, String code, Long logId) throws Exception {
        String countString = redisService.getKey(RedisKeyEnum.SmsSendCount, phoneNum);
        int count = 0;
        if (StringUtils.isNotEmpty(countString)) {
            count = NumberUtils.toInt(countString, 0);
        }
        redisService.setEx(RedisKeyEnum.SmsSendCount, phoneNum, String.valueOf(count++));
        redisService.setEx(RedisKeyEnum.SmsExpire, phoneNum, "");
        SmsCodeDto smsCodeDto = new SmsCodeDto(code, logId);
        redisService.setEx(RedisKeyEnum.SmsCode, phoneNum, JsonUtils.writeValue(smsCodeDto));
    }

    public void verificationCode(String phoneNum, String code) throws Exception {
        String codeDto = redisService.getKey(RedisKeyEnum.SmsCode, phoneNum);
        if (StringUtils.isEmpty(codeDto)) {
            throw new ServiceWorkingException("验证码错误。");
        }
        SmsCodeDto smsCodeDto = JsonUtils.readValue(codeDto, SmsCodeDto.class);
        if (!smsCodeDto.getCode().equals(code)) {
            throw new ServiceWorkingException("验证码错误。");
        }
        DriverSMSLogEntity driverSMSLogEntity = clientSMSLogDao.findById(smsCodeDto.getLogId()).orElse(null);
        if (driverSMSLogEntity != null) {
            driverSMSLogEntity.setModified();
            clientSMSLogDao.save(driverSMSLogEntity);
        }
    }
}
