package com.qifei.drivebehalf.client.common.enums;

public enum OrderStateEnum {
    /**
     * 待分配
     */
    ASSIGNED,
    /**
     * 再分配
     */
    REDISTRIBUTION,
    /**
     * 超时
     */
    OVERTIME,
    /**
     * 拒单
     */
    REFUSE,
    /**
     * 撤销
     */
    CANCEL,
    /**
     * 已接单
     */
    ACCEPTED,
    /**
     * 确认代驾
     */
    CONFIRMDRIVE,
    /**
     * 开始代驾
     */
    DRIVE,
    /**
     * 待支付
     */
    PAYMENT,
    /**
     * 推送支付
     */
    PREPAY,
    /**
     * 支付中
     */
    PAYING,
    /**
     * 完成
     */
    COMPLETE,
    ;
}
