package com.qifei.drivebehalf.client.controller.online.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "在线时长")
@AllArgsConstructor
@NoArgsConstructor
public class DurationResult implements Serializable {
    @ApiModelProperty("在线时长")
    private long duration;
    @ApiModelProperty("在线时长")
    private String text;
}
