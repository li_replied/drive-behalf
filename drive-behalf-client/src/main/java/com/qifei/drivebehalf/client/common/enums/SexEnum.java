package com.qifei.drivebehalf.client.common.enums;

/**
 * 性别
 */
public enum SexEnum {
    M("男"), F("女");

    private String value;

    SexEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
