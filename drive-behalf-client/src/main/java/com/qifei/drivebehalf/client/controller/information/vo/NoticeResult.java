package com.qifei.drivebehalf.client.controller.information.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@ApiModel(description = "公告结果")
public class NoticeResult implements Serializable {
    @ApiModelProperty("标题")
    private String noticeTitle;
    @ApiModelProperty("内容")
    private String noticeContent;
    @ApiModelProperty("更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date createTime;
}
