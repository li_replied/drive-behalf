package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@Entity
@Table(
        name = "driver_recharge_config"
)
public class DriverRechargeConfig implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "config_id", unique = true, nullable = false)
    private Long configId;
    @Column(name = "recharge_amount", precision = 10, scale = 2)
    private BigDecimal rechargeAmount;
    @Column(name = "gift_amount", precision = 10, scale = 2)
    private BigDecimal giftAmount;
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private EnableEnum state;
}
