package com.qifei.drivebehalf.client.common.model;

public enum ResponseCode {

    SUCCESS(200, "执行成功"),
    ERROR(500, "执行失败"),
    SYSTEM_ERROR(500, "服务器错误"),
    NOT_LOGIN(401, "登录已失效"),
    AUTHORITY_ERROR(403, "您没有访问的权限");

    private final Integer code;

    private final String desc;

    ResponseCode(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
