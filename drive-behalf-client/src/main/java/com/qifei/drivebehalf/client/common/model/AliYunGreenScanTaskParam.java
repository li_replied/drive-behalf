package com.qifei.drivebehalf.client.common.model;

import lombok.Data;

/**
 * 阿里云文字安全校验任务参数
 */
@Data
public class AliYunGreenScanTaskParam {
    private String dataId;
    private String content;
}
