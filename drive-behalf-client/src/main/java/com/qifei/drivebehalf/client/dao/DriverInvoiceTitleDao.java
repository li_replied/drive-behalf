package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.entity.DriverInvoiceTitleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DriverInvoiceTitleDao extends JpaRepository<DriverInvoiceTitleEntity, Long>, JpaSpecificationExecutor<DriverInvoiceTitleEntity> {
    List<DriverInvoiceTitleEntity> queryAllByUserIdAndDeleteFlag(Long userId,EnableEnum deleteFlag);

    DriverInvoiceTitleEntity queryFirstByUserIdAndDefaultTagAndDeleteFlag(Long userId, EnableEnum defaultTag,EnableEnum deleteFlag);

    @Query("update DriverInvoiceTitleEntity set defaultTag='NO' where userId=:userId and id <>:id")
    @Modifying
    void updateDefaultTag(@Param("userId") Long userId, @Param("id") Long id);

    @Query("update DriverInvoiceTitleEntity set deleteFlag='YES' where id=:id")
    @Modifying
    void updateInvoiceTitleDeleteFlag(@Param("id") Long id);
}
