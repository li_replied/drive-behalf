package com.qifei.drivebehalf.client.controller.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "撤单参数")
public class CancelParam implements Serializable {
    @NotEmpty(message = "订单编号不能为空")
    @ApiModelProperty("订单编号")
    private String orderNumber;
    @ApiModelProperty("撤销原因")
    private String reason;
}
