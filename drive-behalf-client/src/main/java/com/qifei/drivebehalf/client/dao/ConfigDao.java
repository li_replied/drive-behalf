package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.ConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ConfigDao extends JpaRepository<ConfigEntity, Long>, JpaSpecificationExecutor<ConfigEntity> {
    ConfigEntity queryFirstByConfigCode(String code);
}
