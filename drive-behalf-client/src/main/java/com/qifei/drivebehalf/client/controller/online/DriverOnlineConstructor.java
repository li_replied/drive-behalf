package com.qifei.drivebehalf.client.controller.online;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.controller.online.vo.DurationResult;
import com.qifei.drivebehalf.client.service.DriverOnlineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "司机端： 在线管理")
@RestController
@RequestMapping("/online")
@RequiredArgsConstructor
public class DriverOnlineConstructor {
    private final DriverOnlineService driverOnlineService;



    @ValidToken
    @ApiOperation("在线时长")
    @GetMapping()
    public ApiResponse<DurationResult> duration() throws Exception {
        long duration = driverOnlineService.duration();
        long hour = duration / 60L;
        long minute = duration % 60L;
        String text = "";
        if (hour > 0) {
            text = hour + "小时";
        }
        text += minute + "分钟";
        DurationResult durationResult = new DurationResult(duration, text);
        return ApiResponse.createBySuccess(durationResult);
    }

    @ValidToken
    @ApiOperation("上线")
    @PostMapping()
    public ApiResponse<Void> Online() throws Exception {
        driverOnlineService.online();
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("离线")
    @PutMapping
    public ApiResponse<Void> offline() throws Exception {
        driverOnlineService.offline();
        return ApiResponse.createBySuccess();
    }
}
