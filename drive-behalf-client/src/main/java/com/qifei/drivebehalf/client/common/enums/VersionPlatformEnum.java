package com.qifei.drivebehalf.client.common.enums;

public enum VersionPlatformEnum {
    IOS, ANDROID;
}
