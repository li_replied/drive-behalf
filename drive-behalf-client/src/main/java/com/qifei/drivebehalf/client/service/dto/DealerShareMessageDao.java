package com.qifei.drivebehalf.client.service.dto;

public class DealerShareMessageDao extends ShareMessageDto {
    public DealerShareMessageDao() {
        super("dealer-share");
    }
}
