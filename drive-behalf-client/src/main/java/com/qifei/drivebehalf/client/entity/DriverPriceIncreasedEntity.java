package com.qifei.drivebehalf.client.entity;

import javax.persistence.*;
import java.io.Serializable;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "driver_price_increased")
public class DriverPriceIncreasedEntity extends BaseEntity implements Serializable {

    /**
     * 加价表
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "price_increased_id")
    private Long priceIncreasedId;

    /**
     * 价格表id
     */
    @Column(name = "billing_price")
    private Long billingPrice;

    /**
     * 加价原因
     */
    @Column(name = "increase_reason")
    private String increaseReason;

    /**
     * 加价开始时间
     */
    @Column(name = "price_increased_begin")
    private Date priceIncreasedBegin;

    /**
     * 加价结束时间
     */
    @Column(name = "price_increased_end")
    private Date priceIncreasedEnd;

    /**
     * 加价倍数
     */
    @Column(name = "price_multiple")
    private BigDecimal priceMultiple;

}
