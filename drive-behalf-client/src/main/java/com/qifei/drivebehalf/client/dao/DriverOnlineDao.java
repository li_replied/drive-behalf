package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverOnlineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;

public interface DriverOnlineDao extends JpaRepository<DriverOnlineEntity, Long>, JpaSpecificationExecutor<DriverOnlineEntity> {
    List<DriverOnlineEntity> queryAllByDayAndDriverId(Date day, Long driverId);
}
