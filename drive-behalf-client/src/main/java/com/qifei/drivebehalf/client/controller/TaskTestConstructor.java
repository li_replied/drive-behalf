package com.qifei.drivebehalf.client.controller;

import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.task.PayStateTask;
import com.qifei.drivebehalf.client.task.RegionTask;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "测试定时任务")
@RequestMapping("/test")
@RequiredArgsConstructor
public class TaskTestConstructor {
    private final RegionTask regionTask;

    private final PayStateTask payStateTask;

    @ApiOperation("测试更新全部地区")
    @GetMapping("/update")
    public ApiResponse<Void> update() throws Exception {
        regionTask.updateRegion();
        return ApiResponse.createBySuccess();
    }

    @ApiOperation("测试查询支付状态")
    @GetMapping("/query/Pay")
    public ApiResponse<Void> queryPay() throws Exception {
        payStateTask.queryPay();
        return ApiResponse.createBySuccess();
    }
}
