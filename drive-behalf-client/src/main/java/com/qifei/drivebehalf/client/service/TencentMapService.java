package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.service.dto.DrivingResult;
import com.qifei.drivebehalf.client.service.dto.GeocoderResult;
import com.qifei.drivebehalf.client.service.dto.TencentMapDistrict;
import com.qifei.drivebehalf.client.service.dto.TencentMapDistrictResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(name = "tencent-map", url = "${tencent-map.url}")
public interface TencentMapService {
    @RequestMapping(path = "${tencent-map.get-children-path}", method = RequestMethod.GET)
    TencentMapDistrictResult<List<TencentMapDistrict>> getChildren(
            @RequestParam("id") String id,
            @RequestParam("key") String key
    );

    @RequestMapping(path = "${tencent-map.geocoder-path}", method = RequestMethod.GET)
    GeocoderResult geocoder(
            @RequestParam("location") String location,
            @RequestParam("poi_options") String poiOptions,
            @RequestParam("key") String key
    );

    @RequestMapping(path = "${tencent-map.direction-driving-path}", method = RequestMethod.GET)
    DrivingResult driving(
            @RequestParam("from") String from,
            @RequestParam("to") String to,
            @RequestParam("policy") String policy,
            @RequestParam("cartype") String cartype,
            @RequestParam("cartype") String getMp,
            @RequestParam("no_step") String noStep,
            @RequestParam("key") String key
    );
}
