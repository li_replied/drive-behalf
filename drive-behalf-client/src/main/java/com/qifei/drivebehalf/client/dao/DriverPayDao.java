package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverPayEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverPayDao extends JpaRepository<DriverPayEntity, String>, JpaSpecificationExecutor<DriverPayEntity> {
}
