package com.qifei.drivebehalf.client.controller.shortmessage.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel(description = "发短信参数")
public class SendParam {
    @ApiModelProperty(value = "手机号")
    @NotEmpty(message = "手机号码不能为空")
    @Length(min = 11, max = 11, message = "请输入正确的手机号")
    private String phone;
}
