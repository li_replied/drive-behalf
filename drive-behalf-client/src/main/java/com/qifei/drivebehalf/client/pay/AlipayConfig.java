package com.qifei.drivebehalf.client.pay;

public class AlipayConfig {

    // 商户appid
    public static String APP_ID = "2021000117682804";

    // 支付宝公钥
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnpb3/4vmVtQchuQcYa32w61pTwoig3wA3ZPN6VbEf6C0q3H4PFAg3li/QS4vQTUMPHO7Qm0A9IGN4d1PyS+BZtwDJQti6Wn78KCR0C7hpoHOcgykJNYpbuC/Zu3WENYzlgkFUmRbTs9lx7IuI5IMMzfxoFfsxh/XBbcbqOIIgygIPSfGuEbEAEyInhCYopD3zJtIALWk3xl1rgPoZLFgMAofmeQqb7nW/b56G5VGsp73njzrAeYGZVVwwMZS0A8OQiN/fVQd/8nKILR8ILgMPAEZK7mZLnaM6us6SqnG9g5QWpv6I0BcYFZnjdwopJHCFkfV0IDM0mECAkYptU7INQIDAQAB";

    // 应用公钥
    public static String APP_PRIVATE_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCelvf/i+ZW1ByG5BxhrfbDrWlPCiKDfADdk83pVsR/oLSrcfg8UCDeWL9BLi9BNQw8c7tCbQD0gY3h3U/JL4Fm3AMlC2LpafvwoJHQLuGmgc5yDKQk1ilu4L9m7dYQ1jOWCQVSZFtOz2XHsi4jkgwzN/GgV+zGH9cFtxuo4giDKAg9J8a4RsQATIieEJiikPfMm0gAtaTfGXWuA+hksWAwCh+Z5Cpvudb9vnoblUaynveePOsB5gZlVXDAxlLQDw5CI399VB3/ycogtHwguAw8ARkruZkudozq6zpKqcb2DlBam/ojQFxgVmeN3CikkcIWR9XQgMzSYQICRim1Tsg1AgMBAAECggEAYNychDySZx4OfmT84Lw8J6/WzivGgMBQVwVbktUoFzu0nbJyh4P5T1StG8Z1nkIkQxf8m8mv96bZXcaqYJrfUk+2q/2UYDQXaRTpelnoAKbvzMDj0Ivtqoci4F8LjCUIRO8ljPk2u5/47ktp2tftE6trrR+GwkvYLOl1v2SvBkuFiSJvXrCOOoT6tUHeONtY139/pDeCuL1C7BUldlFUw5w8e0dxk2hvasmoKeg88efF5INM06k26JEdp3bns7RhQZc6gGIxqIwbWqVMgmN1eKJZQA0n7JuY/BK5lfHLz/PSILTClXwse9FIjYCoDjYKauzdzMsWaVb6pdcxV3V/AQKBgQD4jb5xUQ8ctnWGIaA2y/8Hz6hiYonElmcARZmlY1PEiAjC8QA9oz9jz/zEK+UVhu0csNFCobXxHzlY3SQ1bFFq3CvUuvkIEeVyiD54Mfa1hL73IpFrV1giB3WHITjb9z/49MWlbi7/o+/EcR4xTp5t2qxRA+xgToIHERvH4ELj5QKBgQCjV0F5Nf94JYS0SvwxD69SBqr3Mc+iRXlPdgY50yWDKw5U3DvsA1bLxkUvC3E3X5ih7b7XlNKdg0AzdoWlsywyq8cjIBwW6NC/NgRVfYdyheLT3fwM1AA4cVcELRPj5d2m2jefxsDEHPzTxvUjAvlwawqQ44v8pFNniGUmQpiuEQKBgDBfzefaAdnxvO/NDZR8oXcQuFHlobCMISHRafThXwVKAIKISyaz5ft0GFrJvD0zVGSYGTIhrPpvvEaKS6jGGxZ0IWe2uA8VBJYE5DuaZcywOEZ7CNheWH3O+9W/5oQRG/dFFE59IlKIwnp2FY09IuJ4g78Ex3V68h2T+9cr03jtAoGACnr1w344dxAJNLw+9DLqb9vnqlay2NmWUAflnF404PapQ76hxaHtNsPw+nTEfeYPWAx2G0T8QokCXIr1Rk15v4KQE2vjAYFMq4gevRGB2FJI3Ik4u+X6YaS8DZOK3CuKjkMgIItTIQIvIrN4VRfCw3jKw8Ml3z737wOsYckSHNECgYA7sWZPaxZ9RO3QtL8Ye87dD74OY4IVVJcBEQQrWNNyuCk8ozO4ncdx49KG2LJtinOjGphbidmBcA+YH94ffWRGaeyojwCTcBXqX2eGCNUxkL5XDzoHYRWEn7eiZs/uLQjnOq9xpjQfuutjoMh7S5hTKgmvNRAqNYhKKCE1y0ZtVw==";

    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String NOTIFY_URL = "http://car.yimato.com/alipay/notify.do";

    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
    public static String return_url = "http://www.xxx.com/alipay/return_url.do";

    // 请求支付宝的网关地址
    public static String URL = "https://openapi.alipay.com/gateway.do";

    // 编码
    public static String CHARSET = "UTF-8";

    // 返回格式
    public static String FORMAT = "json";

    // 加密类型
    public static String SIGNTYPE = "RSA2";
}
