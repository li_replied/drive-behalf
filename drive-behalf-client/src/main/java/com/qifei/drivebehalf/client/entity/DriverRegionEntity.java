package com.qifei.drivebehalf.client.entity;

import javax.persistence.*;
import java.io.Serializable;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.RegionType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(
        name = "driver_region",
        indexes = {
                @Index(name = "idx_driver_region_parent_id", columnList = "parent_id")
        }
)
public class DriverRegionEntity extends BaseEntity {

    /**
     * 区域编号
     */
    @Id
    @Column(name = "region_id")
    private String regionId;

    /**
     * 区域名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 简称
     */
    @Column(name = "short_name")
    private String shortName;

    /**
     * 上级编号
     */
    @Column(name = "parent_id")
    private String parentId;

    /**
     * 聚合全称
     */
    @Column(name = "merger_name")
    private String mergerName;

    /**
     * 聚合简称
     */
    @Column(name = "merger_short_name")
    private String mergerShortName;

    /**
     * 级别0国家，1省级，2市级，3区县级
     */
    @Column(name = "level_type")
    @Enumerated(EnumType.STRING)
    private RegionType levelType;

}
