package com.qifei.drivebehalf.client.task;

import cn.hutool.core.date.DateUtil;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.entity.DriverPayEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.service.DriverTripOrderService;
import com.qifei.drivebehalf.client.service.KafkaProducer;
import com.qifei.drivebehalf.client.service.RedisService;
import com.qifei.drivebehalf.client.service.WxPayService;
import com.qifei.drivebehalf.client.service.dto.WXCallback;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class PayStateTask {
    private final DriverTripOrderService driverTripOrderService;
    private final WxPayService wxPayService;
    private final RedisService redisService;
    private final KafkaProducer kafkaProducer;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void queryPay() {
        try {
            log.info(String.format("更新支付状态开始 %s", DateUtil.formatDate(new Date())));
            List<DriverTripOrderEntity> list = driverTripOrderService.paying();
            if (list == null || list.isEmpty()) {
                return;
            }
            for (DriverTripOrderEntity orderEntity : list) {
                try {
                    WXCallback wxCallback = wxPayService.payState(orderEntity.getPayId());
                    if (StringUtils.isEmpty(wxCallback.getAttach())) {
                        DriverPayEntity driverPayEntity = wxPayService.find(wxCallback.getOutTradeNo());
                        if (driverPayEntity == null) {
                            continue;
                        }
                        wxCallback.setAttach(driverPayEntity.getOrderNumber());
                    }
                    kafkaProducer.payCallBack(wxCallback);
                } catch (Exception e) {
                    log.error("查询状态错误", e);
                }
            }
        } catch (Exception e) {
            log.error("更新支付状态错误", e);
        } finally {
            log.info(String.format("更新支付状态结束 %s", DateUtil.formatDate(new Date())));
        }

    }

    @Scheduled(cron = "0 0/10 * * * ?")
    public void overtime() {
        try {
            log.info(String.format("更新超时状态开始 %s", DateUtil.formatDate(new Date())));
            List<DriverTripOrderEntity> list = driverTripOrderService.overtime();
            if (list != null || list.isEmpty()) {
                return;
            }
            for (DriverTripOrderEntity driverTripOrderEntity : list) {
                //锁订单
                while (redisService.hasKey(RedisKeyEnum.OrderLock, driverTripOrderEntity.getOrderNumber())) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        log.error("线程等待异常", e);
                    }
                }
                try {
                    redisService.setEx(RedisKeyEnum.OrderLock, driverTripOrderEntity.getOrderNumber());
                    driverTripOrderEntity = driverTripOrderService.info(driverTripOrderEntity.getOrderNumber());
                    if (driverTripOrderEntity.getOrderStatus() == OrderStateEnum.ACCEPTED || driverTripOrderEntity.getOrderStatus() == OrderStateEnum.REDISTRIBUTION) {
                        driverTripOrderService.updateOvertime(driverTripOrderEntity.getOrderNumber());

                    }
                } finally {
                    redisService.delKey(RedisKeyEnum.OrderLock, driverTripOrderEntity.getOrderNumber());
                }

            }
        } catch (Exception e) {
            log.error("更新超时状状态错误", e);
        } finally {
            log.info(String.format("更新超时状态结束 %s", DateUtil.formatDate(new Date())));
        }
    }
}
