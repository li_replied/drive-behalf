package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.PassengerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface PassengerDao extends JpaRepository<PassengerEntity, Long>, JpaSpecificationExecutor<PassengerEntity> {
    PassengerEntity findByPhone(String phone);

    @Query("update PassengerEntity  set money=money+:amount " +
            "where id=:id")
    @Modifying
    void recharge(@Param("id") Long id, @Param("amount") BigDecimal amount);

    @Query("update PassengerEntity  set parentId=:parentId where id=:id")
    @Modifying
    void relation(@Param("id") Long id, @Param("parentId") Long parentId);

    @Query("update PassengerEntity  set retail=retail+:amount " +
            "where id=:id")
    @Modifying
    void retail(@Param("id") Long userId, @Param("amount") BigDecimal amount);

    List<PassengerEntity> queryAllByParentId(Long userId);

    @Query("select pe  from PassengerEntity pe where pe.parentId in " +
            "(select p.id from PassengerEntity p where p.parentId=:userId)")
    Page<PassengerEntity> querySecond(@Param("userId") Long userId, Pageable pageable);
}
