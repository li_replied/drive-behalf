package com.qifei.drivebehalf.client.controller.pay.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "app 支付返回结果")
public class AppSign implements Serializable {
    /**
     * 应用id	appid	string[1,32]	是	微信开放平台审核通过的应用appid ，为二级商户申请的应用appid。
     * 示例值：wx8888888888888888
     */
    private String appid;
    /**
     * 商户号		string[1,32]	是	请填写商户号mchid对应的值。
     */
    private String partnerid;
    /**
     * 预支付交易会话ID	prepayid	string[1,32]	是	微信返回的支付交易会话id。
     * 示例值： WX1217752501201407033233368018
     */
    private String prepayid;
    /**
     * 订单详情扩展字符串	package	string[1,128]	是	暂填写固定值Sign=WXPay
     * 示例值：Sign=WXPay
     */
    @JsonProperty("package")
    private String packageStr = "Sign=WXPay";
    /**
     * 随机字符串	noncestr	string[1,32]	是	随机字符串，不长于32位。推荐随机数生成算法。
     * 示例值： 5K8264ILTKCH16CQ2502SI8ZNMTM67VS
     */
    private String noncestr;
    /**
     * 时间戳	timestamp	string[1,10]	是	时间戳
     * 示例值：1412000000
     */
    private String timestamp;
    /**
     * 签名	paySign	string[1,256]	是	签名，使用字段appId、timeStamp、nonceStr、prepayid计算得出的签名值
     */
    private String sign;

}
