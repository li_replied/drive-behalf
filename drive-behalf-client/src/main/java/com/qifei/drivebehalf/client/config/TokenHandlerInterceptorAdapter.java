package com.qifei.drivebehalf.client.config;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.enums.UserType;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.properties.JWTProperties;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.RedisService;
import com.qifei.drivebehalf.client.common.execution.NotLoginException;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.JWTUtils;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.UUID;

/**
 * token拦截器
 */
@Component
@Slf4j
public class TokenHandlerInterceptorAdapter extends HandlerInterceptorAdapter {

    private JWTProperties jwtProperties;

    @Autowired
    public void setJwtProperties(JWTProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
    }

    private RedisService redisService;

    @Autowired
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        MDC.put("uuid", UUID.randomUUID().toString().replaceAll("-", ""));
        MDC.put("uri", request.getRequestURI());
        if (log.isDebugEnabled()) {
            String token = request.getHeader(jwtProperties.getHeader());
            log.debug(String.format("token:%s", token));
            Map<String, String[]> map = request.getParameterMap();
            log.debug(String.format("请求参数:%s", JsonUtils.writeValue(map)));
        }
        // 如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        boolean hasValidToken = handlerMethod.hasMethodAnnotation(ValidToken.class);
        if (!hasValidToken) {
            try {
                setUser(request);
            } catch (Exception e) {

            }
        } else {
            setUser(request);
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (handler instanceof HandlerMethod) {
            if (log.isDebugEnabled()) {
                log.debug("removeLogin");
            }
            UserLoginUtils.removeLogin();
        }
        super.afterCompletion(request, response, handler, ex);
    }

    private void setUser(HttpServletRequest request) throws Exception {
        String token = request.getHeader(jwtProperties.getHeader());
        if (StringUtils.isEmpty(token)) {
            throw new NotLoginException();
        }
        Claims claims = null;
        try {
            claims = JWTUtils.parser(token, jwtProperties.getSecret());
        } catch (Exception e) {
            throw new NotLoginException(e);
        }
        if (claims == null) {
            throw new NotLoginException();
        }
        String userId = claims.getId();
        if (StringUtils.isEmpty(userId)) {
            throw new NotLoginException();
        }
        String userType = claims.getSubject();
        String value = redisService.getKey(RedisKeyEnum.UserToken, userType + userId);
        if (StringUtils.isEmpty(value)) {
            throw new NotLoginException();
        }
        if (log.isDebugEnabled()) {
            log.debug(String.format("设置登录用户: %s", value));
        }
        UserType type = UserType.valueOf(userType);
        if (type == null) {
            throw new ServiceWorkingException("非法请求。");
        }
        redisService.expire(RedisKeyEnum.UserToken, userType + userId);
        UserEntity userEntity = null;
        switch (type) {
            case CLIENT:
                userEntity = JsonUtils.readValue(value, PassengerEntity.class);
                userEntity.setUserType(UserType.CLIENT);
                break;
            case DRIVER:
                userEntity = JsonUtils.readValue(value, DriverEntity.class);
                userEntity.setUserType(UserType.DRIVER);
                break;
        }
        if (EnableEnum.NO == userEntity.getUserEnable()) {
            throw new ServiceWorkingException("账户已被禁用。");
        }
        UserLoginUtils.setLogin(userEntity);
    }
}
