package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.entity.ScoreEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;

public interface ScoreDao extends JpaRepository<ScoreEntity, Long>, JpaSpecificationExecutor<ScoreEntity> {
    Page<ScoreEntity> findByUserIdAndAmountGreaterThan(Long userId, BigDecimal bigDecimal, Pageable pageable);

    ScoreEntity findFirstByUserIdAndIncomeOrderByCreateTimeDesc(Long userId,EnableEnum income);

    @Query(" select  sum (amount) from ScoreEntity  where userId=:userId and income=:income")
    BigDecimal total(@Param("userId") Long userId, @Param("income") EnableEnum income);

    @Query(" select  sum (amount) from ScoreEntity  where userId=:userId and createTime>=:now and income=:income")
    BigDecimal total(@Param("userId") Long userId, @Param("income") EnableEnum income, @Param("now") Date now);
}
