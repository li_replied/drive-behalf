package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class KafkaMessage implements Serializable {
    private final String topic;

    public KafkaMessage(String topic) {
        this.topic = topic;
    }
}
