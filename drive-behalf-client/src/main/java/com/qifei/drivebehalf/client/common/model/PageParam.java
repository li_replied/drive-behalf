package com.qifei.drivebehalf.client.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * page参数
 */
@ApiModel(description = "分页参数")
@Data
public class PageParam {
    @ApiModelProperty(value = "页码")
    @Range(min = 1 , max = 100, message = "页面数不正确")
    protected int pageNum = 1;
    @ApiModelProperty(value = "每页数量")
    @Range(min = 1 , max = 100, message = "页面数不正确")
    protected int pageSize = 10;
}
