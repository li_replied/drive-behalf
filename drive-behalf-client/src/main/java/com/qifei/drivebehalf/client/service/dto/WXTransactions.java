package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class WXTransactions implements Serializable {
    /**
     * 公众号ID	appid	string[1,32]	是	body 直连商户申请的公众号或移动应用appid。
     */
    private String appid;
    /**
     * 直连商户号	mchid	string[1,32]	是	body 直连商户的商户号，由微信支付生成并下发。
     */
    private String mchid;
    /**
     * 商品描述	description	string[1,127]	是	body 商品描述
     */
    private String description;
    /**
     * 商户订单号	out_trade_no	string[6,32]	是	body 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;
    /**
     * 交易结束时间	time_expire	string[1,64]	否	body 订单失效时间，遵循rfc3339标准格式，格式为YYYY-MM-DDTHH:mm:ss+TIMEZONE，YYYY-MM-DD表示年月日，T出现在字符串中，表示time元素的开头，HH:mm:ss表示时分秒，TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）。例如：2015-05-20T13:29:35+08:00表示，北京时间2015年5月20日 13点29分35秒。
     */
    @JsonProperty("time_expire")
    private String timeExpire;
    /**
     * 附加数据	attach	string[1,128]	否	body 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用
     */
    private String attach;
    /**
     * 通知地址	notify_url	string[1,256]	是	body 通知URL必须为直接可访问的URL，不允许携带查询串。
     */
    @JsonProperty("notify_url")
    private String notifyUrl;
    /**
     * 订单优惠标记		string[1,32]	否	body 订单优惠标记
     */
//    @JsonProperty("goods_tag")
//    private String goodsTag;
    /**
     * +订单金额	amount	object	是	body 订单金额信息
     */
    private WXAmount amount;
    /**
     * +支付者	payer	object	是	body 支付者信息
     */
    private WXPayer payer;



}
