package com.qifei.drivebehalf.client.controller.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@ApiModel(description = "呼叫代驾结果")
public class CallDriverResult implements Serializable {
    @ApiModelProperty("时间戳")
    private Long flag;
    @ApiModelProperty("候选司机")
    private Integer assignedDriverCount;
    @ApiModelProperty("等待超时上限")
    private Integer waitingTimeout;
}
