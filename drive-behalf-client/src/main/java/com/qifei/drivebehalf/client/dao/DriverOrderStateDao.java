package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverOrderStateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverOrderStateDao extends JpaRepository<DriverOrderStateEntity, Long>, JpaSpecificationExecutor<DriverOrderStateEntity> {

}
