package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.RegionType;
import com.qifei.drivebehalf.client.entity.DriverRegionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DriverRegionDao extends JpaRepository<DriverRegionEntity, String>, JpaSpecificationExecutor<DriverRegionEntity> {
    List<DriverRegionEntity> queryByLevelType(RegionType levelType);

    List<DriverRegionEntity> queryByParentId(String id);
}
