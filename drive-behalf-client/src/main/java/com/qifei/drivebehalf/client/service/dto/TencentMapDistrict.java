package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class TencentMapDistrict implements Serializable {
    /**
     * 行政区划唯一标识（adcode）
     */
    private String id;
    /**
     * 简称，如“内蒙古”
     */
    private String name;
    /**
     * 全称，如“内蒙古自治区”
     */
    private String fullname;
    /**
     * 经纬度
     */
    private Location location;
    /**
     * 行政区划拼音，每一下标为一个字的全拼，如：
     * [“nei”,“meng”,“gu”]
     */
    private List<String> pinyin;

    private List<Integer> cidx;

    public static class Location {
        /**
         * 纬度
         */
        private BigDecimal lat;
        /**
         * 经度
         */
        private BigDecimal lng;

        public BigDecimal getLat() {
            return lat;
        }

        public void setLat(BigDecimal lat) {
            this.lat = lat;
        }

        public BigDecimal getLng() {
            return lng;
        }

        public void setLng(BigDecimal lng) {
            this.lng = lng;
        }
    }
}

