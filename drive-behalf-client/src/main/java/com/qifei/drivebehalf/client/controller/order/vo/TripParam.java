package com.qifei.drivebehalf.client.controller.order.vo;

import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "结束行程参数")
public class TripParam implements Serializable {
    @NotEmpty(message = "订单编号不能为空")
    @ApiModelProperty("订单编号")
    private String orderNumber;

    @NotNull(message = "行驶距离不能为空")
    @Range(min = 1, max = 500, message = "行驶距离无效")
    @ApiModelProperty("行驶距离（公里）")
    private Integer distance;

    @NotNull(message = "行驶时间不能为空")
    @Range(min = 1, max = 720, message = "行驶时间无效")
    @ApiModelProperty("行驶时间（分钟）")
    private Integer duration;

    @NotNull(message = "等待费用不能为空")
    @Range(min = 0, max = 200, message = "等待费用无效")
    @ApiModelProperty("等待费用（元）")
    private Double waitingPrice;

    @NotNull(message = "等待时间不能为空")
    @Range(min = 0, max = 120, message = "等待时间无效")
    @ApiModelProperty("等待时间（分钟）")
    private Integer waitingTime;

    @NotNull(message = "实时计价不能为空")
    @Range(min = 1, max = 1000, message = "实时计价无效")
    @ApiModelProperty("实时计价（元）")
    private Double realPrice;

    @ApiModelProperty("终点")
    @NotEmpty(message = "终点地址不能为空")
    private String endPlace;

    @NotNull(message = "终点纬度不能为空")
    @ApiModelProperty("开始纬度")
    @Range(min = 18, max = 52, message = "当前位置不支持")
    private Double endLat;

    @NotNull(message = "终点经度不能为空")
    @ApiModelProperty("开始经度")
    @Range(min = 72, max = 132, message = "当前位置不支持")
    private Double endLng;
}
