package com.qifei.drivebehalf.client.common.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * page返回结果
 */
@ApiModel(description = "分页返回结果")
@Data
public class PageResult<T> {
    @ApiModelProperty(value = "页码")
    private int pageNum;
    @ApiModelProperty(value = "每页数量")
    private int pageSize;
    @ApiModelProperty(value = "总数量")
    private long total;
    @ApiModelProperty(value = "页数")
    private int pages;
    @ApiModelProperty(value = "数据")
    @JsonInclude()
    private List<T> data;

    public static <T> PageResult<T> create(Page<? extends BaseEntity> page, Class<T> cls) throws Exception {
        PageResult<T> pageResult = new PageResult<>();
        List list = page.getContent();
        if (list != null && !list.isEmpty()) {
            pageResult.setData(BeanCopyUtils.copyProperties(list, cls));
        }
        pageResult.setPageNum(page.getNumber() + 1);
        pageResult.setTotal(page.getTotalElements());
        pageResult.setPages(page.getTotalPages());
        pageResult.setPageSize(page.getSize());
        return pageResult;
    }

    public static <T> PageResult<T> createNoData(Page<? extends BaseEntity> page, Class<T> cls) throws Exception {
        PageResult<T> pageResult = new PageResult<>();
        pageResult.setPageNum(page.getNumber() + 1);
        pageResult.setTotal(page.getTotalElements());
        pageResult.setPages(page.getTotalPages());
        pageResult.setPageSize(page.getSize());
        return pageResult;
    }
}
