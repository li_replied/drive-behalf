package com.qifei.drivebehalf.client.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(
        prefix = "tencent-map"
)
public class TencentMapProperties {
    private String distanceMatrixUrl;
}
