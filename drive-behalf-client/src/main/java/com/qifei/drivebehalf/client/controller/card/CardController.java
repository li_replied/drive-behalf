package com.qifei.drivebehalf.client.controller.card;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.card.vo.CardResultVo;
import com.qifei.drivebehalf.client.entity.BankCardEntity;
import com.qifei.drivebehalf.client.service.CardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "客户端银行卡管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/card")
public class CardController {
    private final CardService cardService;

    @ValidToken
    @ApiOperation("查询银行卡")
    @GetMapping("/query")
    public ApiResponse<List<CardResultVo>> query() throws Exception {
        List<BankCardEntity> list = cardService.query();
        if (list == null || list.isEmpty()) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, CardResultVo.class));
    }

    @ValidToken
    @ApiOperation("保存银行卡")
    @PostMapping("/save")
    public ApiResponse<Void> save(
            @Valid @RequestBody CardResultVo param
    ) throws Exception {
        cardService.save(param);
        return ApiResponse.createBySuccess();
    }
}
