package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.DateUtil;
import com.qifei.drivebehalf.client.common.enums.UserType;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.dao.DriverWithdrawDao;
import com.qifei.drivebehalf.client.entity.DriverWithdrawEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class DriverWithdrawService {
    private final DriverWithdrawDao driverWithdrawDao;

    private final DriverService driverService;


    public List<DriverWithdrawEntity> query(Date month) throws Exception {
        if (month == null) {
            month = new Date();
        }

        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账户类型不正确");
        Date begin = DateUtil.beginOfMonth(month);
        Date end = DateUtil.endOfMonth(month);
        return driverWithdrawDao.queryByDriverIdAndCreateTimeBetweenOrderByCreateTimeDesc(userEntity.getId(), begin, end);
    }

    /**
     * 收入
     *
     * @param orderNumber
     * @param id
     * @param amount
     * @throws Exception
     */
    public void income(String orderNumber, Long id, BigDecimal amount) throws Exception {
        long count = driverWithdrawDao.countByOrderNumber(orderNumber);
        if (count == 0) {
            DriverWithdrawEntity driverWithdrawEntity = new DriverWithdrawEntity();
            driverWithdrawEntity.setOrderNumber(orderNumber);
            driverWithdrawEntity.setDriverId(id);
            driverWithdrawEntity.setAmount(amount);
            Date now = new Date();
            driverWithdrawEntity.setDealTime(now);
            driverWithdrawEntity.setCreate();
            driverWithdrawDao.save(driverWithdrawEntity);
            driverService.income(id, amount);
        }
    }
}
