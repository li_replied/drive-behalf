package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.entity.DriverRechargeConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.math.BigDecimal;
import java.util.List;

public interface DriverRechargeConfigDao extends JpaRepository<DriverRechargeConfig, Long>, JpaSpecificationExecutor<DriverRechargeConfig> {
    List<DriverRechargeConfig> queryAllByStateOrderByRechargeAmount(EnableEnum state);

    DriverRechargeConfig queryFirstByRechargeAmountAndState(BigDecimal amount,EnableEnum state);
}
