package com.qifei.drivebehalf.client.controller.pay;

import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.controller.pay.vo.CallbackResult;
import com.qifei.drivebehalf.client.service.WxPayService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(tags = "支付回调")
@Controller
@RequestMapping("/callback")
@RequiredArgsConstructor
@Slf4j
public class CallBackController {
    private final WxPayService wxPayService;

    @PostMapping
    public void callback(HttpServletRequest request, HttpServletResponse response) {
        CallbackResult callbackResult = new CallbackResult();
        try {
            String wechatpaySerial = request.getHeader("Wechatpay-Serial");
            String wechatpayTimestamp = request.getHeader("Wechatpay-Timestamp");
            String wechatpayNonce = request.getHeader("Wechatpay-Nonce");
            String wechatpaySignature = request.getHeader("Wechatpay-Signature");
            ServletInputStream servletInputStream = request.getInputStream();
            byte[] bytes = new byte[1024];
            int count = 0;
            StringBuffer body = new StringBuffer();
            while ((count = servletInputStream.read(bytes)) > 0) {
                body.append(new String(bytes, 0, count, "UTF-8"));
            }
            servletInputStream.close();
            wxPayService.callback(wechatpaySerial, wechatpayTimestamp, wechatpayNonce, wechatpaySignature, body.toString());
            callbackResult.setCode("SUCCESS");
            callbackResult.setMessage("SUCCESS");
        } catch (Exception e) {
            log.error("微信支付回调出错", e);
            callbackResult.setCode("ERROR");
            callbackResult.setMessage(e.getMessage());
        } finally {
            if ("SUCCESS".equals(callbackResult.getCode())) {
                response.setStatus(200);
            } else {
                response.setStatus(500);
            }
            try {
                response.getWriter().write(JsonUtils.writeValue(callbackResult));
                response.getWriter().flush();
            } catch (IOException e) {
                log.error("response 写文件出错", e);
            }
        }
    }
}
