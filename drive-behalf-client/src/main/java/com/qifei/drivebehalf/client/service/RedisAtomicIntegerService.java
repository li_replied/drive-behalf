package com.qifei.drivebehalf.client.service;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RedisAtomicIntegerService implements InitializingBean {
    @Resource
    private RedisTemplate<String, Integer> redisTemplate;
    RedisAtomicInteger redisCount;

    @Override
    public void afterPropertiesSet() throws Exception {
        redisCount = new RedisAtomicInteger("atomic:integer", redisTemplate.getConnectionFactory());
    }

    public int id() {
        return redisCount.incrementAndGet();
    }
}
