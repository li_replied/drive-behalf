package com.qifei.drivebehalf.client.controller.subscribe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@ApiModel(description = "预约结果")
public class SubscribeResult {
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "起点")
    private String startPoint;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "出发时间")
    private Date appointmentTime;
    @ApiModelProperty(value = "手机号")
    private String phone;
}
