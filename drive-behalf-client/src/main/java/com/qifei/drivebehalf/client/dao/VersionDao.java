package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OriginType;
import com.qifei.drivebehalf.client.common.enums.VersionPlatformEnum;
import com.qifei.drivebehalf.client.entity.VersionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VersionDao extends JpaRepository<VersionEntity, Long>, JpaSpecificationExecutor<VersionEntity> {
    VersionEntity findFirstByPlatformAndEnableStateAndOriginOrderByVersionNumberDesc(VersionPlatformEnum platform, EnableEnum enable, OriginType origin);
}
