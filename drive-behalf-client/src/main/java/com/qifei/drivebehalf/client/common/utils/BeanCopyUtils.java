package com.qifei.drivebehalf.client.common.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BeanCopyUtils {

    private static HashMap exclude = new HashMap();

    static {
        exclude.put("class", "");
    }

    /**
     * 拷贝bean属性<br/>集合，数组的属性不能拷贝。需要手动赋值
     * @param source
     * @param cls
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> T copyProperties(Object source, Class<T> cls) throws Exception {
        if (source == null) {
            return null;
        }
        Assert.isTrue(!source.getClass().isArray(), "object must not be array");
        PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(cls);
        if (propertyDescriptors == null || propertyDescriptors.length == 0) {
            return null;
        }
        Class sourceClass = source.getClass();
        T target = BeanUtils.instantiateClass(cls);
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            if (exclude.containsKey(propertyDescriptor.getName())) {
                continue;
            }
            Class targetPropertyType = propertyDescriptor.getPropertyType();
            //基础类型
            if (BeanUtils.isSimpleProperty(targetPropertyType)) {
                PropertyDescriptor sourcePropertyDescriptor = BeanUtils.getPropertyDescriptor(sourceClass, propertyDescriptor.getName());
                if (sourcePropertyDescriptor == null) {
                    continue;
                }
                //判断类型是否一直
                if (sourcePropertyDescriptor.getPropertyType() != targetPropertyType) {
                    continue;
                }
                Object value = sourcePropertyDescriptor.getReadMethod().invoke(source);
                if (value == null) {
                    continue;
                }
                propertyDescriptor.getWriteMethod().invoke(target, value);
            } else {
                PropertyDescriptor sourcePropertyDescriptor = BeanUtils.getPropertyDescriptor(sourceClass, propertyDescriptor.getName());
                if (sourcePropertyDescriptor == null) {
                    continue;
                }
                Object value = sourcePropertyDescriptor.getReadMethod().invoke(source);
                if (value == null) {
                    continue;
                }
                Object transformValue = copyProperties(value, targetPropertyType);
                if (transformValue == null) {
                    continue;
                }
                propertyDescriptor.getWriteMethod().invoke(target, transformValue);
            }
        }
        return target;
    }

    /**
     * 拷贝bean属性<br/>集合，数组的属性不能拷贝。需要手动赋值
     * @param sources
     * @param cls
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> List<T> copyProperties(List sources, Class<T> cls) throws Exception {
        if (sources == null) {
            return null;
        }
        List<T> list = new ArrayList<>();
        for (Object source : sources) {
            T value = copyProperties(source, cls);
            list.add(value);
        }
        return list;
    }
}
