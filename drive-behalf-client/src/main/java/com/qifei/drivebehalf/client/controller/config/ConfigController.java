package com.qifei.drivebehalf.client.controller.config;

import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.service.ConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "系统配置")
@RestController
@RequiredArgsConstructor
@RequestMapping("/config")
public class ConfigController {
    private final ConfigService configService;

    @ApiOperation("公司简介")
    @GetMapping("/introduction")
    public ApiResponse<String> introduction() throws Exception {
        return ApiResponse.createBySuccess(configService.introduction());
    }
}
