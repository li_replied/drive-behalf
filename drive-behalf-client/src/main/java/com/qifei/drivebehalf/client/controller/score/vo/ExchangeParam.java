package com.qifei.drivebehalf.client.controller.score.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ApiModel(description = "兑换积分参数")
public class ExchangeParam implements Serializable {
    @ApiModelProperty("姓名")
    @NotBlank(message = "姓名不能为空")
    private String name;
    @ApiModelProperty("手机号")
    @NotBlank(message = "手机号不能为空")
    private String phone;
    @ApiModelProperty("银行卡")
    @NotBlank(message = "银行卡不能为空")
    private String card;
    @ApiModelProperty("银行名称")
    @NotBlank(message = "银行名称不能为空")
    private String bank;
    @ApiModelProperty("开户银行")
    @NotBlank(message = "开户银行不能为空")
    private String subbranch;
    @ApiModelProperty("兑换积分")
    @NotNull(message = "兑换积分不能为空")
    @Min(value = 0, message = "兑换积分不能小于0")
    private BigDecimal amount = new BigDecimal(0);
}
