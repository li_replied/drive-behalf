package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.PayState;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(
        name = "driver_recharge",
        indexes = {
                @Index(name = "idx_driver_recharge_passenger_id", columnList = "passenger_id"),
                @Index(name = "idx_driver_recharge_order_number", columnList = "order_number"),
        }
)
public class DriverRechargeEntity extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recharge_id", unique = true, nullable = false)
    private Long id;
    @Column(name = "order_number", nullable = false,unique = true)
    private String orderNumber;
    @Column(name = "config_id")
    private Long configId;
    @Column(name = "pay_id")
    private String payId;
    @Column(name = "passenger_id", nullable = false)
    private Long passengerId;
    @Column(name = "transaction_id")
    private String transactionId;
    @Column(name = "success_time")
    private Date successTime;
    @Column(name = "amount", precision = 10, scale = 2)
    private BigDecimal amount = new BigDecimal(0);
    @Column(name = "recharge_amount", precision = 10, scale = 2)
    private BigDecimal giftAmount = new BigDecimal(0);
    @Column(name = "pay_state", nullable = false)
    @Enumerated(EnumType.STRING)
    private PayState payState;
    @Column(name = "income", nullable = false)
    private Boolean income = true;
}
