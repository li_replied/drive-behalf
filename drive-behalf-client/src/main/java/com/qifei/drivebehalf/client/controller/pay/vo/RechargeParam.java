package com.qifei.drivebehalf.client.controller.pay.vo;

import com.qifei.drivebehalf.client.common.enums.PayType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Setter
@Getter
@ApiModel(description = "充值参数")
public class RechargeParam {
    @ApiModelProperty("订单编号")
    @NotNull(message = "订单编号能为空")
    private String orderNumber;
    @NotNull(message = "支付方式不能为空")
    @ApiModelProperty("支付方式")
    private PayType payType;
}
