package com.qifei.drivebehalf.client.controller.shortmessage;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.controller.shortmessage.vo.SendParam;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.ShortMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "短信服务")
@RestController
@RequestMapping("/sms")
public class ShortMessageController {

    private ShortMessageService shortMessageService;

    @Autowired
    public void setShortMessageService(ShortMessageService shortMessageService) {
        this.shortMessageService = shortMessageService;
    }

    @ApiOperation("发送短信验证码")
    @PostMapping("/captcha/send")
    public ApiResponse send(@RequestBody @Validated SendParam param) throws Exception {
        shortMessageService.sendSm(param.getPhone());
        return ApiResponse.createBySuccessMessage("短信验证码已发送");
    }
}
