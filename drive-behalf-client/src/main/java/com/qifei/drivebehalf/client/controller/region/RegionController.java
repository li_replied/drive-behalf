package com.qifei.drivebehalf.client.controller.region;

import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.controller.region.vo.RegionResult;
import com.qifei.drivebehalf.client.service.DriverRegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@Api(tags = "客户端：地区")
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {
    private final DriverRegionService driverRegionService;

    @ApiOperation("查询全部地区")
    @GetMapping("/all")
    public ApiResponse<List<RegionResult>> query() throws Exception {
        List<RegionResult> results = driverRegionService.queryAll();
        return ApiResponse.createBySuccess(results);
    }

    @ApiOperation("查询当前地理位置")
    @GetMapping("/info/{lat}/{lng}")
    public ApiResponse<RegionResult> info(
            @Validated
            @PathVariable("lat")
            @NotEmpty()
                    String lat,
            @PathVariable("lng")
            @NotEmpty()
                    String lng
    ) throws Exception {
        return ApiResponse.createBySuccess(driverRegionService.info(lat, lng));
    }


}
