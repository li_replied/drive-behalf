package com.qifei.drivebehalf.client.common.enums;

import org.apache.commons.lang3.StringUtils;

public enum AudioFileTypeEnum {
    AMR("audio/amr", ".amr"),
    ACC("audio/aac", ".aac"),
    XM4A("audio/x-m4a", ".m4a"),
    MP3("audio/mpeg", ".mp3"),
    WAV("audio/x-wav",".wav");
    private String type;
    private String suffix;

    AudioFileTypeEnum(String type, String suffix) {
        this.type = type;
        this.suffix = suffix;
    }

    public static AudioFileTypeEnum findType(String type) {
        if (StringUtils.isEmpty(type)) {
            return null;
        }
        AudioFileTypeEnum[] fileTypes = AudioFileTypeEnum.values();
        for (AudioFileTypeEnum fileType : fileTypes) {
            if (fileType.type.equals(type)) {
                return fileType;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }

    public String getSuffix() {
        return suffix;
    }
}
