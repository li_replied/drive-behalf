package com.qifei.drivebehalf.client.controller.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "派单返回")
public class AssignedResult implements Serializable {
    @ApiModelProperty("订单编号")
    private String code;
    @ApiModelProperty("联系电话")
    private String phone;
    @ApiModelProperty("所在位置")
    private String place;
    @ApiModelProperty("开始经度")
    private Double startlng;
    @ApiModelProperty("开始纬度")
    private Double startlat;
    @ApiModelProperty("结束经度")
    private Double endlng;
    @ApiModelProperty("结束纬度")
    private Double endlat;
    @ApiModelProperty("过期时间")
    private Long deadline;
    @ApiModelProperty("超时上限")
    private Integer timeout;
    @ApiModelProperty("cid")
    private String cid; // 离线使用
}
