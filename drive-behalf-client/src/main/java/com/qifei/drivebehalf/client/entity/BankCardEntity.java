package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "driver_bank_card",
        indexes = {
                @Index(name = "idx_driver_bank_card_user_id", columnList = "user_id"),
        })
public class BankCardEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "user_id", nullable = false)
    private Long userId;
    private String name;
    private String phone;
    private String card;
    private String bank;
    private String subbranch;
}
