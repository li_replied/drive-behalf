package com.qifei.drivebehalf.client.task;

import com.aliyun.oss.common.utils.DateUtil;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.enums.RegionType;
import com.qifei.drivebehalf.client.entity.DriverRegionEntity;
import com.qifei.drivebehalf.client.service.DriverRegionService;
import com.qifei.drivebehalf.client.service.RedisService;
import com.qifei.drivebehalf.client.service.TencentMapService;
import com.qifei.drivebehalf.client.service.dto.TencentMapDistrict;
import com.qifei.drivebehalf.client.service.dto.TencentMapDistrictResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class RegionTask {
    @Value("${tencent-map.key}")
    private String key;

    private final TencentMapService tencentMapService;

    private final DriverRegionService driverRegionService;

    private final RedisService redisService;

    private static List<String> municipalities = new ArrayList<>();

    static {
        municipalities.add("北京市");
        municipalities.add("上海市");
        municipalities.add("天津市");
        municipalities.add("重庆市");
        municipalities.add("台湾省");
        municipalities.add("香港特别行政区");
        municipalities.add("澳门特别行政区");
    }

    /**
     * 调用腾讯地图更新地区信息
     */
    @Scheduled(cron = "0 0 0 1 * ?")
    public void updateRegion() {
        log.info(String.format("更新腾讯地区行政划分开始 %s", DateUtil.formatIso8601Date(new Date())));
        try {
            TencentMapDistrictResult<List<TencentMapDistrict>> result = tencentMapService.getChildren(null, key);
            if (result.getStatus() != 0) {
                log.error(String.format("请求失败:%s", result.getMessage()));
                return;
            }
            String regionVersion = redisService.getKey(RedisKeyEnum.RegionVersion);
            if (StringUtils.isEmpty(regionVersion)) {
                regionVersion = "0";
            }
            Integer version = Integer.valueOf(regionVersion);
            if (version == result.getDataVersion()) {
                log.info("版本一致，不更新");
                return;
            }
            driverRegionService.deleteAll();
            List<List<TencentMapDistrict>> districtList = result.getResult();
            saveProvince(districtList);
            redisService.setKey(RedisKeyEnum.RegionVersion, String.valueOf(result.getDataVersion()));
            redisService.delKey(RedisKeyEnum.Region);
            driverRegionService.queryAll();
        } catch (Exception e) {
            log.error("更新腾讯地区行政划分", e);
        } finally {
            log.info(String.format("更新腾讯地区行政划分结束 %s", DateUtil.formatIso8601Date(new Date())));
        }
    }

    private void saveProvince(List<List<TencentMapDistrict>> districtList) throws Exception {
        Thread.sleep(500);
        if (districtList == null || districtList.isEmpty()) {
            return;
        }
        List<TencentMapDistrict> provinceList = districtList.get(0);
        for (TencentMapDistrict province : provinceList) {
            DriverRegionEntity driverRegionEntity = new DriverRegionEntity();
            driverRegionEntity.setRegionId(province.getId());
            driverRegionEntity.setParentId("100000");
            driverRegionEntity.setName(province.getFullname());
            driverRegionEntity.setShortName(province.getName());
            driverRegionEntity.setLevelType(RegionType.PROVINCE);
            driverRegionEntity.setCreatedUser(0L);
            driverRegionEntity.setCreateTime(new Date());
            driverRegionService.save(driverRegionEntity);
            saveCity(province);
        }
    }

    private void saveCity(TencentMapDistrict province) throws Exception {
        Thread.sleep(500);
        String parentId = province.getId();
        if (municipalities.contains(province.getFullname())) {
            DriverRegionEntity driverRegionEntity = new DriverRegionEntity();
            long id = Long.valueOf(province.getId());
            id++;
            driverRegionEntity.setRegionId(String.valueOf(id));
            driverRegionEntity.setParentId(parentId);
            driverRegionEntity.setName(province.getFullname());
            driverRegionEntity.setShortName(province.getName());
            driverRegionEntity.setLevelType(RegionType.CITY);
            driverRegionEntity.setCreatedUser(0L);
            driverRegionEntity.setCreateTime(new Date());
            driverRegionService.save(driverRegionEntity);
            saveRegion(province, driverRegionEntity.getRegionId());
            return;
        }
        TencentMapDistrictResult<List<TencentMapDistrict>> result = tencentMapService.getChildren(parentId, key);
        if (result.getStatus() != 0) {
            log.error(String.format("请求失败:%s", result.getMessage()));
            return;
        }
        List<List<TencentMapDistrict>> districtList = result.getResult();
        if (districtList == null || districtList.isEmpty()) {
            return;
        }
        List<TencentMapDistrict> cityList = districtList.get(0);
        for (TencentMapDistrict city : cityList) {
            DriverRegionEntity driverRegionEntity = new DriverRegionEntity();
            driverRegionEntity.setRegionId(city.getId());
            driverRegionEntity.setParentId(parentId);
            driverRegionEntity.setName(city.getFullname());
            driverRegionEntity.setShortName(city.getName());
            driverRegionEntity.setLevelType(RegionType.CITY);
            driverRegionEntity.setCreatedUser(0L);
            driverRegionEntity.setCreateTime(new Date());
            driverRegionService.save(driverRegionEntity);
            saveRegion(city, null);
        }
    }

    private void saveRegion(TencentMapDistrict city, String temp) throws Exception {
        Thread.sleep(500);
        String parentId = city.getId();
        TencentMapDistrictResult<List<TencentMapDistrict>> result = tencentMapService.getChildren(parentId, key);
        if (result.getStatus() != 0) {
            log.error(String.format("请求失败:%s", result.getMessage()));
            return;
        }
        if (StringUtils.isNotEmpty(temp)) {
            parentId = temp;
        }
        List<List<TencentMapDistrict>> districtList = result.getResult();
        if (districtList == null || districtList.isEmpty()) {
            return;
        }
        List<TencentMapDistrict> cityList = districtList.get(0);
        List<DriverRegionEntity> driverRegionEntityList = new ArrayList<>();
        for (TencentMapDistrict region : cityList) {
            DriverRegionEntity driverRegionEntity = new DriverRegionEntity();
            driverRegionEntity.setRegionId(region.getId());
            driverRegionEntity.setParentId(parentId);
            driverRegionEntity.setName(region.getFullname());
            driverRegionEntity.setShortName(region.getName());
            driverRegionEntity.setLevelType(RegionType.DISTRICT);
            driverRegionEntity.setCreatedUser(0L);
            driverRegionEntity.setCreateTime(new Date());
            driverRegionEntityList.add(driverRegionEntity);
        }
        driverRegionService.saveAll(driverRegionEntityList);
    }
}
