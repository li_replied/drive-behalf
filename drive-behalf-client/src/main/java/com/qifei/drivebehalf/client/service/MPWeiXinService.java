package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.service.dto.AccessToken;
import com.qifei.drivebehalf.client.service.dto.Code2Session;
import com.qifei.drivebehalf.client.service.dto.WXSendMessageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(name = "mp-weixin", url = "${weixin.url}")
public interface MPWeiXinService {
    @RequestMapping(path = "${weixin.code2-session}", method = RequestMethod.GET)
    Code2Session code2session(
            @RequestParam("appid")
                    String appId,
            @RequestParam("secret")
                    String secret,
            @RequestParam("js_code")
                    String jsCode,
            @RequestParam("grant_type")
                    String grantType
    );

    @RequestMapping(path = "${weixin.get-access-token}", method = RequestMethod.GET)
    AccessToken getAccessToken(
            @RequestParam("appid")
                    String appId,
            @RequestParam("secret")
                    String secret,
            @RequestParam("grant_type")
                    String grantType
    );

    @RequestMapping(path = "${weixin.message-url}", method = RequestMethod.POST)
    WXSendMessageResult sendMessage(@RequestParam("access_token") String accessToken, @RequestBody String message);
}
