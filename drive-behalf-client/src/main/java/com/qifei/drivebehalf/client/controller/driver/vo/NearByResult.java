package com.qifei.drivebehalf.client.controller.driver.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@ApiModel(description = "附近司机")
public class NearByResult implements Serializable {
    @ApiModelProperty("id")
    protected Long id;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("经度")
    private Double lastLongitude;
    @ApiModelProperty("纬度")
    private Double lastLatitude;
}
