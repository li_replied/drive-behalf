package com.qifei.drivebehalf.client.task;

import com.aliyun.oss.common.utils.DateUtil;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.properties.WeiXinProperties;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.service.MPWeiXinService;
import com.qifei.drivebehalf.client.service.RedisService;
import com.qifei.drivebehalf.client.service.dto.AccessToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;

@Component
@RequiredArgsConstructor
@Slf4j
public class AccessTokenTask {
    private final MPWeiXinService mpWeiXinService;

    private final RedisService redisService;
    @Resource
    private WeiXinProperties weiXinProperties;

    @PostConstruct
    public void init() {
        updateAccessToken();
    }


    @Scheduled(cron = "0 0 0/1 * * ?")
    public void updateAccessToken() {
        try {
            log.info(String.format("更新微信AccessToken开始 %s", DateUtil.formatIso8601Date(new Date())));
            AccessToken accessToken = mpWeiXinService.getAccessToken( weiXinProperties.getAppId(), weiXinProperties.getSecret(),"client_credential");
            log.info(String.format("更新微信AccessToken结果 %s", JsonUtils.writeValue(accessToken)));
            if (StringUtils.isEmpty(accessToken.getAccessToken())) {
                log.info(String.format("更新微信AccessToken错误 %s %s", accessToken.getErrcode(), accessToken.getErrmsg()));
            } else {
                redisService.setEx(RedisKeyEnum.AccessToken, accessToken.getAccessToken());
            }
        } catch (Exception e) {
            log.info(String.format("更新微信AccessToken结束 %s", DateUtil.formatIso8601Date(new Date())));
        }

    }
}
