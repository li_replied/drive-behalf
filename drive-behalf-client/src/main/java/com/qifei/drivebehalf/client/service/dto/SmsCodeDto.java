package com.qifei.drivebehalf.client.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SmsCodeDto implements Serializable {
    private String code;
    private Long logId;
}
