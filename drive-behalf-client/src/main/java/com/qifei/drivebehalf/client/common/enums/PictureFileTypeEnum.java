package com.qifei.drivebehalf.client.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 图片文件类型
 */
public enum PictureFileTypeEnum {
    JPEG("image/jpeg", ".jpeg", 2),
    PNG("image/png", ".png", 2),
    GIF("image/gif", ".gif", 2);
    private String type;
    private String suffix;
    private int size;

    PictureFileTypeEnum(String type, String suffix, int size) {
        this.type = type;
        this.suffix = suffix;
        this.size = size;
    }

    public static PictureFileTypeEnum findType(String type) {
        if (StringUtils.isEmpty(type)) {
            return null;
        }
        PictureFileTypeEnum[] fileTypes = PictureFileTypeEnum.values();
        for (PictureFileTypeEnum fileType : fileTypes) {
            if (fileType.type.equals(type)) {
                return fileType;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }

    public String getSuffix() {
        return suffix;
    }

    public int getSize() {
        return size;
    }
}
