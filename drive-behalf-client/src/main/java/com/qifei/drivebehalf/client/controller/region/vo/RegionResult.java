package com.qifei.drivebehalf.client.controller.region.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ApiModel(description = "地区结果")
public class RegionResult implements Serializable {
    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("地区id")
    private String regionId;

    @ApiModelProperty("结果集")
    private List<RegionResult> data = null;
}
