package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import com.qifei.drivebehalf.client.common.enums.UserType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class UserEntity extends BaseEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
    protected Long id;
    /**
     * 名称
     */
    @Column(name = "name")
    private String name;

    @Column(name = "phone", unique = true, nullable = false)
    private String phone;

    @Column(name = "avatar", length = 512)
    private String avatar;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "client_id")
    private String clientId;

    /**
     * 性别
     */
    @Column(name = "sex", nullable = false)
    @Enumerated(EnumType.STRING)
    private SexEnum sex = SexEnum.M;

    @Column(name = "user_enable")
    @Enumerated(EnumType.STRING)
    private EnableEnum userEnable = EnableEnum.YES;

    @Transient
    private UserType userType;

}
