package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class WXPayer implements Serializable {
    /**
     * 用户标识	openid	string[1,128]	是	用户在直连商户appid下的唯一标识。
     */
    private String openid;

    public WXPayer(String openid) {
        this.openid = openid;
    }
}
