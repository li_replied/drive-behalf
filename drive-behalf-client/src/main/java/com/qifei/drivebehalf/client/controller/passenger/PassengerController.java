package com.qifei.drivebehalf.client.controller.passenger;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.model.PageResult;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.passenger.vo.*;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.PassengerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@Api(tags = "客户端：乘客管理")
@RequestMapping("/passenger")
@RequiredArgsConstructor
public class PassengerController {
    private final PassengerService passengerService;

    @ApiOperation("登录授权")
    @PostMapping("/login")
    public ApiResponse<String> login(@RequestBody @Validated PassengerLoginParam param) throws Exception {
        String token = passengerService.login(param);
        return ApiResponse.createBySuccess(token);
    }

    @ApiOperation("快捷登录")
    @PostMapping("/quick/login")
    public ApiResponse<String> quickLogin(@RequestBody @Validated QuickLoginParam param) throws Exception {
        String token = passengerService.quickLogin(param);
        return ApiResponse.createBySuccess(token);
    }

    @ValidToken
    @ApiOperation("查看用户信息")
    @GetMapping("/info")
    public ApiResponse<PassengerResult> info() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        PassengerEntity passengerEntity = passengerService.info(userEntity.getId());
        Assert.notNull(passengerEntity, "用户信息不存在。");
        PassengerResult passengerResult = BeanCopyUtils.copyProperties(passengerEntity, PassengerResult.class);
        return ApiResponse.createBySuccess(passengerResult);
    }

    @ValidToken
    @ApiOperation("修改用户信息")
    @PostMapping("/modify")
    public ApiResponse<Void> modify(@RequestBody @Validated PassengerModifyParam param) throws Exception {
        passengerService.modify(param);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("推荐码")
    @GetMapping("/recommend")
    public ApiResponse<RecommendResult> recommend() throws Exception {
        RecommendResult recommend = passengerService.recommend();
        return ApiResponse.createBySuccess(recommend);
    }

    @ValidToken
    @ApiOperation("添加推荐人")
    @GetMapping("/relation/{recommend}")
    public ApiResponse<Void> relation(@PathVariable("recommend") String recommend) throws Exception {
        passengerService.relation(recommend);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("查询推荐人")
    @GetMapping("/query/relation")
    public ApiResponse<Long> queryRelation() throws Exception {
        return ApiResponse.createBySuccess(passengerService.queryRelation());
    }

    @ValidToken
    @ApiOperation("上传头像")
    @PostMapping("/avatar")
    public ApiResponse<String> avatar(@RequestParam("file") MultipartFile file) throws Exception {
        return ApiResponse.createBySuccess(passengerService.avatar(file));
    }

    @ValidToken
    @ApiOperation("二级")
    @PostMapping("/second")
    public ApiResponse<PageResult<PassengerResult>> second(@RequestBody @Validated PageParam param) throws Exception {
        Page<PassengerEntity> list = passengerService.second(param);
        PageResult<PassengerResult> result = PageResult.create(list, PassengerResult.class);
        return ApiResponse.createBySuccess(result);
    }

    @ValidToken
    @ApiOperation("下级")
    @GetMapping("/subordinate")
    public ApiResponse<List<PassengerResult>> subordinate() throws Exception {
        List<PassengerEntity> list = passengerService.subordinate();
        List<PassengerResult> result = null;
        if (list != null) {
            result = BeanCopyUtils.copyProperties(list, PassengerResult.class);
        }
        return ApiResponse.createBySuccess(result);
    }
}


