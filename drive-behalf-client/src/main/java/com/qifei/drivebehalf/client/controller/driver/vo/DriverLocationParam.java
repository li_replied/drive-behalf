package com.qifei.drivebehalf.client.controller.driver.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "司机定位参数")
public class DriverLocationParam implements Serializable {
    @NotEmpty(message = "设备标识不能为空")
    @Length(max = 32, min = 32, message = "设备标识无效")
    private String cid;
    @NotNull(message = "经度不能为空")
    private Double longitude;
    @NotNull(message = "纬度不能为空")
    private Double latitude;
    @NotEmpty(message = "地址不能为空")
    @Length(max = 255, min = 1, message = "地址长度无效")
    private String address;
}