package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.enums.UserType;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.properties.FileProperties;
import com.qifei.drivebehalf.client.common.properties.JWTProperties;
import com.qifei.drivebehalf.client.common.properties.WeiXinProperties;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.JWTUtils;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.common.utils.QRCodeUtil;
import com.qifei.drivebehalf.client.controller.passenger.vo.PassengerLoginParam;
import com.qifei.drivebehalf.client.controller.passenger.vo.PassengerModifyParam;
import com.qifei.drivebehalf.client.controller.passenger.vo.QuickLoginParam;
import com.qifei.drivebehalf.client.controller.passenger.vo.RecommendResult;
import com.qifei.drivebehalf.client.dao.PassengerDao;
import com.qifei.drivebehalf.client.entity.DriverDistributionRatioEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.dto.Code2Session;
import com.qifei.drivebehalf.client.service.dto.DealerShareMessageDao;
import com.qifei.drivebehalf.client.service.dto.ShareMessageDto;
import com.qifei.drivebehalf.client.service.dto.WXPhone;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import java.io.File;
import java.math.BigDecimal;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.Security;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class PassengerService {

    private final ShortMessageService shortMessageService;

    private final RedisService redisService;

    private final PassengerDao passengerDao;

    private final MPWeiXinService mpWeiXinService;

    private final KafkaProducer kafkaProducer;

    private final DriverDistributionRatioService driverDistributionRatioService;

    @Resource
    private ScoreService scoreService;

    @Resource
    private DriverTripOrderService driverTripOrderService;

    @Resource
    private JWTProperties jwtProperties;
    @Resource
    private WeiXinProperties weiXinProperties;
    @Resource
    private FileProperties fileProperties;
    @Value("${spring.profiles.active}")
    private String env;

    public String login(PassengerLoginParam param) throws Exception {
        // 本地测试
        if (env.equalsIgnoreCase("dev") == false) {
            shortMessageService.verificationCode(param.getPhone(), param.getSmsCode());
        }
        PassengerEntity passengerEntity = passengerDao.findByPhone(param.getPhone());
        if (passengerEntity == null) {
            passengerEntity = defaultClient(param.getPhone());
        }
        if (passengerEntity.getUserEnable() == EnableEnum.NO) {
            throw new ServiceWorkingException("账号已禁用。");
        }
        if (StringUtils.isNotEmpty(param.getJsCode())) {
            String openId = openId(param.getJsCode());
            if (StringUtils.isNotEmpty(openId)) {
                passengerEntity.setOpenId(openId);
                passengerEntity.setModified();
                passengerDao.save(passengerEntity);
            }
        }
        return cacheUser(passengerEntity);
    }

    public String quickLogin(QuickLoginParam param) throws Exception {
        String[] result = sessionKey(param.getJsCode());
        Assert.notNull(result, "获取openid错误。");
        String phone = //WechatDecryptDataUtil.decryptData(param.getEncryptedData(), result[1], param.getIv());
                getPhoneNumber(param.getEncryptedData(), param.getIv(), result[1]);
        Assert.hasText(phone, "获取电话号码错误。");
        WXPhone wxPhone = JsonUtils.readValue(phone, WXPhone.class);
        Assert.notNull(wxPhone, "获取电话号码错误。");
        Assert.hasText(wxPhone.getPhoneNumber(), "未绑定手机号。");
        PassengerEntity passengerEntity = passengerDao.findByPhone(wxPhone.getPhoneNumber());
        if (passengerEntity == null) {
            passengerEntity = defaultClient(wxPhone.getPhoneNumber());
            if (StringUtils.isNotEmpty(param.getNickName())) {
                passengerEntity.setName(param.getNickName());
                passengerEntity.setNameShow(EnableEnum.YES);
            }
            if (StringUtils.isNotEmpty(param.getAvatar())) {
                passengerEntity.setAvatar(param.getAvatar());
            }
        }
        if (passengerEntity.getUserEnable() == EnableEnum.NO) {
            throw new ServiceWorkingException("账号已禁用。");
        }

        passengerEntity.setOpenId(result[0]);
        passengerEntity.setModified();
        passengerDao.save(passengerEntity);
        return cacheUser(passengerEntity);
    }

    public PassengerEntity info(Long id) throws Exception {
        return passengerDao.findById(id).orElse(null);
    }

    @Transactional
    public void recharge(Long id, BigDecimal amount) throws Exception {
        PassengerEntity passengerEntity = info(id);
        Assert.notNull(passengerEntity, "用户不存在");
        passengerDao.recharge(id, amount);
    }

    @Transactional(rollbackOn = Exception.class)
    public void modify(PassengerModifyParam param) throws Exception {
        PassengerEntity passengerEntity = passengerDao.findById(param.getId()).orElseGet(null);
        Assert.notNull(passengerEntity, "用户不存在。");
        BeanUtils.copyProperties(param, passengerEntity);
        passengerEntity.setModified();
        passengerDao.save(passengerEntity);
    }

    public RecommendResult recommend() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.notNull(userEntity, "账号不存在");
        String phone = userEntity.getPhone();
        String value = "https://hsdj.hongsedaijia.cn/download/?user_id=" + phone;
        byte[] image = QRCodeUtil.createImage(value);
        RecommendResult result = new RecommendResult();
        result.setPhone(phone);
        result.setImage(Base64Utils.encodeToString(image));
        return result;

    }

    @Transactional
    public void relation(String recommend) throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.notNull(userEntity, "账号不存在");
        PassengerEntity user = info(userEntity.getId());
        Assert.notNull(user, "账号不存在");
        Assert.isTrue(user.getParentId() == null, "已经有推荐人");
        PassengerEntity passengerEntity = passengerDao.findByPhone(recommend);
        Assert.notNull(passengerEntity, "推荐人不存在");
        Assert.isTrue(!userEntity.getId().equals(passengerEntity.getId()), "推荐人不允许是自己");
        if (passengerEntity.getParentId() != null) {
            Assert.isTrue(!passengerEntity.getParentId().equals(user.getId()), "推荐人已绑定你的手机号");
        }
        passengerEntity.setRelationTime(new Date());
        passengerDao.relation(userEntity.getId(), passengerEntity.getId());
    }

    public Long queryRelation() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.notNull(userEntity, "账号不存在");
        PassengerEntity user = info(userEntity.getId());
        Assert.notNull(user, "账号不存在");
        return user.getParentId();
    }

    private PassengerEntity defaultClient(String phone) throws Exception {
        PassengerEntity passengerEntity = new PassengerEntity();
        passengerEntity.setPhone(phone);
        passengerEntity.setName(RandomStringUtils.randomAlphabetic(6));
        passengerEntity.setCreate();
        passengerDao.save(passengerEntity);
        return passengerEntity;
    }

    private String openId(String jsCode) throws Exception {
        Code2Session code2Session = mpWeiXinService.code2session(weiXinProperties.getAppId(), weiXinProperties.getSecret(), jsCode, "authorization_code");
        if (StringUtils.isNotEmpty(code2Session.getOpenid())) {
            return code2Session.getOpenid();
        }
        log.error(
                String.format(
                        "获取openid错误  jscode:%s errorcode:%s errormsg:%s",
                        jsCode,
                        code2Session.getErrcode(),
                        code2Session.getErrmsg()
                )
        );
        return null;
    }

    private String[] sessionKey(String jsCode) throws Exception {
        Code2Session code2Session = mpWeiXinService.code2session(weiXinProperties.getAppId(), weiXinProperties.getSecret(), jsCode, "authorization_code");
        if (StringUtils.isNotEmpty(code2Session.getOpenid())) {
            return new String[]{
                    code2Session.getOpenid(),
                    code2Session.getSessionKey()
            };
        }
        log.error(
                String.format(
                        "获取openid错误  jscode:%s errorcode:%s errormsg:%s",
                        jsCode,
                        code2Session.getErrcode(),
                        code2Session.getErrmsg()
                )
        );
        return null;
    }

    private String getPhoneNumber(String encryptedData, String iv, String sessionKey) throws Exception {
        log.info(sessionKey);
        byte[] data = Base64Utils.decode(encryptedData.getBytes("UTF-8"));
        byte[] ivb = Base64Utils.decode(iv.getBytes("UTF-8"));
        byte[] keyb = Base64Utils.decode(sessionKey.getBytes("UTF-8"));
        int base = 16;
        if (keyb.length % base != 0) {
            int groups = keyb.length / base + (keyb.length % base != 0 ? 1 : 0);
            byte[] temp = new byte[groups * base];
            Arrays.fill(temp, (byte) 0);
            System.arraycopy(keyb, 0, temp, 0, keyb.length);
            keyb = temp;
        }
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        AlgorithmParameters params = AlgorithmParameters.getInstance("AES");
        params.init(new IvParameterSpec(ivb));
        Key key = new SecretKeySpec(keyb, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
        parameters.init(new IvParameterSpec(ivb));
        cipher.init(Cipher.DECRYPT_MODE, key, parameters);// 初始化
        byte[] resultByte = cipher.doFinal(data);
        return new String(resultByte, "UTF-8");
    }

    private String cacheUser(PassengerEntity client) throws Exception {
        String token = JWTUtils.compact(String.valueOf(client.getId()), UserType.CLIENT, jwtProperties.getSecret());
        String userJson = JsonUtils.writeValue(client);
        redisService.setEx(RedisKeyEnum.UserToken, UserType.CLIENT.name() + String.valueOf(client.getId()), userJson);
        return token;
    }

    @Transactional
    public void share(ShareMessageDto shareMessageDto) throws Exception {
        String orderNumber = shareMessageDto.getOrderNumber();
        BigDecimal amount = shareMessageDto.getAmount();
        DriverTripOrderEntity driverTripOrderEntity = driverTripOrderService.find(orderNumber);
        if (driverTripOrderEntity == null) {
            return;
        }
        amount = amount.subtract(driverTripOrderEntity.getCommission());
        Long parentId = driverTripOrderEntity.getPassenger().getParentId();
        try {
            if (parentId == null) {
                return;
            }
            DriverDistributionRatioEntity driverDistributionRatioEntity = driverDistributionRatioService.find();
            if (driverDistributionRatioEntity == null) {
                return;
            }
            BigDecimal oneLevelRatio = driverDistributionRatioEntity.getOneLevelRatio();
            if (oneLevelRatio == null) {
                return;
            }
            long count = driverDistributionRatioService.count(orderNumber, parentId);
            if (count > 0) {
                return;
            }
            BigDecimal oneLevel = amount.multiply(oneLevelRatio);
            amount = amount.subtract(oneLevel);
            driverDistributionRatioService.distribution(orderNumber, parentId, oneLevel);
            scoreService.income(parentId, oneLevel);
            passengerDao.retail(parentId, oneLevel);
            if (EnableEnum.NO == driverDistributionRatioEntity.getTwoLevelState()) {
                return;
            }
            PassengerEntity passengerEntity = passengerDao.findById(parentId).orElse(null);
            if (passengerEntity == null) {
                return;
            }
            parentId = passengerEntity.getParentId();
            if (parentId == null) {
                return;
            }
            BigDecimal twoLevelRatio = driverDistributionRatioEntity.getTwoLevelRatio();
            if (twoLevelRatio == null) {
                return;
            }
            BigDecimal twoLevel = amount.multiply(twoLevelRatio);
            amount = amount.subtract(twoLevel);
            driverDistributionRatioService.distribution(orderNumber, parentId, twoLevel);
            scoreService.income(parentId, twoLevel);
            passengerDao.retail(parentId, twoLevel);
        } finally {
            DealerShareMessageDao dealerShareMessageDao = new DealerShareMessageDao();
            dealerShareMessageDao.setAmount(amount);
            dealerShareMessageDao.setOrderNumber(shareMessageDto.getOrderNumber());
            kafkaProducer.send(dealerShareMessageDao);
        }
    }

    public void exchange(Long userId, BigDecimal amount) throws Exception {
        PassengerEntity passengerEntity = info(userId);
        Assert.isTrue(passengerEntity.getRetail().compareTo(amount) == 1, "积分不足");
        passengerDao.retail(passengerEntity.getId(), new BigDecimal(0).subtract(amount));
    }

    public Page<PassengerEntity> second(PageParam param) throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Pageable pageable = PageRequest.of(param.getPageNum() - 1, param.getPageSize());
        return passengerDao.querySecond(userEntity.getId(), pageable);
    }

    public List<PassengerEntity> subordinate() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        return passengerDao.queryAllByParentId(userEntity.getId());
    }

    public String avatar(MultipartFile file) throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.notNull(userEntity, "账号不存在");
        PassengerEntity user = info(userEntity.getId());
        Assert.notNull(user, "账号不存在");
        String path = fileProperties.path(file.getOriginalFilename());
        user.setAvatar(path);
        File toFile = new File(fileProperties.getPath() + path);
        if (!toFile.exists()) {
            toFile.getParentFile().mkdirs();
            toFile.createNewFile();
        }
        file.transferTo(toFile);
        passengerDao.save(user);
        return path;
    }
}
