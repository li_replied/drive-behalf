package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
@Getter
@Setter
public class Routes implements Serializable {
    private List<Route> routes;
}
