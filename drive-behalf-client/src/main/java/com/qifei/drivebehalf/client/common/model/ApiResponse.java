package com.qifei.drivebehalf.client.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "请求返回结果")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponse<T> implements Serializable {

    @ApiModelProperty(value = "执行成功或失败  200:执行成功 500:执行失败 401:登录已失效 403：您没有访问的权限")
    public int status;
    @ApiModelProperty(value = "执行成功或失败的原因")
    public String msg;
    @JsonInclude()
    @ApiModelProperty(value = "执行成功的返回体")
    public T data;

    public ApiResponse() {

    }

    public ApiResponse(int status) {
        this.status = status;
    }

    public ApiResponse(int status, T data) {
        this.status = status;
        this.data = data;
    }

    public ApiResponse(int status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public ApiResponse(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public boolean isSuccess() {
        return this.status == ResponseCode.SUCCESS.getCode();
    }

    public int getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }


    public static <T> ApiResponse<T> createBySuccess() {
        return new ApiResponse<T>(ResponseCode.SUCCESS.getCode());
    }

    public static <T> ApiResponse<T> createBySuccessMessage(String msg) {
        return new ApiResponse<T>(ResponseCode.SUCCESS.getCode(), msg);
    }

    public static <T> ApiResponse<T> createBySuccess(T data) {
        return new ApiResponse<T>(ResponseCode.SUCCESS.getCode(), data);
    }

    public static <T> ApiResponse<T> createBySuccess(String msg, T data) {
        return new ApiResponse<T>(ResponseCode.SUCCESS.getCode(), msg, data);
    }


    public static <T> ApiResponse<T> createByError() {
        return new ApiResponse<T>(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getDesc());
    }

    public static <T> ApiResponse<T> createByErrorMessage(String errorMessage) {
        return new ApiResponse<T>(ResponseCode.ERROR.getCode(), errorMessage);
    }

    public static <T> ApiResponse<T> createByErrorCodeMessage(int errorCode, String errorMessage) {
        return new ApiResponse<T>(errorCode, errorMessage);
    }

    public static <T> ApiResponse<T> createByResponseCode(ResponseCode responseCode) {
        return new ApiResponse<T>(responseCode.getCode(), responseCode.getDesc());
    }

    public static <T> ApiResponse<T> createByResponseCode(ResponseCode responseCode, T data) {
        return new ApiResponse<T>(responseCode.getCode(), responseCode.getDesc(), data);
    }

    @Override
    public String toString() {
        return "ServerResponse [status=" + status + ", msg=" + msg + ", data=" + data + "]";
    }

}
