package com.qifei.drivebehalf.client.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@MappedSuperclass
public class BaseEntity implements Serializable {
    /**
     * 创建人
     */
    @Column(name = "created_user", length = 45, nullable = false)
    protected Long createdUser;
    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date createTime;
    /**
     * 修改人
     */
    @Column(name = "modified_user", length = 45)
    protected Long modifiedUser;
    /**
     * 修改时间
     */
    @Column(name = "modified_time")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date modifiedTime;

    public boolean setCreate() {
        UserEntity user = UserLoginUtils.getLogin();
        if (user == null) {
            setCreatedUser(0L);
        } else {
            setCreatedUser(user.getId());
        }
        setCreateTime(new Date());
        return true;
    }

    public boolean setModified() {
        UserEntity user = UserLoginUtils.getLogin();
        if (user == null) {
            setModifiedUser(0L);
        } else {
            setModifiedUser(user.getId());
        }
        setModifiedTime(new Date());
        return true;
    }

}
