package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.enums.UserType;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.dao.DriverTripOrderDao;
import com.qifei.drivebehalf.client.dao.SubscribeDao;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.entity.SubscribeEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SubscribeService {
    private final SubscribeDao subscribeDao;
    private final DriverTripOrderDao driverTripOrderDao;

    @Resource
    private RedisService redisService;

    public List<SubscribeEntity> list() throws Exception {
        return subscribeDao.queryAllByStateOrderByAppointmentTimeDesc(EnableEnum.NO);
    }

    public List<SubscribeEntity> my() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
        return subscribeDao.queryAllByDriverAndStateOrderByAppointmentTimeDesc((DriverEntity) userEntity, EnableEnum.YES);
    }

    public void contest(Long id) throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
        if (redisService.hasKey(RedisKeyEnum.OrderLock, String.valueOf(id))) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, String.valueOf(id));
            SubscribeEntity subscribeEntity = subscribeDao.findById(id).orElse(null);
            Assert.notNull(subscribeEntity, "订单不存在");
            Assert.isTrue(EnableEnum.NO == subscribeEntity.getState(), "订单已被抢");
            List<DriverTripOrderEntity> list = driverTripOrderDao.findByDriverAndOrderStatusIn((DriverEntity) userEntity, new OrderStateEnum[]{
                    OrderStateEnum.ACCEPTED, OrderStateEnum.CONFIRMDRIVE, OrderStateEnum.DRIVE, OrderStateEnum.PAYMENT
            });
            Assert.isTrue(list.size() == 0, "当前已有待处理订单，不允许重复抢单。");
            subscribeEntity.setDriver((DriverEntity) userEntity);
            subscribeEntity.setState(EnableEnum.YES);
            subscribeEntity.setModified();
            subscribeDao.save(subscribeEntity);
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, String.valueOf(id));
        }
    }
}
