package com.qifei.drivebehalf.client.controller.score.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@ApiModel(description = "积分结果")
public class ScoreResult implements Serializable {
    @ApiModelProperty("时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date createTime;
    @ApiModelProperty("金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal amount = new BigDecimal(0);
    @ApiModelProperty("审批状态  YES 通过 NO 申请中")
    private EnableEnum state;
    @ApiModelProperty("收支  YES 收入 NO 支出")
    private EnableEnum income;
}
