package com.qifei.drivebehalf.client.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(
        prefix = "jwt",
        ignoreUnknownFields = true
)
public class JWTProperties {
    /**
     * 秘钥
     */
    private String secret;
    /**
     * header
     */
    private String header = "TOKEN";

}
