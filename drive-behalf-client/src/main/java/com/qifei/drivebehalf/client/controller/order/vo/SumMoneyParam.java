package com.qifei.drivebehalf.client.controller.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "计算价格")
public class SumMoneyParam implements Serializable {
    @ApiModelProperty("起点")
    @NotEmpty(message = "起点地址不能为空")
    private String startPoint;
    @ApiModelProperty("终点")
    @NotEmpty(message = "终点地址不能为空")
    private String endPoint;
    @ApiModelProperty("订单时间")
    private String datetime;
    @NotEmpty(message = "开始纬度不能为空")
    @ApiModelProperty("开始纬度")
    private String startLat;
    @NotEmpty(message = "开始经度不能为空")
    @ApiModelProperty("开始经度")
    private String startLng;
    @NotEmpty(message = "结束纬度不能为空")
    @ApiModelProperty("结束纬度")
    private String endLat;
    @NotEmpty(message = "结束经度不能为空")
    @ApiModelProperty("结束经度")
    private String endLng;
    @ApiModelProperty("订单号，用于中转")
    private String orderNumber; // 来源于乘客下单 order/place
}
