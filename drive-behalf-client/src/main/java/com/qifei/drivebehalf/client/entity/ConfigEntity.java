package com.qifei.drivebehalf.client.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "sys_config")
public class ConfigEntity implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "config_id", unique = true, nullable = false)
    private Long id;

    @Column(name = "config_code")
    private String configCode;

    @Column(name = "remark", length = 2000)
    private String remark;
}
