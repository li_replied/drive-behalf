package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(
        name = "driver_subscribe"
)
public class SubscribeEntity extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    /**
     * 司机ID
     */
    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "driver_id")
    private DriverEntity driver;

    /**
     * 行程起点
     */
    @Column(name = "start_point", nullable = false)
    private String startPoint;

    /**
     * 预约时间
     */
    @Column(name = "appointment_time", nullable = false)
    private Date appointmentTime;
    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private EnableEnum state;
}
