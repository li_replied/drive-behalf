package com.qifei.drivebehalf.client.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.service.dto.KafkaMessage;
import com.qifei.drivebehalf.client.service.dto.PayBackMessage;
import com.qifei.drivebehalf.client.service.dto.ShareMessageDto;
import com.qifei.drivebehalf.client.service.dto.WXCallback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

@Service
@Slf4j
public class KafkaProducer {
    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * 支付回调
     *
     * @param wxCallback
     */
    public void payCallBack(WXCallback wxCallback) {
        PayBackMessage payBackMessage = new PayBackMessage();
        payBackMessage.setCallback(wxCallback);
        send(payBackMessage);
    }

    /**
     * 分成
     *
     * @param orderNumber
     * @param amount
     */
    public void share(String orderNumber, BigDecimal amount) {
        ShareMessageDto shareMessageDto = new ShareMessageDto();
        shareMessageDto.setOrderNumber(orderNumber);
        shareMessageDto.setAmount(amount);
        send(shareMessageDto);
    }

    /**
     * 发送消息
     *
     * @param kafkaMessage
     */
    public void send(KafkaMessage kafkaMessage) {
        try {
            String message = JsonUtils.writeValue(kafkaMessage);
            kafkaTemplate.send(kafkaMessage.getTopic(), message);
        } catch (JsonProcessingException e) {
            log.error("Kafka 消息发送错误", e);
        }
    }
}
