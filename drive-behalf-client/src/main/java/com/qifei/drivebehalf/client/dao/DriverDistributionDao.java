package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverDistributionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverDistributionDao extends JpaRepository<DriverDistributionEntity, Long>, JpaSpecificationExecutor<DriverDistributionEntity> {
    long countByOrderNoAndUserId(String orderNo,Long id);
}
