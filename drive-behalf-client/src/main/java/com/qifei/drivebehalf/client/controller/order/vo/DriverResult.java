package com.qifei.drivebehalf.client.controller.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@ApiModel(description = "司机个人信息")
public class DriverResult implements Serializable {
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("生日")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    @ApiModelProperty("性别")
    private SexEnum sex = SexEnum.M;
    @ApiModelProperty("点赞")
    private Integer likes;
    @ApiModelProperty("经度")
    private Double lastLongitude;
    @ApiModelProperty("纬度")
    private Double lastLatitude;
}
