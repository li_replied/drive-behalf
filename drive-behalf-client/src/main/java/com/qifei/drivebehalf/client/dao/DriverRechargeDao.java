package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.PayState;
import com.qifei.drivebehalf.client.entity.DriverRechargeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface DriverRechargeDao extends JpaRepository<DriverRechargeEntity, Long>, JpaSpecificationExecutor<DriverRechargeEntity> {
    Page<DriverRechargeEntity> queryAllByPassengerIdAndPayStateOrderByCreateTimeDesc(Long id, PayState payState,Pageable pageable);

    DriverRechargeEntity queryDriverRechargeEntityByOrderNumber(String orderNumber);

    @Query("update DriverRechargeEntity set payId=:payId , transactionId=:transactionId where orderNumber=:orderNumber")
    @Modifying
    void updatePayId(@Param("orderNumber") String orderNumber, @Param("transactionId") String transactionId, @Param("payId") String payId);

    @Query(" select  sum (amount+giftAmount) from DriverRechargeEntity  where passengerId=:passengerId and income=:income and payState=:payState")
    BigDecimal total(@Param("passengerId") Long passengerId, @Param("income") Boolean income, @Param("payState") PayState payState);
}
