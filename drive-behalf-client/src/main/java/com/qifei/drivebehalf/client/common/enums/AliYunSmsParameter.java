package com.qifei.drivebehalf.client.common.enums;

/**
 * 阿里云短信参数
 */
public enum AliYunSmsParameter {
    /**
     * 接收短信的手机号码。
     */
    PhoneNumbers,
    /**
     * 外部流水扩展字段。
     */
    OutId,
    /**
     * 短信签名名称。请在控制台签名管理页面签名名称一列查看。
     */
    SignName,
    /**
     * 短信模板ID。请在控制台模板管理页面模板CODE一列查看。
     */
    TemplateCode,

    /**
     * 短信模板变量对应的实际值，JSON格式。
     */
    TemplateParam,
    /**
     * 上行短信扩展码，无特殊需要此字段的用户请忽略此字段。
     */
    SmsUpExtendCode,
    /**
     * 版本
     */
    Version;
}
