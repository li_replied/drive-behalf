package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(
        name = "driver_online",
        indexes = {
                @Index(name = "idx_driver_online_driver_id", columnList = "driver_id"),
        }
)
public class DriverOnlineEntity extends BaseEntity {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "online_id", unique = true, nullable = false)
    protected Long id;

    @Column(name = "driver_id")
    private Long driverId;

    @Column(name = "day")
    private Date day;

    @Column(name = "begin")
    private Date begin;

    @Column(name = "end")
    private Date end;

}
