package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(
        name = "driver_sms_log",
        indexes = {
                @Index(name = "idx_driver_sms_log_created_user", columnList = "created_user"),
        }
)
public class DriverSMSLogEntity extends BaseEntity implements Serializable {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sms_log_id", unique = true, nullable = false)
    protected Long id;

    @Column(name = "phone")
    private String phone;
    @Column(name = "message")
    private String message;
    @Column(name = "param", length = 2000)
    private String param;
    @Column(name = "result", length = 2000)
    private String result;
    @Column(name = "consume")
    @Enumerated(EnumType.STRING)
    private EnableEnum consume = EnableEnum.NO;
}
