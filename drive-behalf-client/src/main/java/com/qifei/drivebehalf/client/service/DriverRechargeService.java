package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.PayState;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.OrderNumberUtil;
import com.qifei.drivebehalf.client.controller.recharge.vo.TotalResult;
import com.qifei.drivebehalf.client.dao.DriverRechargeConfigDao;
import com.qifei.drivebehalf.client.dao.DriverRechargeDao;
import com.qifei.drivebehalf.client.entity.DriverRechargeConfig;
import com.qifei.drivebehalf.client.entity.DriverRechargeEntity;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.dto.PayCallBackDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DriverRechargeService {
    private final DriverRechargeDao driverRechargeDao;
    private final DriverRechargeConfigDao driverRechargeConfigDao;
    private final PassengerService passengerService;
    @Resource
    private RedisAtomicIntegerService redisAtomicIntegerService;

    public List<DriverRechargeConfig> ratio() throws Exception {
        return driverRechargeConfigDao.queryAllByStateOrderByRechargeAmount(EnableEnum.YES);
    }

    public DriverRechargeConfig find(Long id) throws Exception {
        return driverRechargeConfigDao.findById(id).orElse(null);
    }

    public DriverRechargeConfig find(BigDecimal amount) throws Exception {
        amount = amount.divide(new BigDecimal(100));
        return driverRechargeConfigDao.queryFirstByRechargeAmountAndState(amount, EnableEnum.YES);
    }

    @Transactional
    public void updatePayId(String orderNumber, String transactionId, String payId) throws Exception {
        driverRechargeDao.updatePayId(orderNumber, transactionId, payId);
    }

    public DriverRechargeEntity order(String orderNumber) throws Exception {
        return driverRechargeDao.queryDriverRechargeEntityByOrderNumber(orderNumber);
    }

    public TotalResult total() throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        PassengerEntity passengerEntity = passengerService.info(user.getId());
        BigDecimal recharge = driverRechargeDao.total(user.getId(), true, PayState.SUCCESS);
        BigDecimal consume = driverRechargeDao.total(user.getId(), false, PayState.SUCCESS);
        TotalResult totalResult = new TotalResult();
        totalResult.setTotal(passengerEntity.getMoney());
        totalResult.setRecharge(recharge);
        totalResult.setConsume(consume);
        return totalResult;
    }

    @Transactional
    public String place(Long configId) throws Exception {
        PassengerEntity userEntity = (PassengerEntity) UserLoginUtils.getLogin();
        userEntity = passengerService.info(userEntity.getId());
        Assert.notNull(userEntity, "用户不存在。");
        DriverRechargeConfig driverRechargeConfig = find(configId);
        Assert.notNull(driverRechargeConfig, "充值规则不存在");
        String orderNo = OrderNumberUtil.orderNumber(userEntity.getPhone(), redisAtomicIntegerService.id());
        DriverRechargeEntity driverRechargeEntity = new DriverRechargeEntity();
        driverRechargeEntity.setOrderNumber(orderNo);
        driverRechargeEntity.setIncome(true);
        driverRechargeEntity.setPassengerId(userEntity.getId());
        driverRechargeEntity.setAmount(driverRechargeConfig.getRechargeAmount());
        driverRechargeEntity.setGiftAmount(driverRechargeConfig.getGiftAmount());
        driverRechargeEntity.setConfigId(driverRechargeEntity.getConfigId());
        driverRechargeEntity.setPayState(PayState.PAYING);
        driverRechargeEntity.setCreate();
        driverRechargeDao.save(driverRechargeEntity);
        return orderNo;
    }

    public Page<DriverRechargeEntity> query(PageParam param) throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        Pageable pageable = PageRequest.of(param.getPageNum() - 1, param.getPageSize());
        return driverRechargeDao.queryAllByPassengerIdAndPayStateOrderByCreateTimeDesc(user.getId(), PayState.SUCCESS, pageable);
    }


    @Transactional
    public void payCallBack(PayCallBackDto payCallBackDto) throws Exception {
        DriverRechargeEntity driverRechargeEntity = order(payCallBackDto.getOrderNumber());
        if (driverRechargeEntity == null) {
            return;
        }
        if (driverRechargeEntity.getPayState() != PayState.PAYING) {
            return;
        }
        driverRechargeEntity.setSuccessTime(payCallBackDto.getSuccessTime());
        driverRechargeEntity.setPayState(PayState.SUCCESS);
        driverRechargeEntity.setModified();
        driverRechargeDao.save(driverRechargeEntity);
        passengerService.recharge(payCallBackDto.getPassengerId(), driverRechargeEntity.getAmount().add(driverRechargeEntity.getGiftAmount()));
    }

    public void deduction(Long id, String orderNumber, BigDecimal amount) throws Exception {
        DriverRechargeEntity driverRechargeEntity = new DriverRechargeEntity();
        driverRechargeEntity.setConfigId(id);
        driverRechargeEntity.setOrderNumber(orderNumber);
        driverRechargeEntity.setGiftAmount(new BigDecimal(0));
        driverRechargeEntity.setAmount(amount);
        driverRechargeEntity.setPassengerId(id);
        driverRechargeEntity.setSuccessTime(new Date());
        driverRechargeEntity.setTransactionId(orderNumber);
        driverRechargeEntity.setPayState(PayState.SUCCESS);
        driverRechargeEntity.setIncome(false);
        driverRechargeEntity.setCreate();
        driverRechargeDao.save(driverRechargeEntity);
        passengerService.recharge(id, new BigDecimal(0).subtract(amount));
    }
}
