package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "driver_invoice",
        indexes = {
                @Index(name = "idx_driver_invoice_user_id", columnList = "user_id"),
                @Index(name = "idx_driver_invoice_order_no", columnList = "order_no", unique = true)
        })
public class DriverInvoiceEntity extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "invoice_id")
    private Long id;
    /**
     * 用户
     */
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "title_id", nullable = false)
    private Long titleId;
    /**
     * 订单编号
     */
    @Column(name = "order_no", nullable = false)
    private String orderNo;
    /**
     * 状态
     */
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private EnableEnum state = EnableEnum.NO;
}
