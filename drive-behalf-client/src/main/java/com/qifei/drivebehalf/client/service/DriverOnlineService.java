package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.dao.DriverDao;
import com.qifei.drivebehalf.client.dao.DriverOnlineDao;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.DriverOnlineEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.dto.Online;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DriverOnlineService {
    private final DriverOnlineDao driverOnlineDao;
    private final RedisService redisService;
    private final DriverDao driverDao;

    public long duration() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Long userid = userEntity.getId();
        Date date = DateUtil.beginOfDay(new Date());
        List<DriverOnlineEntity> list = driverOnlineDao.queryAllByDayAndDriverId(date, userid);
        long duration = 0;
        for (DriverOnlineEntity driverOnlineEntity : list) {
            Date begin = driverOnlineEntity.getBegin();
            Date end = driverOnlineEntity.getEnd();
            if (end == null) {
                end = new Date();
            }
            duration += DateUtil.between(begin, end, DateUnit.MINUTE);
        }
        return duration;
    }

    @Transactional(rollbackOn = Exception.class)
    public void online() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        DriverEntity driverEntity = driverDao.findById(userEntity.getId()).orElse(null);
        Assert.notNull(driverEntity, "账户未找到。");
        driverDao.updateOnline(EnableEnum.YES, userEntity.getId());
        String onlineStr = redisService.hGet(RedisKeyEnum.DriverOnLine, String.valueOf(userEntity.getId()));
        Online online = null;
        if (StringUtils.isNotEmpty(onlineStr)) {
            online = JsonUtils.readValue(onlineStr, Online.class);
        }
        Date begin = new Date();
        Date date = DateUtil.beginOfDay(begin);
        if (online != null) {
            if (date.getTime() > online.getDate().getTime()) {
                online.setDate(date);
                DriverOnlineEntity driverOnlineEntity = driverOnlineDao.findById(online.getOnlineId()).orElse(null);
                if (driverOnlineEntity != null) {
                    driverOnlineEntity.setModified();
                    driverOnlineEntity.setEnd(date);
                    driverOnlineDao.save(driverOnlineEntity);
                }
            }
        } else {
            online = new Online();
        }
        Long onlineId = online(userEntity.getId(), date, begin);
        online.setDate(date);
        online.setDriverId(userEntity.getId());
        online.setOnlineId(onlineId);
        redisService.hSet(RedisKeyEnum.DriverOnLine, String.valueOf(userEntity.getId()), JsonUtils.writeValue(online));
    }

    @Transactional(rollbackOn = Exception.class)
    public void offline() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Long userid = userEntity.getId();
        driverDao.updateOnline(EnableEnum.NO, userid);
        String onlineStr = redisService.hGet(RedisKeyEnum.DriverOnLine, String.valueOf(userid));
        if (StringUtils.isEmpty(onlineStr)) {
            return;
        }
        Online online = JsonUtils.readValue(onlineStr, Online.class);
        DriverOnlineEntity driverOnlineEntity = driverOnlineDao.findById(online.getOnlineId()).orElse(null);
        if (driverOnlineEntity == null) {
            return;
        }
        driverOnlineEntity.setEnd(new Date());
        driverOnlineEntity.setModified();
        driverOnlineDao.save(driverOnlineEntity);
    }

    private Long online(Long driverId, Date day, Date begin) throws Exception {
        DriverOnlineEntity driverOnlineEntity = new DriverOnlineEntity();
        driverOnlineEntity.setDriverId(driverId);
        driverOnlineEntity.setBegin(begin);
        driverOnlineEntity.setDay(day);
        driverOnlineEntity.setCreate();
        driverOnlineDao.save(driverOnlineEntity);
        return driverOnlineEntity.getId();
    }
}
