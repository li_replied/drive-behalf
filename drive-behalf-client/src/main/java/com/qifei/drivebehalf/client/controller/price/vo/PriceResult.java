package com.qifei.drivebehalf.client.controller.price.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;

@Setter
@Getter
@ApiModel(description = "价格表")
public class PriceResult implements Serializable {

    @ApiModelProperty("起步公里数")
    private int startingKilometre;

    /**
     * 起步价格(3km以内)
     */
    @ApiModelProperty("起步价格")
    private BigDecimal startingPrice;

    /**
     * 里程费(超过起步里程后按X元每Y公里计算，不足Y公里的部分按Y公里计算)
     */
    @ApiModelProperty("里程费")
    private BigDecimal mileageFee;

    /**
     * 等待时间(X分钟)
     */
    @ApiModelProperty("等待时间")
    private int waitingTime;

    /**
     * 等待费(每等待1分钟加收X元，不足1分钟的部分按1分钟计算)
     */
    @ApiModelProperty("等待费")
    private BigDecimal waitingFee;

    @ApiModelProperty("客服热线")
    private String customerPhone;


    /**
     * 开始时间段
     */
    @ApiModelProperty("开始时间段")
    @JsonFormat(timezone = "GMT+8", pattern = "HH:mm")
    private LocalTime startTime;
    /**
     * 结束时间段
     */
    @ApiModelProperty("结束时间段")
    @JsonFormat(timezone = "GMT+8", pattern = "HH:mm")
    private LocalTime endTime;
}
