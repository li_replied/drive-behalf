package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WXTransactionsResult implements Serializable {
    /**
     * "prepay_id": "wx201410272009395522657a690389285100"
     */
    @JsonProperty("prepay_id")
    private String prepayId;

    @JsonProperty("h5_url")
    private String h5Url;

}
