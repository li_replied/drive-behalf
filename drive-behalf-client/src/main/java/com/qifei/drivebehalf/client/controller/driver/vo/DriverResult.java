package com.qifei.drivebehalf.client.controller.driver.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import com.qifei.drivebehalf.client.controller.order.vo.OrderResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@ApiModel(description = "司机个人信息")
public class DriverResult implements Serializable {
    @ApiModelProperty("id")
    protected Long id;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("生日")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    @ApiModelProperty("性别")
    private SexEnum sex = SexEnum.M;
    @ApiModelProperty("点赞")
    private Integer likes;
    @ApiModelProperty("账户余额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal money = new BigDecimal(0);
    @ApiModelProperty("今日收入")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal todayMoney = new BigDecimal(0);
    @ApiModelProperty("今日订单")
    private Integer todayOrders = 0;
    @ApiModelProperty("接单时限")
    private Integer acceptingTimeout = 90;
    @ApiModelProperty("待处理的订单")
    private OrderResult assignedOrder;
    @ApiModelProperty("进行中的订单")
    private List<OrderResult> startingOrders;
    @ApiModelProperty("已拒绝的订单")
    private List<OrderResult> cancelOrders;
    @ApiModelProperty("未支付的订单")
    private List<OrderResult> unpaidOrders;
}
