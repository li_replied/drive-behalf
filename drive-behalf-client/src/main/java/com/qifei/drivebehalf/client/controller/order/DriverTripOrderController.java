package com.qifei.drivebehalf.client.controller.order;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.model.PageResult;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.common.utils.DateUtils;
import com.qifei.drivebehalf.client.controller.order.vo.*;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.DriverService;
import com.qifei.drivebehalf.client.service.DriverTripOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Api(tags = "订单管理")
@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
@EnableAsync
public class DriverTripOrderController {

    @Value("${order.accept-timeout}")
    private int acceptTimeout;

    @Value("${spring.profiles.active}")
    private String env;

    private final DriverTripOrderService driverTripOrderService;
    private final DriverService driverService;

    @ValidToken
    @ApiOperation(value = "呼叫代驾", tags = "订单管理：客户端")
    @PostMapping("passenger/driver/call")
    public ApiResponse<CallDriverResult> callDriver(@RequestBody @Validated PassengerParam param) throws Exception {
        PassengerEntity passenger = (PassengerEntity) UserLoginUtils.getLogin();
        // 先找1小时内是否有未处理的订单
        DriverTripOrderEntity order = driverTripOrderService.findTopOrder(passenger.getPhone());
        if (order != null) {
            switch (order.getOrderStatus()) {
                case ASSIGNED:
                case REDISTRIBUTION:
                    return ApiResponse.createByErrorMessage("平台正在为您分配代驾司机，请耐心等待...");
                case ACCEPTED:
                    return ApiResponse.createByErrorMessage("已分配代驾司机，请耐心等待...");
                case CONFIRMDRIVE:
                case DRIVE:
                    return ApiResponse.createByErrorMessage("您有正在进行中的订单，请完成后在继续...");
                default:
                    break;
            }
        }
        List<DriverEntity> drivers = driverService.findNearByOnlineDrivers(param);
        if (drivers.size() > 0) {
            long timeFlag = System.currentTimeMillis();
            driverTripOrderService.callDriver(param, passenger, drivers, timeFlag);
            CallDriverResult callDriverResult = new CallDriverResult();
            callDriverResult.setFlag(timeFlag);
            callDriverResult.setAssignedDriverCount(drivers.size());
            callDriverResult.setWaitingTimeout(acceptTimeout);
            return ApiResponse.createBySuccess(String.format("平台已找到%d名代驾司机，正在为您分配...", drivers.size()), callDriverResult);
        } else {
            return ApiResponse.createByErrorMessage("当前区域暂时没有代驾司机，请过会儿再来...");
        }
    }

    @ValidToken
    @ApiOperation(value = "获取呼叫代驾状态", tags = "订单管理：客户端")
    @GetMapping("passenger/status/check/{timeFlag}")
    public ApiResponse<AcceptedResult> checkOrder(@PathVariable("timeFlag") Long timeFlag) throws Exception {
        PassengerEntity passenger = (PassengerEntity) UserLoginUtils.getLogin();
        // 先找1小时内是否有未处理的订单
        DriverTripOrderEntity order = driverTripOrderService.findTopOrder(passenger.getPhone());
        if (order != null && order.getOrderStatus() == OrderStateEnum.ACCEPTED) {
            AcceptedResult result = BeanCopyUtils.copyProperties(order, AcceptedResult.class);
            result.setWaitingTimeout(acceptTimeout);
            return ApiResponse.createBySuccess("已分配代驾司机，稍后会电话联系您，请耐心等待...", result);
        } else if (driverTripOrderService.isCallDriver(passenger.getPhone())) {
            return ApiResponse.createByErrorCodeMessage(201, "平台正在为您分配代驾司机，请耐心等待...");
        } else {
            return ApiResponse.createByErrorCodeMessage(404, "当前区域暂时没有代驾司机，请过会儿再来...");
        }
    }

    @ValidToken
    @ApiOperation(value = "修改订单状态", tags = "订单管理：司机端")
    @PostMapping("driver/status/change")
    public ApiResponse changeOrderStatus(@RequestBody @Validated ChangeOrderParam param) throws Exception {
        driverTripOrderService.changeOrderStatus(param);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation(value = "结束行程", tags = "订单管理：司机端")
    @PostMapping("driver/trip/upload")
    public ApiResponse<OrderResult> uploadTrip(@RequestBody @Validated TripParam param) throws Exception {
        DriverTripOrderEntity order = driverTripOrderService.uploadTrip(param);
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(order, OrderResult.class));
    }

    @ApiOperation(value = "计算费用", tags = "订单管理：客户端")
    @PostMapping("sum/money")
    public ApiResponse<SumMoneyResult> sumMoney(@RequestBody @Validated SumMoneyParam param) throws Exception {
        SumMoneyResult sumMoneyResult = driverTripOrderService.sumMoney(param);
        return ApiResponse.createBySuccess(sumMoneyResult);
    }

    @ValidToken
    @ApiOperation(value = "确定中转费用", tags = "订单管理：司机端")
    @GetMapping("transfer/{orderNumber}")
    public ApiResponse<Void> transferMoney(
            @PathVariable("orderNumber")
            @NotEmpty(message = "订单编号不能为空。")
                    String orderNumber
    ) throws Exception {
        driverTripOrderService.transferMoney(orderNumber);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation(value = "下订单", tags = "订单管理：客户端")
    @PostMapping("/place")
    public ApiResponse<PlaceOrderResult> placeOrder(@RequestBody @Validated PlaceOrderParam param) throws Exception {
        PlaceOrderResult result = driverTripOrderService.placeOrder(param.getUuid());
        return ApiResponse.createBySuccess(result);
    }

    @ValidToken
    @ApiOperation(value = "我的订单", tags = {"订单管理：客户端", "订单管理：司机端"})
    @PostMapping("/list")
    public ApiResponse<PageResult<OrderResult>> orderList(@RequestBody @Validated PageParam param) throws Exception {
        Page<DriverTripOrderEntity> page = driverTripOrderService.list(param);
        if (page == null) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(PageResult.create(page, OrderResult.class));
    }

    @ValidToken
    @ApiOperation(value = "订单详情", tags = {"订单管理：客户端", "订单管理：司机端"})
    @GetMapping("/info/{orderNumber}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderNumber", type = "path", required = true, value = "订单号")
    })
    public ApiResponse<OrderResult> info(
            @Validated
            @NotEmpty(message = "订单编号不能为空")
            @PathVariable("orderNumber")
                    String orderNumber
    ) throws Exception {
        DriverTripOrderEntity driverTripOrderEntity = driverTripOrderService.info(orderNumber);
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(driverTripOrderEntity, OrderResult.class));
    }


    @ValidToken
    @ApiOperation(value = "取消订单", tags = "订单管理：客户端")
    @PostMapping("/passenger/cancel")
    public ApiResponse<Void> cancel(
            @RequestBody
            @Validated
                    CancelParam param
    ) throws Exception {
        driverTripOrderService.cancel(param);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation(value = "查询待确认订单/未分配订单/进行中订单", tags = "订单管理：客户端")
    @GetMapping("/query/confirm")
    public ApiResponse<OrderResult> queryConfirm() throws Exception {
        DriverTripOrderEntity driverTripOrderEntity = driverTripOrderService.queryConfirm();
        if (driverTripOrderEntity == null) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(driverTripOrderEntity, OrderResult.class));
    }

    @ValidToken
    @ApiOperation(value = "查询待确认订单/未分配订单/进行中订单 数量", tags = "订单管理：客户端")
    @GetMapping("/count/confirm")
    public ApiResponse<Long> countConfirm() throws Exception {
        return ApiResponse.createBySuccess(driverTripOrderService.countConfirm());
    }

    @ValidToken
    @ApiOperation(value = "确认订单", tags = "订单管理：客户端")
    @PostMapping("/confirm/{orderNumber}")
    public ApiResponse<Void> confirm(
            @Validated
            @NotEmpty(message = "订单编号不能为空")
            @PathVariable("orderNumber")
                    String orderNumber
    ) throws Exception {
        driverTripOrderService.confirm(orderNumber);
        return ApiResponse.createBySuccess();
    }


    @ApiOperation(value = "查询待分配订单", tags = "订单管理：司机端")
    @GetMapping("/assigned/{lat}/{lng}")
    public ApiResponse<List<OrderResult>> assigned(
            @NotEmpty(message = "经度不能为空")
            @PathVariable("lat")
                    String lat,
            @NotEmpty(message = "维度不能为空")
            @PathVariable("lng")
                    String lng
    ) throws Exception {
        List<DriverTripOrderEntity> list = driverTripOrderService.assigned(lat, lng);
        if (list == null || list.isEmpty()) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, OrderResult.class));
    }

    @ValidToken
    @ApiOperation(value = "抢单", tags = "订单管理：司机端")
    @PostMapping("/contest/{orderNumber}")
    public ApiResponse<Void> contest(
            @PathVariable("orderNumber")
            @NotEmpty(message = "订单编号不能为空。")
                    String orderNumber
    ) throws Exception {
        driverTripOrderService.contest(orderNumber);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation(value = "开始代驾，等待用户确认上车", tags = "订单管理：司机端")
    @PostMapping("/drive/{orderNumber}")
    public ApiResponse<Void> drive(
            @PathVariable("orderNumber")
            @NotEmpty(message = "订单编号不能为空。")
                    String orderNumber
    ) throws Exception {
        driverTripOrderService.drive(orderNumber);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation(value = "已经送达，等待用户确认并付款", tags = "订单管理：司机端")
    @PostMapping("/delivered/{orderNumber}")
    public ApiResponse<Void> delivered(
            @PathVariable("orderNumber")
            @NotEmpty(message = "订单编号不能为空。")
                    String orderNumber
    ) throws Exception {
        driverTripOrderService.delivered(orderNumber);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation(value = "查询进行中订单", tags = "订单管理：司机端")
    @GetMapping("/carryon")
    public ApiResponse<List<OrderResult>> carryon() throws Exception {
        List<DriverTripOrderEntity> list = driverTripOrderService.carryon();
        if (list == null || list.isEmpty()) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, OrderResult.class));
    }

    @ValidToken
    @ApiOperation(value = "查询进行中订单数量", tags = "订单管理：司机端")
    @GetMapping("/count/carryon")
    public ApiResponse<Long> countCarryon() throws Exception {
        long count = driverTripOrderService.countCarryon();
        return ApiResponse.createBySuccess(count);
    }

    @ValidToken
    @ApiOperation(value = "完成订单", tags = "订单管理：司机端")
    @GetMapping("/complete/{orderNumber}")
    public ApiResponse<Void> complete(
            @PathVariable("orderNumber")
            @NotEmpty(message = "订单编号不能为空。")
                    String orderNumber
    ) throws Exception {
        driverTripOrderService.complete(orderNumber);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation(value = "查询订单状态", tags = "订单管理：客户端")
    @GetMapping("/state/{orderNumber}")
    public ApiResponse<Boolean> payState(
            @PathVariable("orderNumber")
            @NotEmpty(message = "订单编号不能为空。")
                    String orderNumber
    ) throws Exception {
        DriverTripOrderEntity driverTripOrderEntity = driverTripOrderService.info(orderNumber);
        boolean result = OrderStateEnum.COMPLETE == driverTripOrderEntity.getOrderStatus();
        return ApiResponse.createBySuccess(result);
    }
}
