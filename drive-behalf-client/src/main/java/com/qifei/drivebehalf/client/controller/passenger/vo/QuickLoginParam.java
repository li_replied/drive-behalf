package com.qifei.drivebehalf.client.controller.passenger.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "用户快捷登录参数")
public class QuickLoginParam implements Serializable {
    @ApiModelProperty("iv")
    @NotEmpty(message = "iv不能为空")
    private String iv;
    @ApiModelProperty("密文")
    @NotEmpty(message = "密文不能为空")
    private String encryptedData;
    @NotEmpty(message = "微信公众号/小程序登录code不能我空")
    @ApiModelProperty("微信公众号/小程序登录code")
    private String jsCode;
    @ApiModelProperty("微信头像")
    private String avatar;
    @ApiModelProperty("微信昵称")
    private String nickName;
}
