package com.qifei.drivebehalf.client.controller.recharge;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.enums.PayState;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.model.PageResult;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.recharge.vo.RechargeRatioResult;
import com.qifei.drivebehalf.client.controller.recharge.vo.RechargeResult;
import com.qifei.drivebehalf.client.controller.recharge.vo.TotalResult;
import com.qifei.drivebehalf.client.entity.DriverRechargeConfig;
import com.qifei.drivebehalf.client.entity.DriverRechargeEntity;
import com.qifei.drivebehalf.client.service.DriverRechargeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "客户端：充值记录")
@RequestMapping("/recharge")
@RequiredArgsConstructor
public class RechargeController {
    private final DriverRechargeService driverRechargeService;

    @ValidToken
    @ApiOperation("下单")
    @PostMapping("/place/{configId}")
    public ApiResponse<String> place(@PathVariable("configId") Long configId) throws Exception {
        String order = driverRechargeService.place(configId);
        return ApiResponse.createBySuccess(order);
    }

    @ValidToken
    @ApiOperation("充值规则")
    @GetMapping("/ratio")
    public ApiResponse<List<RechargeRatioResult>> ratio() throws Exception {
        List<DriverRechargeConfig> list = driverRechargeService.ratio();
        List<RechargeRatioResult> results = BeanCopyUtils.copyProperties(list, RechargeRatioResult.class);
        return ApiResponse.createBySuccess(results);
    }

    @ValidToken
    @ApiOperation("充值统计")
    @GetMapping("/total")
    public ApiResponse<TotalResult> total() throws Exception {
        TotalResult result = driverRechargeService.total();
        return ApiResponse.createBySuccess(result);
    }

    @ValidToken
    @ApiOperation("充值记录")
    @GetMapping("/query")
    public ApiResponse<PageResult<RechargeResult>> query(@Validated PageParam param) throws Exception {
        Page<DriverRechargeEntity> page = driverRechargeService.query(param);
        PageResult<RechargeResult> result = PageResult.create(page, RechargeResult.class);
        return ApiResponse.createBySuccess(result);
    }

    @ValidToken
    @ApiOperation("充值状态记录")
    @GetMapping("/order/{orderNumber}")
    public ApiResponse<PayState> order(@PathVariable("orderNumber") String orderNumber) throws Exception {
        DriverRechargeEntity driverRechargeEntity = driverRechargeService.order(orderNumber);
        Assert.notNull(driverRechargeEntity, "订单不存在");
        return ApiResponse.createBySuccess(driverRechargeEntity.getPayState());
    }
}
