package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WXCallbackResource implements Serializable {
    /**
     * 参数名	变量	类型[长度限制]	必填	描述
     * 加密算法类型	algorithm	string[1,32]	是	对开启结果数据进行加密的加密算法，目前只支持AEAD_AES_256_GCM
     */
    private String algorithm;
    /**
     * 数据密文	ciphertext	string[1,1048576]	是	Base64编码后的开启/停用结果数据密文
     */
    private String ciphertext;
    /**
     * 附加数据	associated_data	string[1,16]	否	附加数据
     */
    @JsonProperty("associated_data")
    private String associatedData;
    /**
     * 随机串	nonce	string[1,16]	是	加密使用的随机串
     */
    private String nonce;

}
