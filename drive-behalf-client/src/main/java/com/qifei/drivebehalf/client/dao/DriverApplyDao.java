package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverApplyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverApplyDao extends JpaRepository<DriverApplyEntity, Long>, JpaSpecificationExecutor<DriverApplyEntity> {

    DriverApplyEntity findDriverApplyEntityByPhone(String phone);
}
