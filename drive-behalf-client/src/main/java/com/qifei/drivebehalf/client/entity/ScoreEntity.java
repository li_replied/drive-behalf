package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 积分收支
 */
@Setter
@Getter
@Entity
@Table(
        name = "driver_score",
        indexes = {
                @Index(name = "idx_driver_score_user_id", columnList = "user_id"),
                @Index(name = "idx_driver_score_create_time", columnList = "create_time"),
        }
)
public class ScoreEntity extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "amount", precision = 10, scale = 2)
    private BigDecimal amount = new BigDecimal(0);
    private String name;
    private String phone;
    private String card;
    private String bank;
    private String subbranch;
    /**
     * 审批
     */
    @Enumerated(EnumType.STRING)
    private EnableEnum state;
    /**
     * 收支
     */
    @Enumerated(EnumType.STRING)
    private EnableEnum income;

}
