package com.qifei.drivebehalf.client.controller.passenger.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "推荐人返回结果")
public class RecommendResult implements Serializable {
    private String phone;
    private String image;
}
