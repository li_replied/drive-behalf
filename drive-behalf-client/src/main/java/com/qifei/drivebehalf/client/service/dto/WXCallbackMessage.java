package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class WXCallbackMessage implements Serializable {
    /**
     * 通知ID	id	string[1,32]	是	通知的唯一ID
     */
    private String id;
    /**
     * 通知创建时间	create_time	string[1,16]	是	通知创建的时间，遵循rfc3339标准格式，格式为YYYY-MM-DDTHH:mm:ss+TIMEZONE，YYYY-MM-DD表示年月日，T出现在字符串中，表示time元素的开头，HH:mm:ss.表示时分秒，TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）。例如：2015-05-20T13:29:35+08:00表示北京时间2015年05月20日13点29分35秒。
     */
    @JsonProperty("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssXXX", timezone = "GMT+8")
    private Date createTime;
    /**
     * 通知类型	event_type	string[1,32]	是	通知的类型，支付成功通知的类型为TRANSACTION.SUCCESS
     * 示例值：TRANSACTION.SUCCESS
     */
    @JsonProperty("event_type")
    private String eventType;
    /**
     * 通知数据类型	resource_type	string[1,32]	是	通知的资源数据类型，支付成功通知为encrypt-resource
     * 示例值：encrypt-resource
     */
    @JsonProperty("resource_type")
    private String resourceType;
    /**
     * -通知数据	resource	object	是	通知资源数据
     */
    private WXCallbackResource resource;
    /**
     * 回调摘要	summary	string[1,64]	是	回调摘要
     * 示例值：支付成功
     */
    private String summary;
}
