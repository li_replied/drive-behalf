package com.qifei.drivebehalf.client.controller.passenger.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ApiModel(description = "用户信息修改")
public class PassengerModifyParam implements Serializable {
    @NotNull(message = "id不能为空")
    @ApiModelProperty("id")
    protected Long id;
    @NotNull(message = "姓名不能为空")
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("是否显示姓名")
    private EnableEnum nameShow = EnableEnum.NO;
    @ApiModelProperty("生日")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    @ApiModelProperty("性别")
    private SexEnum sex = SexEnum.M;
}
