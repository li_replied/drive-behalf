package com.qifei.drivebehalf.client.common.session;

import com.qifei.drivebehalf.client.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserLoginUtils {
    /**
     * 保存登录用户
     */
    private static final ThreadLocal<UserEntity> local = new ThreadLocal<>();

    /**
     * 获取当前登录用户
     *
     * @return
     */
    public static UserEntity getLogin() {
        UserEntity user = local.get();
        return user;
    }

    /**
     * 设置当前登录用户
     *
     * @param login
     */
    public static void setLogin(UserEntity login) {
        local.set(login);
    }

    /**
     * 删除当前用户
     */
    public static void removeLogin() {
        local.remove();
    }
}
