package com.qifei.drivebehalf.client.controller.card.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Setter
@Getter
@ApiModel(description = "银行卡")
public class CardResultVo implements Serializable {
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("姓名")
    @NotBlank(message = "姓名不能为空")
    private String name;
    @ApiModelProperty("手机号")
    @NotBlank(message = "手机号不能为空")
    private String phone;
    @ApiModelProperty("银行卡")
    @NotBlank(message = "银行卡不能为空")
    private String card;
    @ApiModelProperty("银行名称")
    @NotBlank(message = "银行名称不能为空")
    private String bank;
    @ApiModelProperty("开户银行")
    @NotBlank(message = "开户银行不能为空")
    private String subbranch;
}
