package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverCallbackEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverCallbackDao extends JpaRepository<DriverCallbackEntity, Long>, JpaSpecificationExecutor<DriverCallbackEntity> {
}
