package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OriginType;
import com.qifei.drivebehalf.client.common.enums.VersionPlatformEnum;
import com.qifei.drivebehalf.client.dao.VersionDao;
import com.qifei.drivebehalf.client.entity.VersionEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VersionService {

    private final VersionDao versionDao;

    public VersionEntity gain(OriginType origin, VersionPlatformEnum platform) throws Exception {
        return versionDao.findFirstByPlatformAndEnableStateAndOriginOrderByVersionNumberDesc(platform, EnableEnum.YES, origin);
    }
}
