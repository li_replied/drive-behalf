package com.qifei.drivebehalf.client.common.model;

public enum Message {
    USER_PASSWORD_ERROR("手机号或密码错误"),
    USER_PHONE_ERROR("手机号不正确"),
    USER_VERIFICATION_CODE_ERROR("先校验手机号码"),
    USER_PHONE_USED("手机号已使用"),
    USER_MAX_PASSWORD_ERROR("一小时内密码错误次数超过3次"),
    USER_MAX_CHANGE_PASSWORD_ERROR("设置密码次数超限制,请明天再试"),
    USER_PASSWORD_NOSET("账号未设置密码，请使用手机验证码登录"),
    USER_CHANG_PHONE_ERROR("未验证修改手机号前置条件"),
    USER_CHANG_PHONE_COUNT_ERROR("一天内更改手机号次数超过3次"),
    PARAMETER_ERROR("参数错误"),
    SMS_ERROR("短信校验码错误。"),
    SMS_TIMEOUT("短信校验码超时"),
    MAX_UPLOAD_SIZE("文件超出最大限制"),
    PICTURE_NON_CONFORMITY("图片宽高不符合要求"),
    PICTURE_IS_SAME("同一张图片"),
    PICTURE_NOT_FIND("图片不存在"),
    FILE_NOT_SUPPORT("文件格式不支持"),
    USER_NOT_FIND("用户不存在"),
    CLASSIFICATION_NOT_FIND("分类不存在"),
    OPUS_NOT_FIND("作品不存在"),
    OPUS_VIOLATIONS("标题或内容违反网络安全规定"),
    OPUS_READ("作品已阅读"),
    OPUS_LIKED("作品已点赞"),
    OPUS_NOT_LIKE("作品未点赞"),
    COMMENT_NOT_FIND("评论不存在"),
    COMMENT_VIOLATIONS("评论文字违反网络安全规定"),
    VIOLATIONS("内容违反网络安全规定"),
    COMMENT_LIKED("评论已点赞"),
    COMMENT_NOT_LIKE("评论未点赞"),
    COMMENT_TOO_LONG("评论太长"),
    COLLECTED("已收藏"),
    NOT_COLLECTED("未收藏"),
    PAGE_NUM_ERROR("页码不正确"),
    FRIEND_FOLLOWED("已关注"),
    FRIEND_NOT_FOLLOW("未关注"),
    FRIEND_CONTACTED("已回关"),
    FRIEND_NOT_CONTACT("未回关"),
    RECITATION_LIKED("朗读已点赞"),
    RECITATION_NOT_LIKED("朗读未点赞"),
    SIGN_ERROR("签名错误");
    private String message;

    Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
