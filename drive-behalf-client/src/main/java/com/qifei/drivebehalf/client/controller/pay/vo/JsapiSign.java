package com.qifei.drivebehalf.client.controller.pay.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "JSAPI 支付返回结果")
public class JsapiSign implements Serializable {
    /**
     * 公众号id	appId	string[1,16]	是	请填写merchant_appid对应的值。
     */
    @ApiModelProperty("公众号id")
    private String appId;
    /**
     * 时间戳	timeStamp	string[1,32]	是	当前的时间，其他详见时间戳规则。
     */
    @ApiModelProperty("时间戳")
    private String timeStamp;
    /**
     * 随机字符串	nonceStr	string[1,32]	是	随机字符串，不长于32位。推荐随机数生成算法。
     */
    @ApiModelProperty("随机字符串")
    private String nonceStr;
    /**
     * 订单详情扩展字符串	package	string[1,128]	是	统一下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
     * 示例值：prepay_id=wx201410272009395522657a690389285100
     */
    @ApiModelProperty("订单详情扩展字符串")
    @JsonProperty("package")
    private String packageStr;
    /**
     * 签名方式	signType	string[1,32]	是	签名类型，默认为RSA，仅支持RSA。
     * 示例值：RSA
     */
    @ApiModelProperty("签名方式")
    private String signType = "RSA";
    /**
     * 签名 paySign
     * string[1,256]
     * 是 签名，使用字段appId、timeStamp、nonceStr、package按照签名生成算法计算得出的签名值
     */
    @ApiModelProperty("签名")
    private String paySign;

}
