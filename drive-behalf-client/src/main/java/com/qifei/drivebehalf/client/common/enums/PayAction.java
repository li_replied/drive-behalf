package com.qifei.drivebehalf.client.common.enums;

public enum PayAction {
    consume, recharge;
}
