package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class TencentMapDistrictResult<T> implements Serializable {
    /**
     * 状态码，0为正常，其它为异常（可关注message信息）
     */
    private int status;
    /**
     * 状态说明
     */
    private String message;
    /**
     * 行政区划数据版本，便于您判断更新
     */
    @JsonProperty("data_version")
    private int dataVersion;

    private List<T> result;
}
