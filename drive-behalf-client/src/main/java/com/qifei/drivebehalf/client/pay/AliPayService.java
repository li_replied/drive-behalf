package com.qifei.drivebehalf.client.pay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AliPayService {

    public String pay(String amount, String outTradeNo) {
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    AlipayConfig.URL,//支付宝网管
                    AlipayConfig.APP_ID,//appid
                    AlipayConfig.APP_PRIVATE_KEY,//私钥
                    AlipayConfig.FORMAT,//json格式
                    AlipayConfig.CHARSET,//编码格式
                    AlipayConfig.ALIPAY_PUBLIC_KEY,//公钥
                    AlipayConfig.SIGNTYPE);//签名方式

            AlipayTradeAppPayRequest appPayRequest = new AlipayTradeAppPayRequest();
            AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
            model.setBody("我是测试数据");
            model.setSubject("App支付测试Java");
            // 订单号请保证OutTradeNo值每次保证唯一
            model.setOutTradeNo(outTradeNo);
            // 超时时间 自定义
            model.setTimeoutExpress("10m");
            // 前台传的支付金额 string类型
            model.setTotalAmount(amount + "");
            model.setProductCode("QUICK_MSECURITY_PAY");
            appPayRequest.setBizModel(model);
            appPayRequest.setNotifyUrl(AlipayConfig.NOTIFY_URL);//回调地址
            // 这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(appPayRequest);
            // 就是orderString 可以直接给客户端请求，无需再做处理。
            return response.getBody();
        } catch (AlipayApiException e) {
            log.error("app支付订单创建失败", e);
            return null;
        }
    }

    public static void main(String[] args) {
        String orderStr = new AliPayService().pay("0.01", "1111111");
        System.out.println(orderStr);
    }
}
