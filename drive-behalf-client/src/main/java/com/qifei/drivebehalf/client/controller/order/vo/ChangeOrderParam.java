package com.qifei.drivebehalf.client.controller.order.vo;

import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "撤单参数")
public class ChangeOrderParam implements Serializable {
    @NotEmpty(message = "订单编号不能为空")
    @ApiModelProperty("订单编号")
    private String orderNumber;
    @NotNull(message = "状态值不能为空")
    @ApiModelProperty("状态值")
    private OrderStateEnum status;
    @NotEmpty(message = "变更原因不能为空")
    @ApiModelProperty("变更原因")
    private String reason;
}
