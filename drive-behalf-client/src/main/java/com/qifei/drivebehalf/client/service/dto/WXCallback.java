package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class WXCallback implements Serializable {

    /**
     * 公众号ID	appid	string[1,32]	是	直连商户申请的公众号或移动应用appid。
     */
    private String appid;
    /**
     * 直连商户号	mchid	string[1,32]	是	直连商户的商户号，由微信支付生成并下发。
     */
    private String mchid;
    /**
     * 商户订单号	out_trade_no	string[6,32]	是	商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一，详见【商户订单号】。
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;
    /**
     * 微信支付订单号	transaction_id	string[1,32]	否	微信支付系统生成的订单号。
     */
    @JsonProperty("transaction_id")
    private String transactionId;
    /**
     * 交易类型	trade_type	string[1,16]	否	交易类型，枚举值：
     * JSAPI：公众号支付
     * NATIVE：扫码支付
     * APP：APP支付
     * MICROPAY：付款码支付
     * MWEB：H5支付
     * FACEPAY：刷脸支付
     * 示例值：MICROPAY
     */
    @JsonProperty("trade_type")
    private String tradeType;
    /**
     * 交易状态	trade_state	string[1,32]	是	交易状态，枚举值：
     * SUCCESS：支付成功
     * REFUND：转入退款
     * NOTPAY：未支付
     * CLOSED：已关闭
     * REVOKED：已撤销（付款码支付）
     * USERPAYING：用户支付中（付款码支付）
     * PAYERROR：支付失败(其他原因，如银行返回失败)
     * 示例值：SUCCESS
     */
    @JsonProperty("trade_state")
    private String tradeState;
    /**
     * 交易状态描述	trade_state_desc	string[1,256]	是	交易状态描述
     */
    @JsonProperty("trade_state_desc")
    private String tradeStateDesc;
    /**
     * 付款银行	bank_type	string[1,16]	否	银行类型，采用字符串类型的银行标识。
     */
    @JsonProperty("bank_type")
    private String bankType;
    /**
     * 附加数据	attach	string[1,128]	否	附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用
     */
    private String attach;
    /**
     * 支付完成时间	success_time	string[1,64]	否	支付完成时间，遵循rfc3339标准格式，格式为YYYY-MM-DDTHH:mm:ss+TIMEZONE，YYYY-MM-DD表示年月日，T出现在字符串中，表示time元素的开头，HH:mm:ss表示时分秒，TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）。例如：2015-05-20T13:29:35+08:00表示，北京时间2015年5月20日 13点29分35秒。
     */
    @JsonProperty("success_time")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssXXX", timezone = "GMT+8")
    private Date successTime;

}
