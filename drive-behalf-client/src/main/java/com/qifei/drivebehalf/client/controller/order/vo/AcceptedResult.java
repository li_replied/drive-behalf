package com.qifei.drivebehalf.client.controller.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ApiModel(description = "呼叫代驾结果")
public class AcceptedResult implements Serializable {
    @ApiModelProperty("订单编号")
    private String orderNumber;

    @ApiModelProperty("司机信息")
    private DriverResult driver;

    @ApiModelProperty("起步价格")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal startingPrice = new BigDecimal(0);

    @ApiModelProperty("起步公里")
    private Integer startingKilometre;

    @ApiModelProperty("每公里费用")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal mileageFee = new BigDecimal(0);

    @ApiModelProperty("价格倍率")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal priceMultiple = new BigDecimal(0);

    @ApiModelProperty("优惠金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal couponAmount = new BigDecimal(0);

    @ApiModelProperty("每分钟等待费")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal waitingFee = new BigDecimal(0);

    @ApiModelProperty("等待超时上限")
    private Integer waitingTimeout;

    @ApiModelProperty("客服热线")
    private String customerPhone;
}
