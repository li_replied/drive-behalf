package com.qifei.drivebehalf.client.controller.version;


import com.qifei.drivebehalf.client.common.enums.OriginType;
import com.qifei.drivebehalf.client.common.enums.VersionPlatformEnum;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.controller.version.vo.VersionResult;
import com.qifei.drivebehalf.client.entity.VersionEntity;
import com.qifei.drivebehalf.client.service.VersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Api(tags = "版本服务")
@RestController
@RequestMapping("/version")
@RequiredArgsConstructor
public class VersionController {

    private final VersionService versionService;

    @ApiOperation("获取版本信息")
    @GetMapping("/gain/{origin}/{platform}/{versionNumber}")
    public ApiResponse<VersionResult> gain(
            @PathVariable("origin")
                    OriginType origin,
            @PathVariable("platform")
                    VersionPlatformEnum platform,
            @PathVariable("versionNumber")
                    String versionNumber
    ) throws Exception {
        VersionEntity versionEntity = versionService.gain(origin, platform);
        if (versionEntity == null) {
            return ApiResponse.createBySuccess();
        }
        long version = Long.valueOf(versionNumber.replace(".", ""));
        long tagVersion = Long.valueOf(versionEntity.getVersionNumber().replace(".", ""));
        if (version >= tagVersion) {
            return ApiResponse.createBySuccess();
        }
        VersionResult versionResult = new VersionResult();
        BeanUtils.copyProperties(versionEntity, versionResult);
        return ApiResponse.createBySuccess(versionResult);
    }
}
