package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "driver_apply",
        indexes = {
                @Index(name = "idx_driver_apply_phone", columnList = "phone", unique = true),
        }
)
public class DriverApplyEntity extends BaseEntity {

    /**
     * 价格表
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "phone")
    private String phone;
    @Column(name = "birthday")
    private Date birthday;
    @Column(name = "sex")
    @Enumerated(EnumType.STRING)
    private SexEnum sex = SexEnum.M;
    @Column(name = "years")
    private Integer years;
    @Column(name = "status")
    private String status;

}
