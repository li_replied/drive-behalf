package com.qifei.drivebehalf.client.controller.bill;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.model.PageResult;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.bill.vo.BillResult;
import com.qifei.drivebehalf.client.entity.DriverWithdrawEntity;
import com.qifei.drivebehalf.client.service.DriverWithdrawService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@Api(tags = "司机端: 账单明细")
@RestController
@RequiredArgsConstructor
@RequestMapping("/bill")
public class BillController {
    private final DriverWithdrawService driverWithdrawService;

    @ValidToken
    @ApiOperation("查询收支账单")
    @GetMapping("/query/{month}")
    public ApiResponse<List<BillResult>> query(
            @PathVariable("month")
            @DateTimeFormat(pattern = "yyyy-MM")
                    Date month
    ) throws Exception {
        List<DriverWithdrawEntity> list = driverWithdrawService.query(month);
        if (list == null || list.isEmpty()) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, BillResult.class));
    }
}
