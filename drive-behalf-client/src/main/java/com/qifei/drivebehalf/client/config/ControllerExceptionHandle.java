package com.qifei.drivebehalf.client.config;

import com.qifei.drivebehalf.client.common.model.Message;
import com.qifei.drivebehalf.client.common.model.ResponseCode;
import com.qifei.drivebehalf.client.common.execution.NotLoginException;
import com.qifei.drivebehalf.client.common.execution.YUSupperExecution;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandle {

    /**
     * 处理Get请求中 使用@Valid 验证路径中请求实体校验失败后抛出的异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    public ApiResponse handleBindException(BindException ex) {
        List<ObjectError> errorList = ex.getBindingResult().getAllErrors();
        String message = errorList.get(0).getDefaultMessage();
        return ApiResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), message);
    }

    /**
     * 处理请求参数格式错误 @RequestParam上validate失败后抛出的异常是javax.validation.ConstraintViolationException
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ApiResponse handleConstraintViolationException(ConstraintViolationException ex) {
        ConstraintViolation constraintViolation = ex.getConstraintViolations().iterator().next();
        return ApiResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), constraintViolation.getMessage());
    }

    /**
     * 处理请求参数格式错误 @RequestBody上validate失败后抛出的异常是MethodArgumentNotValidException异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        List<ObjectError> errorList = ex.getBindingResult().getAllErrors();
        String message = errorList.get(0).getDefaultMessage();
        return ApiResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), message);
    }

    /**
     * 文件上传错误
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ApiResponse handleMaxUploadSizeExceededException(MaxUploadSizeExceededException ex) {
        return ApiResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), Message.MAX_UPLOAD_SIZE.getMessage());
    }

    /**
     * 未登录异常处理
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(NotLoginException.class)
    public ApiResponse NotLoginExceptions(NotLoginException ex) {
        return ApiResponse.createByResponseCode(ResponseCode.NOT_LOGIN);
    }

    /**
     * 运行时异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(YUSupperExecution.class)
    public ApiResponse handleTuLeSupperExecution(YUSupperExecution ex) {
        return ApiResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), ex.getMessage());
    }

    /**
     * 自定义校验错误
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public ApiResponse handleIllegalArgumentException(IllegalArgumentException ex) {
        return ApiResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), ex.getMessage());
    }

    /**
     * 参数设置错误
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ApiResponse handMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
        log.error("执行错误", ex);
        return ApiResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), Message.PARAMETER_ERROR.getMessage());
    }



    /**
     * 系统未知异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ApiResponse handleExceptions(Exception ex) {
        log.error("执行错误", ex);
        return ApiResponse.createByResponseCode(ResponseCode.SYSTEM_ERROR);
    }
}
