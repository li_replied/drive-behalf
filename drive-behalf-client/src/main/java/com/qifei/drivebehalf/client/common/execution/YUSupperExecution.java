package com.qifei.drivebehalf.client.common.execution;

public class YUSupperExecution extends Exception {

    public YUSupperExecution() {
    }

    public YUSupperExecution(String message) {
        super(message);
    }

    public YUSupperExecution(String message, Throwable cause) {
        super(message, cause);
    }

    public YUSupperExecution(Throwable cause) {
        super(cause);
    }

    public YUSupperExecution(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
