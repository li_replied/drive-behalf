package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverPriceIncreasedEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface DriverPriceIncreasedDao extends JpaRepository<DriverPriceIncreasedEntity, Long>, JpaSpecificationExecutor<DriverPriceIncreasedEntity> {
    List<DriverPriceIncreasedEntity> queryAllByBillingPriceAndPriceIncreasedBeginGreaterThanAndPriceIncreasedEndLessThanOrderByPriceMultipleDesc
            (Long billingPrice,
             Date now1,
             Date now2
            );
}
