package com.qifei.drivebehalf.client.common.execution;

/**
 * 服务执行异常
 */
public class ServiceWorkingException extends YUSupperExecution {

    public ServiceWorkingException() {
    }

    public ServiceWorkingException(String message) {
        super(message);
    }

    public ServiceWorkingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceWorkingException(Throwable cause) {
        super(cause);
    }

    public ServiceWorkingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
