package com.qifei.drivebehalf.client.controller.invoice.vo;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "发票抬头返回结果")
public class InvoiceTitleResult implements Serializable {
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("抬头名称")
    private String titleName;
    @ApiModelProperty("单位税号")
    private String unitTaxNumber;
    @ApiModelProperty("邮箱")
    private String mail;
    @ApiModelProperty("注册电话")
    private String phone;
    @ApiModelProperty("开户银行")
    private String bank;
    @ApiModelProperty("银行账户")
    private String bankAccount;
    @ApiModelProperty("企业地址")
    private String address;
    @ApiModelProperty("企业电话")
    private String registerPhone;
    @ApiModelProperty("默认标识")
    private EnableEnum defaultTag = EnableEnum.NO;
    @ApiModelProperty("开票状态")
    private EnableEnum state = EnableEnum.NO;
}
