package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverWithdrawEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;

public interface DriverWithdrawDao extends JpaRepository<DriverWithdrawEntity, Long>, JpaSpecificationExecutor<DriverWithdrawEntity> {
    long countByOrderNumber(String orderNumber);

    List<DriverWithdrawEntity> queryByDriverIdAndCreateTimeBetweenOrderByCreateTimeDesc(Long driverId, Date begin, Date end);
}
