package com.qifei.drivebehalf.client.common.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(
        prefix = "ali.sms",
        ignoreUnknownFields = true
)
public class AliYunSmsProperties {
    private String signatureMethod;
    private String signName;
    private String signatureVersion;
    private String domain;
    private String action = "SendSms";
    private String version = "2017-05-25";
}


