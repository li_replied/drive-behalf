package com.qifei.drivebehalf.client.controller.driver;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.enums.SwitchEnum;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.driver.vo.*;
import com.qifei.drivebehalf.client.controller.order.vo.OrderResult;
import com.qifei.drivebehalf.client.entity.BannerEntity;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import com.qifei.drivebehalf.client.service.DriverService;
import com.qifei.drivebehalf.client.service.DriverTripOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api(tags = "司机端: 司机管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/driver")
public class DriverController {
    @Value("${order.accept-timeout}")
    private int acceptTimeout;

    private final DriverService driverService;
    private final DriverTripOrderService driverTripOrderService;

    @ApiOperation("注册绑定")
    @PostMapping("/device/bind")
    public ApiResponse<String> bindDevice(@RequestBody @Validated DriverLoginParam param) throws Exception {
        String token = driverService.smLogin(param);
        return ApiResponse.createBySuccess(token);
    }

    @ApiOperation("验证绑定")
    @GetMapping("/device/check/{cid}")
    public ApiResponse<String> checkDevice(@Valid @PathVariable("cid") String cid) throws Exception {
        String token = driverService.smCheck(cid);
        return ApiResponse.createBySuccess(token);
    }

    @ValidToken
    @ApiOperation("上报位置")
    @PostMapping("/location/update")
    public ApiResponse<Integer> updateLocation(@RequestBody @Validated DriverLocationParam param) throws Exception {
        return ApiResponse.createBySuccess(driverService.updateLocation(param));
    }

    @ValidToken
    @ApiOperation("更新听单状态")
    @GetMapping("/online/change/{status}")
    public ApiResponse updateOnlineStatus(@PathVariable("status") SwitchEnum status) throws Exception {
        DriverEntity user = (DriverEntity) UserLoginUtils.getLogin();
        driverService.updateOnlineStatus(user, status, true);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("查询司机信息")
    @GetMapping("/profile/get")
    public ApiResponse<DriverResult> info() throws Exception {
        DriverEntity driverEntity = driverService.info();
        DriverResult result = BeanCopyUtils.copyProperties(driverEntity, DriverResult.class);
        result.setTodayMoney(driverTripOrderService.sumTodayMoney(driverEntity.getId()));
        result.setTodayOrders(driverTripOrderService.countTodayOrder(driverEntity.getId()));
        result.setAcceptingTimeout(acceptTimeout);
        result.setAssignedOrder(BeanCopyUtils.copyProperties(driverTripOrderService.findAssignedOrder(), OrderResult.class));
        List<DriverTripOrderEntity> startingOrders = driverTripOrderService.getStartingOrders(driverEntity);
        if (startingOrders != null && startingOrders.size() > 0) {
            result.setStartingOrders(BeanCopyUtils.copyProperties(startingOrders, OrderResult.class));
        }
        List<DriverTripOrderEntity> cancelOrders = driverTripOrderService.getCancelOrders(driverEntity);
        if (cancelOrders != null && cancelOrders.size() > 0) {
            result.setCancelOrders(BeanCopyUtils.copyProperties(cancelOrders, OrderResult.class));
        }
        List<DriverTripOrderEntity> unpaidOrders = driverTripOrderService.getUnpaidOrders(driverEntity);
        if (unpaidOrders != null && unpaidOrders.size() > 0) {
            result.setUnpaidOrders(BeanCopyUtils.copyProperties(unpaidOrders, OrderResult.class));
        }
        return ApiResponse.createBySuccess(result);
    }

    @ValidToken
    @ApiOperation("查询附件司机")
    @GetMapping("/nearby/list")
    public ApiResponse<List<NearByResult>> getNearBy() throws Exception {
        List<DriverEntity> drivers = driverService.getNearBy();
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(drivers, NearByResult.class));
    }

    @ValidToken
    @ApiOperation("解除绑定")
    @GetMapping("/device/unbind/{cid}")
    public ApiResponse unbindDevice(@PathVariable("cid") String cid) throws Exception {
        driverService.smLogout(cid);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("统计司机成绩")
    @GetMapping("/area/ranking/{type}/list")
    public ApiResponse<List<Map<String, Object>>> sumAreaDriverRanking(@PathVariable("type") String type) throws Exception {
        List<Map<String, Object>> list = driverService.sumAreaRanking(type);
        return ApiResponse.createBySuccess(list);
    }

    // carousel
    @ApiOperation("获取广告")
    @GetMapping("/ad/{type}/list")
    public ApiResponse<List<AdResult>> getAdList(@PathVariable("type") String type) throws Exception {
        List<BannerEntity> list = driverService.getBannersByType(type.toUpperCase());
        if (list != null && list.size() > 0) {
            return ApiResponse.createBySuccess(list.stream()
                    .map(i -> new AdResult(i.getId(), i.getPicPath(), i.getPeriod()))
                    .collect(Collectors.toList()));
        }
        return ApiResponse.createByErrorMessage("暂无");
    }

    @ApiOperation("司机申请")
    @PostMapping("/apply")
    public ApiResponse<Void> apply(
            @Valid @RequestBody ApplyParam param
    ) throws Exception {
        driverService.apply(param);
        return ApiResponse.createBySuccess();
    }
}
