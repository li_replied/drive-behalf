package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.DateUtil;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.controller.score.vo.ExchangeParam;
import com.qifei.drivebehalf.client.controller.score.vo.ScoreTotalResult;
import com.qifei.drivebehalf.client.dao.ScoreDao;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.entity.ScoreEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

@Service
public class ScoreService {
    @Resource
    private ScoreDao scoreDao;
    @Resource
    private PassengerService passengerService;

    public ScoreTotalResult total() throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        PassengerEntity passengerEntity = passengerService.info(user.getId());
        BigDecimal recharge = scoreDao.total(user.getId(), EnableEnum.YES);
        BigDecimal consume = scoreDao.total(user.getId(), EnableEnum.NO);
        BigDecimal today = scoreDao.total(user.getId(), EnableEnum.YES, DateUtil.beginOfDay(new Date()));
        ScoreTotalResult scoreTotalResult = new ScoreTotalResult();
        scoreTotalResult.setTotal(passengerEntity.getRetail());
        scoreTotalResult.setRecharge(recharge);
        scoreTotalResult.setConsume(consume);
        scoreTotalResult.setToday(today);
        return scoreTotalResult;
    }

    public Page<ScoreEntity> query(PageParam param) throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");
        Pageable pageable = PageRequest.of(param.getPageNum() - 1, param.getPageSize(), sort);
        return scoreDao.findByUserIdAndAmountGreaterThan(user.getId(), BigDecimal.ZERO, pageable);
    }

    public ScoreEntity recently() throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        return scoreDao.findFirstByUserIdAndIncomeOrderByCreateTimeDesc(user.getId(), EnableEnum.NO);
    }


    @Transactional
    public void exchange(ExchangeParam param) throws Exception {
        Assert.isTrue(param.getAmount().compareTo(BigDecimal.ZERO) == 1, "提现积分不能小于0");
        BigDecimal[] divided = param.getAmount().divideAndRemainder(BigDecimal.valueOf(10));
        if (divided[1].intValue() != 0) {
            throw new IllegalArgumentException("提现积分必须是10的倍数");
        }
        UserEntity user = UserLoginUtils.getLogin();
        ScoreEntity scoreEntity = new ScoreEntity();
        scoreEntity.setUserId(user.getId());
        scoreEntity.setAmount(param.getAmount());
        scoreEntity.setBank(param.getBank());
        scoreEntity.setCard(param.getCard());
        scoreEntity.setName(param.getName());
        scoreEntity.setPhone(param.getPhone());
        scoreEntity.setSubbranch(param.getSubbranch());
        scoreEntity.setState(EnableEnum.NO);
        scoreEntity.setIncome(EnableEnum.NO);
        scoreEntity.setCreate();
        scoreDao.save(scoreEntity);
        passengerService.exchange(user.getId(), param.getAmount());
    }

    @Transactional
    public void income(Long userId, BigDecimal amount) throws Exception {
        ScoreEntity scoreEntity = new ScoreEntity();
        scoreEntity.setUserId(userId);
        scoreEntity.setAmount(amount);
        scoreEntity.setState(EnableEnum.YES);
        scoreEntity.setIncome(EnableEnum.YES);
        scoreEntity.setCreate();
        scoreDao.save(scoreEntity);
    }

}
