package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AdInfo implements Serializable {
    @JsonProperty("nation_code")
    private String nationCode;
    private String adcode;
    @JsonProperty("city_code")
    private String cityCode;
    private String name;
    private String nation;
    private String province;
    private String city;
    private String district;
}
