package com.qifei.drivebehalf.client.common.enums;

public enum RegionType {
    NATION,PROVINCE,CITY, DISTRICT;
}
