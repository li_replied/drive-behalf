package com.qifei.drivebehalf.client.controller.subscribe;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.subscribe.vo.SubscribeResult;
import com.qifei.drivebehalf.client.entity.SubscribeEntity;
import com.qifei.drivebehalf.client.service.SubscribeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Api(tags = "预约服务")
@RestController
@RequestMapping("/subscribe")
@RequiredArgsConstructor
public class SubscribeController {
    private final SubscribeService subscribeService;

    @ValidToken
    @ApiOperation("查询预约")
    @GetMapping("/list")
    public ApiResponse<List<SubscribeResult>> list() throws Exception {
        List<SubscribeEntity> list = subscribeService.list();
        if (list == null || list.isEmpty()) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, SubscribeResult.class));
    }

    @ValidToken
    @ApiOperation("我的预约")
    @GetMapping("/my")
    public ApiResponse<List<SubscribeResult>> my() throws Exception {
        List<SubscribeEntity> list = subscribeService.my();
        if (list == null || list.isEmpty()) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, SubscribeResult.class));
    }

    @ValidToken
    @ApiOperation(value = "抢单")
    @PostMapping("/contest/{id}")
    public ApiResponse<Void> contest(
            @PathVariable("id")
                    Long id
    ) throws Exception {
        subscribeService.contest(id);
        return ApiResponse.createBySuccess();
    }
}
