package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OriginType;
import com.qifei.drivebehalf.client.common.enums.VersionPlatformEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * 版本
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "driver_version")
public class VersionEntity extends BaseEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "version_id", unique = true, nullable = false)
    protected Long id;
    /**
     * 版本名称
     */
    @Column(name = "version_name", length = 50, nullable = false)
    private String versionName;
    /**
     * 版本号
     */
    @Column(name = "version_number", length = 50, nullable = false)
    private String versionNumber;
    /**
     * 平台
     */
    @Column(name = "platform", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    private VersionPlatformEnum platform;
    /**
     * 必须更新
     */
    @Column(name = "must_update", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    private EnableEnum mustUpdate = EnableEnum.NO;

    @Column(name = "origin",  nullable = false)
    @Enumerated(EnumType.STRING)
    private OriginType origin;
    /**
     * 下载地址
     */
    @Column(name = "download_url", length = 500, nullable = false)
    private String downloadUrl;

    /**
     * 是否开启
     */
    @Column(name = "enable_state", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    private EnableEnum enableState = EnableEnum.NO;

    /**
     * 更新说明
     */
    @Column(name = "information", length = 1000)
    private String information;
}
