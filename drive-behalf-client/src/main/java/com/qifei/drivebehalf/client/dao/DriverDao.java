package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.controller.driver.vo.DriverLocationParam;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface DriverDao extends JpaRepository<DriverEntity, Long>, JpaSpecificationExecutor<DriverEntity> {
    DriverEntity findDriverEntityByPhone(String phone);

    DriverEntity findByClientId(String cid);

    List<DriverEntity> findDriverEntitiesByAgentIdAndOnlineAndIdNot(String agentId, EnableEnum online, Long id);

    List<DriverEntity> findDriverEntitiesByOnlineAndClientIdIn(EnableEnum online, List<String> cids);

    Page<DriverEntity> queryAllByClientIdNotNull(Pageable pageable);

    @Query("update DriverEntity set online=:online where id=:id")
    @Modifying
    void updateOnline(@Param("online") EnableEnum online, @Param("id") Long id);

    @Query("update DriverEntity set online=:online,lastOnlineTime=:t where id=:id")
    @Modifying
    void updateOnline(@Param("online") EnableEnum online, @Param("id") Long id, @Param("t") Long t);

    @Query(value = "update driver_driver set total_online_time=total_online_time+today_online_time,today_online_time=0,online='YES',last_online_time=?2 where user_id=?1", nativeQuery = true)
    @Modifying
    void updateTotalOnlineTime(Long id, Long t);

    @Query("update DriverEntity set money=money+:money where id=:id")
    @Modifying
    void updateMoney(@Param("money") BigDecimal money, @Param("id") Long id);

    @Query("update DriverEntity set clientId=:clientId where id=:id")
    @Modifying
    void updateClientId(@Param("clientId") String clientId, @Param("id") Long id);

    @Query("update DriverEntity set likes=likes+:likes where id=:id")
    @Modifying
    void updateLikes(@Param("likes") Integer likes, @Param("id") Long id);

    @Query("update DriverEntity set lastLongitude=:#{#param.longitude}, " +
            "lastLatitude=:#{#param.latitude}, lastAddress=:#{#param.address}, " +
            "lastUpdateTime=:time where clientId=:#{#param.cid}")
    @Modifying
    void updateLocation(@Param("param") DriverLocationParam param, @Param("time") Long time);

    @Query("update DriverEntity set lastLongitude=:#{#param.longitude}, " +
            "lastLatitude=:#{#param.latitude}, lastAddress=:#{#param.address}, " +
            "lastUpdateTime=:time, todayOnlineTime=:online, lastOnlineTime=:time where clientId=:#{#param.cid}")
    @Modifying
    void updateLocation(@Param("param") DriverLocationParam param, @Param("time") Long time, @Param("online") Integer online);

    // 排序策略 评价优先，其次距离，不管司机当前是否在线，只找最近5分钟内上线过的
    @Query(value = "SELECT t1.* " +
            "FROM driver_driver t1 INNER JOIN (" +
            "SELECT user_id, round( st_distance_sphere ( point ( ?1, ?2 ), point ( last_longitude, last_latitude ) ) ) AS distance " +
            "FROM driver_driver " +
            "WHERE online='YES' and last_online_time >= ( UNIX_TIMESTAMP() - 60 * 5 ) * 1000  ) t2 " +
            "ON t1.user_id = t2.user_id AND t2.distance <= ?3 " +
            "ORDER BY t1.likes desc, t2.distance ASC ", nativeQuery = true)
    List<DriverEntity> findNearByOnlineDrivers(double longitude, double latitude, int scope);

    @Query(value = "select user_id as id, name, (today_online_time + total_online_time) as online from driver_driver where agent_id=?1 order by total_online_time desc", nativeQuery = true)
    List<Map<String, Object>> sumAreaOnlineTimeByAgentId(String agentId);

    @Query(value = "select t1.user_id as id, name,sum(t2.amount) as money from driver_driver t1 left join driver_withdraw t2 on t1.user_id = t2.driver_id and t1.agent_id = ?1 and t2.method='INCOME' group by t1.user_id,t1.name order by money desc", nativeQuery = true)
    List<Map<String, Object>> sumAreaIncomeByAgentId(String agentId);

    @Query(value = "select user_id as id, name, likes from driver_driver where agent_id=?1 order by likes desc", nativeQuery = true)
    List<Map<String, Object>> sumAreaLikesByAgentId(String agentId);

    @Query(value = "select * from driver_driver where online='YES' and last_online_time >= ((UNIX_TIMESTAMP() - 60 * 5 ) * 1000) and agent_id = ?1 and user_id <> ?2 order by last_online_time desc", nativeQuery = true)
    List<DriverEntity> findAreaNearByOnlineDrivers(String agentId, Long id);
}
