package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.entity.DriverInvoiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DriverInvoiceDao extends JpaRepository<DriverInvoiceEntity, Long>, JpaSpecificationExecutor<DriverInvoiceEntity> {
    DriverInvoiceEntity queryFirstByUserIdAndOrderNo(Long userId, String orderNumber);

    void deleteByOrderNo(String orderNumber);
}
