package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.controller.price.vo.PriceQueryParam;
import com.qifei.drivebehalf.client.dao.DriverBillingPriceDao;
import com.qifei.drivebehalf.client.entity.DriverBillingPriceEntity;
import com.qifei.drivebehalf.client.entity.DriverRegionEntity;
import com.qifei.drivebehalf.client.service.dto.GeocoderResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DriverBillingPriceService {
    @Value("${tencent-map.key}")
    private String key;

    private final DriverBillingPriceDao driverBillingPriceDao;

    private final DriverRegionService driverRegionService;

    private final TencentMapService tencentMapService;

    public List<DriverBillingPriceEntity> query(PriceQueryParam param) throws Exception {
        DriverRegionEntity driverRegionEntity = driverRegionService.queryRegion(param.getRegionId());
        Assert.notNull(driverRegionEntity, "区域地址不存在。");
        return driverBillingPriceDao.queryDriverBillingPriceEntityByRegionIdOrderByStartTime(driverRegionEntity.getRegionId());
    }

    public DriverBillingPriceEntity query(String regionId) throws Exception {
        DriverRegionEntity driverRegionEntity = driverRegionService.queryRegion(regionId);
        Assert.notNull(driverRegionEntity, "区域地址不存在。");
        LocalTime now = LocalTime.now();
        return driverBillingPriceDao.queryFirstByRegionIdAndEndTimeGreaterThanEqualAndStartTimeLessThanEqual(driverRegionEntity.getRegionId(), now, now);
    }

    public DriverBillingPriceEntity query(String lat, String lng) throws Exception {
        String location = String.format("%s,%s", lat, lng);
        GeocoderResult geocoder = tencentMapService.geocoder(location, "radius=1;policy=1", key);
        Assert.isTrue(geocoder.getStatus() == 0, "地址换算错误。");
        LocalTime now = LocalTime.now();
        return driverBillingPriceDao.queryFirstByRegionIdAndEndTimeGreaterThanEqualAndStartTimeLessThanEqual(geocoder.getResult().getAdInfo().getAdcode(), now, now);
    }

    public String queryRegionId(String lat, String lng) throws Exception {
        String location = String.format("%s,%s", lat, lng);
        GeocoderResult geocoder = tencentMapService.geocoder(location, "radius=1;policy=1", key);
        Assert.isTrue(geocoder.getStatus() == 0, "地址换算错误。");
        return geocoder.getResult().getAdInfo().getAdcode();
    }

}
