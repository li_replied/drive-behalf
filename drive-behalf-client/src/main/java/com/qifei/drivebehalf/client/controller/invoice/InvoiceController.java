package com.qifei.drivebehalf.client.controller.invoice;

import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.invoice.vo.DrawInvoiceParam;
import com.qifei.drivebehalf.client.controller.invoice.vo.InvoiceTitleParam;
import com.qifei.drivebehalf.client.controller.invoice.vo.InvoiceTitleResult;
import com.qifei.drivebehalf.client.entity.DriverInvoiceTitleEntity;
import com.qifei.drivebehalf.client.service.DriverInvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "发票管理")
@RestController
@RequestMapping("/invoice")
@RequiredArgsConstructor
public class InvoiceController {
    private final DriverInvoiceService driverInvoiceService;

    @ValidToken
    @ApiOperation("查询全部发票")
    @GetMapping("/list")
    public ApiResponse<List<InvoiceTitleResult>> list() throws Exception {
        List<DriverInvoiceTitleEntity> list = driverInvoiceService.query();
        if (list == null) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, InvoiceTitleResult.class));
    }

    @ValidToken
    @ApiOperation("查询发票")
    @GetMapping("/find/{id}")
    public ApiResponse<InvoiceTitleResult> find(
            @Valid
            @PathVariable("id")
                    Long id
    ) throws Exception {
        DriverInvoiceTitleEntity driverInvoiceTitleEntity = driverInvoiceService.find(id);
        if (driverInvoiceTitleEntity == null) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(driverInvoiceTitleEntity, InvoiceTitleResult.class));
    }

    @ValidToken
    @ApiOperation("查询默认发票")
    @GetMapping("/default")
    public ApiResponse<InvoiceTitleResult> queryDefault() throws Exception {
        DriverInvoiceTitleEntity driverInvoiceTitleEntity = driverInvoiceService.queryDefault();
        if (driverInvoiceTitleEntity == null) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(driverInvoiceTitleEntity, InvoiceTitleResult.class));
    }

    @ValidToken
    @ApiOperation("保存发票抬头")
    @PostMapping("/save")
    public ApiResponse<Void> save(@Valid @RequestBody InvoiceTitleParam param) throws Exception {
        driverInvoiceService.save(param);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("删除发票抬头")
    @PostMapping("/delete/{id}")
    public ApiResponse<Void> delete(
            @Valid
            @PathVariable("id")
                    Long id
    ) throws Exception {
        driverInvoiceService.delete(id);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("查询已开发票")
    @GetMapping("/order/{orderNumber}")
    public ApiResponse<InvoiceTitleResult> order(
            @Valid
            @PathVariable("orderNumber")
                    String orderNumber
    ) throws Exception {
        InvoiceTitleResult result = driverInvoiceService.order(orderNumber);
        return ApiResponse.createBySuccess(result);
    }

    @ValidToken
    @ApiOperation("开发票")
    @PostMapping("/draw")
    public ApiResponse<Void> draw(@Valid @RequestBody DrawInvoiceParam param) throws Exception {
        driverInvoiceService.draw(param);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("撤销发票")
    @PostMapping("/cancel/{orderNumber}")
    public ApiResponse<Void> cancel(
            @Valid
            @PathVariable("orderNumber")
                    String orderNumber
    ) throws Exception {
        driverInvoiceService.cancel(orderNumber);
        return ApiResponse.createBySuccess();
    }

}
