package com.qifei.drivebehalf.client.controller.driver.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "广告")
public class AdResult implements Serializable {
    @ApiModelProperty("id")
    protected Long id;
    @ApiModelProperty("图片网址")
    private String url;
    @ApiModelProperty("停留时间")
    private Integer period;

    public AdResult(Long id, String url, Integer period) {
        this.id = id;
        this.url = url;
        this.period = period;
    }
}
