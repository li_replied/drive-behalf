package com.qifei.drivebehalf.client.controller.version.vo;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.VersionPlatformEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "版本信息结果")
public class VersionResult {

    @ApiModelProperty(value = "版本名称")
    private String versionName;

    @ApiModelProperty(value = "版本号")
    private String versionNumber;

    @ApiModelProperty(value = "平台")
    private VersionPlatformEnum platform;

    @ApiModelProperty(value = "是否必须更新")
    private EnableEnum mustUpdate;

    @ApiModelProperty(value = "下载地址")
    private String downloadUrl;
    @ApiModelProperty(value = "说明")
    private String information;
}
