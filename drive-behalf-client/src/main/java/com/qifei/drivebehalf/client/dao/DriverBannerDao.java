package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.entity.BankCardEntity;
import com.qifei.drivebehalf.client.entity.BannerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface DriverBannerDao extends JpaRepository<BannerEntity, Long>, JpaSpecificationExecutor<BannerEntity> {
    List<BannerEntity> findAllByBannerTypeAndStatus(String type, EnableEnum status);
}
