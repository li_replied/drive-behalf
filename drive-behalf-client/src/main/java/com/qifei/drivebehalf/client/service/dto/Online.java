package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.security.KeyStore;
import java.util.Date;

@Getter
@Setter
public class Online implements Serializable {
    private Long driverId;
    private Long onlineId;
    private Long regionId;
    private Date date;
}
