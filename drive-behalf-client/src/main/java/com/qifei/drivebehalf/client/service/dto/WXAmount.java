package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WXAmount implements Serializable {

    /**
     * 总金额	total	int	是	订单总金额，单位为分。
     */

    private int total;
    /**
     * 货币类型	currency	string[1,16]	否	CNY：人民币，境内商户号仅支持人民币。
     */
    private String currency = "CNY";

    public WXAmount() {
    }

    public WXAmount(int total) {
        this.total = total;
    }
}
