package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(
        name = "driver_order_state",
        indexes = {
                @Index(name = "idx_driver_order_state_order_number", columnList = "order_number"),
                @Index(name = "idx_driver_order_state_created_user", columnList = "created_user"),
        }
)
public class DriverOrderStateEntity extends BaseEntity implements Serializable {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_state_id", unique = true, nullable = false)
    protected Long id;
    @Column(name = "order_number", nullable = false)
    private String orderNumber;
    /**
     * 订单状态
     */
    @Column(name = "order_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStateEnum orderStatus;

    @Column(name = "reason", length = 2000)
    private String reason;
}
