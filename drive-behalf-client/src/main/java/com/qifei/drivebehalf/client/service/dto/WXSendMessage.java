package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Setter
@Getter
public class WXSendMessage implements Serializable {
    /**
     * touser	string		是	接收者（用户）的 openid
     */
    private String touser;
    /**
     * template_id	string		是	所需下发的订阅模板id
     */
    @JsonProperty("template_id")
    private String templateId;
    /**
     * page	string		否	点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
     */
    private String page;
    /**
     * data	Object		是	模板内容，格式形如 { "key1": { "value": any }, "key2": { "value": any } }
     */
    private Map<String, DataMap> data;
    /**
     * miniprogram_state	string		否	跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
     */
    @JsonProperty("miniprogram_state")
    private String miniprogramState = "trial";

    public static class DataMap {
        private String value;

        public DataMap(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
