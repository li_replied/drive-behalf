package com.qifei.drivebehalf.client.controller.driver.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@ApiModel(description = "司机申请参数")
public class ApplyParam implements Serializable {
    @NotEmpty(message = "姓名不能为空")
    @ApiModelProperty("姓名")
    private String name;
    @NotEmpty(message = "手机号不能为空")
    @Length(max = 11, min = 11, message = "手机号不正确")
    @ApiModelProperty("手机号")
    private String phone;
    @NotEmpty(message = "手机验证码不能为空")
    @Length(max = 6, min = 6, message = "手机验证码不正确")
    @ApiModelProperty("手机验证码")
    private String code;
    @ApiModelProperty("生日")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    @ApiModelProperty("性别")
    private SexEnum sex = SexEnum.M;
    @ApiModelProperty("驾龄")
    @NotNull(message = "驾龄不能为空")
    private Integer years;
}
