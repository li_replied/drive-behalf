package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.dao.DriverPriceIncreasedDao;
import com.qifei.drivebehalf.client.entity.DriverPriceIncreasedEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DriverPriceIncreasedService {
    private final DriverPriceIncreasedDao driverPriceIncreasedDao;

    public DriverPriceIncreasedEntity query(Long billingPrice, Date datetime) throws Exception {
        List<DriverPriceIncreasedEntity> list = driverPriceIncreasedDao.queryAllByBillingPriceAndPriceIncreasedBeginGreaterThanAndPriceIncreasedEndLessThanOrderByPriceMultipleDesc
                (billingPrice, datetime, datetime);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
