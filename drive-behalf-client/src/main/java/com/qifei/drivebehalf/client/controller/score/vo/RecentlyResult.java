package com.qifei.drivebehalf.client.controller.score.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "兑换积分最近结果")
public class RecentlyResult implements Serializable {
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("银行卡")
    private String card;
    @ApiModelProperty("银行名称")
    private String bank;
    @ApiModelProperty("开户银行")
    private String subbranch;
}
