package com.qifei.drivebehalf.client.common.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * json数据转换工具
 */
public class JsonUtils {
    private static ObjectMapper mapper = new ObjectMapper()
            //在反序列化时忽略在 json 中存在但 Java 对象不存在的属性
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            //在序列化时忽略值为 null 的属性
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            //忽略值为默认值的属性
            .setDefaultPropertyInclusion(JsonInclude.Include.NON_DEFAULT);

    public static <T> T readValue(String json, Class<T> cls) throws JsonProcessingException {
        return mapper.readValue(json, cls);
    }

    public static String writeValue(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    public static  <T> List<T> readValue(String json)throws JsonProcessingException{
        return mapper.readValue(json, new TypeReference<List<T>>(){});
    }
}
