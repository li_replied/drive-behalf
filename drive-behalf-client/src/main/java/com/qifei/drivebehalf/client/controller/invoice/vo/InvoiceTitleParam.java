package com.qifei.drivebehalf.client.controller.invoice.vo;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "保持发票参数")
public class InvoiceTitleParam implements Serializable {
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("抬头名称")
    @NotBlank(message = "抬头名称不能为空")
    private String titleName;
    @NotBlank(message = "单位税号不能为空")
    @ApiModelProperty("单位税号")
    private String unitTaxNumber;
    @NotBlank(message = "邮箱不能为空")
    @ApiModelProperty("邮箱")
    private String mail;
    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty("注册电话")
    private String phone;
    @ApiModelProperty("开户银行")
    private String bank;
    @ApiModelProperty("银行账户")
    private String bankAccount;
    @ApiModelProperty("企业地址")
    private String address;
    @ApiModelProperty("企业电话")
    private String registerPhone;
    @ApiModelProperty("默认标识")
    private EnableEnum defaultTag = EnableEnum.NO;
}
