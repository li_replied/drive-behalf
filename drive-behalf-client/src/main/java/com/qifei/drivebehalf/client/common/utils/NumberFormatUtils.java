package com.qifei.drivebehalf.client.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

public class NumberFormatUtils {
    public static String interceptTwoBits(Double number) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setRoundingMode(RoundingMode.DOWN);
        return numberFormat.format(number);
    }

    public static String interceptTwoBits(BigDecimal number) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setRoundingMode(RoundingMode.DOWN);
        return numberFormat.format(number);
    }
}
