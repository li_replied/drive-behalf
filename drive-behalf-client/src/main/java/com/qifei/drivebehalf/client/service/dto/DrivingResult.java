package com.qifei.drivebehalf.client.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DrivingResult implements Serializable {
    /**
     * 状态码，0为正常，其它为异常（可关注message信息）
     */
    private int status;
    /**
     * 状态说明
     */
    private String message;

    private Routes result;
}
