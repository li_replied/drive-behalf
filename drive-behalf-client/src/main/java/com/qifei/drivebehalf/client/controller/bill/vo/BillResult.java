package com.qifei.drivebehalf.client.controller.bill.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ApiModel(description = "账单返回")
public class BillResult implements Serializable {
    @ApiModelProperty("订单编号")
    private String orderNumber;
    @ApiModelProperty("金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal amount;
    @ApiModelProperty(value = "收入/支出", allowableValues = "收入：INCOME，支出：EXPEND")
    private String method = "INCOME";
    @ApiModelProperty("时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
