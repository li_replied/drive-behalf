package com.qifei.drivebehalf.client.controller.pay;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.enums.PayType;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.controller.pay.vo.AppSign;
import com.qifei.drivebehalf.client.controller.pay.vo.BalanceParam;
import com.qifei.drivebehalf.client.controller.pay.vo.PayParam;
import com.qifei.drivebehalf.client.controller.pay.vo.RechargeParam;
import com.qifei.drivebehalf.client.pay.AliPayService;
import com.qifei.drivebehalf.client.service.WxPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@Api(tags = "客户端：支付")
@RestController
@RequestMapping("/pay")
@RequiredArgsConstructor
public class PayController {

    private final WxPayService wxPayService;
    private final AliPayService aliPayService;

    @ValidToken
    @ApiOperation("支付")
    @PostMapping()
    public ApiResponse<Object> pay(@RequestBody @Valid PayParam payParam) throws Exception {
        String prepayId = wxPayService.placeOrder(payParam.getPayType(), payParam.getOrderNumber());
        if (prepayId == null) {
            return ApiResponse.createBySuccess();
        }
        if (PayType.jsapi == payParam.getPayType()) {
            return ApiResponse.createBySuccess(wxPayService.jsapiSign(prepayId));
        }
        if (PayType.app == payParam.getPayType()) {
            return ApiResponse.createBySuccess(wxPayService.appSign(prepayId));
        }
        throw new ServiceWorkingException("支付类型错误。");
    }

    @ValidToken
    @ApiOperation("充值")
    @PostMapping("/recharge")
    public ApiResponse<Object> recharge(
            @Valid @RequestBody
                    RechargeParam param
    ) throws Exception {
        String prepayId = wxPayService.recharge(param);
        if (PayType.jsapi == param.getPayType()) {
            return ApiResponse.createBySuccess(wxPayService.jsapiSign(prepayId));
        }
        if (PayType.app == param.getPayType()) {
            return ApiResponse.createBySuccess(wxPayService.appSign(prepayId));
        }
        throw new ServiceWorkingException("支付类型错误。");
    }

    @ValidToken
    @ApiOperation("余额支付")
    @PostMapping("/balance")
    public ApiResponse<Void> balance(@Valid @RequestBody BalanceParam param) throws Exception {
        wxPayService.balance(param);
        return ApiResponse.createBySuccess();
    }

    @ValidToken
    @ApiOperation("取消")
    @PostMapping("/cancel/{orderNumber}")
    public ApiResponse<Void> cancel(@PathVariable("orderNumber") String orderNumber) throws Exception {
        wxPayService.cancel(orderNumber);
        return ApiResponse.createBySuccess();
    }


    @ValidToken
    @ApiOperation("支付宝充值")
    @PostMapping("/alipay")
    public ApiResponse<Object> alipay(@PathVariable("amount") String amount) throws Exception {
        String format = DateUtil.format(new Date(),"yyyyMMddHHmmss");
        String numbers = RandomUtil.randomNumbers(5);
        String orderNum = format + numbers;
        return ApiResponse.createBySuccess(aliPayService.pay(amount, orderNum));
    }
}
