package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.controller.card.vo.CardResultVo;
import com.qifei.drivebehalf.client.dao.BankCardDao;
import com.qifei.drivebehalf.client.entity.BankCardEntity;
import com.qifei.drivebehalf.client.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CardService {
    private final BankCardDao bankCardDao;

    public List<BankCardEntity> query() throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        return bankCardDao.queryAllByUserId(user.getId());
    }

    @Transactional
    public void save(CardResultVo param) throws Exception {
        BankCardEntity bankCardEntity = null;
        if (param.getId() != null && param.getId().longValue() > 0L) {
            bankCardEntity = bankCardDao.findById(param.getId()).orElse(null);
            Assert.notNull(bankCardEntity, "银行卡信息不存在");
            bankCardEntity.setModified();
        } else {
            bankCardEntity = new BankCardEntity();
            UserEntity user = UserLoginUtils.getLogin();
            bankCardEntity.setUserId(user.getId());
            bankCardEntity.setCreate();
        }
        bankCardEntity.setCard(param.getCard());
        bankCardEntity.setName(param.getName());
        bankCardEntity.setPhone(param.getPhone());
        bankCardEntity.setSubbranch(param.getSubbranch());
        bankCardEntity.setBank(param.getBank());
        bankCardDao.save(bankCardEntity);
    }
}
