package com.qifei.drivebehalf.client.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 阿里云返回数据
 */
@Data
public class AliYunResponse {
    /**
     * 发送回执ID，可根据该ID在接口QuerySendDetails中查询具体的发送状态。
     */
    @JsonProperty("BizId")
    private String bizId;
    /**
     * 请求状态码。
     */
    @JsonProperty("Code")
    private String code;

    /**
     * 返回OK代表请求成功。
     */
    @JsonProperty("Message")
    private String message;
    /**
     * 请求ID。
     */
    @JsonProperty("RequestId")
    private String requestId;

}
