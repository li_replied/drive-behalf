package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import com.qifei.drivebehalf.client.common.enums.ProfitType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Setter
@Getter
@Entity
@Table(name = "driver_distribution")
public class DriverDistributionEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "distribution_id", unique = true, nullable = false)
    private Long distributionId;
    /**
     * 订单编号
     */
    @Column(name = "order_no")
    private String orderNo;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "amount", precision = 10, scale = 2)
    private BigDecimal amount;
    @Column(name = "profit_type")
    @Enumerated(EnumType.STRING)
    private ProfitType profitType;
}
