package com.qifei.drivebehalf.client.controller.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.OrderStateEnum;
import com.qifei.drivebehalf.client.controller.driver.vo.DriverResult;
import com.qifei.drivebehalf.client.controller.passenger.vo.PassengerResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@ApiModel(description = "订单返回")
public class OrderResult implements Serializable {

    @ApiModelProperty("订单ID")
    private Long tripOrderId;
    @ApiModelProperty("订单日期")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date createTime;
    @ApiModelProperty("乘客信息")
    private PassengerResult passenger;
    @ApiModelProperty("司机信息")
    private DriverResult driver;
    @ApiModelProperty("起点地址")
    private String startPoint;
    @ApiModelProperty("行程终点")
    private String endPoint;
    @ApiModelProperty("行程起点维度")
    private String startLng;
    @ApiModelProperty("行程起点经度")
    private String startLat;
    @ApiModelProperty("行程终点经纬度")
    private String endLng;
    @ApiModelProperty("行程终点经纬度")
    private String endLat;
    @ApiModelProperty("里程")
    private Integer mileage;
    @ApiModelProperty("行驶时长")
    private String drivingTime;
    @ApiModelProperty("预约时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date appointmentTime;
    @ApiModelProperty("订单编号")
    private String orderNumber;
    @ApiModelProperty(value = "订单状态",
            allowableValues = "待分配：ASSIGNED,\n" +
                    "再分配：REDISTRIBUTION,\n" +
                    "超时：OVERTIME,\n" +
                    "撤销：CANCEL,\n" +
                    "已接单：ACCEPTED,\n" +
                    "确认代驾：CONFIRMDRIVE,\n" +
                    "开始代驾：DRIVE,\n" +
                    "待支付：PAYMENT,\n" +
                    "支付中：PAYING,\n" +
                    "完成：COMPLETE")
    private OrderStateEnum orderStatus;
    @ApiModelProperty("取消原因")
    private String cancelReason;
    @ApiModelProperty("支付方式")
    private String payMethod;
    @ApiModelProperty("支付流水号ID")
    private String transactionId;
    @ApiModelProperty("支付时间")
    private Date payTime;
    @ApiModelProperty("行程预估价")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal estimatedPrice;
    @ApiModelProperty("实付金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal payAmount;
    @ApiModelProperty("起步价")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal startingPrice;
    @ApiModelProperty("起步公里")
    private Integer startingKilometre;
    @ApiModelProperty("里程费")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal mileageFee;
    @ApiModelProperty("等待费")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal waitingFee;
    @ApiModelProperty("返程费")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal backFee;
    @ApiModelProperty("其他费用")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal otherFee;
    @ApiModelProperty("加价倍数")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.0")
    private BigDecimal priceMultiple;
    @ApiModelProperty("优惠金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal couponAmount;
    @ApiModelProperty("优惠券ID")
    private Long couponId;
    @ApiModelProperty("佣金")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal commission;
    @ApiModelProperty("佣金比例")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    private BigDecimal commissionRatio;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.0")
    @ApiModelProperty("距离")
    private Double distance;
    @ApiModelProperty("客服热线")
    private String customerPhone;
    @ApiModelProperty("发票状态")
    private EnableEnum invoiceState = EnableEnum.NO;
    @ApiModelProperty("等待时间")
    private Integer waitingTime;
    @ApiModelProperty("等待费用")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.0")
    private BigDecimal waitingPrice = new BigDecimal(0);
}
