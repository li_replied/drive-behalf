package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.DateUtil;
import com.qifei.drivebehalf.client.common.annotation.ValidToken;
import com.qifei.drivebehalf.client.common.enums.*;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.model.PageParam;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.DistanceUtil;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.common.utils.OrderNumberUtil;
import com.qifei.drivebehalf.client.common.utils.UUIDUtils;
import com.qifei.drivebehalf.client.controller.order.vo.*;
import com.qifei.drivebehalf.client.dao.DriverTripOrderDao;
import com.qifei.drivebehalf.client.entity.*;
import com.qifei.drivebehalf.client.service.dto.DrivingResult;
import com.qifei.drivebehalf.client.service.dto.PayCallBackDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
public class DriverTripOrderService {
    @Value("${order.accept-timeout}")
    private int acceptTimeout;
    @Value("${tencent-map.key}")
    private String key;
    private final DriverBillingPriceService driverBillingPriceService;
    private final TencentMapService tencentMapService;
    private final DriverPriceIncreasedService driverPriceIncreasedService;
    private final RedisService redisService;
    private final DriverTripOrderDao driverTripOrderDao;
    private final DriverOrderStateService driverOrderStateService;
    private final NoticeService noticeService;
    private final NotificationService notificationService;
    private final DriverWithdrawService driverWithdrawService;
    private final DriverDistributionRatioService driverDistributionRatioService;
    private final KafkaProducer kafkaProducer;

    @Resource
    private RedisAtomicIntegerService redisAtomicIntegerService;

    @Async
    public void callDriver(PassengerParam param, PassengerEntity passenger, List<DriverEntity> drivers, long startTime) {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, passenger.getPhone())) {
            return;
        } else {
            redisService.setEx(RedisKeyEnum.OrderLock, passenger.getPhone(), String.valueOf(startTime));
        }
        try {
            // 等待次数，每次5秒
            int waittingCount = acceptTimeout > 60 ? acceptTimeout / 5 : 12;
            for (DriverEntity driver : drivers) {
                // 检查司机是否有进行中的订单
                long unfinished = driverTripOrderDao.countByDriverAndOrderStatusIn(driver,
                        new OrderStateEnum[]{
                                OrderStateEnum.ASSIGNED, // 已派单
                                OrderStateEnum.ACCEPTED, // 已接单
                                OrderStateEnum.DRIVE, // 送达中
                        });
                if (unfinished > 0) {
                    log.debug("司机%s有%d个订单正在处理", driver.getPhone(), unfinished);
                    continue;
                }
                // 创建订单
                DriverTripOrderEntity newOrder = addOrder(param, passenger, driver, startTime);
                // 推送司机
                notificationService.sendAssigned(newOrder);
                // 等待司机接单
                for (int i = 0; i < waittingCount; i++) {
                    log.debug("正在等待司机接单：%d，订单：%s", i, newOrder.getOrderNumber());
                    // 每5秒检查一次
                    Thread.sleep(1000 * 5);
                    newOrder = driverTripOrderDao.findByOrderNumber(newOrder.getOrderNumber());
                    if (newOrder != null) {
                        if (newOrder.getOrderStatus() == OrderStateEnum.ACCEPTED) {
                            log.debug("已接单 = 时间：%d，订单：%s，司机：%s", startTime, newOrder.getOrderNumber(), driver.getPhone());
                            driverOrderStateService.log(newOrder.getOrderNumber(), OrderStateEnum.ACCEPTED, "司机已接单");
                            return;
                        } else if (newOrder.getOrderStatus() == OrderStateEnum.REFUSE) {
                            log.debug("已拒单 = 时间：%d，订单：%s，司机：%s", startTime, newOrder.getOrderNumber(), driver.getPhone());
                            driverOrderStateService.log(newOrder.getOrderNumber(), OrderStateEnum.REFUSE, "司机已拒单");
                            return;
                        }
                    }
                }
                // 更新订单状态
                updateOrderStatus(newOrder.getOrderNumber(), OrderStateEnum.OVERTIME, "订单超时");
            }
            log.warn(drivers.size() + "名司机未响应派单，已结束");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, passenger.getPhone());
        }
    }

    public boolean isCallDriver(String phone) {
        return redisService.hasKey(RedisKeyEnum.OrderLock, phone);
    }

    // 获取最新订单
    public DriverTripOrderEntity findTopOrder(String phone) throws Exception {
        DriverTripOrderEntity order = driverTripOrderDao.findTopByPassengerPhoneOrderByCreateTimeDesc(phone);
        return order;
    }

    public DriverTripOrderEntity findAssignedOrder() {
        DriverTripOrderEntity order = driverTripOrderDao.findTopByDriverAndOrderStatus((DriverEntity) UserLoginUtils.getLogin(), OrderStateEnum.ASSIGNED);
        if (order != null) {
            order.setDriver(null);
        }
        return order;
    }

    // 今日收入
    public BigDecimal sumTodayMoney(Long userId) {
        return driverTripOrderDao.sumTodayMoney(userId);
    }

    // 今日订单
    public int countTodayOrder(Long userId) {
        return driverTripOrderDao.countTodayOrder(userId);
    }

    public List<DriverTripOrderEntity> getStartingOrders(DriverEntity driver) {
        List<DriverTripOrderEntity> list = driverTripOrderDao.findByDriverAndOrderStatusIn(driver, new OrderStateEnum[]{
                OrderStateEnum.ACCEPTED,
                OrderStateEnum.DRIVE
        });
        if (list != null && list.size() > 0) {
            list.forEach(i -> {
                i.setDriver(null);
            });
        }
        return list;
    }

    public List<DriverTripOrderEntity> getUnpaidOrders(DriverEntity driver) {
        List<DriverTripOrderEntity> list = driverTripOrderDao.findByDriverAndOrderStatusIn(driver, new OrderStateEnum[]{
                OrderStateEnum.PAYMENT,
                OrderStateEnum.PAYING
        });
        if (list != null && list.size() > 0) {
            list.forEach(i -> {
                i.setDriver(null);
            });
        }
        return list;
    }

    public List<DriverTripOrderEntity> getCancelOrders(DriverEntity driver) {
        List<DriverTripOrderEntity> list = driverTripOrderDao.findByDriverAndOrderStatusIn(driver, new OrderStateEnum[]{
                OrderStateEnum.CANCEL,
                OrderStateEnum.OVERTIME,
                OrderStateEnum.REFUSE
        });
        if (list != null && list.size() > 0) {
            list.forEach(i -> {
                i.setDriver(null);
            });
        }
        return list;
    }

    @Transactional
    public DriverTripOrderEntity addOrder(PassengerParam param, PassengerEntity passenger, DriverEntity driver, long startTime) throws Exception {
        DriverTripOrderEntity order = new DriverTripOrderEntity();
        // 生成订单号
        int id = redisAtomicIntegerService.id();
        String orderNumber = OrderNumberUtil.orderNumber(passenger.getPhone(), id);
        order.setOrderNumber(orderNumber);
        log.debug("准备派单[时间：%d，地点：%s，乘客：%s，司机：%s，订单：%s]",
                startTime, param.getStartPlace(), passenger.getPhone(), driver.getPhone(), orderNumber);
        // 指派司机
        order.setDriver(driver);
        // 设置乘客信息
        order.setAppointmentTime(new Date(startTime));
        order.setPassenger(passenger);
        order.setStartPoint(param.getStartPlace());
        order.setStartLat(String.valueOf(param.getStartLat()));
        order.setStartLng(String.valueOf(param.getStartLng()));
        order.setEndPoint(param.getStartPlace());
        order.setEndLat(String.valueOf(param.getStartLat()));
        order.setEndLng(String.valueOf(param.getStartLng()));
        // 获取所在区域价格
        DriverBillingPriceEntity driverBillingPriceEntity = driverBillingPriceService.query(
                String.valueOf(param.getStartLat()), String.valueOf(param.getStartLng()));
        Assert.notNull(driverBillingPriceEntity, "平台未设置所在区域计费策略");
        order.setCommissionRatio(driverBillingPriceEntity.getCommissionRatio()); // 佣金比例
        order.setFixDeduct(driverBillingPriceEntity.getFixDeduct()); // 固定抽成
        order.setStartingPrice(driverBillingPriceEntity.getStartingPrice()); // 起步价
        order.setStartingKilometre(driverBillingPriceEntity.getStartingKilometre()); // 起步公里
        order.setMileageFee(driverBillingPriceEntity.getMileageFee()); // 里程费
        // 等待费
        if (driverBillingPriceEntity.getWaitingFee().intValue() < 1 || driverBillingPriceEntity.getWaitingTime() < 1) {
            order.setWaitingFee(new BigDecimal(0));
        } else {
            // 计算每分钟多少钱
            order.setWaitingFee(driverBillingPriceEntity.getWaitingFee().divide(new BigDecimal(driverBillingPriceEntity.getWaitingTime())));
        }
        order.setCustomerPhone(driverBillingPriceEntity.getCustomerPhone()); // 所在区域客服
        order.setRegionId(driverBillingPriceEntity.getRegionId()); // 所在区域编号
        // 价格阶梯
        DriverPriceIncreasedEntity driverPriceIncreasedEntity = driverPriceIncreasedService
                .query(driverBillingPriceEntity.getBillId(), new Date(startTime));
        if (driverPriceIncreasedEntity != null) {
            order.setPriceMultiple(driverPriceIncreasedEntity.getPriceMultiple()); // 加价倍数
        } else {
            order.setPriceMultiple(new BigDecimal(0)); // 平台未设置所在区域加价策略
        }
        // 根据佣金比例计算佣金
        DriverDistributionRatioEntity driverDistributionRatioEntity = driverDistributionRatioService.find();
        Assert.notNull(driverDistributionRatioEntity, "平台未设置提成策略");
        if (order.getFixDeduct() != null) {
            order.setFixDeduct(order.getFixDeduct().add(driverDistributionRatioEntity.getHdRatio()));
        } else {
            order.setFixDeduct(driverDistributionRatioEntity.getHdRatio());
        }
        // 初始数据
        order.setEstimatedPrice(new BigDecimal(0)); // 预估价
        order.setPayAmount(new BigDecimal(0)); // 乘客支付价
        order.setDrivingTime("0"); // 司机送达时间
        order.setDistance(0d); // 送达距离
        order.setMileage(0);
        order.setCommission(new BigDecimal(0)); // 司机佣金
        order.setOrderStatus(OrderStateEnum.ASSIGNED);
        order.setCreate();
        order = driverTripOrderDao.saveAndFlush(order);
        driverOrderStateService.log(order.getOrderNumber(), order.getOrderStatus(), "系统派单");
        log.debug("正在派单[时间：%d，订单：%s]", startTime, orderNumber);
        return order;
    }

    @Transactional
    public void changeOrderStatus(ChangeOrderParam param) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, param.getOrderNumber())) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        } else {
            redisService.setEx(RedisKeyEnum.OrderLock, param.getOrderNumber(), param.getOrderNumber());
        }
        try {
            UserEntity driver = UserLoginUtils.getLogin();
            Assert.isTrue(driver != null, "身份验证失效");
            DriverTripOrderEntity order = driverTripOrderDao.findByOrderNumber(param.getOrderNumber());
            Assert.isTrue(order != null, "参数错误");
            Assert.isTrue(driver.getId().equals(order.getDriver().getId()), "非法请求");
            Assert.isTrue(param.getStatus() != order.getOrderStatus(), "重复请求");
            switch (order.getOrderStatus()) {
                case REFUSE:
                case OVERTIME:
                case CANCEL:
                case PAYING:
                case COMPLETE:
                    throw new IllegalArgumentException("当前状态不允许变更");
            }
            if (order.getOrderStatus() == OrderStateEnum.ACCEPTED) {
                switch (param.getStatus()) {
                    case ASSIGNED:
                    case REDISTRIBUTION:
                    case REFUSE:
                    case OVERTIME:
                        throw new IllegalArgumentException("状态变更无效");
                }
            }
            // 推送支付
            if (param.getStatus() == OrderStateEnum.PREPAY) {
                // 推单支付
                noticeService.sendConfirmDelivered(order);
                // 在线支付不修改订单状态，等待回调更新
            } else {
                // 更新订单状态
                if (param.getStatus() == OrderStateEnum.CANCEL || param.getStatus() == OrderStateEnum.REFUSE) {
                    updateOrderStatus(param.getOrderNumber(), param.getStatus(), param.getReason());
                } else {
                    updateOrderStatus(param.getOrderNumber(), param.getStatus(), null);
                }
            }
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, param.getOrderNumber());
        }
    }

    @Transactional
    public DriverTripOrderEntity uploadTrip(TripParam param) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, param.getOrderNumber())) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        } else {
            redisService.setEx(RedisKeyEnum.OrderLock, param.getOrderNumber(), param.getOrderNumber());
        }
        try {
            UserEntity driver = UserLoginUtils.getLogin();
            Assert.isTrue(driver != null, "身份验证失效");
            DriverTripOrderEntity order = driverTripOrderDao.findByOrderNumber(param.getOrderNumber());
            Assert.isTrue(order != null, "参数错误");
            Assert.isTrue(driver.getId().equals(order.getDriver().getId()), "非法请求");
            Assert.isTrue(order.getOrderStatus().equals(OrderStateEnum.DRIVE), "当前订单状态不允许结束行程");
            // 等待支付
            order.setOrderStatus(OrderStateEnum.PAYMENT);
            order.setUpdateTime(new Date());
            // 终点
            order.setEndLat(String.valueOf(param.getEndLat()));
            order.setEndLng(String.valueOf(param.getEndLng()));
            order.setEndPoint(param.getEndPlace());
            // 行驶距离和时间
            order.setMileage(param.getDistance());
            order.setDistance((double) param.getDistance());
            order.setDrivingTime(String.valueOf(param.getDuration()));
            // 等待时间和费用
            order.setWaitingTime(param.getWaitingTime());
            order.setWaitingPrice(new BigDecimal(param.getWaitingPrice()));
            // 总费用
            order.setPayAmount(new BigDecimal(param.getRealPrice()));
            // 倍率
            if (order.getPriceMultiple().doubleValue() > 0) {
                order.setPayAmount(order.getPayAmount().add(
                        order.getPayAmount().multiply(order.getPriceMultiple()))); // 加价
            }
            // 优惠
            if (order.getCouponAmount().doubleValue() > 0) {
                order.setPayAmount(order.getPayAmount().subtract(order.getCouponAmount()));
            }
            // 计算佣金
            if (order.getCommissionRatio().doubleValue() > 0) {
                order.setCommission(order.getPayAmount().subtract(order.getFixDeduct()). // 减去固定抽成
                        multiply(order.getCommissionRatio())); // 加比例
            } else {
                order.setCommission(order.getPayAmount().subtract(order.getFixDeduct())); // 减去固定抽成
            }
            driverTripOrderDao.save(order);
            // 日志
            driverOrderStateService.log(order.getOrderNumber(), OrderStateEnum.PAYMENT, "结束行程");

            return order;
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, param.getOrderNumber());
        }
    }

    @Transactional
    public void updateOrderStatus(String orderNumber, OrderStateEnum status, String reason) throws Exception {
        // 更新订单状态
        driverTripOrderDao.updateOrderState(orderNumber, status, new Date(), reason);
        driverOrderStateService.log(orderNumber, status, reason);
        log.debug("订单状态变更[时间：%d，订单：%s，状态：%s]", System.currentTimeMillis(), orderNumber, status);
    }

    /**********************************老版本*****************************/
    /**
     * 计算金额
     *
     * @param param
     * @return
     * @throws Exception
     */
    public SumMoneyResult sumMoney(SumMoneyParam param) throws Exception {
        // 初始化
        SumMoneyResult currentSumMoney = new SumMoneyResult();
        currentSumMoney.setDatetime(param.getDatetime());
        currentSumMoney.setStartLat(param.getStartLat());
        currentSumMoney.setStartLng(param.getStartLng());
        currentSumMoney.setEndLat(param.getEndLat());
        currentSumMoney.setEndLng(param.getEndLng());
        currentSumMoney.setStartPoint(param.getStartPoint());
        currentSumMoney.setEndPoint(param.getEndPoint());
        currentSumMoney.setMultiple(new BigDecimal(0));
        currentSumMoney.setMultipleFee(new BigDecimal(0));
        // 先判断是否第一次行程
        SumMoneyResult lastSumMoney = getCacheSumMoneyResult(param.getOrderNumber());
        if (lastSumMoney != null) {
            // 中转行程
            currentSumMoney.setTransferCount(lastSumMoney.getTransferCount() + 1);
            currentSumMoney.setUuid(lastSumMoney.getUuid());
            currentSumMoney.setChargedMileage(lastSumMoney.getChargedMileage()); // 更新上一次的里程
            currentSumMoney.setDistance(lastSumMoney.getDistance());
            currentSumMoney.setDuration(lastSumMoney.getDuration());
            // 显示中转前的起点
            currentSumMoney.setEndPoint(currentSumMoney.getEndPoint() + "（中转：" + lastSumMoney.getEndPoint() + "）");
        } else {
            currentSumMoney.setTransferCount(1); // 第一次行程
            currentSumMoney.setUuid(UUIDUtils.uuid()); // 生成缓存key
            currentSumMoney.setDuration("0");
        }
        // 获取所在区域价格
        DriverBillingPriceEntity driverBillingPriceEntity = driverBillingPriceService.query(param.getStartLat(), param.getStartLng());
        Assert.notNull(driverBillingPriceEntity, "价格未设置。");
        currentSumMoney.setCustomerPhone(driverBillingPriceEntity.getCustomerPhone());
        currentSumMoney.setCommissionRatio(driverBillingPriceEntity.getCommissionRatio());
        currentSumMoney.setRegionId(driverBillingPriceEntity.getRegionId());
        // 计算行程
        distance(param, currentSumMoney);
        // 计算价格
        valuation(driverBillingPriceEntity, currentSumMoney);
        // 价格阶梯
        DriverPriceIncreasedEntity driverPriceIncreasedEntity = driverPriceIncreasedService
                .query(driverBillingPriceEntity.getBillId(), DateUtil.parse(param.getDatetime(), "yyyy/MM/dd HH:mm"));
        if (driverPriceIncreasedEntity != null) {
            currentSumMoney.setMultiple(driverPriceIncreasedEntity.getPriceMultiple());
            currentSumMoney.setReason(driverPriceIncreasedEntity.getIncreaseReason());
            BigDecimal total = currentSumMoney.getTotal();
            BigDecimal multiply = total.multiply(driverPriceIncreasedEntity.getPriceMultiple());
            currentSumMoney.setMileageFee(multiply.subtract(total));
            currentSumMoney.setTotal(multiply);
        }
        // 根据佣金比例计算佣金
        DriverDistributionRatioEntity driverDistributionRatioEntity = driverDistributionRatioService.find();
        Assert.notNull(driverDistributionRatioEntity, "佣金比例未设置。");
        BigDecimal hdRatio = driverDistributionRatioEntity.getHdRatio() != null ? driverDistributionRatioEntity.getHdRatio() : new BigDecimal(0);
        BigDecimal commission = currentSumMoney.getTotal().subtract(hdRatio).multiply(driverBillingPriceEntity.getCommissionRatio());
        currentSumMoney.setCommission(commission);
        // 缓存更新
        if (currentSumMoney.getTransferCount() > 1) {
            // 司机端
            redisService.setEx(RedisKeyEnum.SumMoneyInfo, param.getOrderNumber(), JsonUtils.writeValue(currentSumMoney));
        } else {
            // 乘客端
            redisService.setEx(RedisKeyEnum.SumMoneyInfo, currentSumMoney.getUuid(), JsonUtils.writeValue(currentSumMoney));
        }
        return currentSumMoney;
    }

    @Transactional
    public void transferMoney(String orderNumber) throws Exception {
        // 先判断是否第一次行程
        SumMoneyResult currentSumMoney = getCacheSumMoneyResult(orderNumber);
        Assert.notNull(currentSumMoney, "中转异常。");
        Assert.isTrue(currentSumMoney.getTransferCount() > 1, "接口调用错误。");
        // 更新订单信息
        driverTripOrderDao.updateTransferOrder(
                orderNumber,
                currentSumMoney.getEndLng(),
                currentSumMoney.getEndLat(),
                currentSumMoney.getEndPoint(),
                currentSumMoney.getDistance(),
                currentSumMoney.getDuration(),
                currentSumMoney.getTotal(),
                currentSumMoney.getCommission());
    }

    /**
     * 计算金额
     *
     * @param param
     * @return
     * @throws Exception
     */
    public SumMoneyResult showMoney(SumMoneyParam param) throws Exception {
        DriverBillingPriceEntity driverBillingPriceEntity = driverBillingPriceService.query(param.getStartLat(), param.getStartLng());
        Assert.notNull(driverBillingPriceEntity, "价格未设置。");
        SumMoneyResult sumMoneyResult = new SumMoneyResult();
        distance(param, sumMoneyResult);
        int distance = sumMoneyResult.getDistance();
        if (distance < 1000) {
            distance = 1000;
        }
        distance = Double.valueOf(Math.ceil(distance / 1000D)).intValue();
        valuation(driverBillingPriceEntity, distance, sumMoneyResult);
        sumMoneyResult.setDatetime(param.getDatetime());
        sumMoneyResult.setStartLat(param.getStartLat());
        sumMoneyResult.setStartLng(param.getStartLng());
        sumMoneyResult.setEndLat(param.getEndLat());
        sumMoneyResult.setEndLng(param.getEndLng());
        sumMoneyResult.setStartPoint(param.getStartPoint());
        sumMoneyResult.setEndPoint(param.getEndPoint());
        sumMoneyResult.setDistance(distance);
        sumMoneyResult.setMultiple(new BigDecimal(0));
        sumMoneyResult.setMultipleFee(new BigDecimal(0));
        sumMoneyResult.setCustomerPhone(driverBillingPriceEntity.getCustomerPhone());
        sumMoneyResult.setCommissionRatio(driverBillingPriceEntity.getCommissionRatio());
        sumMoneyResult.setRegionId(driverBillingPriceEntity.getRegionId());
        DriverPriceIncreasedEntity driverPriceIncreasedEntity = driverPriceIncreasedService
                .query(driverBillingPriceEntity.getBillId(), DateUtil.parse(param.getDatetime(), "yyyy/MM/dd HH:mm"));
        if (driverPriceIncreasedEntity != null) {
            sumMoneyResult.setMultiple(driverPriceIncreasedEntity.getPriceMultiple());
            sumMoneyResult.setReason(driverPriceIncreasedEntity.getIncreaseReason());
            BigDecimal total = sumMoneyResult.getTotal();
            BigDecimal multiply = total.multiply(driverPriceIncreasedEntity.getPriceMultiple());
            sumMoneyResult.setMileageFee(multiply.subtract(total));
            sumMoneyResult.setTotal(multiply);
        }
        sumMoneyResult.setUuid(UUIDUtils.uuid());
        DriverDistributionRatioEntity driverDistributionRatioEntity = driverDistributionRatioService.find();
        BigDecimal hdRatio = driverDistributionRatioEntity.getHdRatio() != null ? driverDistributionRatioEntity.getHdRatio() : new BigDecimal(0);
        //佣金比例
        BigDecimal commission = sumMoneyResult.getTotal().subtract(hdRatio).multiply(driverBillingPriceEntity.getCommissionRatio());
        sumMoneyResult.setCommission(commission);
        redisService.setEx(RedisKeyEnum.SumMoneyInfo, sumMoneyResult.getUuid(), JsonUtils.writeValue(sumMoneyResult));
        return sumMoneyResult;
    }

    /**
     * 下单
     *
     * @param uuid
     * @return
     * @throws Exception
     */
    @Transactional(rollbackOn = Exception.class)
    public PlaceOrderResult placeOrder(String uuid) throws Exception {
        PlaceOrderResult result = new PlaceOrderResult();
        String sumMoneyStr = redisService.getKey(RedisKeyEnum.SumMoneyInfo, uuid);
        if (StringUtils.isEmpty(sumMoneyStr)) {
            result.setReset(true);
            return result;
        }
        PassengerEntity userEntity = (PassengerEntity) UserLoginUtils.getLogin();
        long count = driverTripOrderDao.countByPassengerAndOrderStatusIn(userEntity, new OrderStateEnum[]{
                OrderStateEnum.ASSIGNED, OrderStateEnum.REDISTRIBUTION, OrderStateEnum.ACCEPTED, OrderStateEnum.CONFIRMDRIVE, OrderStateEnum.DRIVE, OrderStateEnum.PAYMENT, OrderStateEnum.PAYING
        });
        Assert.isTrue(count == 0L, "您有未完成的订单，请完成订单后再下单。");
        SumMoneyResult sumMoneyResult = JsonUtils.readValue(sumMoneyStr, SumMoneyResult.class);
        DriverTripOrderEntity driverTripOrderEntity = new DriverTripOrderEntity();
        driverTripOrderEntity.setAppointmentTime(DateUtil.parse(sumMoneyResult.getDatetime(), "yyyy/MM/dd HH:mm"));
        driverTripOrderEntity.setPassenger(userEntity);
        driverTripOrderEntity.setStartPoint(sumMoneyResult.getStartPoint());
        driverTripOrderEntity.setStartLat(sumMoneyResult.getStartLat());
        driverTripOrderEntity.setStartLng(sumMoneyResult.getStartLng());
        driverTripOrderEntity.setEndPoint(sumMoneyResult.getEndPoint());
        driverTripOrderEntity.setEndLat(sumMoneyResult.getEndLat());
        driverTripOrderEntity.setEndLng(sumMoneyResult.getEndLng());
        int id = redisAtomicIntegerService.id();
        String orderNumber = OrderNumberUtil.orderNumber(userEntity.getPhone(), id);
        driverTripOrderEntity.setOrderNumber(orderNumber);
        driverTripOrderEntity.setMileage(sumMoneyResult.getDistance());
        driverTripOrderEntity.setEstimatedPrice(sumMoneyResult.getTotal());
        driverTripOrderEntity.setStartingPrice(sumMoneyResult.getStartingPrice());
        driverTripOrderEntity.setMileageFee(sumMoneyResult.getMileageFee());
        driverTripOrderEntity.setOrderStatus(OrderStateEnum.ASSIGNED);
        driverTripOrderEntity.setPriceMultiple(sumMoneyResult.getMultiple());
        driverTripOrderEntity.setPayAmount(sumMoneyResult.getTotal());
        driverTripOrderEntity.setDrivingTime(sumMoneyResult.getDuration());
        driverTripOrderEntity.setCommission(sumMoneyResult.getCommission());
        driverTripOrderEntity.setCommissionRatio(sumMoneyResult.getCommissionRatio());
        driverTripOrderEntity.setCustomerPhone(sumMoneyResult.getCustomerPhone());
        driverTripOrderEntity.setRegionId(sumMoneyResult.getRegionId());
        driverTripOrderEntity.setCreate();
        driverTripOrderDao.save(driverTripOrderEntity);
        driverOrderStateService.log(driverTripOrderEntity.getOrderNumber(), driverTripOrderEntity.getOrderStatus(), null);
        result.setOrderNumber(driverTripOrderEntity.getOrderNumber());
        result.setOrderId(driverTripOrderEntity.getTripOrderId());
        String lng = driverTripOrderEntity.getStartLng();
        String lat = driverTripOrderEntity.getStartLat();
        //保存经纬度
        redisService.geoAdd(RedisKeyEnum.OrderPoint, Double.valueOf(lng), Double.valueOf(lat), driverTripOrderEntity.getOrderNumber());
        redisService.delKey(RedisKeyEnum.SumMoneyInfo, uuid);
        redisService.setKey(RedisKeyEnum.ContestOrder, driverTripOrderEntity.getOrderNumber(), driverTripOrderEntity.getOrderNumber());
        // 缓存订单金额信息，用于中转使用
        redisService.setEx(RedisKeyEnum.SumMoneyInfo, orderNumber, sumMoneyStr);
        try {
            noticeService.sendAssigned(driverTripOrderEntity);
        } catch (Exception e) {
            log.error("推送消息错误", e);
        }
        return result;
    }

    /**
     * 我的订单
     *
     * @param param
     * @return
     * @throws Exception
     */
    public Page<DriverTripOrderEntity> list(PageParam param) throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        Pageable pageable = PageRequest.of(param.getPageNum() - 1, param.getPageSize());
        if (UserType.CLIENT == user.getUserType()) {
            return driverTripOrderDao.queryAllByPassengerOrderByCreateTimeDesc((PassengerEntity) user, pageable);
        }
        if (UserType.DRIVER == user.getUserType()) {
            Page<DriverTripOrderEntity> page = driverTripOrderDao.queryAllByDriverOrderByCreateTimeDesc((DriverEntity) user, pageable);
            List<DriverTripOrderEntity> list = page.getContent();
            if (list != null && !list.isEmpty()) {
                for (DriverTripOrderEntity driverTripOrderEntity : list) {
                    PassengerEntity passengerEntity = driverTripOrderEntity.getPassenger();
                    if (EnableEnum.NO == passengerEntity.getNameShow()) {
                        String name = passengerEntity.getName();
                        name = name.substring(0, 1) + "**";
                        passengerEntity.setName(name);
                    }
                }
            }
            return page;
        }
        return null;
    }

    /**
     * 订单详情
     *
     * @param orderNumber
     * @return
     * @throws Exception
     */
    public DriverTripOrderEntity info(String orderNumber) throws Exception {
        DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
        if (driverTripOrderEntity != null) {
            UserEntity userEntity = UserLoginUtils.getLogin();
            if (UserType.CLIENT == userEntity.getUserType()) {
                return driverTripOrderEntity;
            }
            PassengerEntity passengerEntity = driverTripOrderEntity.getPassenger();
            if (EnableEnum.NO == passengerEntity.getNameShow()) {
                String name = passengerEntity.getName();
                name = name.substring(0, 1) + "**";
                passengerEntity.setName(name);
            }
        }
        return driverTripOrderEntity;
    }

    public DriverTripOrderEntity find(String orderNumber) throws Exception {
        return driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
    }

    /**
     * 乘客取消订单
     *
     * @param param
     * @throws Exception
     */
    @Transactional(rollbackOn = Exception.class)
    public void cancel(CancelParam param) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, param.getOrderNumber())) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, param.getOrderNumber());
            UserEntity user = UserLoginUtils.getLogin();
            if (UserType.CLIENT != user.getUserType()) {
                throw new ServiceWorkingException("账号类型不正确。");
            }
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(param.getOrderNumber());
            Assert.notNull(driverTripOrderEntity, "订单不存在。");


            if (OrderStateEnum.ASSIGNED != driverTripOrderEntity.getOrderStatus()
                    && OrderStateEnum.REDISTRIBUTION != driverTripOrderEntity.getOrderStatus()
                    && OrderStateEnum.ACCEPTED != driverTripOrderEntity.getOrderStatus()
                    && OrderStateEnum.CONFIRMDRIVE != driverTripOrderEntity.getOrderStatus()
            ) {
                throw new ServiceWorkingException("订单不能取消。");
            }
            driverTripOrderDao.updateOrderState(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.CANCEL,
                    param.getReason()
            );
            driverOrderStateService.log(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.CANCEL,
                    param.getReason()
            );
            redisService.geoRemove(RedisKeyEnum.OrderPoint, driverTripOrderEntity.getOrderNumber());
            redisService.setKey(RedisKeyEnum.ContestOrder, driverTripOrderEntity.getOrderNumber(), driverTripOrderEntity.getOrderNumber());
            try {
                noticeService.sendCancel(driverTripOrderEntity);
            } catch (Exception e) {
                log.error("推送消息错误", e);
            }
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, param.getOrderNumber());
        }
    }

    public DriverTripOrderEntity queryConfirm() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.isTrue(UserType.CLIENT == userEntity.getUserType(), "账号类型不正确。");
        return driverTripOrderDao.findTopByPassengerAndOrderStatusIn((PassengerEntity) userEntity, new OrderStateEnum[]{
                OrderStateEnum.ASSIGNED, OrderStateEnum.REDISTRIBUTION, OrderStateEnum.ACCEPTED, OrderStateEnum.CONFIRMDRIVE, OrderStateEnum.DRIVE, OrderStateEnum.PAYMENT, OrderStateEnum.PAYING
        });
    }

    public long countConfirm() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.isTrue(UserType.CLIENT == userEntity.getUserType(), "账号类型不正确。");
        return driverTripOrderDao.countByPassengerAndOrderStatusIn((PassengerEntity) userEntity, new OrderStateEnum[]{
                OrderStateEnum.ASSIGNED, OrderStateEnum.REDISTRIBUTION, OrderStateEnum.ACCEPTED, OrderStateEnum.CONFIRMDRIVE, OrderStateEnum.DRIVE, OrderStateEnum.PAYMENT, OrderStateEnum.PAYING
        });
    }

    @Transactional(rollbackOn = Exception.class)
    public void confirm(String orderNumber) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, orderNumber)) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, orderNumber);
            UserEntity userEntity = UserLoginUtils.getLogin();
            Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
            Assert.notNull(driverTripOrderEntity, "订单不存在。");
            Assert.isTrue(
                    driverTripOrderEntity.getOrderStatus() == OrderStateEnum.CONFIRMDRIVE,
                    "订单状态不正确。"
            );

            driverTripOrderDao.updateOrderState(orderNumber, OrderStateEnum.DRIVE);
            driverOrderStateService.log(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.DRIVE,
                    null);
            noticeService.sendDrive(driverTripOrderEntity);
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, orderNumber);
        }
    }

    @Transactional
    public void complete(String orderNumber) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, orderNumber)) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, orderNumber);
            UserEntity userEntity = UserLoginUtils.getLogin();
            Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
            Assert.notNull(driverTripOrderEntity, "订单不存在。");
            Assert.isTrue(
                    driverTripOrderEntity.getOrderStatus() == OrderStateEnum.PAYMENT,
                    "订单状态不正确。"
            );

            driverTripOrderDao.updatePayType(orderNumber, OrderStateEnum.COMPLETE, PayType.app.name(), new Date(), PayMethod.MONEY.name());
            driverOrderStateService.log(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.COMPLETE,
                    null);
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, orderNumber);
        }
    }

    public void updatePayId(String orderNumber, String payId) throws Exception {
        driverTripOrderDao.updatePayId(orderNumber, payId);
    }

    /**
     * 更新支付状态
     *
     * @param orderNumber
     * @param payTime
     * @param transactionId
     * @param pay
     * @throws Exception
     */
    @Transactional(rollbackOn = Exception.class)
    public void updatePay(String orderNumber, Date payTime, String transactionId, EnableEnum pay) throws Exception {
        DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
        Assert.notNull(driverTripOrderEntity, "订单不存在");
        OrderStateEnum stateEnum = OrderStateEnum.PAYMENT;
        if (EnableEnum.YES == pay) {
            stateEnum = OrderStateEnum.COMPLETE;
        }
        driverTripOrderDao.updatePayState(orderNumber, payTime, transactionId, stateEnum);
        driverOrderStateService.log(
                driverTripOrderEntity.getOrderNumber(),
                stateEnum,
                null
        );
        if (EnableEnum.YES == pay) {
            driverWithdrawService.income(
                    driverTripOrderEntity.getOrderNumber(),
                    driverTripOrderEntity.getDriver().getId(),
                    driverTripOrderEntity.getCommission()
            );
            noticeService.sendPayment(driverTripOrderEntity);
            kafkaProducer.share(orderNumber, driverTripOrderEntity.getPayAmount());
        }
    }

    @Transactional
    public void payCallBack(PayCallBackDto payCallBackDto) throws Exception {
        //锁订单
        while (redisService.hasKey(RedisKeyEnum.OrderLock, payCallBackDto.getOrderNumber())) {
            Thread.sleep(200);
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, payCallBackDto.getOrderNumber(), payCallBackDto.getOrderNumber());
            updatePay(
                    payCallBackDto.getOrderNumber(),
                    payCallBackDto.getSuccessTime(),
                    payCallBackDto.getTransactionId(),
                    payCallBackDto.getPayState()
            );
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, payCallBackDto.getOrderNumber());
        }
    }

    public void cancelPay(String orderNumber) throws Exception {
        driverTripOrderDao.updateOrderState(orderNumber, OrderStateEnum.PAYMENT);
    }

    /**
     * 更新支付类型
     *
     * @param orderNumber
     * @param orderState
     * @param payType
     * @throws Exception
     */
    public void updatePayType(String orderNumber, OrderStateEnum orderState, String payType, String payMethod) throws Exception {
        driverTripOrderDao.updatePayType(orderNumber, orderState, payType, new Date(), payMethod);
    }

    /**
     * 查询支付中订单
     *
     * @return
     * @throws Exception
     */
    public List<DriverTripOrderEntity> paying() throws Exception {
        Date now = new Date();
        Date begin = DateUtil.offsetHour(now, -24);
        Date end = now;
        return driverTripOrderDao.queryByOrderStatusAndTransactionDateBetween(OrderStateEnum.PAYING, begin, end);
    }

    /**
     * 查看待分配订单
     *
     * @return
     * @throws Exception
     */
    public List<DriverTripOrderEntity> assigned(String lat, String lng) throws Exception {
        //String regionId = driverBillingPriceService.queryRegionId(lat, lng);
        double latD = Double.valueOf(lat);
        double lngD = Double.valueOf(lng);
        GeoResults<RedisGeoCommands.GeoLocation<String>> results = redisService.geoRadius(RedisKeyEnum.OrderPoint, lngD, latD);
        List<GeoResult<RedisGeoCommands.GeoLocation<String>>> geoResults = results.getContent();
        if (geoResults == null || geoResults.isEmpty()) {
            return null;
        }
        String[] orders = new String[geoResults.size()];
        for (int i = 0; i < geoResults.size(); i++) {
            GeoResult<RedisGeoCommands.GeoLocation<String>> geoResult = geoResults.get(i);
            orders[i] = geoResult.getContent().getName();
        }
        List<DriverTripOrderEntity> list = driverTripOrderDao.queryAllByOrderNumberIn(
                orders
        );

        if (list != null && !list.isEmpty()) {
            for (DriverTripOrderEntity driverTripOrderEntity : list) {
                PassengerEntity passengerEntity = driverTripOrderEntity.getPassenger();
                if (EnableEnum.NO == passengerEntity.getNameShow()) {
                    String name = passengerEntity.getName();
                    name = name.substring(0, 1) + "**";
                    passengerEntity.setName(name);
                }
                double latE = Double.valueOf(driverTripOrderEntity.getStartLat());
                double lngE = Double.valueOf(driverTripOrderEntity.getStartLng());
                BigDecimal distance = DistanceUtil.getDistance(latD, lngD, latE, lngE);
                driverTripOrderEntity.setDistance(distance.doubleValue());
            }
            Collections.sort(list, new Comparator<DriverTripOrderEntity>() {//根据距离排序
                public int compare(DriverTripOrderEntity o1, DriverTripOrderEntity o2) {
                    return o1.getDistance().compareTo(o2.getDistance());
                }
            });
        }
        return list;
    }

    /**
     * 抢单
     *
     * @param orderNumber
     * @throws Exception
     */
    @Transactional(rollbackOn = Exception.class)
    public void contest(String orderNumber) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, orderNumber)) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, orderNumber);
            UserEntity userEntity = UserLoginUtils.getLogin();
            Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
            long count = driverTripOrderDao.countByDriverAndOrderStatusIn((DriverEntity) userEntity, new OrderStateEnum[]{
                    OrderStateEnum.ACCEPTED, OrderStateEnum.CONFIRMDRIVE, OrderStateEnum.DRIVE
            });
            Assert.isTrue(count < 2, "抢单个数超出上限。");
            Assert.isTrue(redisService.delKey(RedisKeyEnum.ContestOrder, orderNumber), "订单已被抢。");
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
            Assert.notNull(driverTripOrderEntity, "订单不存在。");
            if (OrderStateEnum.ASSIGNED != driverTripOrderEntity.getOrderStatus() && OrderStateEnum.REDISTRIBUTION != driverTripOrderEntity.getOrderStatus()) {
                throw new ServiceWorkingException("订单已被抢。");
            }
            try {
                driverTripOrderEntity.setDriver((DriverEntity) userEntity);
                driverTripOrderDao.updateDriverAndOrderStatus(orderNumber, OrderStateEnum.ACCEPTED, (DriverEntity) userEntity);
                driverOrderStateService.log(
                        driverTripOrderEntity.getOrderNumber(),
                        OrderStateEnum.ACCEPTED,
                        null
                );
                redisService.geoRemove(RedisKeyEnum.OrderPoint, orderNumber);
                try {
                    noticeService.sendAccepted(driverTripOrderEntity);
                } catch (Exception e) {
                    log.error("发送消息错误", e);
                }
            } catch (Exception e) {
                redisService.setKey(RedisKeyEnum.ContestOrder, orderNumber, orderNumber);
                throw e;
            }
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, orderNumber);
        }
    }

    /**
     * 结束代驾
     *
     * @param orderNumber
     * @throws Exception
     */
    @Transactional(rollbackOn = Exception.class)
    public void delivered(String orderNumber) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, orderNumber)) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, orderNumber);
            UserEntity userEntity = UserLoginUtils.getLogin();
            Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
            Assert.notNull(driverTripOrderEntity, "订单不存在。");
            Assert.isTrue(OrderStateEnum.DRIVE == driverTripOrderEntity.getOrderStatus(), "订单状态不正确");
            driverTripOrderDao.updateOrderState(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.PAYMENT
            );
            driverOrderStateService.log(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.PAYMENT,
                    null
            );
            redisService.delKey(RedisKeyEnum.SumMoneyInfo, orderNumber); // 删除中转缓存
            try {
                noticeService.sendConfirmDelivered(driverTripOrderEntity);
            } catch (Exception e) {
                log.error("发送消息错误", e);
            }
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, orderNumber);
        }
    }

    /**
     * 开始代驾
     *
     * @param orderNumber
     * @throws Exception
     */
    @Transactional(rollbackOn = Exception.class)
    public void drive(String orderNumber) throws Exception {
        if (redisService.hasKey(RedisKeyEnum.OrderLock, orderNumber)) {
            throw new ServiceWorkingException("订单正在操作中，请稍后再试。");
        }
        try {
            redisService.setEx(RedisKeyEnum.OrderLock, orderNumber);
            UserEntity userEntity = UserLoginUtils.getLogin();
            Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
            DriverTripOrderEntity driverTripOrderEntity = driverTripOrderDao.queryDriverTripOrderEntityByOrderNumber(orderNumber);
            Assert.notNull(driverTripOrderEntity, "订单不存在。");
            Assert.isTrue(OrderStateEnum.ACCEPTED == driverTripOrderEntity.getOrderStatus(), "订单状态不正确");
            driverTripOrderDao.updateOrderState(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.CONFIRMDRIVE
            );
            driverOrderStateService.log(
                    driverTripOrderEntity.getOrderNumber(),
                    OrderStateEnum.CONFIRMDRIVE,
                    null
            );
            try {
                noticeService.sendConfirmDrive(driverTripOrderEntity);
            } catch (Exception e) {
                log.error("发送消息错误", e);
            }
        } finally {
            redisService.delKey(RedisKeyEnum.OrderLock, orderNumber);
        }
    }

    public void updateInvoice(String orderNumber, EnableEnum enableEnum) throws Exception {
        driverTripOrderDao.updateInvoice(orderNumber, enableEnum);
    }

    public List<DriverTripOrderEntity> carryon() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
        List<DriverTripOrderEntity> list = driverTripOrderDao.findByDriverAndOrderStatusIn((DriverEntity) userEntity, new OrderStateEnum[]{
                OrderStateEnum.ACCEPTED, OrderStateEnum.CONFIRMDRIVE, OrderStateEnum.DRIVE, OrderStateEnum.PAYMENT
        });
        return list;
    }

    public long countCarryon() throws Exception {
        UserEntity userEntity = UserLoginUtils.getLogin();
        Assert.isTrue(UserType.DRIVER == userEntity.getUserType(), "账号类型不正确。");
        return driverTripOrderDao.countByDriverAndOrderStatusIn((DriverEntity) userEntity, new OrderStateEnum[]{
                OrderStateEnum.ACCEPTED, OrderStateEnum.CONFIRMDRIVE, OrderStateEnum.DRIVE
        });
    }

    public List<DriverTripOrderEntity> overtime() throws Exception {
        return driverTripOrderDao.findByAppointmentTimeBeforeAndOrderStatusIn(new Date(), new OrderStateEnum[]{
                OrderStateEnum.ASSIGNED, OrderStateEnum.REDISTRIBUTION
        });
    }

    @Transactional(rollbackOn = Exception.class)
    public void updateOvertime(String orderNumber) throws Exception {
        redisService.geoRemove(RedisKeyEnum.OrderPoint, orderNumber);
        driverTripOrderDao.updateOrderState(orderNumber, OrderStateEnum.OVERTIME);
    }

    private void distance(SumMoneyParam param, SumMoneyResult sumMoneyResult) throws Exception {
        String from = String.format("%s,%s", param.getStartLat(), param.getStartLng());
        String to = String.format("%s,%s", param.getEndLat(), param.getEndLng());
        String policy = "LEAST_TIME";
        String cartype = "1";
        String getMp = "0";
        String noStep = "0";
        // 调用腾讯地图服务端接口，获取行驶数据
        DrivingResult drivingResult = tencentMapService.driving(from, to, policy, cartype, getMp, noStep, key);
        if (0 != drivingResult.getStatus() || drivingResult.getResult().getRoutes() == null || drivingResult.getResult().getRoutes().isEmpty()) {
            throw new ServiceWorkingException("计算里程错误。");
        }
        // 方案总距离
        int meters = drivingResult.getResult().getRoutes().get(0).getDistance();
        if (meters < 1000) {
            meters = 1000; // 小于1公里按1公里计算
        }
        // 取整获取公里数
        sumMoneyResult.setDistance(sumMoneyResult.getDistance() + Double.valueOf(Math.ceil(meters / 1000D)).intValue());
        // 方案估算时间（结合路况），单位：分钟
        int minutes = drivingResult.getResult().getRoutes().get(0).getDuration();
        sumMoneyResult.setDuration(String.valueOf(Integer.parseInt(sumMoneyResult.getDuration()) + minutes)); // 累加上一次
    }

    private void valuation(DriverBillingPriceEntity driverBillingPriceEntity, SumMoneyResult sumMoneyResult) throws Exception {
        // 初始化
        int startingKilometre = driverBillingPriceEntity.getStartingKilometre(); // 获取后台起步公里
        BigDecimal mileageFee = driverBillingPriceEntity.getMileageFee(); // 获取后台每公里价格
        BigDecimal startingPrice = driverBillingPriceEntity.getStartingPrice(); // 获取后台起步价格
        // 减去起步公里
        int realDistance = sumMoneyResult.getDistance() - startingKilometre;
        if (realDistance > 0) {
            sumMoneyResult.setChargedMileage(realDistance); // 收费里程
            sumMoneyResult.setMileageFee(mileageFee.multiply(new BigDecimal(realDistance))); // 减去初始起步公里，得到实际价格
            sumMoneyResult.setTotal(startingPrice.add(sumMoneyResult.getMileageFee())); // 计算总价
        } else {
            // 起步价
            sumMoneyResult.setChargedMileage(0);
            sumMoneyResult.setMileageFee(new BigDecimal(0));
            sumMoneyResult.setTotal(startingPrice);
        }
        // 起步价
        sumMoneyResult.setStartingPrice(startingPrice);
        // 起步公里
        sumMoneyResult.setStartingKilometre(startingKilometre);
        // 每公里价格
        sumMoneyResult.setMileageFee(mileageFee);
    }

    private void valuation(DriverBillingPriceEntity driverBillingPriceEntity, int distance, SumMoneyResult sumMoneyResult) throws Exception {
        //起步价
        int startingKilometre = driverBillingPriceEntity.getStartingKilometre();
        BigDecimal total = driverBillingPriceEntity.getStartingPrice();
        int surplus = distance - startingKilometre;
        sumMoneyResult.setStartingPrice(total);
        sumMoneyResult.setMultiple(new BigDecimal(0));
        if (surplus <= 0) {
            sumMoneyResult.setStartingKilometre(startingKilometre);
            sumMoneyResult.setMileageFee(new BigDecimal(0));
            sumMoneyResult.setChargedMileage(0);
            sumMoneyResult.setTotal(total);
            return;
        }
        //里程价
        BigDecimal mileageFee = driverBillingPriceEntity.getMileageFee();
        BigDecimal price = mileageFee.multiply(new BigDecimal(surplus));
        sumMoneyResult.setStartingKilometre(startingKilometre);
        sumMoneyResult.setMileageFee(price);
        sumMoneyResult.setChargedMileage(surplus);
        sumMoneyResult.setTotal(total.add(price));
    }

    private SumMoneyResult getCacheSumMoneyResult(String orderNumber) throws Exception {
        if (StringUtils.isNotEmpty(orderNumber)) {
            String sumMoneyStr = redisService.getKey(RedisKeyEnum.SumMoneyInfo, orderNumber);
            if (StringUtils.isNotEmpty(sumMoneyStr)) {
                SumMoneyResult sumMoneyResult = JsonUtils.readValue(sumMoneyStr, SumMoneyResult.class);
                Assert.notNull(sumMoneyResult, "计算金额缓存异常。");
                return sumMoneyResult;
            }
        }
        return null;
    }
}
