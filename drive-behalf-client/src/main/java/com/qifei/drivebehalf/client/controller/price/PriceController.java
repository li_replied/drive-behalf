package com.qifei.drivebehalf.client.controller.price;

import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.price.vo.PriceQueryParam;
import com.qifei.drivebehalf.client.controller.price.vo.PriceResult;
import com.qifei.drivebehalf.client.entity.DriverBillingPriceEntity;
import com.qifei.drivebehalf.client.service.DriverBillingPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "客户端：收费标准")
@RequestMapping("/price")
@RequiredArgsConstructor
public class PriceController {
    private final DriverBillingPriceService driverBillingPriceService;

    @ApiOperation("根据地区名称查询收费标准")
    @PostMapping()
    public ApiResponse<List<PriceResult>> query(@RequestBody @Validated PriceQueryParam param) throws Exception {
        List<DriverBillingPriceEntity> driverBillingPriceEntity = driverBillingPriceService.query(param);
        List<PriceResult> result = BeanCopyUtils.copyProperties(driverBillingPriceEntity, PriceResult.class);
        return ApiResponse.createBySuccess(result);
    }

    @GetMapping("/{regionId}")
    public ApiResponse<PriceResult> query(
            @PathVariable("regionId")
                    String regionId
    ) throws Exception {
        DriverBillingPriceEntity driverBillingPriceEntity = driverBillingPriceService.query(regionId);
        PriceResult result = BeanCopyUtils.copyProperties(driverBillingPriceEntity, PriceResult.class);
        return ApiResponse.createBySuccess(result);
    }

}
