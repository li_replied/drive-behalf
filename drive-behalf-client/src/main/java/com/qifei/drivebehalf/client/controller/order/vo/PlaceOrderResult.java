package com.qifei.drivebehalf.client.controller.order.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@ApiModel(description = "下单返回")
public class PlaceOrderResult implements Serializable {
    @ApiModelProperty("订单编号")
    private String orderNumber;
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("重新计算金额")
    private boolean reset = false;
}
