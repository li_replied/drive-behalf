package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Geocoder implements Serializable {
    private String address;
    @JsonProperty("ad_info")
    private AdInfo adInfo;
}
