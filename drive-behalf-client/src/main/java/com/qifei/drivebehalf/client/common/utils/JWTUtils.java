package com.qifei.drivebehalf.client.common.utils;

import com.qifei.drivebehalf.client.common.enums.UserType;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;

import java.util.Date;

public class JWTUtils {
    /**
     * 生成token
     *
     * @param userId
     * @param secret
     * @return
     */
    public static String compact(String userId, UserType userType, String secret) {
        Claims claims = new DefaultClaims();
        claims.setIssuedAt(new Date());
        claims.setId(userId);
        claims.setSubject(userType.name());
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public static Claims parser(String token, String secret) throws Exception {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }
}
