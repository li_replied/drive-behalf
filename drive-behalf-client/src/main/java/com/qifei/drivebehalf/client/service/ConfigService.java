package com.qifei.drivebehalf.client.service;

import com.qifei.drivebehalf.client.dao.ConfigDao;
import com.qifei.drivebehalf.client.entity.ConfigEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConfigService {
    private final ConfigDao configDao;

    public String introduction() throws Exception {
        ConfigEntity configEntity = configDao.queryFirstByConfigCode("company_desc");
        if (configEntity == null) {
            return null;
        }
        return configEntity.getRemark();
    }
}
