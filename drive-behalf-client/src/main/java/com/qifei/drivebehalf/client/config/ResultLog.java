package com.qifei.drivebehalf.client.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@Slf4j
@ControllerAdvice
public class ResultLog implements ResponseBodyAdvice {

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        try {
            if (log.isTraceEnabled()) {
                if (o != null) {
                    log.trace(String.format("返回结果:%s", JsonUtils.writeValue(o)));
                } else {
                    log.trace(String.format("返回结果:%s", "null"));
                }
            }
        } catch (JsonProcessingException e) {
            log.error("bean转json错误", e);
        }
        return o;
    }
}
