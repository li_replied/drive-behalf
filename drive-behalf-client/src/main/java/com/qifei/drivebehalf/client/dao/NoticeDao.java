package com.qifei.drivebehalf.client.dao;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.entity.NoticeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface NoticeDao extends JpaRepository<NoticeEntity, Long>, JpaSpecificationExecutor<NoticeEntity> {
    NoticeEntity findFirstByStatusOrderByCreateTimeDesc(EnableEnum status);

    List<NoticeEntity> findNoticeEntitiesByStatusOrderByCreateTimeDesc(EnableEnum status);
}
