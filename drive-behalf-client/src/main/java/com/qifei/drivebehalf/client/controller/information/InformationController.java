package com.qifei.drivebehalf.client.controller.information;

import com.qifei.drivebehalf.client.common.model.ApiResponse;
import com.qifei.drivebehalf.client.common.utils.BeanCopyUtils;
import com.qifei.drivebehalf.client.controller.information.vo.NoticeResult;
import com.qifei.drivebehalf.client.entity.NoticeEntity;
import com.qifei.drivebehalf.client.service.ConfigService;
import com.qifei.drivebehalf.client.service.InformationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "公告信息")
@RestController
@RequiredArgsConstructor
@RequestMapping("/information")
public class InformationController {
    private final ConfigService configService;
    private final InformationService informationService;

    @ApiOperation("公司简介")
    @GetMapping("/profile")
    public ApiResponse<String> profile() throws Exception {
        String remark = configService.introduction();
        return ApiResponse.createBySuccess(remark);
    }

    @ApiOperation("公告")
    @GetMapping("/notice")
    public ApiResponse<NoticeResult> notice() throws Exception {
        NoticeEntity noticeEntity = informationService.info();
        if (noticeEntity == null) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(noticeEntity, NoticeResult.class));
    }

    @ApiOperation("公告列表")
    @GetMapping("/notice/list")
    public ApiResponse<List<NoticeResult>> list() throws Exception {
        List<NoticeEntity> list = informationService.list();
        if (list == null || list.size() == 0) {
            return ApiResponse.createBySuccess();
        }
        return ApiResponse.createBySuccess(BeanCopyUtils.copyProperties(list, NoticeResult.class));
    }
}
