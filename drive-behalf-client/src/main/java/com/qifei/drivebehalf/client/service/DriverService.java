package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.aliyun.oss.common.utils.DateUtil;
import com.qifei.drivebehalf.client.common.enums.*;
import com.qifei.drivebehalf.client.common.execution.ServiceWorkingException;
import com.qifei.drivebehalf.client.common.properties.JWTProperties;
import com.qifei.drivebehalf.client.common.session.UserLoginUtils;
import com.qifei.drivebehalf.client.common.utils.DateUtils;
import com.qifei.drivebehalf.client.common.utils.JWTUtils;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.controller.driver.vo.ApplyParam;
import com.qifei.drivebehalf.client.controller.driver.vo.DriverLocationParam;
import com.qifei.drivebehalf.client.controller.driver.vo.DriverLoginParam;
import com.qifei.drivebehalf.client.controller.order.vo.PassengerParam;
import com.qifei.drivebehalf.client.dao.DriverApplyDao;
import com.qifei.drivebehalf.client.dao.DriverBannerDao;
import com.qifei.drivebehalf.client.dao.DriverDao;
import com.qifei.drivebehalf.client.entity.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DriverService {
    @Value("${order.nearby-query-scope}")
    private int nearbyQueryScope;

    private final ShortMessageService shortMessageService;
    private final RedisService redisService;
    private final DriverDao driverDao;
    private final DriverApplyDao driverApplyDao;
    private final DriverBannerDao bannerDao;
    @Resource
    private JWTProperties jwtProperties;

    @Transactional
    public String smLogin(DriverLoginParam param) throws Exception {
        DriverEntity driverEntity = driverDao.findDriverEntityByPhone(param.getPhone());
        if (driverEntity == null) {
            throw new ServiceWorkingException("用户不存在，请联系管理员授权！");
        }
        if (driverEntity.getUserEnable() == EnableEnum.NO) {
            throw new ServiceWorkingException("该帐号已被禁用！");
        }
        if (StringUtils.isNotEmpty(driverEntity.getClientId())) {
            throw new ServiceWorkingException("该帐号已绑定另外一个设备，请先在原设备解绑。");
        }
        // 绑定设备
        driverDao.updateClientId(param.getCid(), driverEntity.getId());
        return cacheUserReturnToken(driverEntity);
    }

    @Transactional
    public void smLogout(String cid) throws Exception {
        DriverEntity user = (DriverEntity) UserLoginUtils.getLogin();
        Assert.notNull(cid.equals(user.getClientId()), "无效请求");
        driverDao.updateClientId(null, user.getId());
        updateOnlineStatus(user, SwitchEnum.off, false);
    }

    @Transactional
    public String smCheck(String cid) throws Exception {
        DriverEntity driverEntity = driverDao.findByClientId(cid);
        Assert.notNull(driverEntity, "当前设备未绑定");
        Assert.isTrue(driverEntity.getUserEnable() == EnableEnum.YES, "当前账号已禁用");
        // 默认开启听单
        updateOnlineStatus(driverEntity, SwitchEnum.on, false);
        return cacheUserReturnToken(driverEntity);
    }

    public List<DriverEntity> getNearBy() {
        DriverEntity user = (DriverEntity) UserLoginUtils.getLogin();
        //return driverDao.findDriverEntitiesByAgentIdAndOnlineAndIdNot(user.getAgentId(), EnableEnum.YES, user.getId());
        return driverDao.findAreaNearByOnlineDrivers(user.getAgentId(), user.getId());
    }

    // 判断乘客附近是否有司机
    public List<DriverEntity> findNearByOnlineDrivers(PassengerParam param) throws Exception {
        List<DriverEntity> drivers = driverDao.findNearByOnlineDrivers(param.getStartLng(), param.getStartLat(), nearbyQueryScope * 1000);
        Assert.isTrue(drivers.size() > 0, "当前区域暂时没有司机");
        Assert.isTrue(drivers.size() >= param.getNeedDriverCount(), String.format("当前区域只有%d名司机可提供服务", drivers.size()));
        return drivers;
    }

    private List<DriverEntity> findDrivers(double lng, double lat) {
        // 先从缓存缓存中找
//        List<String> cids = redisService.geoRadius(RedisKeyEnum.DriverLocation, lng, lat, nearbyQueryScope)
//                .getContent()
//                .stream()
//                .map(i -> i.getContent().getName())
//                .collect(Collectors.toList());
        List<String> cids = null; // 先不从缓存获取，筛选功能太弱
        if (cids == null || cids.isEmpty()) {
            return driverDao.findNearByOnlineDrivers(lng, lat, nearbyQueryScope * 1000);
        } else {
            return driverDao.findDriverEntitiesByOnlineAndClientIdIn(EnableEnum.YES, cids);
        }
    }

    public DriverEntity info() throws Exception {
        UserEntity user = UserLoginUtils.getLogin();
        DriverEntity driverEntity = driverDao.findById(user.getId()).orElse(null);
        Assert.notNull(driverEntity, "身份验证失败");
        return driverEntity;
    }

    public List<Map<String, Object>> sumAreaRanking(String type) throws Exception {
        DriverEntity user = (DriverEntity) UserLoginUtils.getLogin();
        if (type.equalsIgnoreCase("money")) {
            return driverDao.sumAreaIncomeByAgentId(user.getAgentId());
        } else if (type.equalsIgnoreCase("online")) {
            return driverDao.sumAreaOnlineTimeByAgentId(user.getAgentId());
        } else if (type.equalsIgnoreCase("likes")) {
            return driverDao.sumAreaLikesByAgentId(user.getAgentId());
        }
        throw new Exception("无效的参数");
    }

    public Page<DriverEntity> query(int pageNum) throws Exception {
        Pageable pageable = PageRequest.of(pageNum - 1, 100);
        return driverDao.queryAllByClientIdNotNull(pageable);
    }

    public void income(Long id, BigDecimal decimal) throws Exception {
        driverDao.updateMoney(decimal, id);
    }

    public void apply(ApplyParam param) throws Exception {
        shortMessageService.verificationCode(param.getPhone(), param.getCode());
        DriverApplyEntity driverApplyEntity = driverApplyDao.findDriverApplyEntityByPhone(param.getPhone());
        Assert.isNull(driverApplyEntity, "已经注册成功司机");
        driverApplyEntity = new DriverApplyEntity();
        driverApplyEntity.setName(param.getName());
        driverApplyEntity.setYears(param.getYears());
        driverApplyEntity.setPhone(param.getPhone());
        driverApplyEntity.setBirthday(param.getBirthday());
        driverApplyEntity.setSex(param.getSex());
        driverApplyEntity.setStatus("disable");
        driverApplyEntity.setCreate();
        driverApplyDao.save(driverApplyEntity);
    }

    @Transactional
    public void updateOnlineStatus(DriverEntity user, SwitchEnum online, boolean reload) throws Exception {
        // 上线
        if (online == SwitchEnum.on) {
            // 判断是否第二天
            if (user.getLastOnlineTime() != null && user.getLastOnlineTime().longValue() < DateUtils.getToday()) {
                // 累计总时长，并清零今天在线时长
                updateTotalOnlineTime(user);
                driverDao.updateTotalOnlineTime(user.getId(), System.currentTimeMillis());
            } else {
                // 更新在线状态
                driverDao.updateOnline(EnableEnum.YES, user.getId(), System.currentTimeMillis());
                if (reload) {
                    // 更新今日在线时间
                    user = driverDao.findById(user.getId()).orElse(null);
                }
            }
            user.setOnline(EnableEnum.YES);
            user.setLastOnlineTime(System.currentTimeMillis());
        }
        // 下线
        else {
            driverDao.updateOnline(EnableEnum.NO, user.getId(), System.currentTimeMillis());
            user.setOnline(EnableEnum.NO);
            user.setLastOnlineTime(System.currentTimeMillis());
        }
        cacheUser(user); // 更新缓存信息
    }

    public List<BannerEntity> getBannersByType(String type) throws Exception {
        return bannerDao.findAllByBannerTypeAndStatus(type, EnableEnum.YES);
    }

    @Transactional
    public int updateLocation(DriverLocationParam param) throws Exception {
        // 缓存司机定位
        //redisService.geoAdd(RedisKeyEnum.DriverLocation, param.getLongitude(), param.getLatitude(), param.getCid());
        // 计算在线时间
        int online = calculateOnlineTime();
        if (online > 0) {
            driverDao.updateLocation(param, System.currentTimeMillis(), online);
        } else {
            driverDao.updateLocation(param, System.currentTimeMillis());
        }
        return online;
    }

    private void updateTotalOnlineTime(DriverEntity user) {
        int today = user.getTodayOnlineTime() != null ? user.getTodayOnlineTime().intValue() : 0;
        int total = user.getTotalOnlineTime() != null ? user.getTotalOnlineTime().intValue() : 0;
        user.setTotalOnlineTime(total + today);
        user.setTodayOnlineTime(0);
    }

    private int calculateOnlineTime() {
        DriverEntity user = (DriverEntity) UserLoginUtils.getLogin();
        if (user.getOnline() == EnableEnum.YES && user.getLastOnlineTime() != null) {
            // 计算差值
            int seconds = (int) (System.currentTimeMillis() - user.getLastOnlineTime().longValue()) / 1000;
            // 加上上一次停止时的时长
            seconds += user.getTodayOnlineTime() != null ? user.getTodayOnlineTime().intValue() : 0;
            return seconds;
        }

        return 0;
    }

    private String cacheUserReturnToken(DriverEntity client) throws Exception {
        String token = JWTUtils.compact(String.valueOf(client.getId()), UserType.DRIVER, jwtProperties.getSecret());
        if (StringUtils.isNotEmpty(token)) {
            String userJson = JsonUtils.writeValue(client);
            redisService.setEx(RedisKeyEnum.UserToken, UserType.DRIVER.name() + client.getId(), userJson);
            return token;
        }
        throw new Exception("令牌创建异常");
    }

    private void cacheUser(UserEntity client) throws Exception {
        String userJson = JsonUtils.writeValue(client);
        redisService.setEx(RedisKeyEnum.UserToken, UserType.DRIVER.name() + client.getId(), userJson);
    }
}
