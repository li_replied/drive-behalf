package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(
        name = "driver_passenger",
        indexes = {
                @Index(name = "idx_driver_passenger_phone", columnList = "phone"),
        }
)
public class PassengerEntity extends UserEntity {
    @Column(name = "name_show")
    @Enumerated(EnumType.STRING)
    private EnableEnum nameShow = EnableEnum.NO;
    @Column(name = "open_id")
    private String openId;
    @Column(name = "free_state")
    @Enumerated(EnumType.STRING)
    private EnableEnum freeState = EnableEnum.NO;
    @Column(name = "money", precision = 10, scale = 2, updatable = false)
    private BigDecimal money = new BigDecimal(0);
    @Column(name = "retail", precision = 10, scale = 2, updatable = false)
    private BigDecimal retail = new BigDecimal(0);
    @Column(name = "parent_id")
    private Long parentId;
    @Column(name = "relation_time")
    private Date relationTime;
}
