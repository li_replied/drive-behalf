package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Route implements Serializable {
    /**
     * 方案交通方式，固定值：“DRIVING”
     */
    private String mode;
    /**
     * 方案标签，表明方案特色，详细说明见下文
     */
    private List<String> tags;
    /**
     * 方案总距离
     */
    private int distance;
    /**
     * 方案估算时间（含路况）
     */
    private int duration;
    /**
     * 方案途经红绿灯个数
     */
    @JsonProperty("traffic_light_count")
    private int traffic_light_count;
    /**
     * 预估过路费（仅供参考），单位：元
     */
    private int toll;
    /**
     * 方案路线坐标点串（该点串经过压缩，解压请参考：polyline 坐标解压）
     */
    private List<String> polyline;
}
