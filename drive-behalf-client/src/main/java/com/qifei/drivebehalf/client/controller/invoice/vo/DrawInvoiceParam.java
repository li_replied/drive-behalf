package com.qifei.drivebehalf.client.controller.invoice.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@ApiModel(description = "开票参数")
public class DrawInvoiceParam {
    @ApiModelProperty("发票id")
    @NotNull(message = "发票id不能为空")
    private Long titleId;
    @ApiModelProperty("订单编号")
    @NotBlank(message = "订单编号不能为空")
    private String orderNumber;
}
