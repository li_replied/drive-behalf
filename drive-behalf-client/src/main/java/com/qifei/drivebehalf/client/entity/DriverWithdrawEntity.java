package com.qifei.drivebehalf.client.entity;

import com.qifei.drivebehalf.client.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(
        name = "driver_withdraw",
        indexes = {
                @Index(name = "idx_driver_withdraw_driver_id", columnList = "driver_id"),
                @Index(name = "idx_driver_withdraw_create_time", columnList = "create_time"),
        }
)
public class DriverWithdrawEntity extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "withdraw_id", unique = true, nullable = false)
    private Long id;
    @Column(name = "order_number", unique = true, nullable = false)
    private String orderNumber;
    @Column(name = "driver_id", nullable = false)
    private Long driverId;
    @Column(name = "amount", nullable = false)
    private BigDecimal amount;
    @Column(name = "status", nullable = false)
    private String status = "FINISH";
    @Column(name = "method", nullable = false)
    private String method = "INCOME";
    @Column(name = "deal_time")
    private Date dealTime;
    @Column(name = "remarks")
    private String remarks;
}
