package com.qifei.drivebehalf.client.common.enums;

/**
 * redis key
 */
public enum RedisKeyEnum {

    DriverLocation("driver:location", 0L), // 司机定位

    CallDriverExpire("driver:call:expire:", 60L * 5L), // 呼叫司机等待时限

    /**
     * 短信发送次数
     */
    SmsSendCount("sms:send:count:", 60L * 30L),
    /**
     * 短信发送间隔
     */
    SmsExpire("sms:send:expire:", 60L),
    /**
     * 短信校验码
     */
    SmsCode("sms:send:code:", 60L * 5L),
    /**
     * 用户token  60L * 60L * 24 * 7
     */
    UserToken("user:token:", 60L * 60L * 24 * 7),
    /**
     * 地区列表
     */
    Region("region:list", 0L),
    /**
     * 本地地区版本
     */
    RegionVersion("region:version", 0L),
    /**
     * 计算金额缓存
     */
    SumMoneyInfo("order:sum:money:", 60L * 20L),
    /**
     * 司机在线状态
     */
    DriverOnLine("online:driver", 0L),
    /**
     * 微信token
     */
    AccessToken("access:token", 7200L),
    /**
     * 订单锁
     */
    OrderLock("order:lock:", 60L * 15L),
    /**
     * 抢单列表
     */
    ContestOrder("contest:order:", 0L),
    /**
     * 支付中
     */
    Paying("order:paying:", 60L * 60L * 2L),
    /**
     * 各推 token
     */
    GeTuiToken("getui:token", 60L * 60L * 23L),
    /**
     * 订单地理位置
     */
    OrderPoint("order:point", 0L);
    /**
     * 失效时间，单位：秒
     */
    private Long ttl;
    /**
     * key
     */
    private String key;

    RedisKeyEnum(String key, Long ttl) {
        this.key = key;
        this.ttl = ttl;
    }

    public Long getTtl() {
        return ttl;
    }

    public String getKey() {
        return key;
    }
}
