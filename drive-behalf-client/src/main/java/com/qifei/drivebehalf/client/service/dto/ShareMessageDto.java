package com.qifei.drivebehalf.client.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class ShareMessageDto extends KafkaMessage implements Serializable {
    protected String orderNumber;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "0.00")
    protected BigDecimal amount;

    public ShareMessageDto() {
        super("share");
    }

    public ShareMessageDto(String topic) {
        super(topic);
    }
}
