package com.qifei.drivebehalf.client.service;

import cn.hutool.core.date.DateUtil;
import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.base.notify.Notify;
import com.gexin.rp.sdk.dto.GtReq;
import com.gexin.rp.sdk.exceptions.PushSingleException;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import com.gexin.rp.sdk.template.style.Style0;
import com.qifei.drivebehalf.client.common.enums.EnableEnum;
import com.qifei.drivebehalf.client.common.enums.RedisKeyEnum;
import com.qifei.drivebehalf.client.common.utils.JsonUtils;
import com.qifei.drivebehalf.client.common.utils.NumberFormatUtils;
import com.qifei.drivebehalf.client.controller.order.vo.AssignedResult;
import com.qifei.drivebehalf.client.entity.DriverEntity;
import com.qifei.drivebehalf.client.entity.DriverTripOrderEntity;
import com.qifei.drivebehalf.client.entity.PassengerEntity;
import com.qifei.drivebehalf.client.service.dto.WXSendMessage;
import com.qifei.drivebehalf.client.service.dto.WXSendMessageResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationService implements InitializingBean {
    private final RedisService redisService;
    private final MPWeiXinService mpWeiXinService;
    private final SmService smService;
    private final DriverService driverService;

    @Value("${order.accept-timeout}")
    private int acceptTimeout;

    private IGtPush push;

    @Value("${weixin.accepted-message-id}")
    private String acceptedMessageId;
    @Value("${weixin.confirm-drive-id}")
    private String confirmDriveId;
    @Value("${weixin.confirm-delivered-id}")
    private String confirmDeliveredId;
    @Value("${ge-tui.app-id}")
    private String appId;
    @Value("${ge-tui.app-key}")
    private String appKey;
    @Value("${ge-tui.master-secret}")
    private String masterSecret;

    private NotificationTemplate getNotificationTemplate(String title, String message, String transmission) {
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);
        // 通知风格
        Style0 style = new Style0();
        // 设置通知栏标题与内容
        style.setTitle(title);
        style.setText(message);
        // 设置通知是否响铃，震动，或者可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        style.setChannelLevel(3); //设置通知渠道重要性
        style.setBadgeAddNum(1);
        template.setStyle(style);
        // 透传
        if (StringUtils.isNotEmpty(transmission)) {
            template.setTransmissionType(1);  // 透传消息接受方式设置，1：立即启动APP，2：客户端收到消息后需要自行处理
            template.setTransmissionContent(transmission);
        }
        return template;
    }

    private TransmissionTemplate getTransmissionTemplate(String transmission) {
        TransmissionTemplate template = new TransmissionTemplate();
        template.setAppId(appId);
        template.setAppkey(appKey);
        template.setTransmissionType(2);
        template.setTransmissionContent(transmission);
        return template;
    }

    private TransmissionTemplate getOfflineTemplate(String title, String message, AssignedResult transmission) throws Exception {
        TransmissionTemplate template = new TransmissionTemplate();
        template.setAppId(appId);
        template.setAppkey(appKey);
        template.setTransmissionType(2);
        template.setTransmissionContent(JsonUtils.writeValue(transmission));
        // 通知
        Notify notify = new Notify();
        notify.setTitle(title);
        notify.setContent(message);
        notify.setIntent(String.format(
                "intent:#Intent;launchFlags=0x10000000;component=com.fastops.drivebehalf/.ui.activity.AcceptOrderActivity;" +
                        "d.startlat=%s;" +
                        "i.timeout=%d;" +
                        "S.code=%s;" +
                        "S.cid=%s;" +
                        "S.phone=%s;" +
                        "S.place=%s;" +
                        "d.startlng=%s;" +
                        "d.endlat=%s;" +
                        "d.endlng=%s;" +
                        "l.deadline=%d;end",
                transmission.getStartlat(),
                transmission.getTimeout(),
                transmission.getCode(),
                transmission.getCid(),
                transmission.getPhone(),
                transmission.getPlace(),
                transmission.getStartlng(),
                transmission.getEndlat(),
                transmission.getEndlng(),
                transmission.getDeadline()));
        notify.setType(GtReq.NotifyInfo.Type._intent);
        template.set3rdNotifyInfo(notify);//设置第三方通知

        return template;
    }

    // 平台派单通知
    public void sendAssigned(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        DriverEntity driver = driverTripOrderEntity.getDriver();
        Assert.isTrue(driver != null, "通知司机不能为null");
        Assert.isTrue(driver.getClientId() != null && driver.getClientId() != "", "通知司机未绑定");
        PassengerEntity passenger = driverTripOrderEntity.getPassenger();
        Assert.isTrue(passenger != null, "乘客信息不能为null");
        // 透传参数
        AssignedResult transmission = new AssignedResult();
        transmission.setCode(driverTripOrderEntity.getOrderNumber());
        transmission.setPhone(passenger.getPhone());
        transmission.setPlace(driverTripOrderEntity.getStartPoint());
        transmission.setStartlat(driver.getLastLatitude());
        transmission.setStartlng(driver.getLastLongitude());
        transmission.setEndlat(Double.parseDouble(driverTripOrderEntity.getStartLat()));
        transmission.setEndlng(Double.parseDouble(driverTripOrderEntity.getStartLng()));
        transmission.setDeadline(System.currentTimeMillis() + acceptTimeout * 1000);
        transmission.setTimeout(acceptTimeout);
        transmission.setCid(driver.getClientId());

        // 初始化个推模板
        SingleMessage message = new SingleMessage();
        message.setPushNetWorkType(0); // 可选，1为wifi，0为不限制网络环境。根据手机处于的网络情况，决定是否下发
        // 在线
        long fiveAgo = System.currentTimeMillis() - 60 * 5 * 1000;
        if (driver.getOnline() == EnableEnum.YES && driver.getLastOnlineTime() >= fiveAgo) {
            message.setOffline(false); // 非离线消息
            message.setData(getTransmissionTemplate(JsonUtils.writeValue(transmission)));
        }
        // 离线
        else {
            String title = "平台派单" + driverTripOrderEntity.getOrderNumber();
            String content = String.format("乘客电话：%s 所在位置：%s ",
                    driverTripOrderEntity.getPassenger().getPhone(),
                    driverTripOrderEntity.getStartPoint());
            message.setOffline(true);
            message.setOfflineExpireTime(acceptTimeout * 1000);
            message.setData(getOfflineTemplate(title, content, transmission));
        }
        // 指定目标
        Target target = new Target();
        target.setAppId(appId);
        target.setClientId(driver.getClientId());
        // 发送
        IPushResult ret = null;
        try {
            ret = push.pushMessageToSingle(message, target);
        } catch (RequestException e) {
            e.printStackTrace();
            ret = push.pushMessageToSingle(message, target, e.getRequestId());
        }
        if (ret != null) {
            log.info("派单成功通知：" + driverTripOrderEntity.getOrderNumber());
            log.debug(ret.getResponse().toString());
        } else {
            log.error("发送消息异常：" + driverTripOrderEntity.getOrderNumber());
        }
    }

    // 发送接单消息
    public void sendAccepted(DriverTripOrderEntity driverTripOrderEntity) throws Exception {
        PassengerEntity passengerEntity = driverTripOrderEntity.getPassenger();
        DriverEntity driverEntity = driverTripOrderEntity.getDriver();
        String userPhone = passengerEntity.getPhone();
        String title = "代驾司机[" + driverEntity.getName() + "]已接单";
        String orderNumber = driverTripOrderEntity.getOrderNumber();
        String name = driverTripOrderEntity.getDriver().getName();
        String phone = driverTripOrderEntity.getDriver().getPhone();
        String customerPhone = driverTripOrderEntity.getCustomerPhone();
        // 判断是否微信用户，否则发生短信通知
        if (StringUtils.isEmpty(passengerEntity.getOpenId())) {
            smService.accepted(userPhone, orderNumber, name, phone, customerPhone);
            return;
        }
        // 发送微信通知
        Map<String, WXSendMessage.DataMap> map = new HashMap<>();
        //接单信息
        map.put("thing1", new WXSendMessage.DataMap(title + "(" + orderNumber + ")"));
        //代驾司机
        map.put("thing2", new WXSendMessage.DataMap(name));
        //司机电话
        map.put("phone_number3", new WXSendMessage.DataMap(phone));
        //客服热线
        map.put("phone_number4", new WXSendMessage.DataMap(customerPhone));
        String redirectUrl = "/pages/order/orderdetails/orderdetails?ordernum=" + driverTripOrderEntity.getOrderNumber();
        // 如果微信通知失败，则发送短信通知
        if (sendWx(passengerEntity.getOpenId(), acceptedMessageId, redirectUrl, map)) {
            smService.accepted(userPhone, orderNumber, name, phone, customerPhone);
        }
    }

    private boolean sendWx(String openId, String templateId, String page, Map<String, WXSendMessage.DataMap> param) throws Exception {
        WXSendMessage wxSendMessage = new WXSendMessage();
        wxSendMessage.setTouser(openId);
        wxSendMessage.setTemplateId(templateId);
        wxSendMessage.setPage(page);
        wxSendMessage.setData(param);
        String accessToken = redisService.getKey(RedisKeyEnum.AccessToken);
        WXSendMessageResult sendMessage = mpWeiXinService.sendMessage(accessToken, JsonUtils.writeValue(wxSendMessage));
        return sendMessage.getErrcode() == 0;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        push = new IGtPush("https://api.getui.com/apiex.htm", appKey, masterSecret);
    }

}
