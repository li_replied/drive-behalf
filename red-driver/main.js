import Vue from 'vue'
import App from './App'
import http from './common/axios.js'
import * as filters from './global/filters/filters.js'
import command from 'global/command/command.js'
import cuCustom from './colorui/components/cu-custom.vue'
import screenTextScroll from '@/components/p-screenTextScroll/screenTextScroll.vue'
Vue.component('textscroll',screenTextScroll)
Vue.component('cu-custom',cuCustom)
Vue.config.productionTip = false
Vue.prototype.http = http
App.mpType = 'app'
// console.log('978', TMap)
//过滤器集合
Object.keys(filters).forEach(key =>{
	Vue.filter(key,filters[key])
})


const app = new Vue({
    ...App
})
app.$mount()
