import helper from "./helper.js"

export default {
	/**
	 * 呼叫代驾
	 * @param {Object} data
	 */
	callDriver(data) {
		return helper.post('client/api/order/passenger/driver/call', data);
	},
	/**
	 * 检查订单状态
	 * @param {Object} data
	 */
	checkCallStatus(flag) {
		return helper.get(`client/api/order/passenger/status/check/${flag}`);
	},
	/**
	 * 检查token是否合法
	 * @param {Object} data
	 */
	check() {
		return helper.get('client/api/session/check');
	},
	/**
	 * 获取短信验证码
	 * @param {Object} data
	 */
	sendCode(data) {
		return helper.post('client/api/sms/captcha/send', data);
	},
	/**
	 * 快捷登录
	 * @param {Object} file
	 */
	quickLogin(data) {
		return helper.post('client/api/passenger/quick/login', data)
	},
	/**
	 * 用户登录
	 * @param {Object} data
	 */
	sendlogin(data) {
		return helper.post('client/api/passenger/login', data);
	},
	/**
	 * 用户信息
	 * @param {Object} data
	 */
	passengerinfo(data) {
		return helper.get('client/api/passenger/info', data);
	},

	/**
	 * 修改用户信息
	 * @param {Object} data
	 */
	modify(data) {
		return helper.post('client/api/passenger/modify', data);
	},

	/**
	 * 退出登录
	 * @param {Object} data
	 */
	outlogin(data) {
		return helper.post('client/api/logout', data);
	},
	/**
	 * 收费标准
	 * @param {Object} data
	 */
	pricelist(data) {
		return helper.post('client/api/price', data);
	},
	// 当时的收费
	price(regionId) {
		return helper.get(`client/api/price/${regionId.regionId}`);
	},

	/**
	 *   取消订单
	 * @param {Object} data
	 */
	ordercancel(data) {
		return helper.post('client/api/order/passenger/cancel', data);
	},
	/**
	 *   订单详情
	 * @param {Object} data
	 */
	orderDetail(data) {
		return helper.get(`client/api/order/info/${data}`);
	},
	/**
	 * 我的订单
	 * @param {Object} data
	 */
	orderlist(data) {
		return helper.post('client/api/order/list', data);
	},
	/**
	 * 	查看订单数量
	 * @param {Object} file
	 */
	countConfirm(month) {
		return helper.get('client/api/order/count/confirm');
	},
	/**
	 * 去下单
	 * @param {Object} data
	 */
	orderPlace(data) {
		return helper.post('client/api/order/place', data);
	},
	/**
	 * 费用计算
	 * @param {Object} file
	 */
	sumMoney(data) {
		return helper.post('client/api/order/sum/money', data);
	},
	/**
	 * 获取所有区
	 * @param {Object} file
	 */
	regionall() {
		return helper.get('client/api/region/all');
	},
	/**
	 * 获取当前位置
	 * @param {Object} file
	 */
	regioninfo(data) {
		let lat = data.lat;
		let lng = data.lng;
		return helper.get(`client/api/region/info/${lat}/${lng}`);
	},
	/**
	 * 支付
	 * @param {Object} file
	 */

	payjsapi(data) {
		return helper.post('client/api/pay', data);
	},
	// 取消支付
	payCancel(orderNumber) {
		return helper.post(`client/api/pay/cancel/${orderNumber}`);
	},
	/**
	 * 查询待确认订单
	 * @param {Object} file
	 */
	queryConfirm() {
		return helper.get('client/api/order/query/confirm');
	},
	/**
	 * 确认订单
	 * @param {Object} file
	 */
	orderConfirm(orderNumber) {
		return helper.post(`client/api/order/confirm/${orderNumber}`);
	},
	/**
	 * 查询订单状态
	 * @param {Object} file
	 */
	orderState(orderNumber) {
		return helper.get(`client/api/order/state/${orderNumber}`)
	},
	/**
	 * 公司简介
	 * @param {Object} file
	 */
	introduction() {
		return helper.get('client/api/config/introduction')
	},
	/**
	 * 公告
	 * @param {Object} file
	 */
	notice() {
		return helper.get('client/api/information/notice')
	},
	/**
	 * 发票列表
	 * @param {Object} file
	 */

	invoicelist() {
		return helper.get('client/api/invoice/list')
	},
	/**
	 * 保存的发票接口
	 * @param {Object} file
	 */
	invoicedraw(data) {
		return helper.post('client/api/invoice/draw', data)
	},
	/**
	 * 新增和修改发票
	 * @param {Object} file
	 */
	invoicesave(data) {
		return helper.post('client/api/invoice/save', data)
	},
	/**
	 * 
     查询已开发票
	 * @param {Object} file
	 */
	invoiceorder(orderNumber) {
		return helper.get(`client/api/invoice/order/${orderNumber}`)
	},
	/**
	 * 
	 查询默认发票
	 * @param {Object} file
	 */

	invoicedefault() {
		return helper.get('client/api/invoice/default')
	},
	/**
	 * 
	 查询发票
	 * @param {Object} file
	 */
	invoicefind(id) {
		return helper.get(`client/api/invoice/find/${id}`)
	},
	/**
	 * 
	 撤销发票
	 * @param {Object} file
	 */
	invoicecancel(orderNumber) {
		return helper.post(`client/api/invoice/cancel/${orderNumber}`)
	},
	/**
	 * 
	 删除发票
	 * @param {Object} file
	 */
	invoicedel(id) {
		return helper.post(`client/api/invoice/delete/${id}`)
	},
	/**
	 * 
	 充值列表
	 * @param {Object} file
	 */
	rechargeRatio(id) {
		return helper.get('client/api/recharge/ratio')
	},
	/**
	 * 	 
    充值按钮
	 * @param {Object} file
	 */
	payRecharge(data) {
		return helper.post('client/api/pay/recharge', data)
	},
	/**
	 * 	 
	 * 余额支付
	 * @param {Object} file
	 */
	payBalance(data) {
		return helper.post('client/api/pay/balance', data)
	},
	/**
	 * 	 
	 * 余额交易记录
	 * @param {Object} file
	 */
	rechargeQuery(data) {
		return helper.get('client/api/recharge/query', data)
	},
	/**
	 * 	 
	 * 充值统计
	 * @param {Object} file
	 */
	rechargeTotal() {
		return helper.get('client/api/recharge/total')
	},
	/**
	 * 	 
	 * 推荐码
	 * @param {Object} file
	 */
	passengerRecommend() {
		return helper.get('client/api/passenger/recommend')
	},
	/**
	 * 	 
	 * 添加推荐人
	 * @param {Object} file
	 */
	passengeRelation(recommend) {
		return helper.get(`client/api/passenger/relation/${recommend}`)
	},
	/**
	 * 	 
	 * 积分记录
	 * @param {Object} file
	 */
	scoreQuery(data) {
		return helper.get('client/api/score/query', data)
	},
	/**
	 * 	 
	 * 兑换积分（提现）
	 * @param {Object} file
	 */
	scoreExchange(data) {
		return helper.post('client/api/score/exchange', data)
	},
	/**
	 * 	 
	 * 最近提现银行卡记录
	 * @param {Object} file
	 */
	scoreRecently() {
		return helper.get('client/api/score/recently')
	},
	/**
	 * 	 
	 * 积分合计
	 * @param {Object} file
	 */
	scoreTotal() {
		return helper.get('client/api/score/total')
	},
	/**
	 * 	 
	 * 充值状态记录
	 * @param {Object} file
	 */
	rechargeOrder(orderNumber) {
		return helper.get(`client/api/recharge/order/${orderNumber}`)
	},
	/**
	 * 	 
	 * 充值下单
	 * @param {Object} file
	 */
	rechargePlace(configId) {
		return helper.post(`client/api/recharge/place/${configId}`)
	},

	/**
	 * 	 
	 * 发送短信息
	 * @param {Object} file
	 */
	noPhoneSend(configId) {
		return helper.post('client/api/short/message/no/phone/send')
	},

	/**
	 * 	 
	 * 余额支付
	 * @param {Object} file
	 */
	payBalance(data) {
		return helper.post('client/api/pay/balance', data)
	},

	/**
	 * 	 
	 * 查询推荐人
	 * @param {Object} file
	 */
	queryRelation() {
		return helper.get('client/api/passenger/query/relation')
	},
	/**
	 * 	 
	 * 司机信息
	 * @param {Object} file
	 */
	driverApply(data) {
		return helper.post('client/api/driver/apply', data)
	},
	// 版本更新
	versionGain(data) {
		let platform = data.name;
		let version = data.version;
		let origin = data.origin;
		return helper.get(`client/api/version/gain/${origin}/${platform}/${version}`);
	},
	// 上传图像
	passengerAvatar(data) {
		return helper.post('client/api/passenger/avatar', data)
	},
	// 我的下级
	subordinate() {
		return helper.get('client/api/passenger/subordinate')
	},
	// 二级推荐
	passengerSecond(data) {
		return helper.post('client/api/passenger/second',data)
	},
	// 保存银行卡
	cardSave(data) {
		return helper.post('client/api/card/save',data)
	},
	// 查询银行卡
	cardQuery() {
		return helper.get('client/api/card/query')
	}














}
