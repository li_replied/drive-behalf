import Vue from 'vue'

function check_auth(key){
	if(key == '1'){
		return true
	}
	return false
}


Vue.directive('jump',{
	inserted(el,binding){
		let is_jump = check_auth(binding.value)
		if(!is_jump){
			el.parentNode && el.parentNode.removeChild(el)
		}
	}
})